<?php

use App\Models\OperationsDocument;
use Illuminate\Database\Seeder;

class OperationsDocumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $csv = database_path('seeds/csv/operations-documents.csv');
        $excel = App::make('excel');

        $data = $excel->load($csv, function($reader) {
            $results = $reader->all();

            foreach ($results as $row) {
                OperationsDocument::updateOrCreate(
                    ['id' => $row->id],
                    ['name' => $row->name,
                     'url' => $row->url]);
            }
        });
    }
}
