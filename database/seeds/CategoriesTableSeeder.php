<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        $csv = database_path('seeds/csv/categories.csv');
        $excel = App::make('excel');

        $excel->load($csv, function ($reader) {
            $results = $reader->all();

            foreach ($results as $row) {
                Category::updateOrCreate([
                    'id' => $row->id
                ], [
                    'parent_id'     => ($row->parent_id == 0) ? null: $row->parent_id,
                    'name'          => $row->name,
                    'resource_type' => $row->resource_type
                ]);
            }
        });
    }
}
