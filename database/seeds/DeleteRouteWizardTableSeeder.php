<?php

use App\Models\RouteWizard;
use Illuminate\Database\Seeder;

class DeleteRouteWizardTableSeeder extends Seeder
{
    public function run()
    {
        $routeWizard = RouteWizard::find(3);
        if($routeWizard) {
            $routeWizard->delete();
        }
    }
}
