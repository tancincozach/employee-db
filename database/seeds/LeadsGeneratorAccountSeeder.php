<?php

use Illuminate\Database\Seeder;

use App\Models\ACL\Role;
use App\Models\User;
use App\Models\ACL\UserRole;
use App\Models\ACL\ResourceRolePermission;
use App\Models\ACL\ResourceUserRolePermission;
use App\Models\ACL\Resource;

class LeadsGeneratorAccountSeeder extends Seeder
{
    const USER_NAME = 'leadsgenerator';

    const EMAIL = 'leadsgenerator@fullscale.io';

    const PASSWORD = '4bFsEafX';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::firstOrNew(
            ['name' => 'leadsgenerator'],
            [
                'display_name' => 'Leads Generator',
                'description'  => 'Leads Generator'
            ]
        );
        $role->save();
        $roleId = $role->id;

        $isUserExists = User::where('user_name', self::USER_NAME)
            ->where('email', self::EMAIL)->first();

        if (!$isUserExists) {
            $user = new User();
            $user->user_name = self::USER_NAME;
            $user->email = self::EMAIL;
            $user->password = bcrypt(self::PASSWORD);
            $user->can_login = 1;
            $user->is_verified = 1;
            $user->save();

            $user_role = new UserRole();
            $user_role->is_enabled = 1;
            $user_role->user_id = $user->id;
            $user_role->role_id = $roleId;
            $user_role->save();
            $userRoleId = $user_role->id;

            $this->permissions()->each(function ($item) use ($roleId, $userRoleId) {
                $resource = Resource::where('name', $item['resource'])->first();

                $resource_role_permission = new ResourceRolePermission();
                $resource_role_permission->can_add     = $item['can_add'];
                $resource_role_permission->can_edit    = $item['can_edit'];
                $resource_role_permission->can_view    = $item['can_view'];
                $resource_role_permission->can_delete  = $item['can_delete'];
                $resource_role_permission->role_id     = $roleId;
                $resource_role_permission->resource_id = $resource->id;
                $resource_role_permission->save();

                $resource_user_role_permissions = new ResourceUserRolePermission();
                $resource_user_role_permissions->can_add      = $item['can_add'];
                $resource_user_role_permissions->can_edit     = $item['can_edit'];
                $resource_user_role_permissions->can_view     = $item['can_view'];
                $resource_user_role_permissions->can_delete   = $item['can_delete'];
                $resource_user_role_permissions->user_role_id = $userRoleId;
                $resource_user_role_permissions->resource_id  = $resource->id;
                $resource_user_role_permissions->save();
            });
        }
    }

    public function permissions()
    {
        return collect([
            [
                'resource'   => 'employees',
                'can_add'    => 0,
                'can_edit'   => 0,
                'can_view'   => 1,
                'can_delete' => 0,
            ],
        ]);
    }
}
