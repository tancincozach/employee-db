<?php

use App\Models\WeeklyFloorQuestion;
use Illuminate\Database\Seeder;

class WeeklyFloorQuestionTableSeeder extends Seeder
{
    public function run()
    {
        $csv = database_path('seeds/csv/weekly_floor_questionnaire.csv');
        $excel = App::make('excel');

        $data = $excel->load($csv, function ($reader) {
            $results = $reader->all();
            foreach ($results as $row) {
                WeeklyFloorQuestion::updateOrCreate(
                    [
                    'weekly_floor_question' => $row->weekly_floor_question,
                    ],
                    [
                    'id' => $row->id,
                    'parent_id' => $row->parent_id,
                    'answer_element_type' => $row->answer_element_type,
                    'weekly_floor_question' => $row->weekly_floor_question,
                    'weekly_floor_question_options' => $row->weekly_floor_question_options,
                    'is_red' => $row->is_red,
                    'red_answer' => $row->red_answer,
                    ]
                );
            }
        });
    }
}
