<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Client;
use App\Models\ClientContact;
use App\Models\ACL\UserRole;
use App\Models\ACL\ResourceUserRolePermission;
use App\Models\ACL\ResourceRolePermission;

class GenerateClientContactAccounts extends Seeder
{
    /*
     * The email of the tester
     *
     * @var string
     */
    const BASE_USERNAME = 'paeng185111';

    /*
     * The default password for all counts
     *
     * @var string
     */
    const PASSWORD = 'password';

    /*
     * The user type for client
     *
     * @var integer
     */
    const USER_TYPE = 2;

    /*
     * The id for client role
     *
     * @var integer
     */
    const CLIENT_ROLE = 5;

    /*
     * The contact name of the tester
     *
     * @var string
     */
    const CONTACT_NAME = 'Paeng Test';

    /*
     * The contact number of the tester
     *
     * @var string
     */
    const CONTACT_NUMBER = '1234567890';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = Client::all();

        foreach ($clients as $key => $client) {
            $increment = $key + 1;
            $generated_username = self::BASE_USERNAME . '+' . $increment;
            $generated_email = $generated_username . '@gmail.com';

            $isEmailExists = User::where('user_name', $generated_username)
                ->where('email', $generated_email)->first();

            if (!$isEmailExists) {
                $user = new User();
                $user->user_name = $generated_username;
                $user->email = $generated_email;
                $user->password = bcrypt(self::PASSWORD);
                $user->user_type_id = self::USER_TYPE;
                $user->can_login = 1;
                $user->is_verified = 1;
                $user->save();

                $user_role = new UserRole();
                $user_role->is_enabled = 1;
                $user_role->user_id = $user->id;
                $user_role->role_id = self::CLIENT_ROLE;
                $user_role->save();

                $resource_role_permission = ResourceRolePermission::where('role_id', self::CLIENT_ROLE)->get();
                $resource_role_permission->each(function ($item) use ($user_role) {
                    $resource_user_role_permissions = new ResourceUserRolePermission();
                    $resource_user_role_permissions->can_add = $item->can_add;
                    $resource_user_role_permissions->can_edit = $item->can_edit;
                    $resource_user_role_permissions->can_view = $item->can_view;
                    $resource_user_role_permissions->can_delete = $item->can_delete;
                    $resource_user_role_permissions->user_role_id = $user_role->id;
                    $resource_user_role_permissions->resource_id = $item->resource_id;
                    $resource_user_role_permissions->save();
                });

                $client_contact = new ClientContact();
                $client_contact->client_id = $client->id;
                $client_contact->user_id = $user->id;
                $client_contact->name = self::CONTACT_NAME;
                $client_contact->email = $generated_email;
                $client_contact->contact_no = self::CONTACT_NUMBER;
                $client_contact->save();
            }
        }
    }
}
