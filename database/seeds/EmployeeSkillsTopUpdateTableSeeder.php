<?php
use Illuminate\Database\Seeder;
use App\Models\EmployeeTopSkill;

class EmployeeSkillsTopUpdateTableSeeder extends Seeder
{
    public function run()
    {
        $results = EmployeeTopSkill::whereNull('deleted_at')
                                    ->where('proficiency', '>=', 5)
                                    ->get();
        foreach($results as $row) {
            EmployeeTopSkill::updateOrCreate([
                            'id' => $row->id
                        ],
                        [
                            'checked_top_skill' => 1
                        ]
                    );
        }
    }
}
