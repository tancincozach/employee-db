<?php

use Illuminate\Database\Seeder;

use App\Models\HakcerRankTest;

class UpdateHackerRankTestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = database_path('seeds/csv/hacker_rank_tests_test_url.csv');
        $excel = App::make('excel');

        $data = $excel->load($csv, function($reader) {
            $results = $reader->all();
            foreach($results as $row) {
                HakcerRankTest::update(
                    [
                        'test_id' => $row->test_id
                    ],
                    [
                        'test_url' => $row->test_url
                    ]
                );
            }
        });
    }
}
