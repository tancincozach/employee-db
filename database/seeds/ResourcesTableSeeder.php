<?php

use App\Models\ACL\Resource;
use Illuminate\Database\Seeder;

/**
 * Class ResourcesTableSeeder
 */
class ResourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = database_path('seeds/csv/resources-extended.csv');
        $excel = App::make('excel');

        $excel->load($csv, function($reader) {
            $results = $reader->all();

            foreach ($results as $row) {
                Resource::updateOrCreate([
                    'id' => $row->id
                ], [
                    'name' => $row->name,
                    'display_name' => $row->display_name,
                    'description' => $row->description,
                ]);
            }
        });
    }
}
