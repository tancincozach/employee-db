<?php

use App\Models\SkillCategory;
use Illuminate\Database\Seeder;

class SkillCategoriesTableSeeder extends Seeder
{
    public function run()
    {
        $csv = database_path('seeds/csv/skill_categories.csv');
        $excel = App::make('excel');

        $excel->load($csv, function ($reader) {
            $results = $reader->all();

            foreach ($results as $row) {
                SkillCategory::updateOrCreate([
                    'id' => $row->id
                ], [
                    'skill_id' => $row->skill_id,
                    'category_id' => $row->category_id,
                ]);
            }
        });
    }
}
