<?php

use Illuminate\Database\Seeder;

use App\Models\PerformEvalQuestion as Question;
use App\Models\PerformEvalCriteria as QuestionCriteria;

class InitialPerformEvalQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = $this->setQuestionsWithCriterias(); // with criterias

        foreach ($questions as $q) {
            $criterias = $q['criteria'];
            unset($q['criteria']);

            $question = Question::updateOrCreate([
                'form_id' => $q['form_id'],
            ], $q);

            $criteria = [];

            foreach ($criterias as $c) {
                $criteria[] = new QuestionCriteria($c);
            }

            $question->criteria()->saveMany($criteria);
        }
    }

    /**
     * setQuestionsWithCriterias
     * To set array of data for questions with it's criterias
     *
     * @return void
     */
    private function setQuestionsWithCriterias()
    {
        $default_criteria = [
            [
                'type' => "tiptap",
                'sequence_no' => 1,
            ],
            [
                'type' => "radio",
                'sequence_no' => 2,
                'label' => '5',
                'value' => '5',
            ],
            [
                'type' => "radio",
                'sequence_no' => 3,
                'label' => '4',
                'value' => '4',
            ],
            [
                'type' => "radio",
                'sequence_no' => 4,
                'label' => '3',
                'value' => '3',
                'is_need_explain' => 1,
            ],
            [
                'type' => "radio",
                'sequence_no' => 5,
                'label' => '2',
                'value' => '2',
                'is_need_explain' => 1,
            ],
            [
                'type' => "radio",
                'sequence_no' => 6,
                'label' => '1',
                'value' => '1',
                'is_need_explain' => 1,
            ],
            [
                'type' => "radio",
                'sequence_no' => 7,
                'label' => 'N/A',
                'value' => '0',
                'is_need_explain' => 1,
            ],
        ];

        return [
            [
                'label' => 'Start date:',
                'form_id' => 'start_date_period',
                'page' => 1,
                'sequence_no' => 1,
                'required' => 1,
                'criteria' => [
                    [
                        'type' => 'date',
                        'sequence_no' => 1,
                    ],
                ],
            ],
            [
                'label' => 'End date:',
                'form_id' => 'end_date_period',
                'page' => 1,
                'sequence_no' => 2,
                'required' => 1,
                'criteria' => [
                    [
                        'type' => 'date',
                        'sequence_no' => 1,
                    ],
                ],
            ],
            [
                'label' => 'Attach a current position description; if applicable, make note of any significant changes since last year’s performance review.',
                'form_id' => 'significant_changes',
                'page' => 2,
                'sequence_no' => 1,
                'required' => 1,
                'criteria' => [
                    [
                        'type' => "tiptap",
                        'sequence_no' => 1,
                    ],
                ],
            ],
            [
                'label' => 'If performance goals were set at the last performance review, attach a copy of these goals and comment on the employee’s progress.',
                'form_id' => 'employee_goals',
                'page' => 2,
                'sequence_no' => 2,
                'required' => 1,
                'criteria' => [
                    [
                        'type' => "tiptap",
                        'sequence_no' => 1,
                    ],
                ],
            ],
            [
                'label' => 'Skill and proficiency in carrying out assignments',
                'form_id' => 'skill_and_profiency',
                'page' => 4,
                'sequence_no' => 1,
                'required' => 1,
                'criteria' => $default_criteria,
            ],
            [
                'label' => 'Possesses skills and knowledge to perform the job competently',
                'form_id' => 'skills_and_knowledge',
                'page' => 4,
                'sequence_no' => 2,
                'required' => 1,
                'criteria' => $default_criteria,
            ],
            [
                'label' => 'Reliability (attendance, punctuality, meeting deadlines)',
                'form_id' => 'reliability',
                'page' => 4,
                'sequence_no' => 3,
                'required' => 1,
                'criteria' => $default_criteria,
            ],
            [
                'label' => 'Holds self accountable for assigned responsibilities; sees tasks through to completion in a timely manner',
                'form_id' => 'self_accountable',
                'page' => 4,
                'sequence_no' => 4,
                'required' => 1,
                'criteria' => $default_criteria,
            ],
            [
                'label' => 'Proficiency at improving work methods and procedures as a means toward greater efficiency',
                'form_id' => 'methods_and_procedures',
                'page' => 4,
                'sequence_no' => 5,
                'required' => 1,
                'criteria' => $default_criteria,
            ],
            [
                'label' => 'Communicates effectively with team and relationship contacts',
                'form_id' => 'communicate_effective',
                'page' => 4,
                'sequence_no' => 6,
                'required' => 1,
                'criteria' => $default_criteria,
            ],
            [
                'label' => 'Ability to work cooperatively with supervision or as part of a team',
                'form_id' => 'work_cooperatively',
                'page' => 5,
                'sequence_no' => 7,
                'required' => 1,
                'criteria' => $default_criteria,
            ],
            [
                'label' => 'Displays positive attitude through all written and oral communication',
                'form_id' => 'positive_attitude',
                'page' => 5,
                'sequence_no' => 8,
                'required' => 1,
                'criteria' => $default_criteria,
            ],
            [
                'label' => 'Adeptness at analyzing facts, problem solving, decision-making, and demonstrating good judgment',
                'form_id' => 'good_judgment',
                'page' => 5,
                'sequence_no' => 9,
                'required' => 1,
                'criteria' => $default_criteria,
            ],
            [
                'label' => 'Identifies performance expectations, gives timely feedback and conducts formal performance appraisals.',
                'form_id' => 'perform_expect',
                'page' => 5,
                'sequence_no' => 10,
                'required' => 1,
                'criteria' => $default_criteria,
            ],
            [
                'label' => 'List all aspects of employee’s performance that contribute to his or her effectiveness.',
                'form_id' => 'contribute',
                'page' => 6,
                'sequence_no' => 1,
                'required' => 1,
                'criteria' => [
                    [
                        'type' => "tiptap",
                        'sequence_no' => 1,
                    ],
                ],
            ],
            [
                'label' => 'List aspects of employee’s performance that require improvement for greater effectiveness.',
                'form_id' => 'require_improvement',
                'page' => 6,
                'sequence_no' => 2,
                'required' => 1,
                'criteria' => [
                    [
                        'type' => "tiptap",
                        'sequence_no' => 1,
                    ],
                ],
            ],
            [
                'label' => 'In what way is the employee ready for increased responsibility? What additional training will he/she need to be successful?',
                'form_id' => 'employee_ready',
                'page' => 6,
                'sequence_no' => 3,
                'required' => 1,
                'criteria' => [
                    [
                        'type' => "tiptap",
                        'sequence_no' => 1,
                    ],
                ],
            ],
        ];
    }
}
