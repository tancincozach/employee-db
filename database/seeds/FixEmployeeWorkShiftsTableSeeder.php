<?php

use App\Models\Employee;
use App\Models\Client;
use App\Models\EmployeeWorkShift;
use App\Models\EmployeeClientProject;
use App\Models\WorkShift;
use App\Models\EmployeeStatus;
use App\Models\Status;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class FixEmployeeWorkShiftsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $removed_workshifts = [];
        $new_hire_workshifts = [];
        $override_workshifts = [];

        foreach(EmployeeWorkShift::distinct()->select('employee_id')->get() as $e) {
            $employee_id = $e->employee_id;
            $removed_workshifts = array_merge($removed_workshifts,
                                              $this->remove_conflicting_workshifts($employee_id));
        }

        $new_hire_workshifts = $this->add_workshifts_to_new_hires();
        $override_workshifts = $this->add_workshifts_to_override_employees();

        $this->print_output($removed_workshifts, $new_hire_workshifts, $override_workshifts);
    }

    private function add_workshifts_to_override_employees() {
        $result = [];

        $shift_ids = WorkShift::whereIn('shift', ['Mid', 'Graveyard'])
            ->get()->map(function($p) { return $p->id; })->all();

        foreach($this->get_override_employees_with_work_shifts() as $ews) {
            $count = $ews->count;
            foreach($shift_ids as $shift_id) {

                if($count>=2)
                    break;

                $attributes = [
                    'employee_id' => $ews->employee_id,
                    'shift_id'    => $shift_id
                ];

                if(!EmployeeWorkShift::where($attributes)->exists() && $this->allHasValue($attributes)) {
                    $r = EmployeeWorkShift::create($attributes);
                    array_push($result, $r->id);
                    $count++;
                }
            }
        }
        return $result;
    }

    private function allHasValue($arr) {
        return !in_array(null, array_values($arr));
    }

    private function get_override_employees_with_work_shifts() {
        $override_projects = Client::leftJoin('client_projects','clients.id','client_projects.client_id')
            ->select('client_projects.*')
            ->where('clients.company','Override')
            ->get()->map(function($p) { return $p->id; })->all();

        return Employee::select('employees.id as employee_id', DB::raw('count(employee_work_shifts.id) as count'))
            ->leftJoin('employee_client_projects','employee_client_projects.employee_id','employees.id')
            ->leftJoin('employee_work_shifts','employee_work_shifts.employee_id','employee_client_projects.employee_id')
            ->whereIn('employee_client_projects.client_project_id', $override_projects)
            ->whereNull('employee_work_shifts.deleted_at')
            ->groupBy('employee_work_shifts.employee_id')
            ->get()->all();
    }

    private function add_workshifts_to_new_hires()
    {
        $shifts = WorkShift::whereIn('shift', ['Mid', 'Graveyard'])->get()->all();
        $result = [];

        foreach($this->get_new_employees_with_lacking_workshifts() as $e) {
            $count = $e->total;
            foreach($shifts as $shift) {

                if($count>=2)
                    break;

                $attributes = [
                    'employee_id' => $e->employee_id,
                    'shift_id'    => $shift->id
                ];

                if(!EmployeeWorkShift::where($attributes)->exists() && $this->allHasValue($attributes)) {
                    $r = EmployeeWorkShift::create($attributes);
                    array_push($result, $r->id);
                    $count++;
                }
            }
        }
        return $result;
    }

    private function get_new_employees_with_lacking_workshifts()
    {
        $status_ids = Status::whereIn('name', ['New', 'Hired'])->get()->map(function($p) { return $p->id;  })->all();
        $employee_ids = EmployeeStatus::whereIn('status_id', $status_ids)->distinct()->select('employee_id')->get()->map(function($p) { return $p->employee_id; })->all();

        $result = Employee::select('employees.id as employee_id', DB::raw('count(employee_work_shifts.id) as total'))
            ->leftJoin('employee_work_shifts', 'employees.id', 'employee_work_shifts.employee_id')
            ->whereIn('employees.id', $employee_ids)
            ->groupBy('employee_work_shifts.employee_id')->get()->map(function($p) {
                if($p->total < 2)
                    return $p;
            })->all();
        return array_values(array_filter($result));
    }

    private function print_output($removed_workshifts, $new_hire_workshifts, $override_workshifts)
    {
        $output = new ConsoleOutput();

        $output->writeln("Affected records - " .
            count($removed_workshifts) .
            " (ids): " .
            join(", ", $removed_workshifts));

        $output->writeln("New hire workshifts added - " .
            count($new_hire_workshifts) .
            " (ids): " .
            join(", ", $new_hire_workshifts));

        $output->writeln("'Override' employees workshifts added - " .
            count($override_workshifts) .
            " (ids): " .
            join(", ", $override_workshifts));
    }

    private function remove_conflicting_workshifts($employee_id)
    {
        $result = [];
        $dictionary = [];
        $work_shifts = EmployeeWorkShift::where('employee_id', $employee_id)->get();

        foreach($work_shifts as $ews) {
            $start_time = $ews->start_time;
            $end_time   = $ews->end_time;

            if($ews->shift) {
                if(!$start_time)
                    $start_time = $ews->shift->start_time;
                if(!$end_time)
                    $end_time = $ews->shift->end_time;
            }

            $pieces = [$ews->shift_id, $start_time, $end_time];
            if(count($pieces) < 2)
                continue;

            $k = join($pieces);

            if(!in_array($k, $dictionary)) {
                array_push($dictionary, $k);
            } else {
                array_push($result, $ews->id);
                $ews->delete();
            }

        }
        return $result;
    }

    private function has_workshifts($request, $id = null)
    {
        $employee_id = $request->get('employee_id');
        $shift_id    = $request->get('shift_id');
        $start_time  = $request->get('start_time');
        $end_time    = $request->get('end_time');

        if($shift = WorkShift::find($shift_id)) {
            $start_time = $shift->start_time;
            $end_time   = $shift->end_time;
        }

        $result = EmployeeWorkShift::where('employee_id', $employee_id);

        if($id)
          $result->where('id', '!=', $id);

        $result->where(function($query) use ($shift_id, $start_time, $end_time) {

                if($shift_id)
                  $query->where('shift_id', $shift_id);

                if($start_time)
                    $query->orWhere('start_time', 'LIKE', '%' . $start_time . '%');

                if($end_time)
                    $query->orWhere('end_time', 'LIKE', '%' . $end_time . '%');

                return $query;
            });
        return $result->exists();
    }
}
