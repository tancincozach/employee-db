<?php

use Illuminate\Database\Seeder;
use App\Models\EmployeeTopSkill;

class EmployeeTopSkillsRemoveLowProficiencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $results = EmployeeTopSkill::whereNull('deleted_at')
                                    ->where('proficiency', '<=', 4);
        $results->delete();
    }
}
