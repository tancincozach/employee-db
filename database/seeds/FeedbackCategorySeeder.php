<?php

use App\Models\FeedbackCategory;
use Illuminate\Database\Seeder;

/**
 * Class FeedbackCategorySeeder
 */
class FeedbackCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = database_path('seeds/csv/feedback-category.csv');
        $excel = App::make('excel');

        $excel->load($csv, function($reader) {
            $results = $reader->all();

            foreach ($results as $row) {
                FeedbackCategory::updateOrCreate([
                    'id' => $row->id
                ], [
                    'name' => $row->name,
                    'created_at' => $row->created_at,
                    'updated_at' => $row->updated_at,
                ]);
            }
        });
    }
}
