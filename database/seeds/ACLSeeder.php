<?php

use Illuminate\Database\Seeder;
use App\Models\ACL\Role;
use App\Models\ACL\Resource;
use App\Models\ACL\ResourceRolePermission;
use App\Models\ACL\ResourceUserRolePermission;

class ACLSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // disable foreign key sets
        Schema::disableForeignKeyConstraints();

        // load excel
        $excel = App::make('excel');

        // populate roles
        $csv = database_path('seeds/csv/roles.csv');

        $data = $excel->load($csv, function($reader) {
            $results = $reader->all();

            // truncate table
            Role::truncate();

            foreach ($results as $row) {
                Role::updateOrCreate([
                    'id'           => $row->id,
                    'name'         => $row->name,
                ], [
                    'id'           => $row->id,
                    'name'         => $row->name,
                    'display_name' => $row->display_name,
                    'description'  => $row->description,
                    'level'        => $row->level,
                    'is_admin'     => $row->is_admin,
                    'is_client'    => $row->is_client,
                    'is_enabled'   => $row->is_enabled,
                    'is_default'   => $row->is_default,
                ]);
            }
        });

        $csv = database_path('seeds/csv/resources.csv');

        $data = $excel->load($csv, function($reader) {
            $results = $reader->all();

            // truncate table
            Resource::truncate();

            foreach ($results as $row) {
                Resource::updateOrCreate([
                    'id'   => $row->id,
                    'name' => $row->name,
                ], [
                    'id'          => $row->id,
                    'name'        => $row->name,
                    'description' => $row->description,
                ]);
            }
        });

        $csv = database_path('seeds/csv/resource_role_permissions.csv');

        $data = $excel->load($csv, function($reader) {
            $results = $reader->all();

            // truncate table
            ResourceRolePermission::truncate();

            foreach ($results as $row) {
                ResourceRolePermission::updateOrCreate([
                    'role_id'     => $row->role_id,
                    'resource_id' => $row->resource_id,
                ], [
                    'can_add'     => $row->can_add,
                    'can_edit'    => $row->can_edit,
                    'can_view'    => $row->can_view,
                    'can_delete'  => $row->can_delete,
                    'role_id'     => $row->role_id,
                    'resource_id' => $row->resource_id,
                ]);
            }
        });

        $csv = database_path('seeds/csv/resource_user_role_permissions.csv');

        $data = $excel->load($csv, function($reader) {
            $results = $reader->all();

            // truncate table
            ResourceUserRolePermission::truncate();

            foreach ($results as $row) {
                ResourceUserRolePermission::updateOrCreate([
                    'user_role_id' => $row->role_id,
                    'resource_id'  => $row->resource_id,
                ], [
                    'can_add'      => $row->can_add,
                    'can_edit'     => $row->can_edit,
                    'can_view'     => $row->can_view,
                    'can_delete'   => $row->can_delete,
                    'user_role_id' => $row->role_id,
                    'resource_id'  => $row->resource_id,
                ]);
            }
        });

        // enable back foreign key checks
        Schema::enableForeignKeyConstraints();
    }
}
