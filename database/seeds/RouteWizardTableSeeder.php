<?php

use App\Models\RouteWizard;
use Illuminate\Database\Seeder;

class RouteWizardTableSeeder extends Seeder
{
    public function run()
    {
        $csv = database_path('seeds/csv/route_wizard.csv');
        $excel = App::make('excel');

        $data = $excel->load($csv, function($reader) {
            $results = $reader->all();
            RouteWizard::truncate();
            foreach($results as $row) {
                RouteWizard::updateOrCreate(
                    [
                        'id'         => $row->id,
                    ],
                    [
                    'id' => $row->id,
                    'route_name' => $row->route_name,
                    'group_id' => $row->group_id,
                    'icon' => $row->icon,
                    'route_uri' => $row->route_uri,
                    'caption' => $row->caption,
                    'sequence_order' => $row->sequence_order,
                    'goto_group_id' => $row->goto_group_id
                ]);
            }
        });
    }
}
