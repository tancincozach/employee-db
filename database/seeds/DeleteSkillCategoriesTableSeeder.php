<?php

use App\Models\SkillCategory;
use Illuminate\Database\Seeder;

class DeleteSkillCategoriesTableSeeder extends Seeder
{
    public function run()
    {
        $skillCategory = SkillCategory::find(15);
        if($skillCategory) {
            $skillCategory->delete();
        }
    }
}
