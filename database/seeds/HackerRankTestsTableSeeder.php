<?php

use App\Models\HackerRankTest;
use Illuminate\Database\Seeder;

class HackerRankTestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = database_path('seeds/csv/hacker_rank_tests_test_url.csv');
        $excel = App::make('excel');

        $data = $excel->load($csv, function($reader) {
            $results = $reader->all();
            foreach($results as $row) {
                HackerRankTest::updateOrCreate(
                    [
                        'test_id' => $row->test_id
                    ],
                    [
                        'test_url' => $row->test_url
                    ]
                );
            }
        });
    }
}
