<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Zone;
use App\Models\UserTimezone;
use App\Models\ClientContact;
use App\Models\EmployeeTimesheetUser;
use App\Models\Timesheet;
use App\Models\Employee;

class GenerateUserTimezones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Matt
        $user = User::where('email', 'deco@fullscale.io')->first();
        $timezone = Zone::where('zone_name', Zone::DEFAULT_TIMEZONE)->first();

        if (!empty($user) && !empty($timezone)) {
            UserTimezone::firstOrCreate(
                ['user_id' => $user->id],
                ['zone_id' => $timezone->zone_id]
            );
        }

        // Clients
        $clients = User::select('id', 'email')->where('user_type_id', 2)->get();
        $clients->each(function ($item) {
            $zone_id = 0;
            $clientContact = ClientContact::where('email', $item->email)->first();

            if (!empty($clientContact) && !empty($contact = $clientContact->client)) {
                $zone_id = $contact->zone_id;
            }

            if (!empty($item->id) && !empty($zone_id)) {
                UserTimezone::firstOrCreate(
                    ['user_id' => $item->id],
                    ['zone_id' => $zone_id]
                );
            }
        });

        // Employees
        $employees = Employee::select('id', 'user_id')->whereNotNull('employee_no')->get();
        $employees->each(function ($item) {
            $timesheet_user = EmployeeTimesheetUser::where('employee_id', $item->id)->first();
            $timesheet_user = !empty($timesheet_user) ? $timesheet_user->timesheet_user_id : 0;
            $timesheet = Timesheet::select('tz_str')->distinct()->where('user_id', $timesheet_user)->first();

            $timezone = !empty($timesheet) ? $timesheet->tz_str : '';
            $zone_id = Zone::where('zone_name', $timezone)->first();
            $zone_id = !empty($zone_id) ? $zone_id->zone_id : 0;

            if (!empty($item->id) && !empty($zone_id)) {
                UserTimezone::firstOrCreate(
                    ['user_id' => $item->user_id],
                    ['zone_id' => $zone_id]
                );
            }
        });
    }
}
