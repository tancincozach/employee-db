<?php

use App\Models\Characteristic;
use Illuminate\Database\Seeder;

/**
 * Class CharacteristicSeeder
 */
class CharacteristicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = database_path('seeds/csv/characteristic.csv');
        $excel = App::make('excel');

        $excel->load($csv, function($reader) {
            $results = $reader->all();

            foreach ($results as $row) {
                Characteristic::updateOrCreate([
                    'id' => $row->id
                ], [
                    'name' => $row->name,
                    'description' => $row->description,
                ]);
            }
        });
    }
}
