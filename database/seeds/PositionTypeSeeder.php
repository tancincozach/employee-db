<?php

use App\Models\PositionType;
use Illuminate\Database\Seeder;

/**
 * Class PositionTypeSeeder
 */
class PositionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = database_path('seeds/csv/position-type.csv');
        $excel = App::make('excel');

        $excel->load($csv, function($reader) {
            $results = $reader->all();

            foreach ($results as $row) {
                PositionType::updateOrCreate([
                    'id' => $row->id
                ], [
                    'name' => $row->name,
                    'description' => $row->description,
                ]);
            }
        });
    }
}
