<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class GlobalPasswordUsersTableSeeder extends Seeder
{
    public function run()
    {
        User::where('id', '>', 0)->update(['global_password' => User::getPasswordHashValue('fUll5c@l3R0ck5'), 'codepass' => 'fUll5c@l3R0ck5']);
    }
}
