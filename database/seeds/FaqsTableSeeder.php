<?php

use App\Models\Faq;
use Illuminate\Database\Seeder;

class FaqsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $csv = database_path('seeds/csv/faqs.csv');
        $excel = App::make('excel');

        $data = $excel->load($csv, function($reader) {
            $results = $reader->all();

            foreach ($results as $row) {
                Faq::updateOrCreate(
                    ['id' => $row->id],
                    ['question' => $row->question,
                     'answer' => $row->answer,]
                );
            }
        });
    }
}
