<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ACL\Resource;

class InsertClientTeamBuilderPageResource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
public function up()
    {
        Resource::updateOrCreate(
            [
                'name' => 'client_talent_pool_page',
            ],
            [
                'display_name' => 'Client Talent Pool Page',
                'description' => 'Client Talent Pool Page',
            ]
        );
    }
}
