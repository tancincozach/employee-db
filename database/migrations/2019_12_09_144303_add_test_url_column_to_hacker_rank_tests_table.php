<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTestUrlColumnToHackerRankTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::table('hacker_rank_tests', function (Blueprint $table) {
            $table->string('test_url', 200)->nullable()->after('name');                              
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hacker_rank_tests', function (Blueprint $table) {
            $table->dropColumn('test_url');
        });
    }
}