<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class PopulateWizardRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        $arr = [
            [
                'id' => 3,
                'route_uri' => 'statistics',
                'icon' => 'la la-dashboard color-success',
            ], [
                'id' => 7,
                'route_uri' => 'clients',
                'icon' => 'la la-file-text color-success',
            ],
        ];

        foreach ($arr as $resource) {
            RouteWizard::updateOrCreate(
                [
                    'id' => $resource['id'],
                ],
                [
                    'icon' => $resource['icon'],
                    'route_uri' => $resource['route_uri'],
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
