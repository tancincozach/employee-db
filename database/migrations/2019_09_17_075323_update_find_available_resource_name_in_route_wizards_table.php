<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class UpdateFindAvailableResourceNameInRouteWizardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::where('route_name', 'Find Available Resources')->where('group_id', '3')->update(['route_name' => 'Find Resources']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RouteWizard::where('route_name', 'Find Resources')->where('group_id', '3')->update(['route_name' => 'Find Available Resources']);
    }
}
