<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformEvalQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perform_eval_question', function (Blueprint $table) {
            $table->increments('id');
            $table->string('form_id');
            $table->text('label');
            $table->unsignedTinyInteger('sequence_no');
            $table->unsignedTinyInteger('page');
            $table->unsignedTinyInteger('required')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perform_eval_question');
    }
}
