<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\AllQuestionChoice;
use App\Models\AllQuestionResponse;

class UpdateQuestionChoicesDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        AllQuestionChoice::where('description', 'Morning and Late')->update(['description' => 'Morning, day and evening hours']);
        
        AllQuestionResponse::where('all_question_choice_description', 'Morning and Late')
            ->update(['all_question_choice_description' => 'Morning, day and evening hours']);
    }
}
