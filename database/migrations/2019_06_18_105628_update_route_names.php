<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\RouteWizard;
use Illuminate\Database\Migrations\Migration;

class UpdateRouteNames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        RouteWizard::where('route_name', 'View Resource Suggestions')->update(['route_name' => 'Smart Team Builder']);
        RouteWizard::where('route_name', 'Go to Client Smart Team Builder')->update(['route_name' => 'Smart Team Builder']);
        RouteWizard::where('route_name', 'Smart Team Builder')->update(['icon' => 'la la-users color-secondary']);
        RouteWizard::where('route_name', 'View Dashboards')->update(['route_name' => 'View Dashboard']);
    }
}
