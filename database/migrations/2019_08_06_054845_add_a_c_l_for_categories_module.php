<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ACL\Resource;

class AddACLForCategoriesModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resourcesArr = [
            [
                'name' => 'categories',
                'display_name' => 'Categories',
                'description' => 'Categories',
            ],
            [
                'name' => 'categories_list',
                'display_name' => 'Categories List',
                'description' => 'Categories List',
            ]

        ];

        foreach ($resourcesArr as $resource) {
            Resource::updateOrCreate(
                [
                    'name' => $resource['name'],
                ],
                [
                    'display_name' => $resource['display_name'],
                    'description' => $resource['description'],
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Resource::whereIn('name', ['categories', 'categories_list'])->forceDelete();
    }
}
