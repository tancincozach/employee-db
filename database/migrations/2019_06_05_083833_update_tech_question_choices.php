<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\AllQuestion as Question;
use App\Models\AllQuestionChoice as QuestionChoice;
use App\Models\Skill;

class UpdateTechQuestionChoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            // tech question id based in prod data
            $techQuestionId = 25;

            // remove existing choices
            QuestionChoice::where('all_question_id', $techQuestionId)->forceDelete();

            // adding new choices for tech question
            $newChoices = $this->setNewChoices();
            foreach ($newChoices as $key => $choice) {
                $questionChoice                     = new QuestionChoice();
                $questionChoice->all_question_id    = $techQuestionId;// tech question id
                $questionChoice->description        = $choice;
                $questionChoice->display_sequence   = $key + 1;
                $questionChoice->save();
            }

            // new ajax skill insert
            $skill = new Skill();
            $skill->name = 'Ajax';
            $skill->save();
        } catch (Throwable $e) {
            echo 'Unable to properly migrate' . __FILE__;
        }
    }

    private function setNewChoices()
    {
        return [
            '.Net',
            'PHP',
            'Ruby on Rails',
            'Android',
            'iOS',
            'ReactJS',
            'React Native',
            'Javascript',
            'Angular',
            'Java',
            'Python',
            'NodeJS',
            'C++',
            'C#',
            'MySQL',
            'SQL',
            'jQuery',
            'Ajax',
        ];
    }
}
