<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class AlterViewemployeeToEmployeehourssummaryInRouteWizards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::where('route_name', 'View Employee Reports')->where('group_id', '5')->where('route_uri', 'worklogs/employee-report')->update(['route_name' => 'Employee Hours Summary']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RouteWizard::where('route_name', 'Employee Hours Summary')->where('group_id', '5')->where('route_uri', 'worklogs/employee-report')->update(['route_name' => 'View Employee Reports']);
    }
}
