<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ACL\Resource;

class AddHackerRankResultsPermissionToResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Resource::updateOrCreate(
            [
                'name' => 'hacker_rank_results',
            ],
            [
                'display_name' => 'Hacker Rank Test Results',
                'description'  => 'List of Hacker Rank Test Results',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Resource::where('name', 'hacker_rank_results')->delete();
    }
}
