<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ACL\Resource;

class AddJobcodeAssignmentsResourcePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Resource::updateOrCreate(
            [
                'name' => 'jobcode_assignments',
            ],
            [
                'display_name' => 'Jobcode Assignments',
                'description'  => 'Jobcode Assignments',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Resource::where('name', 'jobcode_assignments')->forceDelete();
    }
}
