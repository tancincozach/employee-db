<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class UpdateRoutesTableForSearchTalents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::updateOrCreate(
            [
                'route_name' => 'Search Talents',
            ],
            [
                'route_uri' => 'search-talents',
            ]
        );
    }
}
