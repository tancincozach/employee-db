<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class AddViewemployeereportsToRouteWizards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resourcesArr = [
            [
                'group_id' => 5,
                'icon' => 'la la-calendar-check-o color-success',
                'route_name' => 'View Employee Reports',
                'sequence_order' => 3,
                'route_uri' => 'worklogs/employee-report',
                'goto_group_id' => 0
            ]
        ];
        foreach ($resourcesArr as $resource) {
            RouteWizard::updateOrCreate(
                [
                    'route_name' => $resource['route_name'],
                ],
                [
                    'group_id' => $resource['group_id'],
                    'icon' => $resource['icon'],
                    'sequence_order' => $resource['sequence_order'],
                    'route_uri' => $resource['route_uri'],
                    'goto_group_id' => $resource['goto_group_id']
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RouteWizard::where('route_name', 'View Employee Reports')->delete();
    }
}
