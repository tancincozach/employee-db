<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class UpdateIconForSendClientProfileAccessInRouteWizardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::where('route_name', 'Send Client Profile Access')->where('group_id', '2')->update(['icon' => 'la la-user-secret color-neutral']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RouteWizard::where('route_name', 'Send Client Profile Access')->where('group_id', '2')->update(['icon' => 'la la-user-secret color-secondary']);
    }
}
