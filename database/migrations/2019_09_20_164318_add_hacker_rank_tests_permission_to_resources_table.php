<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ACL\Resource;

class AddHackerRankTestsPermissionToResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Resource::updateOrCreate(
            [
                'name' => 'hacker_rank_tests',
            ],
            [
                'display_name' => 'Hacker Rank Tests',
                'description'  => 'List of Hacker Rank Tests',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Resource::where('name', 'hacker_rank_tests')->delete();
    }
}
