<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class UpdateReviewClientDataRouteWizardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $arr = [
            [
                'id' => 5,
                'route_name' => 'Review Recently Signed-Up Clients',
            ],
        ];

        foreach ($arr as $resource) {
            RouteWizard::updateOrCreate(
                [
                    'id' => $resource['id'],
                ],
                [
                    'route_name' => $resource['route_name'],
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
