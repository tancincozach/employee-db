<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\RouteWizard;

class UpdateRouteWizardsForTimesheetReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::where('route_uri', 'worklogs/daily')->update(['route_uri' => 'daily-tracker']);
        RouteWizard::where('route_uri', 'worklogs/employee-report')->update(['route_uri' => 'monthly-summary']);
        RouteWizard::where('route_uri', 'worklogs/report')->update(['route_uri' => 'weekly-reports']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
