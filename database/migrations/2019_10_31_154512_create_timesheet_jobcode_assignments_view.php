<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimesheetJobcodeAssignmentsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW timesheet_jobcode_assignments AS
            SELECT
                 ecp.id AS id
                ,c.id AS client_id
                ,e.id AS employee_id
                ,tu.id AS timesheet_user_id
                ,ctj.timesheet_jobcode_id AS timesheet_jobcode_id
                ,CONCAT(tu.last_name, ', ', tu.first_name) AS timesheet_user_name
                ,CONCAT(IFNULL(e.last_name, ''), ', ', IFNULL(e.first_name, ''), ' ', IFNULL(e.middle_name, ''), ' ', IFNULL(e.ext, '')) AS employee_name
                ,c.company
                ,ecp.created_at
                ,ecp.updated_at
                ,ecp.deleted_at
            FROM employee_client_projects AS ecp
            LEFT JOIN client_projects AS cp ON ecp.client_project_id = cp.id
            LEFT JOIN clients AS c ON cp.client_id = c.id
            LEFT JOIN employees AS e ON ecp.employee_id = e.id
            LEFT JOIN employee_timesheet_users AS etu ON e.id = etu.employee_id
            LEFT JOIN timesheet_users AS tu ON etu.timesheet_user_id = tu.id
            LEFT JOIN client_timesheet_jobcodes AS ctj ON c.id = ctj.client_id
            ORDER BY e.last_name;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS timesheet_jobcode_assignments");
    }
}
