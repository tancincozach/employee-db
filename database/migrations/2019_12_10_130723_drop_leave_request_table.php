<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class DropLeaveRequestTable
 */
class DropLeaveRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('employee_approvers');
        Schema::dropIfExists('employee_leave_credits');
        Schema::dropIfExists('employee_leave_credit_histories');
        Schema::dropIfExists('leave_approvers');
        Schema::dropIfExists('leave_requests');
        Schema::dropIfExists('leave_types');
        Schema::dropIfExists('leave_credit_types');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('employee_approvers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')
                ->unsigned();
            $table->integer('leave_approver_id')
                ->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('employee_id')
                ->references('id')
                ->on('employees');
            $table->foreign('leave_approver_id')
                ->references('id')
                ->on('leave_approvers');
        });

        Schema::create('employee_leave_credits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')
                ->unsigned();
            $table->integer('leave_credit_type_id')
                ->unsigned();
            $table->decimal('balance');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('employee_id')
                ->references('id')
                ->on('employees');
            $table->foreign('leave_credit_type_id')
                ->references('id')
                ->on('leave_credit_types');
        });

        Schema::create('employee_leave_credit_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')
                ->unsigned();
            $table->integer('leave_credit_type_id')
                ->unsigned();
            $table->decimal('balance');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('employee_id')
                ->references('id')
                ->on('employees');
            $table->foreign('leave_credit_type_id')
                ->references('id')
                ->on('leave_credit_types');
        });

        Schema::create('leave_approvers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('approver_id')
                ->unsigned();
            $table->integer('oic_id')
                ->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('approver_id')
                ->references('id')
                ->on('employees');
            $table->foreign('oic_id')
                ->references('id')
                ->on('employees');
        });

        Schema::create('leave_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')
                ->unsigned();
            $table->integer('leave_type_id')
                ->unsigned();
            $table->string('batch');
            $table->enum('duration', ['Whole Day', 'Half Day'])
                ->default('Whole Day');
            $table->tinyInteger('is_paid');
            $table->date('start_date');
            $table->date('end_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->enum('status', ['Pending', 'Approved', 'Disapproved', 'Cancelled'])
                ->default('Pending');
            $table->text('reason')
                ->nullable();
            $table->integer('approver_id')
                ->unsigned()
                ->nullable();
            $table->text('approver_comment')
                ->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('employee_id')
                ->references('id')
                ->on('employees');
            $table->foreign('leave_type_id')
                ->references('id')
                ->on('leave_types');
            $table->foreign('approver_id')
                ->references('id')
                ->on('employees');
        });

        Schema::create('leave_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('is_enabled');
            $table->integer('leave_credit_type_id')
                ->unsigned()
                ->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('leave_credit_type_id')
                ->references('id')
                ->on('leave_credit_types');
        });

        Schema::create('leave_credit_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('limit')
                ->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
