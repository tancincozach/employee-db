<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSentColumnWeeklyFloorReportTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('weekly_floor_report', 'sent')) {
            Schema::table('weekly_floor_report', function (Blueprint $table) {
                //
                $table->integer('sent')->default(0)->after('status');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('weekly_floor_report', function (Blueprint $table) {
            //
        });
    }
}
