<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobcodeAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobcode_assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('jobcode_id');
            $table->tinyInteger('active')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('jobcode_assignments', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('timesheet_users');
            $table->foreign('jobcode_id')->references('id')->on('timesheet_jobcodes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobcode_assignments');
    }
}
