<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatedByUserInClientTeambuilderBucketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_teambuilder_buckets', function (Blueprint $table) {
            $table->integer('updated_by_user_id')->unsigned()->nullable()->after('is_suggested_admin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_teambuilder_buckets', function (Blueprint $table) {
            $table->dropColumn('updated_by_user_id');
        });
    }
}
