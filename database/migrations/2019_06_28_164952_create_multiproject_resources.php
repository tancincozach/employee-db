<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultiprojectResources extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multiproject_resources', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')->nullable()->unsigned();
            $table->integer('job_position_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            // Table indexes
            $table->index('employee_id');
            $table->index('job_position_id');

            // Table foreign keys
            $table->foreign('employee_id')
                ->references('id')->on('employees');
            $table->foreign('job_position_id')
                ->references('id')->on('job_positions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multiproject_resources');
    }
}
