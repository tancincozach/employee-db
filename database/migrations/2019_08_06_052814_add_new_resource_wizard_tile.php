<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class AddNewResourceWizardTile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::updateOrCreate(
            [   
                'route_name' => 'View Resource Reports',
                'group_id'   => 6
            ],
            [
                'route_name' => 'View Resource Reports',
                'group_id' => 6,
                'icon' => 'la la-calendar-check-o color-success',
                'route_uri' => 'worklogs/employee-report',
                'sequence_order' => 3,
                'goto_group_id' => 0,
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RouteWizard::where('route_name', 'View Resource Reports')
                    ->where('group_id', 6)
                    ->delete();
    }
}
