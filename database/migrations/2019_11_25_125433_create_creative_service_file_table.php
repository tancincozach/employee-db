<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCreativeServiceFileTable
 */
class CreateCreativeServiceFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creative_service_file', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('creative_service_id');
            $table->foreign('creative_service_id')->references('id')->on('creative_service');
            $table->string('file');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creative_service_file');
    }
}
