<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class PopulateRoutesWizardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resourcesArr = [
            [
                'group_id' => 3,
                'icon' => 'ks-icon-user color-primary',
                'route_name' => 'Go to Talent Pool Suggestions',
                'sequence_order' => 1,
                'route_uri' => '',
                'goto_group_id' => 0
            ], [
                'group_id' => 3,
                'icon' => 'ks-icon-user color-primary',
                'route_name' => 'Search Talents',
                'sequence_order' => 2,
                'route_uri' => '',
                'goto_group_id' => 0
            ], [
                'group_id' => 3,
                'icon' => 'la la-file-text color-success',
                'route_name' => 'View Timesheets',
                'sequence_order' => 3,
                'route_uri' => '',
                'goto_group_id' => 0
            ],
            [
                'group_id' => 3,
                'icon' => 'la la-dashboard color-warning',
                'route_name' => 'Schedule Talent Interview',
                'sequence_order' => 4,
                'route_uri' => '',
                'goto_group_id' => 0
            ],
            [
                'group_id' => 3,
                'icon' => 'la la-pie-chart color-info',
                'route_name' => 'Manage Projects',
                'sequence_order' => 5,
                'route_uri' => '',
                'goto_group_id' => 0
            ],
            [
                'group_id' => 3,
                'icon' => 'la la-file-text color-success',
                'route_name' => 'View Reports',
                'sequence_order' => 6,
                'route_uri' => '',
                'goto_group_id' => 0
            ]

        ];
        foreach ($resourcesArr as $resource) {
            RouteWizard::updateOrCreate(
                [
                    'route_name' => $resource['route_name'],
                ],
                [
                    'group_id' => $resource['group_id'],
                    'icon' => $resource['icon'],
                    'sequence_order' => $resource['sequence_order'],
                    'route_uri' => $resource['route_uri'],
                    'goto_group_id' => $resource['goto_group_id']
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
