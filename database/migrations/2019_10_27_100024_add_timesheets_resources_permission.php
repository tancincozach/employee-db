<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ACL\Resource;

class AddTimesheetsResourcesPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resources = [
            [
                'name'         => 'timesheets',
                'display_name' => 'Timesheets',
                'description'  => 'Timesheets',
            ],
            [
                'name'         => 'timesheet_jobcodes',
                'display_name' => 'Timesheet Jobcodes',
                'description'  => 'Timesheet Jobcodes',
            ],
            [
                'name'         => 'timesheet_users',
                'display_name' => 'Timesheet Users',
                'description'  => 'Timesheet Users',
            ],
            [
                'name'         => 'client_timesheet_jobcodes',
                'display_name' => 'Client Timesheets Mapping',
                'description'  => 'Client Timesheets Mapping',
            ],
            [
                'name'         => 'employee_timesheet_users',
                'display_name' => 'Employee Timesheets Mapping',
                'description'  => 'Employee Timesheets Mapping',
            ],
            [
                'name'         => 'timesheet_reports',
                'display_name' => 'Timesheet Reports',
                'description'  => 'Timesheet Reports',
            ],
        ];

        foreach ($resources as $resource) {
            Resource::updateOrCreate(
                [
                    'name' => $resource['name'],
                ],
                [
                    'display_name' => $resource['display_name'],
                    'description'  => $resource['description'],
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Resource::whereIn('name', [
            'timesheets', 'timesheet_jobcodes',
            'timesheet_users', 'client_timesheet_jobcodes',
            'employee_timesheet_users', 'timesheet_reports',
        ])->forceDelete();
    }
}
