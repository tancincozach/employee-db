<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTopSkillInEmployeeSkills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_skills', function (Blueprint $table) {
            $table->tinyInteger('checked_top_skill')
                ->default(0)
                ->after('project_roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_skills', function (Blueprint $table) {
            $table->dropColumn('checked_top_skill');
        });
    }
}
