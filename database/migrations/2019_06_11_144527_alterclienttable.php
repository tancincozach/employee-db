<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alterclienttable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->boolean('reviewed')
                ->default(0)
                ->after('status')
                ->comment('0 - not reviewed; 1 - done review');

                $table->boolean('status')
                ->default(2)
                ->comment('0 - active; 1 - end of contract; 2 - prospect')
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('reviewed');
            $table->boolean('status')
                ->comment('')
                ->change();
        }); 
    }
}
