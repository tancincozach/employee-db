<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimesheetJobcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timesheet_jobcodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('parent_id')->nullable();
            $table->tinyInteger('assigned_to_all')->nullable();
            $table->tinyInteger('active')->nullable();
            $table->string('type')->nullable();
            $table->tinyInteger('has_children')->nullable();
            $table->dateTime('last_modified')->nullable();
            $table->dateTime('created')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timesheet_jobcodes');
    }
}
