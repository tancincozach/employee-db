<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class AddViewReportsInRoutesWizardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resources = [
            [
                'group_id' => '2',
                'icon' => 'la la-calendar-check-o color-success',
                'route_name' => 'View Client Reports',
                'route_uri' => 'search/clients?redirectTo=worklogs-report',
                'sequence_order' => '7',
                'goto_group_id' => '0',
            ],
        ];
        foreach ($resources as $resource) {
            RouteWizard::Create(
                [
                    'route_name' => $resource['route_name'],
                    'group_id' => $resource['group_id'],
                    'icon' => $resource['icon'],
                    'route_uri' => $resource['route_uri'],
                    'sequence_order' => $resource['sequence_order'],
                    'goto_group_id' => $resource['goto_group_id'],
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RouteWizard::where('route_name', 'View Client Reports')->delete();
    }
}
