<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHackerRankTestResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hacker_rank_test_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('result_id');
            $table->integer('test_id');
            $table->integer('percent_score');
            $table->datetime('date_time_start_taken');
            $table->datetime('date_time_end_taken');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hacker_rank_test_results');
    }
}
