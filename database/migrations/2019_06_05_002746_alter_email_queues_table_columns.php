<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmailQueuesTableColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_queues', function (Blueprint $table) {
            $table->unsignedInteger('status_id')
                    ->comment('(0 - pending, 1 - sent, 2 - error)')
                    ->change();

            $table->string('status_desc')
                    ->nullable()
                    ->comment('(status description)')
                    ->change();

            $table->unsignedInteger('sender_id')
                    ->nullable()
                    ->comment('(employee id, 0 for server initiated)')
                    ->change();
                    
            $table->string('sender_name')
                    ->nullable()
                    ->comment('(employee name, or Server)')
                    ->change();

            $table->string('sender_email')
                    ->default('noreply@fullscale.io')
                    ->comment('(employee email, or noreply@fullscale.io)')
                    ->change();

            $table->string('subject')
                    ->comment('(email subject)')
                    ->change();
                
            $table->text('send_to')
                    ->comment('(emails of the recepients)')
                    ->change();

            $table->text('cc')
                    ->nullable()
                    ->comment('(emails of the cced recepients)')
                    ->change();

            $table->text('bcc')
                    ->nullable()
                    ->comment('(emails of the bcced recepients)')
                    ->change();

            $table->longText('body')
                    ->comment('(email body)')
                    ->change();

            $table->text('attachments')
                    ->nullable()
                    ->comment('(urls of the files, must have separator)')
                    ->change();

            $table->boolean('priority')
                    ->default(1)
                    ->comment('(100 - most urgent, 1 - lowest prio)')
                    ->change();

            $table->dateTime('sent_at')
                    ->nullable()
                    ->default(null)
                    ->comment('(actual sent date & time using server time)')
                    ->change();
        }); 
    }

}
