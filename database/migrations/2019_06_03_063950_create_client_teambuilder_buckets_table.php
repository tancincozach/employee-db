<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTeambuilderBucketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_teambuilder_buckets', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('client_project_id')->unsigned();
            $table->integer('employee_id')->unsigned();
            $table->integer('job_position_id')->unsigned();
            $table->boolean('client_selected')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_id')
                ->references('id')
                ->on('clients');

            $table->foreign('client_project_id')
                ->references('id')
                ->on('client_projects');

            $table->foreign('employee_id')
                ->references('id')
                ->on('employees');

            $table->foreign('job_position_id')
                ->references('id')
                ->on('job_positions');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_teambuilder_buckets');
    }
}
