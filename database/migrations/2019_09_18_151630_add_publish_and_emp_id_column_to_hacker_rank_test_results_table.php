<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPublishAndEmpIdColumnToHackerRankTestResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hacker_rank_test_results', function (Blueprint $table) {
            $table->integer('employee_id')->after('percent_score');
            $table->boolean('published')->after('employee_id')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hacker_rank_test_results', function (Blueprint $table) {});
    }
}
