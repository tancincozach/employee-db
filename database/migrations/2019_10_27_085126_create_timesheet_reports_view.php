<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimesheetReportsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW timesheet_reports AS
            SELECT  t.id AS id
                   ,t.user_id as timesheet_user_id
                   ,CONCAT(tu.last_name, ', ', tu.first_name) AS timesheet_user
                   ,CONCAT(
                        COALESCE(e.last_name, ''), ', ',
                        COALESCE(e.first_name, ''), ' ',
                        COALESCE(e.middle_name, ''), ' ',
                        COALESCE(e.ext, '')
                    ) AS employee
                   ,t.jobcode_id as timesheet_jobcode_id
                   ,tj.name as timesheet_jobcode_name
                   ,c.id as client_id
                   ,c.company as client_name
                   ,e.id as employee_id
                   ,u.email as email
                   ,t.date as log_date
                   ,t.start as start
                   ,t.end as end
                   ,t.duration as duration
                   ,ROUND(t.duration / 3600, 2) as working_hrs
                   ,t.tz as tz
                   ,t.tz_str as tz_str
                   ,t.type as type
                   ,t.location as location
                   ,t.on_the_clock as on_the_clock
                   ,t.notes as notes
                   ,t.last_modified as last_modified
                   ,tj.assigned_to_all as assigned_to_all
                   ,tj.active as jobcode_active
                   ,tj.type as jobcode_type
                   ,tu.active as user_active
                   ,tu.username as timesheet_username
                   ,tu.email as timesheet_email
                   ,tu.last_active as user_last_active
                   ,tu.pto_balances as pto_balances
                   ,tu.created as user_created
                   ,c.timezone as client_timezone
                   ,e.last_name as last_name
                   ,e.first_name as first_name
                   ,e.middle_name as middle_name
                   ,e.ext as ext
                   ,t.created_at as created_at
                   ,t.updated_at as updated_at
                   ,t.deleted_at as deleted_at
            FROM timesheets t
            LEFT JOIN timesheet_jobcodes tj ON t.jobcode_id = tj.id
            LEFT JOIN timesheet_users tu ON t.user_id = tu.id
            LEFT JOIN client_timesheet_jobcodes ctj ON t.jobcode_id = ctj.timesheet_jobcode_id
            LEFT JOIN clients c ON ctj.client_id = c.id
            LEFT JOIN employee_timesheet_users etu ON t.user_id = etu.timesheet_user_id
            LEFT JOIN employees e ON etu.employee_id = e.id
            LEFT JOIN users u ON e.user_id = u.id
            ORDER BY t.date, t.start;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS timesheet_reports");
    }
}
