<?php

/**
 * 
 * Revamped Tsheet Reports Module
 * Author: Marjhun Christopher B. Galanido
 * 
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\WeeklyReport;
use App\Models\ClientProjectJobcode;
use App\Models\WeeklyReportBatchViewable;

class UpdateTsheetReportsTableRevamped extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * 
         * UPDATING CLIENT PROJECT JOBCODES TABLE
         * 
         * Truncated for fresh data and no impediments
         * for adding the new column
         * 
         * Removed Columns:
         *  - client_project_id
         * 
         * Added Columns
         *  - client_id (FK)
         *  - is_holiday 
         * 
         */
        // ClientProjectJobcode::truncate();

        Schema::table('client_project_jobcodes', function(Blueprint $table){
            $table->dropForeign('client_project_jobcodes_client_project_id_foreign');
            $table->dropColumn('client_project_id');

            $table->integer('client_id')
                ->unsigned()
                ->nullable()
                ->after('id');

            $table->tinyInteger('is_holiday')
                ->unsigned()
                ->default(0)
                ->after('jobcode');

            $table->foreign('client_id')
                ->references('id')
                ->on('clients');
        });


        /**
         * 
         * UPDATING WEEKLY REPORT TABLE
         * 
         * Truncated for fresh data and no impediments 
         * for adding the new column
         * 
         * Removed columns
         *  - client_project_id
         *  - jobcode
         * 
         * Added column
         *  - jobcode_id (FK)
         * 
         */
        WeeklyReport::truncate();

        Schema::table('weekly_reports', function (Blueprint $table) {
            $table->dropForeign('weekly_reports_client_project_id_foreign');
            $table->dropColumn([
                'client_project_id',
                'jobcode'
            ]);

            $table->integer('jobcode_id')
                ->unsigned()
                ->after('timesheet_id');
            
            $table->foreign('jobcode_id')
                ->references('id')
                ->on('client_project_jobcodes');
        });


        /**
         * 
         * UPDATING WEEKLY REPORT BATCH VIEWABLES TABLE
         * 
         * Truncated for fresh data and no impediments
         * for adding the new column
         * 
         * Removed Columns:
         *  - client_project_id
         * 
         * Added Columns
         *  - client_id (FK)
         *  - is_holiday 
         * 
         */
        WeeklyReportBatchViewable::truncate();

        Schema::table('weekly_report_batch_viewables', function(Blueprint $table){
            $table->dropColumn('jobcode');

            $table->integer('jobcode_id')
                ->unsigned()
                ->after('client_id');
            
            $table->foreign('jobcode_id')
                ->references('id')
                ->on('client_project_jobcodes');
        });

        // END
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /**
         * Reversing Weekly Reports 
         */
        Schema::table('weekly_reports', function (Blueprint $table) {
            $table->dropForeign('weekly_reports_jobcode_id_foreign');
            $table->dropColumn('jobcode_id');

            $table->integer('client_project_id')
                ->unsigned()
                ->after('timesheet_id');
            $table->string('jobcode')
                ->after('client_project_id');            
            
            $table->foreign('client_project_id')
                ->references('id')
                ->on('client_projects');
        });

        /**
         * Reversing Client Project Jobcodes
         */
        Schema::table('client_project_jobcodes', function(Blueprint $table){
            $table->dropForeign('client_project_jobcodes_client_id_foreign');
            $table->dropColumn([
                'client_id',
                'is_holiday'    
            ]);

            $table->integer('client_project_id')
                ->unsigned()
                ->after('id');

            $table->foreign('client_project_id')
                ->references('id')
                ->on('client_projects');
        });

        /**
         * Reversing Weekly Report Batch Viewables
         */
        Schema::table('weekly_report_batch_viewables', function(Blueprint $table){
            $table->dropForeign('weekly_report_batch_viewables_jobcode_id_foreign');
            $table->dropColumn('jobcode_id');

            $table->string('jobcode')
                ->after('client_id');
        });
    }
}
