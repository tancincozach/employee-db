<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class RemoveOnboardChecklistFromRouteWizardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::where('route_name', 'Manage Client On-Board Checklist')
            ->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RouteWizard::withTrashed()
            ->where('route_name', 'Manage Client On-Board Checklist')
            ->restore();
    }
}
