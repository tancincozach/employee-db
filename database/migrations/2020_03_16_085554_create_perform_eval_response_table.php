<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformEvalResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perform_eval_response', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->unsignedInteger('employee_id')->comment('Employee resource to be evaluated.');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->unsignedInteger('client_id')->comment('Client evaluator');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->unsignedInteger('evaluator_id')->comment('Admin evaluator');
            $table->foreign('evaluator_id')->references('id')->on('users');
            $table->unsignedInteger('perform_eval_question_id');
            $table->string('perform_eval_question_form_id');
            $table->uuid('perform_eval_criteria_id');
            $table->text('response_value')->nullable()->comment('Form value.');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perform_eval_response');
    }
}
