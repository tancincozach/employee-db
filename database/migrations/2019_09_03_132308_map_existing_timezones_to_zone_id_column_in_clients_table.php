<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Client;
use App\Models\Zone;

use App\Helpers\RocksHelper;

class MapExistingTimezonesToZoneIdColumnInClientsTable extends Migration
{
    /*
     * This is the default timezone used mostly in clients
     *
     * @var string
     */
    const DEFAULT_TIMEZONE = 'America/Chicago';

    /*
     * The default zone id
     *
     * @var integer
     */
    protected $zone_id;

    /*
     * Set the default timezone in the class constructor
     */
    public function __construct()
    {
        $zone = Zone::where('zone_name', self::DEFAULT_TIMEZONE)->first();
        $this->zone_id = $zone->zone_id;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $clients = Client::all();

        $clients->each(function ($item, $key) {
            $timezone = $item->timezone;

            if (is_numeric($timezone)) {
                $this->processTimezoneNumeric($item->id, $timezone);
            } else {
                $this->processValidatedTimezone($item->id, $timezone);
            }
        });
    }

    /*
     * Process timezone with a numeric value
     *
     * @param integer        $clientId
     * @param integer|string $timezone
     * @return void
     */
    private function processTimezoneNumeric($clientId, $timezone)
    {
        $zone = Zone::where('zone_id', $timezone)->first();

        if ($zone) {
            $this->updateClient($clientId, $zone->zone_name, $zone->zone_id);
            return;
        }

        // If we could not find the timezone,
        // set to the default timezone.
        $this->updateClient($clientId, self::DEFAULT_TIMEZONE, $this->zone_id);
    }

    /*
     * Validates and process a timezone
     *
     * @param integer $clientId
     * @param string $timezone
     * @return void
     */
    private function processValidatedTimezone($clientId, $timezone)
    {
        $isValid = RocksHelper::isValidTimezone($timezone);
        $zone = Zone::where('zone_name', $timezone)->first();

        // If we could not find the timezone,
        // set to the default timezone.
        if (!$isValid && empty($zone)) {
            $this->updateClient($clientId, self::DEFAULT_TIMEZONE, $this->zone_id);
            return;
        }

        $this->updateClient($clientId, $timezone, $zone->zone_id);
    }

    /*
     * Updates client data
     *
     * @param integer $id
     * @param string  $timezone
     * @param integer $zone_id
     * @return void
     */
    private function updateClient($id, $timezone, $zone_id)
    {
        Client::where('id', $id)->update([
            'timezone' => $timezone,
            'zone_id'  => $zone_id,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Cients with 'Central' timezone
        Client::whereIn('company', [
            'Stackify',
            'GigaBook',
            'OneHQ',
            'H3',
            'Homebase',
            'LaborChart',
            'Wicket',
            'Initech',
            'Atonix',
            'DivvyHQ',
            'Marknology',
            'Companion Protect',
            'Stenovate',
            'League Ally',
            'Virtual Summits LLC',
            'CitySmart',
        ])->update([
            'timezone' => 'Central',
            'zone_id'  => 0,
        ]);

        // Cients with 'GMT -5' timezone
        Client::whereIn('company', [
            'Dealer Teamwork 1',
            'Dealer Teamwork 2',
        ])->update([
            'timezone' => 'GMT -5',
            'zone_id'  => 0,
        ]);

        // Cients with 'Mountain' timezone
        Client::whereIn('company', [
            'Urban Necessities',
        ])->update([
            'timezone' => 'Mountain',
            'zone_id'  => 0,
        ]);

        // Cients with 'Eastern' timezone
        Client::whereIn('company', [
            'Screen Steps',
        ])->update([
            'timezone' => 'Eastern',
            'zone_id'  => 0,
        ]);

        // Cients with 'Central Time Zone' timezone
        Client::whereIn('company', [
            'Flight Sched Pro',
        ])->update([
            'timezone' => 'Central Time Zone',
            'zone_id'  => 0,
        ]);

        // Cients with 'CST (same as Kansas)' timezone
        Client::whereIn('company', [
            'BOLD Marketing LLC',
        ])->update([
            'timezone' => 'CST (same as Kansas)',
            'zone_id'  => 0,
        ]);

        // Cients with 'CST' timezone
        Client::whereIn('company', [
            'Mixtape',
        ])->update([
            'timezone' => 'CST',
            'zone_id'  => 0,
        ]);
    }
}
