<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnStatusInClientTeambuilderBuckets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_teambuilder_buckets', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('client_teambuilder_buckets', function (Blueprint $table) {
            $table->enum('status', ['suggested', 'selected', 'declined', 'assigned'])
                ->after('job_position_id')
                ->default('suggested');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
