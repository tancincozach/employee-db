<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmployeeOtherDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_other_details', function(Blueprint $table){
            $table->boolean('skill_assessment')->default(false)->after('notes');
            $table->string('availability')->default(null)->nullable()->after('skill_assessment');
            $table->text('availability_note')->default(null)->nullable()->after('availability');
            $table->string('preferred_shift')->default(null)->nullable()->after('availability_note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_other_details', function(Blueprint $table){
            $table->dropColumn('skill_assessment');
            $table->dropColumn('availability');
            $table->dropColumn('availability_note');
            $table->dropColumn('preferred_shift');
        });
    }
}
