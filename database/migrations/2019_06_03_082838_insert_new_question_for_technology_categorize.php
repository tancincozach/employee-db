<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\AllQuestion as Question;
use App\Models\AllQuestionChoice as QuestionChoice;
use App\Models\AllQuestionCategory as QuestionCategory;
use App\Models\Category;

class InsertNewQuestionForTechnologyCategorize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $categoryId = $this->insertInitialCategory();
        $questions = $this->setQuestionsWithChoices(); // with choices

        foreach ($questions as $q) {
            $choices = $q['question_choices'];

            if ($q['id'] == 8) {
                $q['group_choices'] = 0;
                $choices = $this->choiceSkillCategory();
            }

            unset($q['id']);
            unset($q['question_choices']);
            $q['all_question_category_id'] = $categoryId;
            $question = Question::create($q);
            $choice = [];

            foreach ($choices as $c) {
                $choice[] = new QuestionChoice($c);
            }

            $question->questionChoices()->saveMany($choice);
        }
    }

    private function insertInitialCategory()
    {
        $category = new QuestionCategory();
        $category->name = 'Revised Client Onboarding';
        $category->save();

        return $category->id;
    }

    private function setQuestionsWithChoices()
    {
        $results = Question::with(['questionChoices'])
            ->where('all_question_category_id', 1)
            ->get();
        $questionContent = $this->insertContentWritingQuestion();

        return $results->push($questionContent)->toArray();
    }

    private function choiceSkillCategory()
    {
        $categories = Category::all();

        return $categories->map(function ($category, $index) {
            return [
                'description'     => $category->name,
                'display_sequence' => $index + 1,
            ];
        })->push([
            'description'     => 'Others',
            'display_sequence' => 3,
        ])->all();
    }

    private function insertContentWritingQuestion()
    {
        return [
            'id' => null,
            'description' => 'What are you using for Content Writing?',
            'type' => 'multiple_response',
            'form_label' => 'content_writing',
            'page' => 4,
            'display_sequence' => 2,
            'required' => 0,
            'question_choices' => [
                [
                    'description' => "Adobe RoboHelp",
                    'display_sequence' => 1,
                ],
                [
                    'description' => 'Authoring Tools',
                    'display_sequence' => 2,
                ],
                [
                    'description' => 'MadCap Flare',
                    'display_sequence' => 3,
                ],
                [
                    'description' => 'Technical Writing',
                    'display_sequence' => 4,
                ],
                [
                    'description' => 'Copy Writing and Social Media',
                    'display_sequence' => 5,
                ],
            ],
        ];
    }
}
