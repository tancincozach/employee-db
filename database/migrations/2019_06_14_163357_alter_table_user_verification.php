<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserVerification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_verifications', function (Blueprint $table) {
            $table->enum('type', ['forgot_password', 'email_verification', 'access_verification'])->default('email_verification')->after('verification_token');
            $table->integer('client_id')->unsigned()->nullable()->after('type');

            $table->foreign('client_id')
                ->references('id')
                ->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('user_verifications', function (Blueprint $table) {
            $table->dropForeign(['client_id']);
            $table->dropColumn('type');
            $table->dropColumn('client_id');
        });
    }
}
