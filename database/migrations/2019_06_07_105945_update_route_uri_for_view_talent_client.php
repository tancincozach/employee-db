<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class UpdateRouteUriForViewTalentClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::updateOrCreate(
            [
                'route_name' => 'Go To Talent Pool Suggestions',
            ],
            [
                'route_uri' => 'team-builder-client',
            ]
        );
    }
}
