<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCategorySkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $additionalData = [
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ];

        // TODO: add microsoft skill ids here
        $microsoftSkills = \App\Models\Skill::whereIn('id', [3])->get();

        // TODO: add opensource skill ids here
        $openSourceSkills = \App\Models\Skill::whereIn('id', [1])->get();

        foreach ($microsoftSkills as $mSkill) {
            $mSkill->categories()->attach(5, $additionalData); // microsoft category id
        }

        foreach ($openSourceSkills as $oSkill) {
            $oSkill->categories()->attach(6, $additionalData); // open source category id
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
