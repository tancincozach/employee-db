<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class PopulateRouteWizardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        $resourcesArr = [
            [
                'group_id' => 0,
                'icon' => 'ks-icon-user',
                'route_name' => 'Manage Employees',
                'sequence_order' => 1,
                'route_uri' => '',
                'goto_group_id' => 0
            ], [
                'group_id' => 0,
                'icon' => 'ks-icon-user',
                'route_name' => 'Manage Clients',
                'sequence_order' => 2,
                'route_uri' => '',
                'goto_group_id' => 2
            ], [
                'group_id' => 0,
                'icon' => 'la la-dashboard',
                'route_name' => 'View Dashboards',
                'sequence_order' => 3,
                'route_uri' => '',
                'goto_group_id' => 0
            ], [
                'group_id' => 0,
                'icon' => 'ks-icon-user',
                'route_name' => 'Manage Applicants',
                'sequence_order' => 4,
                'route_uri' => '',
                'goto_group_id' => 0
            ], [
                'group_id' => 2,
                'icon' => 'ks-icon-combo-chart',
                'route_name' => 'Review Recently Sign-Up Clients',
                'sequence_order' => 1,
                'route_uri' => '',
                'goto_group_id' => 0
            ], [
                'group_id' => 2,
                'icon' => 'la la-pie-chart',
                'route_name' => 'Manage Client On-Board Checklist',
                'sequence_order' => 2,
                'route_uri' => '',
                'goto_group_id' => 0
            ], [
                'group_id' => 2,
                'icon' => 'la la-file-text',
                'route_name' => 'View Client Listing',
                'sequence_order' => 3,
                'route_uri' => '',
                'goto_group_id' => 0
            ], [
                'group_id' => 2,
                'icon' => 'la la-dashboard',
                'route_name' => 'Go to Client Smart Team Builder',
                'sequence_order' => 4,
                'route_uri' => '',
                'goto_group_id' => 0
            ], [
                'group_id' => 2,
                'icon' => 'ks-icon-user',
                'route_name' => 'View Client Profile',
                'sequence_order' => 5,
                'route_uri' => '',
                'goto_group_id' => 0
            ], [
                'group_id' => 2,
                'icon' => 'la la-user-secret',
                'route_name' => 'Send Client Profile Access',
                'sequence_order' => 6,
                'route_uri' => '',
                'goto_group_id' => 0
            ],

        ];

        foreach ($resourcesArr as $resource) {
            RouteWizard::updateOrCreate(
                [
                    'route_name' => $resource['route_name'],
                ],
                [
                    'group_id' => $resource['group_id'],
                    'icon' => $resource['icon'],
                    'sequence_order' => $resource['sequence_order'],
                    'route_uri' => $resource['route_uri'],
                    'goto_group_id' => $resource['goto_group_id']
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
