<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIndexesToTimesheetMappingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_timesheet_jobcodes', function (Blueprint $table) {
            $table->unique('timesheet_jobcode_id');
        });

        Schema::table('employee_timesheet_users', function (Blueprint $table) {
            $table->unique('timesheet_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
