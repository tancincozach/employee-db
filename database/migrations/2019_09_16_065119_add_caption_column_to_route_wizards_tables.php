<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCaptionColumnToRouteWizardsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('route_wizards', function (Blueprint $table) {
            $table->text('caption')->after('route_uri');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('route_wizards', 'caption')) {
            Schema::table('route_wizards', function (Blueprint $table) {
                $table->dropColumn('caption');
            });
        }
    }
}
