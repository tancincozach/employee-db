<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class ChangeRedirectionForViewClientReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::where('route_name', 'View Client Reports')->update(['route_uri' => 'worklogs/report']);
        //RouteWizard::where('route_name', 'View Reports')->update(['route_uri' => 'worklogs-report']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
