<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ACL\Resource;

class InsertNewResourceForTeamBuilder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Resource::updateOrCreate(
            [
                'name'         => 'teambuilder_api',
            ],
            [
                'display_name' => 'Client Teambuilder Bucket',
                'description'  => 'Client Teambuilder Bucket'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
