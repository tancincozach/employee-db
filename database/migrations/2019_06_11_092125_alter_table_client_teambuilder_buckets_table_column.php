<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableClientTeambuilderBucketsTableColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_teambuilder_buckets', function (Blueprint $table) {
            $table->dropForeign(['client_project_id']);
            $table->dropColumn('client_project_id');
        });

        Schema::table('client_teambuilder_buckets', function (Blueprint $table) {
            $table->unsignedInteger('client_project_id')->nullable()->after('client_id');
            $table->foreign('client_project_id')->references('id')->on('client_projects');
        });
    }
}
