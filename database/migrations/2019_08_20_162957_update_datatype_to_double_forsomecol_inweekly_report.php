<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDatatypeToDoubleForsomecolInweeklyReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     /* There is a bug on laravel migration for changing datatype to `DOUBLE` for a quick fix we use DB raw */
     DB::statement('ALTER TABLE weekly_reports MODIFY COLUMN working_hrs DOUBLE NOT NULL');
     DB::statement('ALTER TABLE weekly_reports MODIFY COLUMN break_hrs DOUBLE NOT NULL');
     DB::statement('ALTER TABLE weekly_reports MODIFY COLUMN night_differential_hrs DOUBLE NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('weekly_reports', function (Blueprint $table) {
            $table->decimal('working_hrs', 8, 2)->nullable(false)->change();
        });
        Schema::table('weekly_reports', function (Blueprint $table) {
            $table->decimal('break_hrs', 8, 2)->nullable(false)->change();
        });
        Schema::table('weekly_reports', function (Blueprint $table) {
            $table->decimal('night_differential_hrs', 8, 2)->nullable(false)->change();
        });
    }
}
