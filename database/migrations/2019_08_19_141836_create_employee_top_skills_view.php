<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTopSkillsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW employee_top_skills AS 
SELECT es.employee_id, es.skill_id, es.proficiency,
(SELECT COUNT(*)+1 FROM employee_skills _es
WHERE _es.employee_id = es.employee_id AND (_es.proficiency > es.proficiency OR (_es.proficiency = es.proficiency AND _es.id < es.id)) AND ISNULL(_es.deleted_at)) skill_rank
FROM employee_skills es
WHERE ISNULL(es.deleted_at) HAVING skill_rank <= 3
ORDER BY es.employee_id ASC, es.proficiency DESC");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW employee_top_skills");
    }
}
