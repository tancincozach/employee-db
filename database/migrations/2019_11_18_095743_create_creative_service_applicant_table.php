<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCreativeServiceApplicantTable
 */
class CreateCreativeServiceApplicantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creative_service_applicant', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('creative_service_id');
            $table->foreign('creative_service_id')->references('id')->on('creative_service');
            $table->text('full_name');
            $table->text('email');
            $table->boolean('primary');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creative_service_applicant');
    }
}
