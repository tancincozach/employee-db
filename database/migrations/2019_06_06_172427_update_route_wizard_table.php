<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class UpdateRouteWizardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $arr = [
            [
                'id' => 1,
                'route_uri' => 'employees',
                'icon' => 'ks-icon-user color-primary',
            ], [
                'id' => 2,
                'route_uri' => 'action-items',
                'icon' => 'la la-user-secret color-info',
            ], [
                'id' => 3,
                'route_uri' => 'dashboard',
                'icon' => 'la la-dashboard color-success',
            ], [
                'id' => 4,
                'route_uri' => 'applicants',
                'icon' => 'ks-icon-user color-warning',
            ], [
                'id' => 5,
                'route_uri' => 'review-client-prospects',
                'icon' => 'ks-icon-combo-chart color-primary',
            ], [
                'id' => 6,
                'route_uri' => '',
                'icon' => 'la la-pie-chart color-info',
            ], [
                'id' => 7,
                'route_uri' => '',
                'icon' => 'la la-file-text color-success',
            ], [
                'id' => 8,
                'route_uri' => '',
                'icon' => 'la la-dashboard color-warning',
            ], [
                'id' => 9,
                'route_uri' => '',
                'icon' => 'ks-icon-user color-danger',
            ], [
                'id' => 10,
                'route_uri' => '',
                'icon' => 'la la-user-secret color-secondary',
            ]
        ];

        foreach ($arr as $resource) {
            RouteWizard::updateOrCreate(
                [
                    'id' => $resource['id'],
                ],
                [
                    'icon' => $resource['icon'],
                    'route_uri' => $resource['route_uri'],
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
