<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\AllQuestionChoice as QuestionChoice;

class UpdateAllQuestionChoiceOthers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        QuestionChoice::withoutTimestamps()
            ->from('all_question_choices as aqc')
            ->join('all_questions as aq', 'aqc.all_question_id', '=', 'aq.id')
            ->where('aqc.description', 'like', 'Other%')
            ->where('aq.all_question_category_id', 2)
            ->withTrashed()
            ->update([
                'aqc.is_active' => 0,
                'aqc.updated_at' => date('Y-m-d H:i:s'),
            ]);
    }
}
