<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\AllQuestion as Question;
use App\Models\AllQuestionChoice as QuestionChoice;

class UpdateContentQuestionAndNewTechChoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            // disabling content writing question
            Question::find(35)->update(['is_active' => 0]);

            // tech question id based in prod data
            $techQuestionId = 25;

            // remove existing choices
            QuestionChoice::where('all_question_id', $techQuestionId)->update(['is_active' => 0]);

            // adding new choices for tech question
            $newChoices = $this->setNewChoices();
            foreach ($newChoices as $key => $choice) {
                $questionChoice                     = new QuestionChoice();
                $questionChoice->all_question_id    = $techQuestionId;// tech question id
                $questionChoice->description        = $choice;
                $questionChoice->display_sequence   = $key + 1;
                $questionChoice->save();
            }
        } catch (Throwable $e) {
            echo 'Unable to properly migrate' . __FILE__;
        }
    }

    private function setNewChoices()
    {
        return [
            '.NET',
            'PHP',
            'Ruby on Rails',
            'Android',
            'iOS',
            'React',
            'Javascript',
            'Angular',
            'Java',
            'Python',
            'NodeJS',
            'C++',
            'C#',
        ];
    }
}
