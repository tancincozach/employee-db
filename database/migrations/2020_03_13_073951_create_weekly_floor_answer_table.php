<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeklyFloorAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weekly_floor_answer', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('weekly_floor_report_id');
            $table->foreign('weekly_floor_report_id')->references('id')->on('weekly_floor_report');
            $table->integer('weekly_floor_question_id')->unsigned();
            $table->foreign('weekly_floor_question_id')->references('id')->on('weekly_floor_question');
            $table->text('weekly_floor_answer')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weekly_floor_answer');
    }
}
