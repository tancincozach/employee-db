<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\PerformEvalQuestion;

class UpdatePerformEvalQuestionAllRequiredFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        PerformEvalQuestion::where('deleted_at', null)->update(['required' => 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        PerformEvalQuestion::where('deleted_at', null)->update(['required' => 1]);
    }
}
