<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\AllQuestion;
use App\Models\AllQuestionChoice;

class InsertNewQuestionInAllQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resourcesArr = [
            [
                'all_question_category_id' => 2,
                'description'              => 'Is the primary contact also the signatory for legal document?',
                'type'                     => 'single_response',
                'form_label'               => 'legal_document',
                'page'                     => 1,
                'display_sequence'         => 6,
                'required'                 => 1,
                'group_choices'            => 0,
                'is_active'                => 1,
                //added this since the all_question_choices has a relation with the all_question table
                'all_question_choices' => [
                    [
                        'all_question_id'  => null,
                        'description'      => 'Yes',
                        'display_sequence' => 1,
                        'group'            => null,
                        'is_active'        => 1,
                    ],
                    [
                        'all_question_id'  => null,
                        'description'      => 'No',
                        'display_sequence' => 2,
                        'group'            => null,
                        'is_active'        => 1,
                    ],
                ],
            ], [
                'all_question_category_id' => 2,
                'description'              => 'What is the signatory contact name?',
                'type'                     => 'short_text',
                'form_label'               => 'signatory_contact_name',
                'page'                     => 1,
                'display_sequence'         => 7,
                'required'                 => 1,
                'group_choices'            => 0,
                'is_active'                => 1,
            ], [
                'all_question_category_id' => 2,
                'description'              => 'What is the signatory email address?',
                'type'                     => 'short_text',
                'form_label'               => 'signatory_email_add',
                'page'                     => 1,
                'display_sequence'         => 8,
                'required'                 => 1,
                'group_choices'            => 0,
                'is_active'                => 1,
            ],
        ];

        foreach ($resourcesArr as $resource) {
            //put it $allQuestion since we will fetch here the auto increment id
            $allQuestion = AllQuestion::updateOrCreate(
                [
                    'form_label' => $resource['form_label'],
                ],
                [
                    'all_question_category_id' => $resource['all_question_category_id'],
                    'description'              => $resource['description'],
                    'type'                     => $resource['type'],
                    'page'                     => $resource['page'],
                    'display_sequence'         => $resource['display_sequence'],
                    'required'                 => $resource['required'],
                    'group_choices'            => $resource['group_choices'],
                    'is_active'                => $resource['is_active'],
                ]
            );

            if (!empty($allQuestion)) { //if has data always delete
                AllQuestionChoice::where('all_question_id', $allQuestion->id)->delete();

                // check if this question has choices
                if (empty($resource['all_question_choices'])) {
                    continue;
                }

                // if this is array and all_question_choices has value then go
                if (is_array($resource['all_question_choices']) && count($resource['all_question_choices'])) {
                    foreach ($resource['all_question_choices'] as $choice) {
                        AllQuestionChoice::create(
                            array_merge($choice, ['all_question_id' => $allQuestion->id])
                        );
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        AllQuestionChoice::where('all_question_id', AllQuestion::where('form_label', 'legal_document')->first()->id ?? 0)->forceDelete();
        AllQuestion::where('form_label', 'legal_document')->forceDelete();
        AllQuestion::where('form_label', 'signatory_contact_name')->forceDelete();
        AllQuestion::where('form_label', 'signatory_email_add')->forceDelete();
    }
}
