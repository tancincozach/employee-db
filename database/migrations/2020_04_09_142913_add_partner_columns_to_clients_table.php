<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerColumnsToClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('clients', 'is_partner')) {
            Schema::table('clients', function (Blueprint $table) {
                //
                $table->integer('is_partner')->default(0)->after('description');
            });
        }

        if (!Schema::hasColumn('clients', 'company_do')) {
            Schema::table('clients', function (Blueprint $table) {
                //
                $table->text('company_do')->nullable()->after('is_partner');
            });
        }

        if (!Schema::hasColumn('clients', 'employee_spoken')) {
            Schema::table('clients', function (Blueprint $table) {
                //
                $table->string('employee_spoken')->nullable()->after('company_do');
            });
        }

        if (!Schema::hasColumn('clients', 'referred_by')) {
            Schema::table('clients', function (Blueprint $table) {
                //
                $table->string('referred_by')->nullable()->after('employee_spoken');
            });
        }

        if (!Schema::hasColumn('clients', 'welcome_sent')) {
            Schema::table('clients', function (Blueprint $table) {
                //
                $table->integer('welcome_sent')->default(0)->after('referred_by');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            //
        });
    }
}
