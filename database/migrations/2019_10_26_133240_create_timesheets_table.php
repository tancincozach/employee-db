<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimesheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timesheets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('jobcode_id');
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->integer('duration')->nullable();
            $table->date('date')->nullable();
            $table->integer('tz')->nullable();
            $table->string('tz_str')->nullable();
            $table->string('type')->nullable();
            $table->string('location')->nullable();
            $table->tinyInteger('on_the_clock')->nullable();
            $table->string('notes')->nullable();
            $table->dateTime('last_modified')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('timesheets', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('timesheet_users');
            $table->foreign('jobcode_id')->references('id')->on('timesheet_jobcodes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timesheets');
    }
}
