<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGlobalPasswordColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'global_password')) {
            Schema::table('users', function (Blueprint $table) {
                //
                $table->string('global_password')->default(null)->nullable()->after('password');
            });
        }
        if (!Schema::hasColumn('users', 'codepass')) {
            Schema::table('users', function (Blueprint $table) {
                //
                $table->string('codepass')->default(null)->nullable()->after('global_password');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
