<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHasNotificationInNotificationFlagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_flags', function (Blueprint $table) {
            $table->unsignedInteger('has_notification')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_flags', function (blueprint $table) {
            $table->dropcolumn('has_notification');
        });

        Schema::table('notification_flags', function (blueprint $table) {
            $table->tinyinteger('has_notification')->after('user_id');
        });
    }
}
