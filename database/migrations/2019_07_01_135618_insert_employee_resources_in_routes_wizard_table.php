<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class InsertEmployeeResourcesInRoutesWizardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resourcesArr = [
            [
                'group_id' => 4,
                'icon' => 'la la-dashboard color-success',
                'route_name' => 'Daily Report',
                'sequence_order' => 1,
                'route_uri' => 'daily-report',
                'goto_group_id' => 0
            ], [
                'group_id' => 4,
                'icon' => 'la la-user color-success',
                'route_name' => 'Work From Home',
                'sequence_order' => 2,
                'route_uri' => 'work-from-home-list',
                'goto_group_id' => 0
            ]
        ];
        foreach ($resourcesArr as $resource) {
            RouteWizard::updateOrCreate(
                [
                    'route_name' => $resource['route_name'],
                ],
                [
                    'group_id' => $resource['group_id'],
                    'icon' => $resource['icon'],
                    'sequence_order' => $resource['sequence_order'],
                    'route_uri' => $resource['route_uri'],
                    'goto_group_id' => $resource['goto_group_id']
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
