<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class RemoveViewReportsInRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::where('route_name', 'View Reports')->delete();
    }

    public function down() 
    {
        RouteWizard::where('route_name', 'View Reports')->withTrashed()->restore();
    }
}
