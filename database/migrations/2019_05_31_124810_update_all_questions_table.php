<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\AllQuestion;

class UpdateAllQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // needs seeder here - :(
        class_exists('InitialAllQuestionsSeeder') and Artisan::call('db:seed', ['--class' => InitialAllQuestionsSeeder::class]);
        class_exists('UpdateAllQuestionsTableSeeder') and Artisan::call('db:seed', ['--class' => UpdateAllQuestionsTableSeeder::class]);
        class_exists('AllQuestionChoiceGroupSeeder') and Artisan::call('db:seed', ['--class' => AllQuestionChoiceGroupSeeder::class]);

        try {
            AllQuestion::find(1)->update(['page'  => 1, 'display_sequence' => 1]);
            AllQuestion::find(14)->update(['page' => 1, 'display_sequence' => 2]);
            AllQuestion::find(15)->update(['page' => 1, 'display_sequence' => 3]);
            AllQuestion::find(16)->update(['page' => 1, 'display_sequence' => 4]);
            AllQuestion::find(17)->update(['page' => 1, 'display_sequence' => 5]);

            AllQuestion::find(2)->update(['page' => 2, 'display_sequence' => 1]);
            AllQuestion::find(4)->update(['page' => 2, 'display_sequence' => 2]);
            AllQuestion::find(5)->update(['page' => 2, 'display_sequence' => 3]);

            AllQuestion::find(6)->update(['page' => 3, 'display_sequence' => 1]);
            AllQuestion::find(7)->update(['page' => 3, 'display_sequence' => 2]);

            AllQuestion::find(8)->update(['page' => 4, 'display_sequence' => 1]);

            AllQuestion::find(9)->update(['page'  => 5, 'display_sequence' => 1]);
            AllQuestion::find(10)->update(['page' => 5, 'display_sequence' => 2]);
            AllQuestion::find(11)->update(['page' => 5, 'display_sequence' => 3]);
            AllQuestion::find(12)->update(['page' => 5, 'display_sequence' => 4]);
            AllQuestion::find(13)->update(['page' => 5, 'display_sequence' => 5]);
        } catch (Throwable $e) {
            echo 'Unable to properly migrate' . __FILE__;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
