<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimesheetUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timesheet_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->tinyInteger('active')->nullable();
            $table->integer('employee_number')->nullable();
            $table->string('username')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile_number')->nullable();
            $table->date('hire_date')->nullable();
            $table->date('term_date')->nullable();
            $table->dateTime('last_modified')->nullable();
            $table->dateTime('last_active')->nullable();
            $table->dateTime('created')->nullable();
            $table->string('company_name')->nullable();
            $table->string('profile_image_url')->nullable();
            $table->text('pto_balances')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timesheet_users');
    }
}
