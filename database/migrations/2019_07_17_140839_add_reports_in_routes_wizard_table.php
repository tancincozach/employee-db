<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class AddReportsInRoutesWizardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Admin Side
        $resources = [
            [
                'group_id' => '5',
                'icon' => 'la la-users color-info',
                'route_name' => 'View Employees',
                'route_uri' => 'employees',
                'sequence_order' => '1',
                'goto_group_id' => '0',
            ],
            [
                'group_id' => '5',
                'icon' => 'la la-calendar color-warning',
                'route_name' => 'Employee Daily Tracker',
                'route_uri' => 'worklogs/daily',
                'sequence_order' => '2',
                'goto_group_id' => '0',
            ],
            [
                'group_id' => '6',
                'icon' => 'la la-bar-chart color-danger',
                'route_name' => 'View Working Hour Report',
                'route_uri' => 'worklogs/report',
                'sequence_order' => '1',
                'goto_group_id' => '0',
            ],
            [
                'group_id' => '6',
                'icon' => 'la la-calendar color-primary',
                'route_name' => 'Employee Daily Tracker',
                'route_uri' => 'worklogs/daily',
                'sequence_order' => '2',
                'goto_group_id' => '0',
            ],
        ];
        foreach ($resources as $resource) {
            RouteWizard::Create(
                [
                    'route_name' => $resource['route_name'],
                    'group_id' => $resource['group_id'],
                    'icon' => $resource['icon'],
                    'route_uri' => $resource['route_uri'],
                    'sequence_order' => $resource['sequence_order'],
                    'goto_group_id' => $resource['goto_group_id'],
                ]
            );
        }

        RouteWizard::where('route_name', 'Manage Employees')->update(['route_uri' => 'manage-employees', 'goto_group_id' => 5]);
        RouteWizard::where('route_name', 'View Reports')->update(['route_uri' => 'manage-reports', 'goto_group_id' => 6]);
    }
}
