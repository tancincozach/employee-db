<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSuggestedAdminColumnToClientTeambuilderBucketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_teambuilder_buckets', function (Blueprint $table) {
            $table->boolean('is_suggested_admin')->after('status')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_teambuilder_buckets', function (Blueprint $table) {});
    }
}
