<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClientTeambuilderBucketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_teambuilder_buckets', function (Blueprint $table) {
            $table->enum('status', ['pooled', 'selected', 'declined', 'assigned'])
                ->after('job_position_id')
                ->default('pooled');

            $table->dropColumn('client_selected');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
