<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class UpdateRouteUriForSearchClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $routes = [
            'Manage Client On-Board Checklist',
            'Go to Client Smart Team Builder',
            'View Client Profile',
            'Send Client Profile Access',

        ];
        RouteWizard::whereIn('route_name', $routes)->update(['route_uri' => 'search/clients']);
    }
}
