<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class AlterFeedbackFormListing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      RouteWizard::where('route_uri', 'feedback-entry')
                 ->update(['route_uri' => 'employee-feedback-list']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      RouteWizard::where('route_uri', 'employee-feedback-list')
                 ->update(['route_uri' => 'feedback-entry']);
    }
}
