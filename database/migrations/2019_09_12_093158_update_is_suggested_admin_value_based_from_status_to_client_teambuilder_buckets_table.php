<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ClientTeambuilderBucket;

class UpdateIsSuggestedAdminValueBasedFromStatusToClientTeambuilderBucketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ClientTeambuilderBucket::where('status', 'selected')->update(['is_suggested_admin' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_teambuilder_buckets', function (Blueprint $table) {});
    }
}
