<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class AddRedirectPageToRouteUriClientWizardRoute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $routes = [
            'client-onboard-checklist'  => 'Manage Client On-Board Checklist',
            'smart-team-builder'        => 'Go to Client Smart Team Builder',
            'client'                    => 'View Client Profile',
            'client-login-access'       => 'Send Client Profile Access',

        ];
        foreach ($routes as $key => $value) {
            RouteWizard::where('route_name', $value)->update(['route_uri' => 'search/clients?redirectTo=' . $key]);
        }
    }
}
