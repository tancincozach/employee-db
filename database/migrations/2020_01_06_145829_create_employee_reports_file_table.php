<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeReportsFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_reports_file', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('employee_reports_id')->unsigned();
            $table->foreign('employee_reports_id')->references('id')->on('employee_reports');
            $table->string('file');
            $table->string('original_file');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_reports_file');
    }
}
