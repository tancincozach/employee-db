<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ACL\Resource;

class AddTimesheetJobcodeAssignmentsResourcePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Resource::updateOrCreate(
            [
                'name' => 'timesheet_jobcode_assignments',
            ],
            [
                'display_name' => 'Timesheet Jobcode Assignments',
                'description'  => 'Timesheet Jobcode Assignments',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Resource::where('name', 'timesheet_jobcode_assignments')->forceDelete();
    }
}
