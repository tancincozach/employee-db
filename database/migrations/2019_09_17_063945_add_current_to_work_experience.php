<?php

use App\Models\WorkExperience;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrentToWorkExperience extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_experiences', function (Blueprint $table) {
            $table->boolean('current')->default(false)->after('reason_for_leaving');
        });

        DB::table('work_experiences')
            ->where('company_name', 'like', '%Full Scale%')
            ->orWhere('company_name', 'like', '%FullScale%')
            ->update(['current' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_experiences', function (Blueprint $table) {
            $table->dropColumn('current');
        });

        DB::table('work_experiences')
            ->where('company_name', 'like', '%Full Scale%')
            ->orWhere('company_name', 'like', '%FullScale%')
            ->update(['current' => false]);
    }
}
