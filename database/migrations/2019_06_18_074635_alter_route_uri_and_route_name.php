<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\RouteWizard;
use Illuminate\Database\Migrations\Migration;

class AlterRouteUriAndRouteName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::where('route_name', 'Go to Talent Pool Suggestions')->update(['route_name' => 'View Resource Suggestions']);
        RouteWizard::where('route_name', 'Search Talents')->update(['route_name' => 'Find Available Resources']);
        RouteWizard::where('route_uri', 'search-talents')->update(['route_uri' => 'available-resources']);

        RouteWizard::where('route_name', 'View Timesheets')->delete();
        RouteWizard::where('route_name', 'Schedule Talent Interview')->delete();
        RouteWizard::where('route_name', 'Manage Projects')->delete();

        RouteWizard::where('route_name', 'View Reports')->update(['sequence_order' => 3]);
    }
}
