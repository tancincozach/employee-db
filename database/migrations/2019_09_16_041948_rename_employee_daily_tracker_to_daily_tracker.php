<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class RenameEmployeeDailyTrackerToDailyTracker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::where('group_id', 5)->where('route_name', 'Employee Daily Tracker')->update(['route_name' => 'Daily Tracker']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RouteWizard::where('group_id', 5)->where('route_name', 'Daily Tracker')->update(['route_name' => 'Employee Daily Tracker']);
    }
}
