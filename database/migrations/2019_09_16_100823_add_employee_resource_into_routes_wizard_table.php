<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class AddEmployeeResourceIntoRoutesWizardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::updateOrCreate(
            [
                'route_name' => 'Employee Resources',
                'group_id' => 0,
                'icon' => 'ks-icon color-success fa fa-info-circle',
                'route_uri' => 'employee-resources',
                'caption' => "This will allow all the employee to view their existing company documents, holidays, events, HMO, and other documents related to their employment.",
                'sequence_order' => 5,
                'goto_group_id' => 0,
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RouteWizard::where('route_name', 'Employee Resources')
        ->where('group_id', 0)
        ->delete();
    }
}
