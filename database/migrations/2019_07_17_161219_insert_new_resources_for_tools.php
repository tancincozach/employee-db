<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ACL\Resource;
use App\Models\ACL\ResourceRolePermission;
use App\Models\ACL\ResourceUserRolePermission;

class InsertNewResourcesForTools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resourcesArr = [
            [
                'name' => 'tools',
                'display_name' => 'Tools',
                'description' => 'Tools Sidebar',
            ],
            [
                'name' => 'route_wizards_page',
                'display_name' => 'Tools > Route Wizards Tiles Page',
                'description' => 'Route Wizards Tile Page',
            ],
        ];

        foreach ($resourcesArr as $resource) {
            Resource::updateOrCreate(
                [
                    'name' => $resource['name'],
                ],
                [
                    'display_name' => $resource['display_name'],
                    'description' => $resource['description'],
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $resourcesArr = [
            [
                'name' => 'tools',
                'display_name' => 'Tools',
                'description' => 'Tools Sidebar',
            ],
            [
                'name' => 'route_wizards_page',
                'display_name' => 'Tools > Route Wizards Tiles Page',
                'description' => 'Route Wizards Tile Page',
            ],
        ];

        foreach ($resourcesArr as $resource) {
            $resource = Resource::where('name', $resource['name'])->where('display_name', $resource['display_name'])->first();
            ResourceRolePermission::where('resource_id', $resource['id'])->forceDelete();
            ResourceUserRolePermission::where('resource_id', $resource['id'])->forceDelete();
            $resource->forceDelete();
        }
    }
}
