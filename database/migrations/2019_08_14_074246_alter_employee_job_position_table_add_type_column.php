<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Seeder;
use App\Models\JobPosition;

class AlterEmployeeJobPositionTableAddTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_positions', function(Blueprint $table){
            $table->enum('type', ['developer', 'tester', 'manager', 'creative', 'leadership', 'administrative'])
                ->after('job_description');
        });

        Artisan::call('db:seed', [
            '--class' => JobPositionsTypeTableSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_positions', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
