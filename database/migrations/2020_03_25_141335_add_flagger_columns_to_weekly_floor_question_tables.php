<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlaggerColumnsToWeeklyFloorQuestionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasColumn('weekly_floor_question', 'is_red')) {
            Schema::table('weekly_floor_question', function (Blueprint $table) {
                //
                $table->tinyInteger('is_red')->default(0)->after('sequence_no');
            });
        }

        if (!Schema::hasColumn('weekly_floor_question', 'red_answer')) {
            Schema::table('weekly_floor_question', function (Blueprint $table) {
                //
                $table->string('red_answer')->nullable()->after('is_red');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('weekly_floor_question', function (Blueprint $table) {
            //
        });
    }
}
