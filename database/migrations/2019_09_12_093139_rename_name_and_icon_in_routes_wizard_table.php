<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RouteWizard;

class RenameNameAndIconInRoutesWizardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        RouteWizard::where('group_id', 6)->where('route_name', 'Resource Daily Tracker')->update(['route_name' => 'Daily Tracker']);
        RouteWizard::where('group_id', 6)->where('route_name', 'View Workin Hour Report')->update(['route_name' => 'Weekly Reports']);
        RouteWizard::where('group_id', 6)->where('route_name', 'Employee Hours Summary')->update(['route_name' => 'Weekly Reports']);
        RouteWizard::where('group_id', 6)->where('route_name', 'View Working Hour Report')->update(['route_uri' => 'worklogs/report']);
        RouteWizard::where('group_id', 6)->where('route_name', 'Weekly Reports')->update(['route_uri' => 'worklogs/report']);
        RouteWizard::where('group_id', 6)->where('route_name', 'View Resource Reports')->update(['icon' => 'ks-icon la la-calendar-check-o color-success']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
