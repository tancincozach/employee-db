<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\PerformEvalQuestion;

class UpdatePerformEvalQuestionRemoveYearOrAnnuallyInLabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        PerformEvalQuestion::where('form_id', 'significant_changes')->update(['label' => 'Attach a current position description; if applicable, make note of any significant changes since last performance review.']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        PerformEvalQuestion::where('form_id', 'significant_changes')->update(['label' => 'Attach a current position description; if applicable, make note of any significant changes since last year’s performance review.']);
    }
}
