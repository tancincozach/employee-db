<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreferContactColumnToEmployeeOtherDetailsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_other_details', function (Blueprint $table) {
            //
            $table->string('preferred_contact')->default(null)->nullable()->after('preferred_shift');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_other_details', function (Blueprint $table) {
            //
        });
    }
}
