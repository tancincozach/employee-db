<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHackerRankTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hacker_rank_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('test_id');
            $table->string('unique_id');
            $table->integer('skill_id');
            $table->tinyInteger('has_tag');
            $table->text('name');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hacker_tests');
    }
}
