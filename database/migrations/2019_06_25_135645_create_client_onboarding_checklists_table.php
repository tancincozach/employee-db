<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientOnboardingChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_onboarding_checklists', function (Blueprint $table) {
            $table->tinyInteger('id')->unsigned()->autoIncrement();
            $table->integer('client_id')->unsigned();
            $table->integer('asset_id')->unsigned()->nullable();
            $table->enum('step', ['contract_signed', 'initial_deposit', 'start_guide', 'introductory_call','team_emails_sent','first_week_check_up','first_month_check_up']);
            $table->timestamp('date_occurred');
            $table->string('client_signatory')->nullable();
            $table->string('sent_by')->nullable();
            $table->string('sent_to')->nullable();
            $table->text('attendees')->nullable();
            $table->longText('client_feedback')->nullable();
            $table->mediumText('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();


            // Table indexes
            $table->index('client_id');
            $table->index('asset_id');

            // Table foreign keys
            $table->foreign('client_id')
                ->references('id')->on('clients');
            $table->foreign('asset_id')
                ->references('id')->on('assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_onboarding_checklists');
    }
}
