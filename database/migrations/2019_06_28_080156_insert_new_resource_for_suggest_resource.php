<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ACL\Resource;

class InsertNewResourceForSuggestResource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Resource::updateOrCreate(
            [
                'name' => 'suggest_resource_page'
            ],
            [
                'display_name' => 'Suggest Resource Page',
                'description' => 'Suggest Resource Page',
            ]
        );
    }
}
