<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AlterEmployeeSkillsTable
 */
class AlterEmployeeSkillsCheckedTopSkillColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_skills', function (Blueprint $table) {
            $table->boolean('checked_top_skill')->default(1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_skills', function (Blueprint $table) {
            $table->tinyInteger('checked_top_skill')->default(0)->change();
        });
    }
}
