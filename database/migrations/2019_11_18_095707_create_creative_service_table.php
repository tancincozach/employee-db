<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCreativeServiceTable
 */
class CreateCreativeServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creative_service', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('company');
            $table->text('overview');
            $table->text('project');
            $table->text('purpose');
            $table->timestamp('needed_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creative_service');
    }
}
