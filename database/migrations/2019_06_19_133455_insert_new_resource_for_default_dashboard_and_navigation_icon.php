<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ACL\Resource;

class InsertNewResourceForDefaultDashboardAndNavigationIcon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resourcesArr = [
            [
                'name' => 'navigation_sidebar_action',
                'display_name' => 'Navigation Sidebar Action',
                'description' => 'Navigation Sidebar Action',
            ],
            [
                'name' => 'default_dashboard_page',
                'display_name' => 'Default Dashboard Page',
                'description' => 'Default Dashboard Page',
            ],
        ];

        foreach ($resourcesArr as $resource) {
            Resource::updateOrCreate(
                [
                    'name' => $resource['name'],
                ],
                [
                    'display_name' => $resource['display_name'],
                    'description' => $resource['description'],
                ]
            );
        }
    }
}
