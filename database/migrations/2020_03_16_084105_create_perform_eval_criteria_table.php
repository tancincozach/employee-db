<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformEvalCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perform_eval_criteria', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->unsignedInteger('perform_eval_question_id');
            $table->foreign('perform_eval_question_id')->references('id')->on('perform_eval_question');
            $table->text('label')->nullable()->default(null);
            $table->text('value')->nullable()->default(null);
            $table->string('type')->comment('Choice type: radio, tiptap, date');
            $table->unsignedTinyInteger('sequence_no');
            $table->unsignedTinyInteger('is_need_explain')->default(0)->comment('If choices are in need of a justification.');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perform_eval_criteria');
    }
}
