<?php


namespace App\Helpers;


class LogHelper
{

    public static function getRawRequest()
    {
        $request = '';

        // include request method
        $request .= request()->getMethod() . "\n";
        // request URI
        $request .= '/' . ltrim(request()->decodedPath(), '/') . "\n\n";

        foreach (getallheaders() as $name => $value) {
            $request .= "$name: $value\n";
        }

        $request .= "\n" . json_encode(request()->all());

        return $request;
    }

}