<?php

namespace App\Helpers;

use App\Models\Zone;
use DateTime;
use DateTimeZone;

class TimezoneHelper
{
    private static function timezoneList() {
        static $timezones = null;
    
        if ($timezones === null) {
            $timezones = [];
            $offsets = [];
            $now = new DateTime('now', new DateTimeZone('UTC'));
    
            foreach (DateTimeZone::listIdentifiers() as $timezone) {
                $now->setTimezone(new DateTimeZone($timezone));
                $offsets[] = $offset = $now->getOffset();
                $timezones[$timezone] = '(' . TimezoneHelper::formatGmtOffset($offset) . ') ' . TimezoneHelper::formatTimezoneName($timezone);
            }
    
            array_multisort($offsets, $timezones);
        }
    
        return $timezones;
    }

    public static function getTimezoneCaption($timezone)
    {
        $timezone_list = TimezoneHelper::timezoneList();
        
        if (empty($timezone_list[$timezone])) {
            return $timezone;
        }
        return $timezone_list[$timezone];
    }
    
    private static function formatGmtOffset($offset) {
        $hours = intval($offset / 3600);
        $minutes = abs(intval($offset % 3600 / 60));
        return 'GMT' . ($offset ? sprintf('%+03d:%02d', $hours, $minutes) : ' 00:00');
    }
    
    private static function formatTimezoneName($name) {
        $name = str_replace('/', ', ', $name);
        $name = str_replace('_', ' ', $name);
        $name = str_replace('St ', 'St. ', $name);
        return $name;
    }
}
