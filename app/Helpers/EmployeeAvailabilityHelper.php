<?php

namespace App\Helpers;

use App\Models\Employee;

class EmployeeAvailabilityHelper
{
    const ROCKS_ID = 30;

    protected $id;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function isDeployable()
    {
        $result = \DB::table('employee_job_positions AS ejp')
            ->distinct()->select('jp.type')
            ->leftJoin('job_positions AS jp', 'jp.id', '=', 'ejp.position_id')
            ->where('ejp.employee_id', $this->id)
            ->whereNotIn('jp.type', ['administrative', 'leadership'])
            ->whereNull('ejp.deleted_at')
            ->get();

        return !$result->isEmpty();
    }

    public function hasProjectsHandled()
    {
        $hasProject = \DB::table('employee_client_projects')
            ->where('employee_id', $this->id)
            ->get();

        return !$hasProject->isEmpty();
    }

    public function hasRocksProject()
    {
        $has = \DB::table('employee_client_projects AS ecp')
            ->select('ecp.id')
            ->leftJoin('client_projects AS cp', 'cp.id', '=', 'ecp.client_project_id')
            ->where('ecp.employee_id', $this->id)
            ->where('cp.client_id', self::ROCKS_ID)
            ->whereNull('ecp.deleted_at')
            ->get();

        return !$has->isEmpty();
    }

    public function isNotAssigned()
    {
        $assigned = \DB::table('client_teambuilder_buckets')
            ->where('employee_id', $this->id)
            ->where('status', 'assigned')
            ->whereNull('deleted_at')
            ->get();

        return $assigned->isEmpty();
    }

    public function isManager()
    {
        $isManager = \DB::table('employee_job_positions AS ejp')
            ->leftJoin('job_positions AS jp', 'jp.id', '=', 'ejp.position_id')
            ->where('ejp.employee_id', $this->id)
            ->where('jp.type', 'manager')
            ->whereNull('ejp.deleted_at')
            ->get();

        return !$isManager->isEmpty();
    }

    public function getActiveIds()
    {
        return \DB::table('employees AS e')
            ->distinct()->select('e.id')
            ->leftJoin('employee_statuses AS es', 'es.employee_id', '=', 'e.id')
            ->whereNotNull('e.employee_no')
            ->whereIn('es.status_id', Employee::ACTIVE_EMPLOYEE_STATUS)
            ->pluck('id');
    }

    public function getDeployableIds()
    {
        $filtered = $this->getActiveIds()->filter(function ($item) {
            $this->setId($item);

            return $this->isDeployable();
        });

        return array_values($filtered->toArray());
    }

    public function getIds($availability)
    {
        $filtered = $this->getActiveIds()->filter(function ($item) use ($availability) {
            $this->setId($item);

            if ($availability) {
                return $this->isAvailable();
            } else {
                return $this->isDeployable() && !$this->hasRocksProject() && !$this->isManager();
            }
        });

        return array_values($filtered->toArray());
    }

    public function isAvailable()
    {
        return $this->isManager() || $this->hasRocksProject();
    }
}
