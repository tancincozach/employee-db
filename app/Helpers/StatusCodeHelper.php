<?php


namespace App\Helpers;

class StatusCodeHelper
{

    /**
     * Gets the error code message
     *
     * @param      $statusCode
     * @param bool $header
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public static function statusCode($statusCode, $header = false)
    {
        $codeGroup = true === $header ? 'http' : 'custom';
        return config("status_codes.$codeGroup.$statusCode", 'Unknown error');
    }
}
