<?php


namespace App\Helpers;

use App\Models\Address;
use App\Models\Country;
use App\Models\Employee;
use App\Models\User;
use Laravel\Passport\Token;
use Lcobucci\JWT\Parser;

class AuthHelper
{
    /**
     * Retrieve user token from Authorization header or the provided authorization hash string
     *
     * @param string $authorizationHash Optional. If provided, will be parsed
     * @param boolean $validOnly Default value is TRUE which will only compare token which is currently active
     * @param boolean $debug Optional. Switch between debugging mode
     *
     * @return bool|\Illuminate\Database\Query\Builder|mixed
     */
    public static function getTokenFromAuthorizationHeader($authorizationHash = '', $validOnly = true, $debug = false)
    {
        $token = !empty($authorizationHash) ? trim($authorizationHash) : request()->header('Authorization');

        if (empty($token)) {
            // return empty string by default
            return '';
        }
        
        try {
            $token = str_replace('Bearer ', '', trim($token));
            $id = (new Parser())->parse($token)->getHeader('jti');

            // only return valid token: not expired or revoked
            if (true === $validOnly) {
                return \DB::table((new Token)->getTable())->where('id', $id)->where('expires_at', '>', date('Y-m-d H:i:s'))->first();
            }

            // return the whole query results
            return DB::table((new Token)->getTable())->find($id);
        } catch (\Throwable $e) {
            \Log::error('Error on getting user token from authorization string. ' . $e->getMessage());

            // for whatever reason the code above halted, return empty string
            return true === $debug ? $e->getMessage() : '';
        }
    }

    /**
     * Retrieves user information with the given token
     *
     * @param      $token
     * @param null $userId
     *
     * @return array|bool|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public static function getTokenUserInfo($token, $userId = null)
    {
        // Make sure there are no trailing spaces
        !empty($token) and $token = trim($token);
        // check if we are not eating nothing
        // we'll ensure we are processing a valid token length
        if (empty($token) || strlen($token) < 80) {
            return false;
        }

        // else if token is of questionable length
        if (strlen($token) > 100) {
            // we'll try to parse it and consider the token as raw Authorization header string
            if (!empty($tokenInfo = self::getTokenFromAuthorizationHeader($token))) {
                $token = $tokenInfo->id;
            }
        }

        // now check whose token is it
        // no app model is available since this is Auth core
        $user = \DB::table((new Token)->getTable() . ' as t')
            ->leftJoin((new User())->getTable() . ' as u', 'u.id', '=', 't.user_id')
            ->leftJoin((new Employee())->getTable() . ' as e', 'e.user_id', '=', 'u.id')
            ->leftJoin((new Address())->getTable() . ' as a', function ($join) {
                $join->on('a.employee_id', '=', 'e.id')
                    ->where('a.is_permanent', '=', true);
            })
            ->leftJoin((new Country())->getTable() . ' as c', 'c.id', '=', 'a.country_id')
            ->where('t.id', $token);

        if (!empty($userId) && is_numeric($userId)) {
            $user->where('user_id', $userId);
        }

        // specify which database fields to retrieve
        $user->selectRaw("e.id, u.user_name, u.email, e.employee_no, CONCAT_WS(' ', CONCAT(e.last_name, ','), e.first_name, e.middle_name) name, e.first_name, e.last_name, e.middle_name, e.ext, e.gender, e.civil_status, e.contact_no, e.date_of_birth, a.line_1 address1, a.line_2 address2, a.city, a.state_province state, a.postal_zip_code post_code, c.code country_code, c.printable_name country, IF(e.id > '', 'Asia/Manila', '') timezone, '' business_name, t.id token, t.expires_at token_expiry");

        // get user info now
        return $user->first() ?? [];
    }
}
