<?php

namespace App\Helpers;

class RocksHelper
{
    /**
     * Cleans up name with unwanted characters.
     * Great for cleaning up full name in this format:
     * LastName, FirstName MiddleName NameExtension
     *
     * @param $string
     *
     * @return string
     */
    public static function cleanName($string)
    {
        return trim(str_replace(' ,', ',', static::trimExtraSpaces($string)), " \t\n\r\0\x0B,");
    }

    /**
     * Removes extra spaces from string
     *
     * @param $string
     *
     * @return string|string[]|null
     */
    public static function trimExtraSpaces($string)
    {
        return preg_replace('/\s+/', ' ', $string);
    }

    /**
     * Returns valid date from date string
     *
     * @param        $string
     * @param string $dateformat
     *
     * @return false|string
     */
    public static function getValidDate($string, string $dateformat = 'Y-m-d')
    {
        return false === ($date = strtotime($string)) ? '' : date($dateformat, $date);
    }

    /**
     * Removes not numeric characters from a string
     *
     * @param $string
     *
     * @return string|string[]|null
     */
    public static function getNumericValue($string)
    {
        return preg_replace('~\D~', '', $string);
    }

    /**
     * Validates timezone string
     *
     * @param $string
     *
     * @return bool
     */
    public static function isValidTimezone($string)
    {
        if (empty($string)) {
            return false;
        }

        return in_array($string, \DateTimeZone::listIdentifiers()) ? true : false;
    }

    /**
     * Get timezone based on country code
     *
     * @param $countryCode
     *
     * @return string
     */
    public static function getTimezoneByCountryCode($countryCode)
    {
        if (empty($countryCode)) {
            return '';
        }

        // get timezone info of the provided country code
        $timezoneInfo = \DateTimeZone::listIdentifiers(\DateTimeZone::PER_COUNTRY, strtoupper($countryCode));

        // check if there are results
        if (empty($timezoneInfo)) {
            return '';
        }

        // always return the first result
        return is_array($timezoneInfo) ? $timezoneInfo[0] : '';
    }

    /*
     * Get the abbreviated timezone
     *
     * @param string $date
     * @param string $timezone
     *
     * @return string
     */
    public static function getAbbreviatedTimezone($date, $timezone = '')
    {
        if (empty($timezone)) {
            return '';
        }

        $datetime = static::getValidDate($date);
        $datetime = new \DateTime($datetime);
        $datetime->setTimeZone(new \DateTimeZone($timezone));

        return $datetime->format('T');
    }
}
