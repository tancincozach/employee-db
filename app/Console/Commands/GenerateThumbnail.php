<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Services\Asset\ResizeImageService;

class GenerateThumbnail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image:thumbnail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Thumbnail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         // create all necessary thumbnails
         $imageDirectories = ['profile-photo'];

         // loop all directories to be processed - Derek
         foreach ($imageDirectories as $imageDirectory) {
            foreach (Storage::files($imageDirectory) as $file) {
                // check if file is a valid image
                if (in_array(Storage::mimeType($file), ['image/jpeg', 'image/png'])) {
                    // process thumbnailing
                    ResizeImageService::run([
                        
                        'image' => public_path('storage/' . $file), // path of image source
                        'folder' => $imageDirectory,                // location where you save the thumbnails                
                        'fileName' => basename($file),              // it will only fetch the picture name not the entire path where picture is located
                        'format' => 'thumbnail',
                    ]);
                }
            }
         }
    }
}
