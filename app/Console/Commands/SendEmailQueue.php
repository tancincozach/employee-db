<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Repositories\EmailQueue\EmailQueueRepository;
use App\Mail\EmailQueue;
use App;

class SendEmailQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emailqueue:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email Queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed $emailQueue
     */
    public function handle(EmailQueueRepository $emailQueue)
    {
        $emails = $emailQueue->fetchForSending();

        foreach ($emails as $email) {
            try {
                $emailsToSend = explode(',', str_replace(' ', '', trim($email->send_to)));
                $emailsCc = empty($email->cc) ? null : explode(',', str_replace(' ', '', trim($email->cc)));
                $emailsBcc = empty($email->bcc) ? null : explode(',', str_replace(' ', '', trim($email->bcc)));

                $mail = Mail::to($emailsToSend);

                !empty($emailsCc) and $mail->cc($emailsCc);
                !empty($emailsBcc) and $mail->bcc($emailsBcc);

                // queue mail for sending
                $mail->queue(new EmailQueue($email));
            } catch(\Throwable $e) {

                // Mark this email as error
                $emailQueue->markAsError($email, $e->getMessage());
                continue;
            }

            // Consider as sent
            $emailQueue->markAsSent($email);
        }
    }
}
