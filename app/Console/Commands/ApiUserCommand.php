<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ApiUser;

class ApiUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apiuser:manage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manage API users';

    protected $apiUser = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ApiUser $apiUser)
    {
        $this->apiUser = $apiUser;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $command = strtolower(trim($this->ask('What do you like to do? [new, remove]')));

        switch ($command) {
            case 'new':
                    $owerName = $this->ask('New API owner name');
                    $allowedIps = $this->ask('Bind to what IPs? (Enter none for no restriction or enter multiple IP address comma-separated)');

                    // process allowed IPs
                    $allowedIps = trim($allowedIps);
                    $allowedIps = explode(',', str_replace(' ', '', $allowedIps));

                    $this->apiUser->generateOwner($owerName, $allowedIps);

                    echo "$owerName added";
                return;
                break;

            default:
                return 'Unknown command';
                break;
        }
    }
}
