<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Repositories\WorkFromHome\WorkFromHomeRequestRepository;
use App\Mail\WorkFromHome;
use App;

class SendWorkFromHome extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'workfromhome:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Work From Home';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(WorkFromHomeRequestRepository $wfh)
    {
        $workfromhomes = $wfh->fetchForSending();
        $to = [
            'lloyds@fullscale.io',
            'rremedio@fullscale.io'
        ];

        $cc = [
            'july@fullscale.io',
            'reynaldo@fullscale.io',
            'jim@fullscale.io'
        ];

        if (in_array(strtolower(trim(App::Environment())), ['prod', 'production'])) {
            $to = [
                'rnacu@fullscale.io',
            ];

            $cc = [
                'marites@fullscale.io',
                'keseo@fullscale.io',
            ];
        }

        foreach ($workfromhomes as $workfromhome) {
            try {
                Mail::to($to)
                ->cc($cc)
                ->queue(new WorkFromHome($workfromhome));
            } catch(Exception $e) {
                Log::error("Failed to send work from home request");
            }
            $wfh->markAsSent($workfromhome);
        }
    }
}
