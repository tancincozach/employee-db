<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Models\Client;
use App\Models\Employee;
use App\Mail\WeeklyReport;
use App;
use App\Services\ThirdParty\Timesheet as TimesheetService;
use App\Models\ClientTimesheetJobcode;

class SendWeeklyReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weeklyreport:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Weekly Report to Clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cc = [
            'lloyds@fullscale.io',
            'rremedio@fullscale.io'
        ];

        if (strtolower(trim(App::Environment())) == 'prod') {
            $cc = [
                'marites@fullscale.io',
                'keseo@fullscale.io',
                'rnacu@fullscale.io'
            ];
        }

        try {
            foreach(Client::whereNull('deleted_at')->get() as $client){
                if(!$this->hasActiveResources($client))
                    continue;

                $contacts = $client->contacts;
                if($contacts->first() && $contacts->first()->email) {
                    $to = $contacts->first()->email;
                    if($to && filter_var($to, FILTER_VALIDATE_EMAIL)) {

                        // To prevent me from getting fired.
                        if(App::isLocal())
                            $to = 'lloyds@fullscale.io';
                        Mail::to($to)->cc($cc)->queue(new WeeklyReport($client));
                    }
                }
            }
        } catch (Exception $e) {

        }
    }

    private function hasActiveResources($client) {
        $code = ClientTimesheetJobcode::where('client_id', $client->id)->first();
        if($code) {
            $job_code = $code->timesheet_jobcode_id;
            $monday = date( 'Y-m-d', strtotime( 'monday last week'  )  );
            $saturday = date( 'Y-m-d', strtotime( 'saturday last week'  )  );
            $count = TimesheetService::getWeekly(['job_code_id' => $job_code, 'start' => $monday, 'end' => $saturday])->count();
            return ($count > 0);
        }
        return false;
    }
}
