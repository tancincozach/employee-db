<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\TimesheetMapService;

class MapRawTimesheets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timesheet:map';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Map a raw timesheet jobcodes and users to rocks employees and clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(TimesheetMapService $service)
    {
        $service->execute();
    }
}
