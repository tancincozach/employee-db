<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ThirdParty\Timesheet;

class GenerateRawJobcodeAssignments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timesheet:generate-assignments
                            {--user_ids=}
                            {--type=}
                            {--jobcode_id=}
                            {--active=}
                            {--modified_since=}
                            {--modified_before=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a raw timesheet jobcode assignments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Timesheet $service)
    {
        $userIds        = $this->option('user_ids');
        $type           = $this->option('type');
        $jobcodeId      = $this->option('jobcode_id');
        $active         = $this->option('active');
        $modifiedSince  = date('c', strtotime($this->option('modified_since')));
        $modifiedBefore = date('c', strtotime($this->option('modified_before')));

        $service->retrieveJobcodeAssignments([
            'user_ids'        => $userIds,
            'type'            => $type,
            'jobcode_id'      => $jobcodeId,
            'active'          => $active,
            'modified_since'  => $modifiedSince,
            'modified_before' => $modifiedBefore,
        ]);
    }
}
