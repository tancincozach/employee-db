<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\EmployeeReport;
use App\Models\Employee;
use App\Models\EmailQueue;
use Carbon\Carbon;

class CheckNonSendersOfDailyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dailyreport:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check employees who did not submit a daily report.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $yesterday = Carbon::yesterday();
        //$yesterday = Carbon::createFromFormat('Y-m-d H:i:s.u', '2019-06-10 03:45:27.612584'); //TODO remove this

        /** Emails MUST not have white space characters before or after. **/
        $to_email = 'james@fullscale.io,cberdon@fullscale.io';
        $subject = 'Daily Report Status - ' . $yesterday->formatLocalized('%B %d, %Y');

        //find all employees who submitted yesterday
        $employees_submitted_yesterday = EmployeeReport::select(['employee_id'])
        ->where('report_date', $yesterday->toDateString())
        ->orderBy('employee_id')
        ->pluck('employee_id')
        ->all();
        //find all employees
        $all_employees = Employee::select(['id AS employee_id'])
        ->whereNotNull('employee_no')
        ->pluck('employee_id')
        ->all();
        //check all employees vs employees who submitted their daily reports yesterday
        $diff = array_merge(array_diff($all_employees, $employees_submitted_yesterday));

        $employee_records = Employee::select(['employees.id', 'employee_no', 'first_name', 'last_name', 'statuses.name AS status_name', 'job_positions.job_title AS job_title'])
        ->whereIn('employees.id', $diff)
        ->leftJoin('employee_job_positions', 'employees.id', 'employee_job_positions.employee_id')
        ->leftJoin('job_positions', 'employee_job_positions.position_id', 'job_positions.id')
        ->leftJoin('employee_statuses', 'employees.id', 'employee_statuses.employee_id')
        ->leftJoin('statuses', 'employee_statuses.status_id', 'statuses.id')
        ->whereIn('statuses.name', ['New', 'Probationary', 'Regular'])
        ->groupBy('employee_no')
        ->orderBy('last_name')
        ->get();

        EmailQueue::create([
            'status_id' => 0,
            'sender_id' => 1,
            'sender_name' => 'Fullscale Reports',
            'sender_email' => 'noreply@fullscale.io',
            'subject' => $subject,
            'send_to' => $to_email,
            'body' => view('emails.non-senders-of-daily-reports', compact('employee_records', 'all_employees', 'employees_submitted_yesterday')),
        ]);
    }
}
