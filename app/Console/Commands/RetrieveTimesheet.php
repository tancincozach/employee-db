<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ThirdParty\TSheets;
use \DateTime;
use Carbon\Carbon;

class RetrieveTimesheet extends Command
{
    /*
     * The date format
     * @var string
     */
    const DATE_FORMAT = 'Y-m-d';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timesheet:get {--m|modified} {start-date?} {end-date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve and store Timesheet to Rocks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed $emailQueue
     */
    public function handle()
    {
        $arg1    = $this->argument('start-date');
        $arg2    = $this->argument('end-date');
        $option1 = $this->option('modified');

        $startDate = $this->setDate($arg1);
        $endDate   = $this->setDate($arg2);
        $proceed   = $startDate && $endDate 
            && $this->isValidRange($startDate, $endDate);

        if ($proceed) {
            $tsheets = new TSheets($startDate, $endDate, $option1);
            $tsheets->retrieveAndStore();

            return;
        }

        return $this->info('The specified date is invalid!');
    }

    /*
     * Set a valid date
     *
     * @param  string $date
     * @return string|bool
     */
    private function setDate($date)
    {
        if (is_null($date)) {
            return Carbon::yesterday()->format(self::DATE_FORMAT);
        }

        if (!$this->isValidDate($date)) {
            return false;
        }

        return $date;
    }

    /*
     * Checks if date is valid
     *
     * @param string $date
     * @return bool
     */
    private function isValidDate($date)
    {
        $d = DateTime::createFromFormat(self::DATE_FORMAT, $date);

        return $d && $d->format(self::DATE_FORMAT) === $date;
    }
    
    /*
     * Checks if date range is valid
     *
     * @param string $from
     * @param string $to
     * @return bool
     */
    private function isValidRange($from, $to)
    {
        // Lets allow equal dates
        if ($from === $to) {
            return true;
        }

        $from = Carbon::createFromFormat(self::DATE_FORMAT, $from);
        $to   = Carbon::createFromFormat(self::DATE_FORMAT, $to);

        return $from < $to;
    }
}
