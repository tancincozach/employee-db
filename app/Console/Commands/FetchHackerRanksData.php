<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ThirdParty\HackerRank;

use \DateTime;
use Carbon\Carbon;

class FetchHackerRanksData extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hackerranks:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching the Data for the Hacker Ranks Test Results.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $hackerRanks = new HackerRank();
        $hackerRanks->retrieveAndStoreTests();
        $hackerRanks->retrieveAndStoreTestResults();
        return true;

    }

   
}
