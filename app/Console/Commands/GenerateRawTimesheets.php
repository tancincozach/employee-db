<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ThirdParty\Timesheet;

class GenerateRawTimesheets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timesheet:generate-raw
                            {--ids=}
                            {--start_date=}
                            {--end_date=}
                            {--jobcode_ids=}
                            {--user_ids=}
                            {--on_the_clock=}
                            {--jobcode_type=}
                            {--modified_since=}
                            {--modified_before=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a raw timesheets from TSheets API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Timesheet $service)
    {
        $ids            = $this->option('ids');
        $startDate      = $this->option('start_date');
        $endDate        = $this->option('end_date');
        $jobcodeIds     = $this->option('jobcode_ids');
        $userIds        = $this->option('user_ids');
        $onTheClock     = $this->option('on_the_clock');
        $jobcodeType    = $this->option('jobcode_type');
        $modifiedSince  = date('c', strtotime($this->option('modified_since')));
        $modifiedBefore = date('c', strtotime($this->option('modified_before')));

        $service->retrieveTimesheets([
            'ids'             => $ids,
            'start_date'      => $startDate,
            'end_date'        => $endDate,
            'jobcode_ids'     => $jobcodeIds,
            'user_ids'        => $userIds,
            'on_the_clock'    => $onTheClock,
            'jobcode_type'    => $jobcodeType,
            'modified_since'  => $modifiedSince,
            'modified_before' => $modifiedBefore,
        ]);
    }
}
