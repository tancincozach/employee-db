<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Models\WeeklyFloorReport;
use App\Models\WeeklyFloorQuestion;
use App\Models\WeeklyFloorAnswer;
use App\Models\Clients;
use App\Models\ClientContact;
use App\Mail\WeeklyFloorReports;
use App;

class SendWeeklyFloorReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weeklyfloorreport:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Weekly Floor Report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $weeklyFloorReports = WeeklyFloorReport::where('sent', 0)->limit(config('email.limit', 50))->get();

            foreach ($weeklyFloorReports as $weeklyFloorReport) {
                $contactData = ClientContact::where('client_id', $weeklyFloorReport->client_id)->get();
                $contactArray = array();

                foreach ($contactData as $row) {
                    $contactArray[] = $row->email;
                }

                if (App::environment(['local', 'staging', 'dev'])) {
                    // The environment is either local OR staging...
                    $contacts = array('rnacu@fullscale.io', 'july@fullscale.io', 'gbobilles@fullscale.io');
                } else {
                    //$contacts = $contactArray;
                    $contacts = array('rnacu@fullscale.io', 'deco@fullscale.io', 'ccabang@fullscale.io');
                }
                
                 // To prevent me from getting fired.
                if (App::isLocal()) {
                    $contacts = array();
                    $contacts[] = 'b402117dd4-8e12ba@inbox.mailtrap.io';
                }

                $weeklyFloorReportData = array();
                $floorParentQuestions = WeeklyFloorQuestion::where('deleted_at', NULL)->orderBy('sequence_no', 'asc')->get();
                $answerCtr = 0;

                if (sizeof($floorParentQuestions) > 0) {
                    
                    foreach ($floorParentQuestions as $qRow) {
                        $tempData['id'] = $qRow->id;
                        $tempData['parent_id'] = $qRow->parent_id;
                        $tempData['question'] = $qRow->weekly_floor_question;
                        $tempData['sequence'] = $qRow->sequence_no;
                        $tempData['is_red'] = $qRow->is_red;
                        $tempData['red_answer'] = !empty($qRow->red_answer) ? ucwords($qRow->red_answer) : '';
                        $ansRecords = WeeklyFloorAnswer::where(['weekly_floor_report_id' => $weeklyFloorReport->id, 'weekly_floor_question_id' => $qRow->id])->get();
                        
                        if (count($ansRecords) > 0) {
                            foreach ($ansRecords as $answer) {
                                if (!empty($answer->weekly_floor_answer)) {
                                    $tempData['answer'] = ucwords($answer->weekly_floor_answer);
                                    $answerCtr++;
                                }
                            }
                        }
                        $weeklyFloorReportData[] = $tempData;
                    }
                }
                if ($answerCtr > 0) {
                    Mail::to($contacts)->queue(new WeeklyFloorReports($weeklyFloorReport, $weeklyFloorReportData));
                    WeeklyFloorReport::find($weeklyFloorReport->id)->update(['sent' => 1]);
                }
            }
        } catch (Exception $e) {
        }
    }
}
