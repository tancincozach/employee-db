<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\SendDailyReports::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('dailyreport:send')->cron(config('email.cron', '* * * * *'))->withoutOverlapping();
        $schedule->command('weeklyfloorreport:send')->cron(config('email.cron', '* * * * *'))->withoutOverlapping();
        $schedule->command('workfromhome:send')->cron(config('email.cron', '* * * * *'))->withoutOverlapping();
        $schedule->command('emailqueue:send')->cron(config('email.cron', '* * * * *'))->withoutOverlapping();
        $schedule->command('hackerranks:get')->cron('0 */6 * * *')->withoutOverlapping();
        $schedule->command('weeklyreport:send')->weeklyOn(6, '16:00')->withoutOverlapping();
        $this->getRawTimesheets($schedule);
    }

    /*
     * Schedules to get raw timesheets
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    private function getRawTimesheets(Schedule $schedule)
    {
        // Schedule: Hourly
        // Action: Pull data starting yesterday
        $yesterday = Carbon::yesterday()->format('Y-m-d');
        $schedule->command("timesheet:generate-raw --start_date={$yesterday}")->hourly()->after(function () {
            Artisan::call('timesheet:map');
            Artisan::call('timesheet:generate-assignments --active=true');
        });

        // Schedule: Daily at 12MN and 4AM
        // Action:   Pull data from the last 7 days
        $start = Carbon::now()->subDays(7)->format('c');
        $end = Carbon::now()->format('c');
        $schedule->command("timesheet:generate-raw --modified_since={$start} --modified_before={$end}")->twiceDaily(0, 4)->after(function () {
            Artisan::call('timesheet:map');
            Artisan::call('timesheet:generate-assignments --active=true');
        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
