<?php

namespace App\Services;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as ExcelWriter;
use App\Repositories\WeeklyReportBatchDetail\WeeklyReportBatchDetailRepository;
use App\Criterias\WeeklyReportBatchDetail\SearchWeeklyReportBatchbyID;
use Illuminate\Support\Facades\Storage;
use Dingo\Api\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\WeeklyReport;
use App\Models\WeeklyReportBatchViewable;
use App\Models\WeeklyReportBatch;
use App\Models\WeeklyReportBatchDetail;
use Carbon\Carbon;

/**
 * This class parse TSheet csv file
 */
class TSheetParserService
{
    const RESULT_PATH = '/tsheet-results/';
    const COLUMN_HEADER = [
        'last Name',
        'first Name',
        'day',
        'date',
        'local_start_time',
        'local_end_time',
        'regular',
        'break',
        'night_diff'
    ];

    private $unparsedData;
    private $parsedDataperProject;
    private $resultFor;
    private $parsedDataToBeSaved;
    private $weekrbdRepository;
    private $wrbdPreviousId;
    private $wrbdId;
    private $weeklyReportBatchResult;
    private $assetResult;
    private $dateContentValid;
    private $viaApi = false;

    /**
     * Constructor
     *
     * @param Dingo\Api\Http\Request $storedFile
     * @param App\Repositories\WeeklyReportBatchDetail\WeeklyReportBatchDetailRepository $wrbdRepository
     * @param array $assetResult   results from Asset Service class
     */
    public function __construct(
        Request $request,
        WeeklyReportBatchDetailRepository $wrbdRepository = null,
        $assetResult = [],
        $weeklyReportBatchResult = [],
        $viaAPI = false
    ) {
        $this->weeklyReportBatchResult  = $weeklyReportBatchResult;
        $this->assetResult = $assetResult;

        // $storeFile = str_replace('/storage/', '', $assetResult->path);
        $this->weekrbdRepository = $wrbdRepository;

        // bypass everything if this is TRUE - April
        if (true === $viaAPI) {
            $this->viaApi = $viaAPI;
            return $this;
        }

        // $file = fopen(Storage::disk(env("FILESYSTEM_DRIVER"))->path($storeFile), "r");
        $storeFile = $this->getFilePath($assetResult->path);
        $file = fopen($storeFile, "r");
        while (!feof($file)) {
            $sheetData[] = fgetcsv($file);
        }
        fclose($file);

        $this->resultFor = $request->get('isParseAndDownload') ? 'payroll' : 'client';
        $this->unparsedData = $sheetData;
        $this->dataParser();

        // Default True if for Payroll
        $this->dateContentValid = true;
        // For Client
        if (!$request->get('isParseAndDownload')) {
            if (!$this->validateDateRecords($this->getParsedData(), $request->get('weekStartDate'), $request->get('weekEndDate'))) {
                $this->dateContentValid = false;
            }
        } else {
            //For Payroll
            $this->DeleteFileAfterReading($assetResult->path);
        }

        // delete all payroll records
        $this->getDeleteForPayrollRecords();
    }

    public function setUnparsedData(array $unParsedData)
    {
        $this->unparsedData = $unParsedData;
    }

    /**
     * Delete payroll parsed files
     */
    public function getDeleteForPayrollRecords()
    {
        foreach (Storage::disk('public')->files(self::RESULT_PATH) as $filename) {
            if (strpos($filename, 'payroll') !== false) {
                Storage::disk('public')->delete($filename);
            }
        }
    }

    /**
     * Delete File After reading
     */
    private function DeleteFileAfterReading($fetchedPath)
    {
        try {
            $path = str_replace_first("/storage/", "", $fetchedPath);
            Storage::disk('public')->delete($path);
        } catch (Exception $e) {
            Log::error("Failed to Delete File");
        }
    }

    /**
     * Get File Path
     */
    private function getFilePath($fetchedPath)
    {
        $path = str_replace_first("/storage/", "", $fetchedPath);
        $s3FileKey = $path;
        $temp = explode("/", $path);
        $fileName = $temp[count($temp) - 1];

        if (strcasecmp($this->resultFor, 'client') == 0) {
            $disk = getenv('FILESYSTEM_DRIVER'); //Possible values (s3/public)
            if ($disk == 's3') {
                try {
                    $adapter = Storage::disk($disk)->getAdapter();
                    $client = $adapter->getClient();
                    $client->registerStreamWrapper();
                    $object = $client->headObject([
                        'Bucket'    => $adapter->getBucket(),
                        'Key'       => $s3FileKey,
                    ]);

                    $realPath = $disk == 's3' ? "s3://{$adapter->getBucket()}/{$s3FileKey}" : $fetchedPath;

                    return $realPath;
                } catch (S3Exception $e) {
                    return "Error 404: File not found";
                }
            }
        }
        $realPath = Storage::disk('public')->path($path);

        return $realPath;
    }

    /**
     * Data Parser
     * processed unparsed data to the other room
     */
    private function dataParser()
    {
        $previousEmp = '';
        $previousJobcode = '';

        foreach (array_slice($this->unparsedData, 1) as $line) {
            $end = new \DateTime($line[9]);
            $start = new \DateTime($line[8]);

            $nightDiff = $this->getNightDiff($start, $end);

            $filter = [
                'employee'              => $line[0],
                'first Name'            => $line[2],
                'last Name'             => $line[3],
                'day'                   => $line[7],
                'date'                  => $line[6],
                'local_start_time'      => strcasecmp($line[12], 'Paid Time Off') == 0 ? date('Y-m-d H:i', strtotime($line[6])) : date('Y-m-d H:i', strtotime($line[8])),
                'local_end_time'        => strcasecmp($line[12], 'Paid Time Off') == 0 ? date('Y-m-d H:i', strtotime($line[6])) : date('Y-m-d H:i', strtotime($line[9])),
                'regular'               => strcasecmp($line[12], 'Rest/Lunch Break') == 0 && strcasecmp($this->resultFor, 'payroll') == 0 ? 0 : $line[11],
                'jobcode'               => $line[12]
            ];

            if ($this->resultFor == 'payroll') {
                // insert new array object to specific indexes
                $filter = array_slice($filter, 0, count($filter) - 1, true) +
                    [
                        'break'         => $line[12] != 'Rest/Lunch Break' ? 0 : $line[11],
                        "night_diff"    => $nightDiff
                    ] +
                    array_slice($filter, count($filter) - 1, count($filter) - 1, true);
            }

            //If it came from the API, then we should save the timesheet ID which is stored in index 19
            if (true === $this->viaApi) {
                $filter['timesheet_id'] = $line[19];
            }

            $this->parsedDataperProject[$line[12]][$line[0]][] = $filter;
            if ($this->resultFor == 'payroll' && $line[0] == $previousEmp && $line[12] == 'Rest/Lunch Break') {
                $this->parsedDataperProject[$previousJobcode][$line[0]][] = $filter;
            }

            $this->parsedDataToBeSaved[$line[12]][$line[0]][$line[6]][] = $filter;

            $previousEmp = $line[0];
            $previousJobcode = $line[12];
        }
    }

    public function callDataParser()
    {
        $this->dataParser();
        $this->readySaveData();
    }

    /**
     * Data Writing to Excel
     *
     * @param array  $rawEmpWorkHours parsed data
     * @param string $title file title/name for the output file
     *
     * @return string $file_title
     */
    public function writeReport($rawEmpWorkHours = [], $title = '')
    {
        set_time_limit(300); //5 mins

        $spreadsheet = new Spreadsheet();
        $NightDiffHours = 0;
        $RegularHours = 0;
        $BreakHours = 0;
        $summaryReport = [];

        $title = empty($title) ? 'workHours'. microtime(true) .'.xlsx' : $title . '-result-' . $this->resultFor . '.xlsx';

        foreach ($rawEmpWorkHours as $jobcode => $empWorkHoursByJobcode) {
            if (!empty($jobcode)) {
                $jobcodeTitle = str_replace('/', ' or ', $jobcode);  // Worksheet titles doesn't work well with "/"

                $newWorksheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet(
                    $spreadsheet,
                    $jobcodeTitle
                );

                //New Worksheet
                $spreadsheet->addSheet($newWorksheet);
                $currentSheet = $spreadsheet->getSheetByName($jobcodeTitle);
                $currentSheet->freezePane('A2');

                //data starts at line 2
                $rowIndex = 2;

                $this->writeHeaderReportRows($currentSheet);

                foreach ($empWorkHoursByJobcode as $emp => $logs) {
                    foreach ($logs as $key => $values) {
                        $currentSheet->setCellValueByColumnAndRow(1, $rowIndex, $values[self::COLUMN_HEADER[0]]);
                        $currentSheet->setCellValueByColumnAndRow(2, $rowIndex, $values[self::COLUMN_HEADER[1]]);
                        $currentSheet->setCellValueByColumnAndRow(3, $rowIndex, $values[self::COLUMN_HEADER[2]]);
                        $currentSheet->setCellValueByColumnAndRow(4, $rowIndex, $values[self::COLUMN_HEADER[3]]);
                        $currentSheet->setCellValueByColumnAndRow(5, $rowIndex, $values[self::COLUMN_HEADER[4]]);
                        $currentSheet->setCellValueByColumnAndRow(6, $rowIndex, $values[self::COLUMN_HEADER[5]]);
                        $currentSheet->setCellValueByColumnAndRow(7, $rowIndex, $values[self::COLUMN_HEADER[6]]);

                        if (strcasecmp($this->resultFor, 'payroll') == 0) {
                            $currentSheet->setCellValueByColumnAndRow(8, $rowIndex, $values[self::COLUMN_HEADER[7]]);
                            $currentSheet->setCellValueByColumnAndRow(9, $rowIndex, $values[self::COLUMN_HEADER[8]]);
                            $BreakHours += $values[self::COLUMN_HEADER[7]];
                            $NightDiffHours += $values[self::COLUMN_HEADER[8]];
                        }

                        $RegularHours += $values[self::COLUMN_HEADER[6]];
                        $rowIndex++;

                        $empMergedName = str_replace(
                            ' ',
                            '',
                            strtolower($values['last Name'] . $values['first Name'])
                        );
                    }

                    $total = [
                        'regularHours'                  => $RegularHours,
                        'breakHours'                    => $BreakHours,
                        'nightDiffHours'                => $NightDiffHours
                    ];

                    $this->writeTotalPerEmployee($currentSheet, $rowIndex, $total);

                    if (strcasecmp($this->resultFor, 'payroll') == 0 && strcasecmp($jobcodeTitle, 'Rest or Lunch Break') != 0) {
                        $hoursToAdd = in_array($jobcodeTitle, ['Holiday', 'Sick', 'Vacation']) ? 8 : $RegularHours;
                        if (array_key_exists($empMergedName, $summaryReport)) {
                            $summaryReport[$empMergedName]['totalRegularHours'] += $hoursToAdd;
                            $summaryReport[$empMergedName]['totalBreakHours'] += $BreakHours;
                            $summaryReport[$empMergedName]['totalNightDiffHours'] += $NightDiffHours;
                        } else {
                            $summaryReport[$empMergedName] = array(
                                'firstName'                 => $values['first Name'],
                                'lastName'                  => $values['last Name'],
                                'totalRegularHours'         => $hoursToAdd,
                                'totalBreakHours'           => $BreakHours,
                                'totalNightDiffHours'       => $NightDiffHours
                            );
                        }

                        if (array_key_exists('jobs', $summaryReport[$empMergedName])) {
                            $summaryReport[$empMergedName]['jobs'] = $summaryReport[$empMergedName]['jobs'] . ' / ' . $jobcode;
                        } else {
                            $summaryReport[$empMergedName]['jobs'] = $jobcode;
                        }
                    }

                    $NightDiffHours = 0;
                    $RegularHours = 0;
                    $BreakHours = 0;
                    $rowIndex += 4;
                }
            }
        }

        $spreadsheet->setActiveSheetIndex(0);

        if ($this->resultFor == 'payroll') {
            $this->writeSummaryReport($spreadsheet, $summaryReport);
        }

        $writer = new ExcelWriter($spreadsheet);

        // Get directories
        $publicDirectory = Storage::disk('public')->directories();

        // If directory not exist, create.
        if (!(in_array(str_replace('/', '', self::RESULT_PATH), $publicDirectory))) {
            Storage::disk('public')->makeDirectory(str_replace('/', '', self::RESULT_PATH));
        }

        $writer->save(Storage::disk('public')->path(self::RESULT_PATH . $title));

        //default public
        $path = Storage::disk('public')->url(self::RESULT_PATH . $title);

        //Flag for s3 and not for payroll
        if (strcasecmp(env('FILESYSTEM_DRIVER'), 's3') == 0 && strcasecmp($this->resultFor, 'payroll') != 0) {
            $file = fopen(Storage::disk('public')->path(self::RESULT_PATH . $title), 'r+');

            Storage::disk('s3')->put(self::RESULT_PATH . $title, $file);

            fclose($file);
            // delete local file
            Storage::disk('public')->delete(self::RESULT_PATH . $title);

            $path = Storage::url(ltrim(self::RESULT_PATH, '/') . $title);
        }

        if (strcasecmp($this->resultFor, 'payroll') != 0) {
            $this->setWeeklyReportBatchDetail(self::RESULT_PATH . $title, $this->getRequiredDataForDBSave());
            $this->readySaveData();
        }

        return response()->json(['data' => ['path' => $path]], 200);
    }

    private function writeHeaderReportRows($currentSheet)
    {
        $rowHeader = 1;

        $currentSheet->setCellValueByColumnAndRow(1, $rowHeader, self::COLUMN_HEADER[0]);
        $currentSheet->setCellValueByColumnAndRow(2, $rowHeader, self::COLUMN_HEADER[1]);
        $currentSheet->setCellValueByColumnAndRow(3, $rowHeader, self::COLUMN_HEADER[2]);
        $currentSheet->setCellValueByColumnAndRow(4, $rowHeader, self::COLUMN_HEADER[3]);
        $currentSheet->setCellValueByColumnAndRow(5, $rowHeader, self::COLUMN_HEADER[4]);
        $currentSheet->setCellValueByColumnAndRow(6, $rowHeader, self::COLUMN_HEADER[5]);
        $currentSheet->setCellValueByColumnAndRow(7, $rowHeader, self::COLUMN_HEADER[6]);
        $currentSheet->getColumnDimension('A')->setAutoSize(true);
        $currentSheet->getColumnDimension('B')->setAutoSize(true);
        $currentSheet->getColumnDimension('C')->setAutoSize(true);
        $currentSheet->getColumnDimension('D')->setAutoSize(true);
        $currentSheet->getColumnDimension('E')->setAutoSize(true);
        $currentSheet->getColumnDimension('F')->setAutoSize(true);
        $currentSheet->getColumnDimension('G')->setAutoSize(true);

        if (strcasecmp($this->resultFor, 'payroll') == 0) {
            $currentSheet->setCellValueByColumnAndRow(8, $rowHeader, self::COLUMN_HEADER[7]);
            $currentSheet->setCellValueByColumnAndRow(9, $rowHeader, self::COLUMN_HEADER[8]);
            $currentSheet->getColumnDimension('H')->setAutoSize(true);
            $currentSheet->getColumnDimension('I')->setAutoSize(true);
        }
    }

    private function writeTotalPerEmployee($currentSheet, $rowIndex, $total = [])
    {
        $currentSheet->setCellValueByColumnAndRow(6, $rowIndex, 'Total');
        $currentSheet->getStyleByColumnAndRow(6, $rowIndex)
            ->getAlignment()->setHorizontal('center');

        $currentSheet->setCellValueByColumnAndRow(7, $rowIndex, $total['regularHours']);
        $currentSheet->getStyleByColumnAndRow(7, $rowIndex)
            ->getAlignment()->setHorizontal('center');

        if (strcasecmp($this->resultFor, 'payroll') == 0) {
            $currentSheet->setCellValueByColumnAndRow(8, $rowIndex, $total['breakHours']);
            $currentSheet->getStyleByColumnAndRow(8, $rowIndex)
                ->getAlignment()->setHorizontal('center');

            $currentSheet->setCellValueByColumnAndRow(9, $rowIndex, $total['nightDiffHours']);
            $currentSheet->getStyleByColumnAndRow(9, $rowIndex)
                ->getAlignment()->setHorizontal('center');
        }
    }

    private function writeSummaryReport($spreadsheet, $summaryReport)
    {
        $summarySheet = $spreadsheet->getActiveSheet();
        $summarySheet->setCellValueByColumnAndRow(1, 1, 'Last Name');
        $summarySheet->getColumnDimension('A')->setAutoSize(true);
        $summarySheet->setCellValueByColumnAndRow(2, 1, 'First Name');
        $summarySheet->getColumnDimension('B')->setAutoSize(true);
        $summarySheet->setCellValueByColumnAndRow(3, 1, 'Total Regular Hours');
        $summarySheet->getColumnDimension('C')->setAutoSize(true);
        $summarySheet->setCellValueByColumnAndRow(4, 1, 'Total Break');
        $summarySheet->getColumnDimension('D')->setAutoSize(true);
        $summarySheet->setCellValueByColumnAndRow(5, 1, 'Total Night Diff');
        $summarySheet->getColumnDimension('E')->setAutoSize(true);
        $summarySheet->setCellValueByColumnAndRow(6, 1, 'Job Codes');
        $summarySheet->getColumnDimension('F')->setAutoSize(true);
        $summarySheet->freezePane('A2');

        $rowIndex = 2;
        ksort($summaryReport);

        foreach ($summaryReport as $emp => $value) {
            $summarySheet->setCellValueByColumnAndRow(1, $rowIndex, $value['lastName']);
            $summarySheet->setCellValueByColumnAndRow(2, $rowIndex, $value['firstName']);
            $summarySheet->setCellValueByColumnAndRow(3, $rowIndex, $value['totalRegularHours']);
            $summarySheet->setCellValueByColumnAndRow(4, $rowIndex, $value['totalBreakHours']);
            $summarySheet->setCellValueByColumnAndRow(5, $rowIndex, $value['totalNightDiffHours']);
            $summarySheet->setCellValueByColumnAndRow(6, $rowIndex, $value['jobs']);
            ++$rowIndex;
        }
    }

    private function endTimeLesserOrEqual10MinsAfter10PM($endTime)
    {
        $endHour = $endTime->format('H');
        $endMinute = $endTime->format('i');

        return $endMinute <= 10 && $endHour == 22;
    }

    /**
     * Calculate Night Diff
     *
     * @param DateTime $start start time per log/row
     * @param DateTime $end   end time per log/row
     *
     * @return FloatTime Computed Night diff per Log
     */
    private function getNightDiff($start, $end)
    {
        $nightDiffStart = new \DateTime($start->format('m/d/Y') . " 22:00:00");
        $nightDiffEnd = new \DateTime($start->format('m/d/Y') . " 06:00:00");

        if (($start >= $nightDiffEnd && $start < $nightDiffStart)
            && ($end > $nightDiffEnd && $end < $nightDiffStart)
            || $this->endTimeLesserOrEqual10MinsAfter10PM($end)
        ) {
            return 0;
        }

        if ($start < $nightDiffStart && $start >= $nightDiffEnd) {
            $timeDiff = $end->diff($nightDiffStart);
        } elseif ($start < $nightDiffEnd
            && ($end >= $nightDiffEnd && $end <= $nightDiffStart)
        ) {
            $timeDiff = $nightDiffEnd->diff($start);
        } elseif ($start < $nightDiffEnd && $end > $nightDiffStart) {
            $NightDiff = $nightDiffEnd->diff($start);
            $DawnDiff = $nightDiffStart->diff($end);
        } else {
            $timeDiff = $end->diff($start);
        }

        if (!empty($DawnDiff) && !empty($NightDiff)) {
            $floatResults = $this->getFloatStrTime($NightDiff->format('%H:%i:%s')) + $this->getFloatStrTime($DawnDiff->format('%H:%i:%s'));
        } else {
            $floatResults = $this->getFloatStrTime($timeDiff->format('%H:%i:%s'));
        }

        return $floatResults;
    }

    /**
     * Convert String Time into Float
     *
     * @param String $strtime ex. 10:20:01
     *
     * @return FloatTime $result converted string to float time
     */
    private function getFloatStrTime($strtime)
    {
        $time = explode(":", $strtime);
        //seconds to minute
        $minutes = intval($time[2]) / 60;
        $minutes += intval($time[1]);
        //minutes to hour
        $hr = $minutes / 60;
        $result = intval($time[0]) + $hr;
        return number_format($result, 2);
    }

    /**
     * Get parsed Data
     *
     * @return array parsedData
     */
    public function getParsedData()
    {
        return $this->parsedDataperProject;
    }

    private function getRequiredDataForDBSave()
    {
        return [
            'weekly_report_batch_id'    => $this->weeklyReportBatchResult->id,
            'asset_id'                  => $this->assetResult->id,
            'uploaded_by_user_id'       => $this->weeklyReportBatchResult->created_by_user_id
        ];
    }

    private function setWeeklyReportBatchDetail($title, $array = [])
    {
        $wrbd = $this->getWeeklyReportBatchDetailbyWeeklyReportBatchID($array['weekly_report_batch_id']);

        $result = $this->weekrbdRepository->create(
            [
                'weekly_report_batch_id'    => $array['weekly_report_batch_id'],
                'file_version'              => count($wrbd) + 1,
                'asset_id'                  => $array['asset_id'],
                'parsed_filename'           => $title,
                'status'                    => 'success',
                'uploaded_by_user_id'       => $array['uploaded_by_user_id']
            ]
        );

        //get previous id if another entry already exist
        $this->wrbdPreviousId = count($wrbd) >= 1 ? $wrbd[count($wrbd) - 1]->id : false;
        $this->wrbdId = $result->id;
        return $result;
    }

    public function getOldIDWeeklyReportBatchDetails()
    {
        return $this->wrbdPreviousId;
    }

    private function getWeeklyReportBatchDetailbyWeeklyReportBatchID($weeklyReportBatchId)
    {
        return $wreportbatchdetail = $this->weekrbdRepository->getByCriteria(new SearchWeeklyReportBatchbyID(
            $weeklyReportBatchId
        ));
    }

    private function readySaveData()
    {
        $result = [];
        $totalNightDiffHours = 0;
        $totalRegularHours = 0;
        $totalBreakHours = 0;
        $timeSheetID = 0;

        /** Generate dummy records in weekly_report_batches & weekly_report_batch_details tables
         *  when both tables are empty and truncated (auto-increment starting point at 1)
         */
        if (WeeklyReportBatch::isEmpty()) {
            WeeklyReportBatch::createDummy();
            WeeklyReportBatchDetail::createDummy();
        }



        foreach ($this->parsedDataToBeSaved as $jobcode => $jobworkhours) {
            if (!empty($jobcode)) {
                foreach ($jobworkhours as $emp => $dates) {
                    foreach ($dates as $date => $logs) {
                        foreach ($logs as $key => $log) {
                            foreach ($log as $key => $value) {

                                // Log::info(print_r($log, 1));
                                switch ($key) {
                                    case 'night_diff':
                                        $totalNightDiffHours += $value;
                                        break;
                                    case 'regular':
                                        $totalRegularHours += $value;
                                        break;
                                    case 'break':
                                        $totalBreakHours += $value;
                                        break;
                                    case 'timesheet_id':
                                        $timeSheetID = $value;
                                        break;
                                    default:
                                        break;
                                }
                            }

                            $dt = new \DateTime($date);

                            $arr = [
                                'jobcode_id'                       => $jobcode,
                                'employee_username'             => $emp,
                                'log_date'                      => $dt->format('y-m-d H:i:s'),
                                'weekly_report_batch_detail_id' => !empty($this->wrbdId) ? $this->wrbdId : 1,
                                'working_hrs'                   => $log['regular'],
                                'break_hrs'                     => !empty($log['break']) ? $log['break'] : 0,
                                'night_differential_hrs'        => !empty($log['night_diff']) ? $log['night_diff'] : 0,
                                'data_source'                   => (!$this->viaApi) ? 'file' : 'api'
                            ];

                            if (true === $this->viaApi) {
                                $result[$jobcode][$emp][] = array_merge($arr, ['timesheet_id' => $log['timesheet_id'], 'start_date' => $log['local_start_time'], 'end_date' => $log['local_end_time']]);
                            } else {
                                $result[$jobcode][$emp][] = $arr;
                            }

                            $totalNightDiffHours = 0;
                            $totalRegularHours = 0;
                            $totalBreakHours = 0;
                            $timeSheetID = 0;
                        }
                    }
                }
            }
        }
        $this->parsedDataToBeSaved = $result;
    }

    public function getParsedDataToBeSaved()
    {
        return $this->parsedDataToBeSaved;
    }

    /**
     * Traverse Array to Validate Date
     *
     * @return Boolean If records are valid
     */
    public function validateDateRecords($array = [], $startDate, $endDate)
    {
        $isValid = true;
        if (empty($array)) {
            return $isValid = false;
        }

        // Projects Iteration
        foreach ($array as $project) {
            // Employees Iteration
            foreach ($project as $employee) {
                // Logs Iteration
                foreach ($employee as $log) {
                    if (empty($log["date"])) {
                        continue;
                    }

                    if (!$this->isDateValid($log["date"], $startDate, $endDate)) {
                        $isValid = false;
                        break;
                    }
                }
            }
        }

        return $isValid;
    }

    /**
     * Date in Range Validation
     *
     * @return Boolean If in range.
     */
    private function isDateValid($givenDate, $startDate, $endDate)
    {
        $dateStart = strtotime($startDate);
        $dateEnd = strtotime($endDate);
        $dateGiven = strtotime($givenDate);

        return ($dateGiven >= $dateStart) && ($dateGiven <= $dateEnd);
    }

    /**
     * Return bool if date content is valid
     *
     * @return Boolean
     */
    public function isDateContentValid()
    {
        return $this->dateContentValid;
    }

    public function getValidationMessage()
    {
        return response()->json(
            [
                'status_code' => 400,
                'message' => "Date input and Date content are mismatched."
            ],
            400
        );
    }

    /**
     *  For Transformer Formatter
     */
    public static function formatWorkLogs($worklogsOptions)
    {
        $worklogs = [];
        $weeksInMonth = self::weekEndDates($worklogsOptions['month'], $worklogsOptions['year']);
        $worklogsOptions['startDate'] = $worklogsOptions['year'].'-'. $worklogsOptions['month'].'-1';

        foreach ($weeksInMonth as $week) {
            $worklogsOptions['endDate'] = $week;
            $endWeekRange = strtotime($week);
            $startWeekRange = strtotime($worklogsOptions['startDate']);

            $qryRes = WeeklyReport::GetWorklogs($worklogsOptions)->first();

            $worklogs[ date('M', $startWeekRange) .' '. date('j', $startWeekRange) .'-'. date('j', $endWeekRange) ] = $qryRes['working_hrs'];
            $worklogsOptions['startDate'] = date('Y-m-d', strtotime($week . ' +1 day'));
        }

        return $worklogs;
    }

    /**
     * Return header keys for worklogs
     */
    public static function getDateHeaders($worklogs)
    {
        return array_keys($worklogs);
    }

    /**
     * Get the Week Ranges per Month
     */
    private static function weekEndDates($month, $year)
    {
        $dates = [];

        $date = new \DateTime("$year-$month-01");
        $days = (int)$date->format('t'); // total number of days in the month
        $mlastDay = $date->format('Y-m-t'); // get last date of the month
        $oneDay = new \DateInterval('P1D');

        for ($day = 1; $day < $days; $day++) {
            $dayOfWeek = $date->format('l');

            if (strcasecmp($dayOfWeek, 'Sunday') == 0) {
                $dates[] = $date->format('Y-m-d');
            }

            $date->add($oneDay);
        }

        $dates[] = $mlastDay;

        return $dates;
    }

    public static function updateOrCreateProjectJobcode($projectId, $jobcode)
    {
        return \App\Models\ClientProjectJobcode::updateOrCreate(['jobcode' => $jobcode], ['client_project_id' => $projectId]);
    }

    private static function getDates($start, $end = null)
    {
        $dates = [];
        $pointStart = new Carbon($start);
        $pointEnd = new Carbon($end);

        if ($end != null) {
            while ($pointStart->toDateString() <= $pointEnd->toDateString()) {
                array_push($dates, array('day' => $pointStart->format('D'), 'log_date' => $pointStart->format('Y-m-d')));
                $pointStart->addDay(1);
            }
        } else {
            array_push($dates, $pointStart->format('M d'));
        }

        return $dates;
    }

    public static function getWeeklyHeaders($clientId, $month)
    {
        $weeks = WeeklyReportBatchViewable::GetDateRangeViewablesByClientId($clientId, $month);
        $weekHeaders = [];
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        foreach($weeks as $week) {
            $weekStart = new Carbon($week['start_date']);
            $weekEnd = new Carbon($week['end_date']);

            $dates = self::getWeekly($weekStart, $weekEnd, $clientId);
            array_push($weekHeaders, $dates[0]); // Returns only 1 week
        }

        return $weekHeaders;
    }

    private static function getWeekly($start, $end, $clientId)
    {
        $weeks = [];

        while ($start->toDateString() <= $end->toDateString()) {
            $isPosted = false;
            $temp = clone $start;
            $eow = $temp->endOfWeek();
            $tempEnd = null;

            switch (true) {
                case $eow->toDateString() <= $end->toDateString():
                        // If falls on weekends
                    if ($start->toDateString() === $end->toDateString() || $eow->toDateString() == $start->toDateString()) {
                        $week = $start->format('M d');
                    } else {
                        if ($start->month == $eow->month) {
                            $week = $start->format('M d').' - '.$eow->format('d');
                        } else {
                            $week = $start->format('M d').' - '.$eow->format('M d');
                        }
                    }

                    $dates = self::getDates($start, $eow);
                    $tempEnd = $eow;
                    break;

                case $eow->toDateString() > $end->toDateString():
                    if ($start->month == $eow->month) {
                        $week = $start->format('M d').' - '.$end->format('d');
                    } else {
                        $week = $start->format('M d').' - '.$end->format('M d');
                    }


                    $dates = self::getDates($start, $end);
                    $tempEnd = $end;
                    break;
            }

            $isPosted = WeeklyReportBatchViewable::IsExistPostedReport($clientId, $start->toDateString(), $tempEnd->toDateString());

            array_push($weeks, array('name' => $week, 'dates' => $dates, 'isPosted' => $isPosted));

            $start->endOfWeek()->addDay(1);
        }

        return $weeks;
    }

    public static function getWeeklyContent($headers, $workLogs, $jobcodeIds)
    {
        $weeklyContent = [];
        $workLogs['jobcode_id'] = $jobcodeIds;

        foreach ($headers as $week) {
            $totalCtr = 0;
            $ptoCtr = 0;
            $holidayCtr = 0;
            $log = [];
            $totalHrs = 0;
            $totalMins = 0;
            $finalTotalHours = 0;

            foreach ($week['dates'] as $days) {
                $workLogs['log_date'] = $days['log_date'];

                $jobcodeHrs = WeeklyReport::GetByLogDate($workLogs);

                $workHrs = 0;
                $pto = 0;
                $holidayHrs = 0;

                foreach ($jobcodeHrs as $jobcode) {
                    empty($jobcode->working_hrs) and $jobcode->working_hrs = 0;

                    switch ($jobcode->jobcode_id) {
                        case $jobcodeIds[WeeklyReport::JOBCODE_INDEX]:
                            $workHrs += $jobcode->working_hrs;
                            break;

                        case $jobcodeIds[WeeklyReport::PTO_INDEX]:
                            $pto += $jobcode->working_hrs;
                            break;

                        case in_array($jobcode->jobcode_id, $jobcodeIds[WeeklyReport::HOLIDAY_INDEX]):
                            $holidayHrs += $jobcode->working_hrs;
                            break;
                    }
                }

                $log[ $days['log_date'] ] = [
                    'work'    => number_format($workHrs, 2),
                    'pto'     => number_format($pto, 2),
                    'holiday' => number_format($holidayHrs, 2)
                ];

                $totalCtr += number_format($workHrs, 2);
                $ptoCtr += $pto;
                $holidayCtr += $holidayHrs;
            }

            $weeklyContent[] = [
                'name'         => $workLogs['employee'],
                'workLogs'     => $log,
                'ptoTotal'     => $ptoCtr,
                'holidayTotal' => $holidayCtr,
                'Total'        => number_format($totalCtr, 2),
            ];
        }

        return $weeklyContent;
    }

    public static function getHeadersByMonth($year, $month, $clientId)
    {
        $weeks = [];
        $start = new Carbon();
        $start->month = $month;
        $start->year = $year;
        $start->day = 01;

        $startClone = clone $start;

        $now = Carbon::now();

        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        $end = ($now->year == $year && $now->month == $month) ? $now->startOfWeek()->subDay(1) : $startClone->endOfMonth();

        $range = static::overrideMonthRange((int) $month, (int) $year);
        $start = $range['start'];
        $endDate = static::getEndDate((int) $year, (int) $month);
        $end = $endDate ?: $range['end'];

        return array_reverse(self::getWeekly($start, $end, $clientId));
    }

    /*
     * This will override the default start and end date of the month
     *
     * @param integer $month
     * @param integer $year
     *
     * @return array
     */
    public static function overrideMonthRange($month, $year)
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        $start = Carbon::createFromDate($year, $month, 1);
        $end = clone $start;

        $start->startOfMonth()->startOfWeek();
        $end->endOfMonth()->endOfWeek();

        return [
            'start' => $start,
            'end'   => $end,
        ];
    }

    /*
     * Get end date if the specified year and month
     * is equal to the currrent year and month
     *
     * @param integer $year
     * @param integer $month
     *
     * @return array|boolean
     */
    public static function getEndDate($year, $month)
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        $now = Carbon::now();
        $startOfMonth = Carbon::now()->startOfMonth();
        $date = Carbon::createFromDate($year, $month)->startOfMonth();

        if ($date == $startOfMonth) {
            $now->subWeek();
            $end   = $now->endOfWeek();

            return $end;
        }

        return false;
    }
}
