<?php

namespace App\Services\Asset;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ResizeImageService
{
    public $result;
    protected $image;
    protected $folder;
    protected $fileName;
    protected $format;

    public function __construct($image, $folder, $fileName, $format)
    {
        $this->image = $image;
        $this->folder = $folder;
        $this->fileName = $fileName;
        $this->format = $format;
        $this->sizeFormats = [
            'thumbnail' => 75  // width size as value
        ];
    }

    public static function run($args)
    {
        $service = new self($args['image'], $args['folder'], $args['fileName'], $args['format']);
        $service->resizeImage();
        return $service;
    }

    public function resizeImage()
    {
        if (!isset($this->sizeFormats[$this->format])) {
            $error = 'Wrong image size format name: '. $this->format;
            throw new \Exception($error);
        }

        $size = $this->sizeFormats[$this->format];
        $quality = 100;

        $thumbnailPath = $this->folder . '/' . $this->format . '/' . $this->fileName;
        $image_resize = Image::make($this->image)
            ->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->encode('jpg', $quality);

        Storage::put($thumbnailPath, $image_resize);
    }
}
