<?php
/**
 * Created by PhpStorm.
 * User: raymund
 * Date: 6/5/19
 * Time: 8:50 AM
 */

namespace App\Services;


use App\Models\AllQuestionResponse;
use App\Models\Category;
use App\Models\Skill;
use App\Models\SkillCategory;

class CategorySkillHelper
{
    public static function getSkillsByClient($clientId, $additionalSkills = [])
    {
        $developerSkills = [];
        // get skills from categories selected
        $skills = self::getTechRequired($clientId);
        
        if($skills->isEmpty()) {
            $developerSkills = (empty($additionalSkills)) ? Skill::pluck('name') : $additionalSkills;
        } else {
            $otherSkills = [];
            $mergedSkills = [];

            $skills->values()->map(function($item) use (&$otherSkills){
                // assumes all skills are added in `all_question_choice_description`.
                // so if `response` is empty, it should be others
                if(!empty($item)) {
                    $otherSkills = explode(',', preg_replace('/\s+/', '', $item));
                }
            });

            if (empty($additionalSkills)) {
                $mergedSkills = array_merge($skills->keys()->toArray(), $otherSkills);
            } else {
                // Additional Skills are already included from Client Onboarding
                $mergedSkills = array_merge($additionalSkills, $otherSkills);
            }

            $developerSkills = Skill::whereIn('name', $mergedSkills)
                ->get()
                ->map(function($item){
                    return $item->name;
                });
        }

        return $developerSkills;
    }

    public static function getTechRequired ($clientId)
    {
        return AllQuestionResponse::where([
                ['client_id', '=', $clientId],
                ['all_question_form_label', '=', 'tech_require']
            ])->pluck('response', 'all_question_choice_description');
    }

    public static function getServiceNeeded ($clientId)
    {
        return AllQuestionResponse::where([
                ['client_id', '=', $clientId],
                ['all_question_form_label', '=', 'service_need']
            ])->pluck('response', 'all_question_choice_description');
    }

    public static function inUnassignedSkillsCategory ($skills) 
    {
        $resourceTypes = SkillCategory::join('skills', 'skills.id', '=', 'skill_categories.skill_id')
                                        ->join('categories', 'categories.id', '=', 'skill_categories.category_id')
                                        ->whereIn('skills.name', $skills)
                                        ->pluck('categories.resource_type')->toArray();

        // Returns true if Resource Types has Developers or Testers
        return !empty(array_intersect(Category::UNASSIGN_ONLY_RESOURCE_TYPE, $resourceTypes));
    }

    public static function isUnassignedCategoryResourceType ($categories) 
    {
        $resourceTypes = Category::whereIn('id', $categories)->pluck('resource_type')->toArray();
        
        // Returns true if Resource Types has Developers or Testers
        return !empty(array_intersect(Category::UNASSIGN_ONLY_RESOURCE_TYPE, $resourceTypes));
    }

    public static function getSkillNames ($skillId) 
    {
        return Skill::whereIn('id', $skillId)->pluck('name')->toArray();
    }

    public static function getSkillNameByCategory($categories) {
        $result = SkillCategory::join('skills', 'skills.id', '=', 'skill_categories.skill_id')
                                ->join('categories', 'categories.id', '=', 'skill_categories.category_id')
                                ->whereIn('categories.name', $categories)
                                ->pluck('skills.name');                                

        return (isset($result)) ? $result : [];
    }

    /**
     * @param array $selectedCategories
     * @return array
     */
    public static function getAllCategory($selectedCategories = [])
    {
        $categories = [];
        foreach ($selectedCategories as $categoryId) {
            $categories[] = (int) $categoryId;
            $category = Category::find($categoryId);
            if ($category) {
                if ($category->children()) {
                    $children = $category->children()->pluck('id')->toArray();
                    $categories = array_merge($categories, $children);
                }
            }
        }
        return $categories;
    }

    /**
     * @param array $selectedCategories
     * @param array $selectedSkills
     * @return array
     */
    public static function getAllSkillByCategory($selectedCategories = [], $selectedSkills = [])
    {
        $categories = self::getAllCategory($selectedCategories);

        $skills = SkillCategory::whereIn('category_id', $categories)->pluck('skill_id')->toArray();
        return array_merge($skills, $selectedSkills);
    }
}
