<?php

namespace App\Services;

class EmailQueueService
{

    //$template is the template name under emails folder,
    //$data is the contents to be passed to the template
    public static function renderTemplate($template, $data){
        return view('emails.'. $template, $data)->render();
    }
    
}
