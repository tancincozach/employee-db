<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\EmailQueue;
use App\Models\User;
use App\Models\UserVerification;
use App\Models\ClientContact;
use App\Models\ACL\Role;
use App\Models\ACL\UserRole;
use App\Models\ACL\ResourceRolePermission;
use App\Models\ACL\ResourceUserRolePermission;
use Carbon\Carbon;

use App\Models\Client;

class ClientService
{

    public static function addContactUser($contacts)
    {
        foreach ($contacts as $contact) {
            DB::beginTransaction();
            try {
                if (isset($contact['is_deactivated'])) {
                    User::where('id', '=', $contact['id'])->update(['can_login'=> '0', 'is_verified'=> '0']);
                } else {
                    $user = User::where('email', '=', $contact['email'])->first();
                    $userId = $user['id'] ? $user['id'] : 0;
                    $verifiedUser = UserVerification::where('user_id', '=', $userId)
                                    ->whereNotNull('verified_at')->get()->count() ? true : false;
                    $sendVerification = $sendAdditionalCompany = false;

                    if ($userId) {
                        $user = User::find($userId);
                        $user->can_login = empty($contact['user']['can_login']) ? 0 : 1;
                        $user->updated_by_user_id = Auth::id();

                        if (!$verifiedUser && !$user->can_login) {
                            UserVerification::where('user_id', '=', $userId)
                                            ->whereRaw('verified_at IS NULL')
                                            ->whereRaw('expired_at >= NOW()')
                                            ->update(['expired_at' => DB::raw('NOW()')]);
                        } else {
                            if (!$verifiedUser) {
                                $sendVerification = true;
                            } elseif ($verifiedUser && $user->can_login) {
                                $sendAdditionalCompany = true;
                            }
                        }

                        if (!$user->save()) {
                            throw new Exception('Failed to Update User.');
                        }
                    } else {
                        // Create New user
                        $userId = self::createNewUser($contact);

                        // Create New User Role
                        $newUserRoleId = self::createNewRole($userId);

                        // Create new Resource User Role Permission
                        self::createNewResourceUserRolePermissions($newUserRoleId);

                        $sendVerification = true;
                    }
                    
                    if ($sendVerification) { // bag.o
                        // Create new User Verification
                        self::createNewUserVerifications($userId, $contact['id']);
                        // Create new Email Queue
                        self::createNewEmailQueue($contact, $userId, 0);
                    } elseif ($sendAdditionalCompany) { //ginamit
                        // Create new Email Queue
                        self::createNewEmailQueue($contact, $userId, 1);
                    } else {
                        // Create new Email Queue
                        self::createNewEmailQueue($contact, $userId, 2);
                    }
                    
                    // Update client contact
                    self::updateClientContacts($contact['id'], $userId);
                }
                
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
            }
        }
    }

    private static function createNewUser($userInfo)
    {
        $user = new User();
        $user->user_name = strtok($userInfo['email'], '@') . rand(1000, 9999);
        $user->email = $userInfo['email'];
        $user->password = User::getPasswordHashValue(Str::random(7));
        $user->user_type_id = 2;
        $user->can_login = 1;
        $user->updated_by_user_id = Auth::id();
        
        if (!$user->save()) {
            throw new Exception('Failed Creating New User.');
        }

        return $user->id;
    }

    private static function createNewRole($userId)
    {
        $user_role = new UserRole();
        $user_role->is_enabled = 1;
        $user_role->user_id = $userId;
        // Get Client Role id
        $role = Role::where('name', '=', 'client')->get()->first();
        $user_role->role_id = $role->id;
        
        if (!$user_role->save()) {
            throw new Exception('Failed Creating New Role.');
        }

        return $user_role->id;
    }

    private static function createNewResourceUserRolePermissions($userRoleId)
    {
        $role = Role::where('name', '=', 'client')->get()->first();
        $permissions = ResourceRolePermission::where('role_id', '=', $role->id)->get();

        foreach ($permissions as $permission) {
            $newPermission = new ResourceUserRolePermission();
            $newPermission->can_add = $permission->can_add;
            $newPermission->can_delete = $permission->can_delete;
            $newPermission->can_edit = $permission->can_edit;
            $newPermission->can_view = $permission->can_view;
            $newPermission->resource_id = $permission->resource_id;
            $newPermission->user_role_id = $userRoleId;

            if (!$newPermission->save()) {
                throw new Exception('Failed Creating New Permission.');
            }
        }
    }

    private static function createNewUserVerifications($userId, $contactId)
    {
        $client_contact = ClientContact::find($contactId)->first();
        $user_verification = new UserVerification();
        $user_verification->user_id = $userId;
        $user_verification->verification_token = hash('sha1', Str::random(7));
        $user_verification->verified_at = null;
        $user_verification->client_id = $client_contact->client_id;
        $user_verification->type = 'email_verification';

        $carbon = new Carbon();
        $user_verification->expired_at = $carbon->addDays(7);

        if (!$user_verification->save()) {
            throw new Exception('Failed Creating New User Verification.');
        }

        return $user_verification->id;
    }

    private static function createNewEmailQueue($contact, $userId, $type)
    {
        $email = new EmailQueue();
        $email->status_id = 0;
        $email->status_desc = "Pending Email";
        $email->subject = "Welcome To FULL SCALE!";
        $email->send_to = $contact['email'];

        $userVerification = UserVerification::where('user_id', '=', $userId)->orderBy('created_at', 'desc')->get()->first();
        $link = env('APP_URL') . '/user-verification/' .  $userVerification->verification_token . '/' . $userId . '/' . self::generateHash($userVerification->verification_token, $userId);

        if (!$type) {
            $arr = ['email' => $contact['email'], 'link' => $link];
            $email->body = EmailQueueService::renderTemplate('user-verification', $arr);
        } elseif ($type == 2) {
            $arr = ['email' => $contact['email'], 'link' => $link];
            $email->body = EmailQueueService::renderTemplate('user-verification', $arr);
        } else {
            $arr = [
                'client_name' => ClientContact::find($contact['id'])->client->company,
                'email' => $contact['email'],
                'link' => $link
            ];
            $email->body = EmailQueueService::renderTemplate('additional-client', $arr);
        }
        
        $email->save();
    }

    private static function updateClientContacts($contactId, $userId)
    {
        $client_contact = ClientContact::find($contactId);
        $client_contact->user_id = $userId;

        if (!$client_contact->save()) {
            throw new Exception('Failed Updating Client Contact.');
        }
        
        return $client_contact->id;
    }
    
    private static function generateHash($code, $i)
    {
        return hash('sha256', $code . $i);
    }

    public static function sendTeamBuilderNotification($emailContacts)
    {
        $email = new EmailQueue();
        $email->status_id = 0;
        $email->status_desc = "Pending Email";
        $email->subject = "Team Resources Adjustment";
        $email->send_to = implode($emailContacts, ',');

        $link = env('APP_URL') . '/client-wizard/team-builder-client';

        $arr = ['link' =>  $link];
        $email->body = EmailQueueService::renderTemplate('team-builder-client-notification', $arr);

        $email->save();
    }

    public static function getClientLatestPhoto(Client $client, $format = 'original')
    {
        $image = null;
        switch ($format) {
            case 'original':
                $image = $client->photo->last();
                break;
            case 'thumbnail':
                $image = $client->photo->last();
                if (!$image) {
                    return $image;
                }
                $image->path = preg_replace('/profile-photo\/(\d+)/', "profile-photo/".$client->id."/thumbnail", $image->path);
                break;
            default:
                $image = $client->photo->last();
        };

        return $image;
    }
}
