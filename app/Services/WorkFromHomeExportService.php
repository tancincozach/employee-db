<?php

namespace App\Services;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Shared\File;

use App\Models\WorkFromHome\WorkFromHomeRequest;
use DB;

class WorkFromHomeExportService
{
    public static function export($dates, $employee_ids = [])
    {
        $spreadsheet = app(Spreadsheet::class);
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setCellValue('A1', 'Report generated ' . date('F d, Y h:i A T'));
        $content = self::collection($dates, $employee_ids)->toArray();
        $currentRow = 3;
        $cols = ['employee_no', 'full_name', 'reason', 'start_date', 'end_date',];
        
        foreach ($content as $index => $resources) {
            if ($currentRow == 3) {
                foreach ($cols as $index => $attr)
                    $worksheet->setCellValueByColumnAndRow($index + 1, $currentRow, strtoupper(self::removeHtmlTags($attr)));
                $currentRow++;
            }

            foreach ($cols as $index => $attr)
                $worksheet->setCellValueByColumnAndRow($index + 1, $currentRow, self::removeHtmlTags($resources[$attr]));
            $currentRow++;
        }

        $writer = new Csv($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="work_from_home_'.$dates[0].'-'.$dates[1].'.csv"');
        $writer->save("php://output");
    }

    public static function collection($dates, $employee_ids)
    {
        return WorkFromHomeRequest::select('work_from_home_requests.*')
        ->leftJoin('employees', 'employees.id', '=', 'work_from_home_requests.employee_id')
        ->where(function ($query) use ($employee_ids) {
            if(!empty($employee_ids))
                $query->whereIn('employee_id', explode(',', $employee_ids));
        })
        ->where(function ($query) use ($dates) {
          $query->orWhere(function ($q) use ($dates) {
              $q->whereBetween('work_from_home_requests.start_date', $dates);
              $q->orWhereBetween('work_from_home_requests.end_date', $dates);
          });
        })
        ->orderBy('work_from_home_requests.updated_at')
        ->addSelect(DB::raw('CONCAT(employees.first_name, " ", employees.last_name) as full_name'))
        ->addSelect(DB::raw('employees.employee_no'))
        ->get();
    }

    private static function removeHtmlTags($str) {
      $str = preg_replace('#<[^>]+>#', ' ', $str);
      $str = preg_replace('/\n+|\t+|\s+/',' ', $str);
      return $str;
    }
}
