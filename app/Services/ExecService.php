<?php

namespace App\Services;

/**
 * A way to keep track of external processes. Execute a process in php and control of the process.
 *
 * Class ExecService
 * @package App\Services
 * @compantibility Linux only
 */
class ExecService
{
    /**
     * @var int
     */
    private $pid;

    /**
     * @var string|bool
     */
    private $command;

    /**
     * ExecService constructor.
     * @param bool $command
     */
    public function __construct($command = false)
    {
        if ($command != false) {
            $this->command = $command;
            $this->run();
        }
    }

    public function run()
    {
        // /dev/null will execute the command in the background without PHP waiting for it to finish
        $command = $this->command . '> /dev/null 2>&1 & echo $!';
        exec($command, $output);
        $this->pid = intval($output[0]);
    }

    /**
     * @param $pid
     */
    public function setPid($pid)
    {
        $this->pid = $pid;
    }

    /**
     * @return int
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @return bool
     */
    public function status()
    {
        $command = 'ps -p ' . $this->pid;
        exec($command, $output);
        return ! isset($output[1]) ? false : true;
    }

    /**
     * @return bool
     */
    public function start()
    {
        if ($this->command != '')
        {
            $this->run();
        }

        return true;
    }

    /**
     * @return bool
     */
    public function stop()
    {
        $command = 'kill ' . $this->pid;
        exec($command);

        if ($this->status() == false) {
            return true;
        } else {
            return false;
        }
    }
}
