<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\PerformEvalResponse;
use App\Models\PerformEvalQuestion;
use App\Models\Employee;
use App\Models\Client;
use App\Models\ClientContact;
use App\Models\EmailQueue;
use App;

class PerformEvalResponseService
{
    public $emailBodyContent = '';
    private $emailSubject = '';

    public function submitResponse($data)
    {
        $success = true;
        $responses = $data['responses'];

        DB::beginTransaction();
        foreach ($responses as $key => $response)
        {
            $evalResponse = PerformEvalResponse::create([
                'employee_id' => $data['employeeId'],
                'client_id' => $data['clientId'],
                'evaluator_id' => $data['evaluatorId'],
                'perform_eval_question_id' => $response['question']['id'],
                'perform_eval_question_form_id' => $response['id'],
                'perform_eval_criteria_id' => $response['criteria']['id'],
                'response_value' => $response['value']
            ]);

            if (empty($evalResponse)) {
                $success = false;
                DB::rollback();
            }
        }
        DB::commit();

        return $success;
    }

    public function setEmailData($data)
    {
        $header = $questJobDefinitions = $questPerformRatings = $questPerformSummaries = [];
        $responses = $data['responses'];
        $employee = Employee::find($data['employeeId']);
        $client = Client::find($data['clientId']);
        $positions = array_map(function($p) {
            return $p['position']['job_title'];
        }, $employee->positions->toArray());

        $evalName = 'Unknown User';// user that doesn't have employee id or client contact id
        $evaluator = Employee::where('user_id', $data['evaluatorId'])->first();
        if ($evaluator) {
            $evalName = $evaluator->getFullNameFirst();
        } else {
            $clientContact = ClientContact::where('user_id', $data['evaluatorId'])->first();
            if ($clientContact) {
                $evalName = $clientContact->name;
            }
        }

        $empName = $employee->getFullNameFirst();
        $this->emailSubject = $empName . ' Performance Review by ' . $evalName;

        $header = [
            'employee_name' => $employee->getName(),
            'client' => $client->company,
            'year_period' => [],
            'title' => implode(', ', $positions),
        ];

        foreach ($responses as $response) {
            if ($response['page'] == 1) {
                $header['year_period'][$response['id']] = date('m/d/Y', strtotime($response['value']));
            }

            if ($response['page'] == 2) {
                $questJobDefinitions[] = [
                    'question' => $response['question']['label'],
                    'answer' => $response['value'],
                ];
            }

            if ($response['page'] == 4 || $response['page'] == 5) {
                if (empty($questPerformRatings[$response['id']])) {
                    $questPerformRatings[$response['id']] = [
                        'question' => $response['question']['label'],
                    ];
                }

                $answerType = 'ratings';
                if ($response['criteria']['type'] != 'radio') {
                    $answerType = 'explain';
                }

                $questPerformRatings[$response['id']][$answerType] = $response['value'];
            }

            if ($response['page'] == 6) {
                $questPerformSummaries[] = [
                    'question' => $response['question']['label'],
                    'answer' => $response['value'],
                ];
            }
        }

        return compact('header', 'questJobDefinitions', 'questPerformRatings', 'questPerformSummaries');
    }

    public function sendEmail()
    {
        // production testing
        $to = [
            'deco@fullscale.io',
            'darrell@fullscale.io',
            'keseo@fullscale.io',
            'rnacu@fullscale.io',
            'ccabang@fullscale.io',
        ];

        if (strtolower(trim(App::Environment())) != 'prod') {
            $to = [
                'rnacu@fullscale.io',
                'gbobilles@fullscale.io',
                'rpepito@fullscale.io',
                'july@fullscale.io',
                'reynaldo@fullscale.io',
            ];
        }

        EmailQueue::create([
            'subject' => $this->emailSubject,
            'send_to' => implode($to, ','),
            'body' => $this->emailBodyContent,
        ]);
    }
}
