<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use App\Models\Notification;
use App\Models\NotificationUser;
use App\Models\NotificationFlag;
use App\Events\ClientOnboarded;
use App\Events\NewEmployee;
use App\Events\ClientSelectedResources;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;
use Carbon\Carbon;

class NotificationService
{
    /**
     * Notification configs
     * 
     * @var array
     */
    private static $config = [
        ClientOnboarded::class => [
            'multiple' => '<b>:num new clients</b> have onboarded.',
            'icon'     => 'la-building',
        ],
        NewEmployee::class => [
            'multiple' => '<b>:num new employees</b> have onboarded.',
            'icon'     => 'la-user-plus',
        ],
        ClientSelectedResources::class => [
            'multiple' => '<b>:num clients</b> have made changes on the preferred resources.',
            'icon'     => 'la-group',
        ],
    ];

    /**
     * Send notification to specified users
     * 
     * @param Illuminate\Database\Eloquent\Collection $users
     * @param array $data
     * @param string $type
     * @return void
     */
    public static function send(Collection $users, array $data, $type = '')
    {
        $notification = Notification::createNotification($data, $type);

        $users->each(function ($user) use ($notification) {
            NotificationUser::assignNotification($notification->id, $user->id);

            $incrementCount = !NotificationUser::checkNotificationCount($user->id);
            if ($incrementCount) {
                NotificationFlag::setHasNotification($user->id);
            }
        });
    }

    /**
     * Mark as read notifications
     * 
     * @param array $data
     * @param integer $userId
     * @return void
     */
    public static function markAsRead(array $data, $userId)
    {
        NotificationUser::markAsRead($data, $userId);
    }

    /**
     * Open notifications
     * 
     * @param integer $userId
     * @return void
     */
    public static function openNotification($userId)
    {
        NotificationFlag::setHasNotification($userId, true);
    }

    /**
     * Format notifications
     * 
     * @param Illuminate\Database\Eloquent\Collection $notifications
     * @return array
     */
    public static function format(Collection $notifications)
    {
        $result = $notifications->groupBy('type')->map(function ($notification) {
            $isGrouped = $notification->count() > 1;
            if (!$isGrouped) {
                return self::item($notification);
            }

            $groupRead = $notification->groupBy('read_at')->first();
            $isUnreadGrouped = $groupRead->count() > 1;
            if (!$isUnreadGrouped) {
                return self::item($groupRead);
            } else {
                return self::grouped($groupRead, $groupRead->count());
            }
        })->values();

        return self::paginate($result, 50);
    }

    /**
     * Grouped notifications
     * 
     * @param Illuminate\Database\Eloquent\Collection $notification
     * @param integer $count
     * @return Illuminate\Database\Eloquent\Collection
     */
    private static function grouped(Collection $notification, $count)
    {
        $ids = $notification->pluck('notification_id');
        $grouped = collect($notification->first());
        $type = $grouped->get('type');
        $created = $grouped->get('created_at');
        $created = Carbon::createFromFormat('Y-m-d H:i:s', $created);
        $separator = self::separator($created);
        $created = self::formatDiffForHumans($created->diffForHumans());
        $data = json_decode($grouped->get('data'));
        $target = self::$config[$type]['multiple'];
        $content = preg_replace('/:num/', $count, $target);
        $route = self::route($type, $data, true);

        $grouped->put('content', $content);
        $grouped->put('redirect', $route['uri']);
        $grouped->put('uri_params', $route['params']);
        $grouped->put('icon', self::$config[$type]['icon']);
        $grouped->put('created', $created);
        $grouped->put('separator', $separator);
        $grouped->put('is_grouped', 1);
        $grouped->put('ids', $ids);
        $grouped->forget('notification_id');
        $grouped->forget('data');
        $grouped->forget('updated_at');
        $grouped->forget('deleted_at');

        return $grouped;
    }

    /**
     * Individual notification
     * 
     * @param Illuminate\Database\Eloquent\Collection $notification
     * @return Illuminate\Database\Eloquent\Collection
     */
    private static function item(Collection $notification)
    {
        $item = collect($notification->first());
        $id = $item->get('notification_id');
        $data = json_decode($item->get('data'));
        $type = $item->get('type');
        $created = $item->get('created_at');
        $created = Carbon::createFromFormat('Y-m-d H:i:s', $created);
        $separator = self::separator($created);
        $created = self::formatDiffForHumans($created->diffForHumans());
        $content = self::itemContent($type, $data);
        $route = self::route($type, $data);

        $item->put('id', $id);
        $item->put('data', $data);
        $item->put('content', $content);
        $item->put('redirect', $route['uri']);
        $item->put('uri_params', $route['params']);
        $item->put('icon', self::$config[$type]['icon']);
        $item->put('created', $created);
        $item->put('separator', $separator);
        $item->forget('notification_id');
        $item->forget('updated_at');
        $item->forget('deleted_at');

        return $item;
    }

    /**
     * Set notification route
     * 
     * @param string $type
     * @param array  $data
     * @param bool   $isGrouped
     * @return array
     */
    private static function route($type, $data, $isGrouped = false)
    {
        switch ($type) {
            case ClientOnboarded::class:
                return $isGrouped ? 
                    [ 'uri' => $data->redirect_uri, 'params' => []] : 
                    [ 'uri' => $data->redirect_uri, 'params' => []];

            case NewEmployee::class:
                return $isGrouped ? 
                    [ 'uri' => 'employees', 'params' => []] : 
                    [ 'uri' => 'employee', 'params' => ['id' => $data->id]];

            case ClientSelectedResources::class:
                return $isGrouped ? 
                    [ 'uri' => 'clients', 'params' => []] : 
                    [ 'uri' => 'client', 'params' => ['id' => (string)$data->id]];

            default: 
                return [];
        }
    }

    /**
     * Set content for individual notification
     * 
     * @param string $type
     * @param array  $data
     * @return string
     */
    private static function itemContent($type, $data)
    {
        switch ($type) {
            case ClientOnboarded::class:
                return '<b>'.$data->company.'</b> has onboarded.';

            case NewEmployee::class:
                return 'New Employee <b>'.$data->employee.'</b> has onboarded.';

            case ClientSelectedResources::class:
                return '<b>'.$data->company.'</b> has made changes on the preferred resources.';

            default: 
                return '';
        }
    }

    /**
     * Date separator
     * 
     * @param Carbon\Carbon $date
     * @return string
     */
    private static function separator(Carbon $date)
    {
        return $date->isToday() ? 'Today' : (
            $date->isYesterday() ? 'Yesterday' : $date->format('m/d')
        );
    }

    /**
     * Format human readable date
     * 
     * @param string $date
     * @return string
     */
    private static function formatDiffForHumans($date)
    {
        return Str::contains($date, 'seconds ago') ? 'Just now' : $date;
    }

    /**
     * Paginate standard laravel collection
     * 
     * @param Illuminate\Database\Eloquent\Collection $collection
     * @param integer $perPage
     * @return Illuminate\Pagination\LengthAwarePaginator
     */
    public static function paginate($collection, $perPage)
    {
        $page = LengthAwarePaginator::resolveCurrentPage('page');

        return new LengthAwarePaginator(
            $collection->forPage($page, $perPage),
            $collection->count(),
            $perPage,
            $page,
            [
                'path' => LengthAwarePaginator::resolveCurrentPath(),
                'pageName' => 'page',
            ]
        );
    }

    /**
     * Spell out number to words
     * 
     * @param integer $number
     * @return string
     */
    protected static function numToWords($number)
    {
        $formatter = new \NumberFormatter('en', \NumberFormatter::SPELLOUT);
        return $formatter->format($number);
    }
}
