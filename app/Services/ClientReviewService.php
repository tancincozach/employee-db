<?php

namespace App\Services;

use Validator;
use App\Models\Client;
use App\Models\ClientContact;
use App\Traits\ModelTrait;
use App\Models\ClientProject;
use App\Models\ClientTeambuilderBucket;
use Illuminate\Support\MessageBag;

class ClientReviewService
{
    use ModelTrait;

    protected $client_details = [];

    protected $client_contacts = [];

    protected $project = [];

    protected $suggested_resource = [];

    protected $client_access = [];

    public function __construct(array $request)
    {
        $this->client_details     = $request['client_details'];
        $this->client_contacts    = $request['client_contacts'];
        $this->project            = $request['project'];
        $this->suggested_resource = $request['suggested_resource'];
        $this->client_access      = $request['client_access'];
    }

    public function updateClientDetails()
    {
        $this->model = app(Client::class);
        $id = $this->client_details['id'];
        $action = 'update_client_details';
        $this->rules = [
            'company' => 'sometimes|required|unique:clients,company,' . $id,
        ];

        try {
            $this->client_details['reviewed'] = 1;
            $result = $this->update($this->client_details);
            if ($result instanceof MessageBag) {
                $this->setResponse($action, $result->all(), 'error');
            } else {
                $this->setResponse($action, $result);
            }
        } catch (\Exception $e) {
            $this->setResponse($action, $e->getMessage(), 'error');
        }
        return $this;
    }

    public function updateClientContacts()
    {
        $this->model = app(ClientContact::class);
        $primary = $this->primaryKey();
        $client_id = $this->client_details[$primary];

        foreach ($this->client_contacts as $contact) {
            $contact['client_id'] = $client_id;
            if (is_null($contact[$primary])) {
                $this->createContact($contact);
            } else {
                $this->updateContact($contact);
            }
        }
        return $this;
    }
    
    private function createContact(array $contact)
    {
        $action = 'add_client_contact_' . $contact['email'];
        $this->rules = [
            'client_id' => 'sometimes|required',
            'name' =>      'sometimes|required',
            'email' =>     'sometimes|required',
        ];

        try {
            $result = $this->create($contact);
            if ($result instanceof MessageBag) {
                $this->setResponse($action, $result->all(), 'error');
            } else {
                $this->setResponse($action, $result);
            }
        } catch (\Exception $e) {
            $this->setResponse($action, $e->getMessage(), 'error');
        }
    }

    private function updateContact(array $contact)
    {
        $action = 'update_client_contact_' . $contact['email'];
        $this->rules = [
            'client_id' => 'sometimes|required',
            'name' =>      'sometimes|required',
            'email' =>     'sometimes|required',
        ];

        try {
            $result = $this->update($contact);
            if ($result instanceof MessageBag) {
                $this->setResponse($action, $result->all(), 'error');
            } else {
                $this->setResponse($action, $result);
            }
        } catch (\Exception $e) {
            $this->setResponse($action, $e->getMessage(), 'error');
        }
    }

    public function setupProject()
    {
        $this->model = app(ClientProject::class);
        $action = 'add_client_project';
        $client_id = $this->client_details['id'];
        $this->rules = [
            'client_id' =>    'sometimes|required',
            'project_name' => 'sometimes|required',
            'status_id' =>    'sometimes|required',
        ];
        
        try {
            $this->project['client_id'] = $client_id;
            $result = $this->create($this->project);
            if ($result instanceof MessageBag) {
                $this->setResponse($action, $result->all(), 'error');
            } else {
                $this->setResponse($action, $result);
            }
        } catch (\Exception $e) {
            $this->setResponse($action, $e->getMessage(), 'error');
        }
        return $this;
    }

    public function assignSuggestedResource()
    {
        $this->model = app(ClientTeambuilderBucket::class);
        $this->rules = [
            'client_id'         => 'sometimes|required',
            'employee_id'       => 'sometimes|required',
            'job_position_id'   => 'sometimes|required',
            'status'            => 'sometimes|required',
        ];

        foreach ($this->suggested_resource as $resource) {
            $isFound = $this->model->where('client_id', $resource['client_id'])->where('employee_id', $resource['employee_id'])->first();
            
            if ($isFound){
                $resource['id'] = $isFound['id'];
                $this->updateSuggestedResource($resource);
            }
            else {
                $this->saveSuggestedResource($resource);
            }
        }
        return $this;
    }

    private function saveSuggestedResource(array $resource)
    {
        $action = 'create_suggested_resource';
        try {
            $result = $this->create($resource);

            // If encounters an error during creation
            if ($result instanceof MessageBag) {
                $this->setResponse($action, $result->all(), 'error');
            } else {
                $this->setResponse($action, $result);
            }
        } catch (\Exception $e) {
            $this->setResponse($action, $e->getMessage(), 'error');
        }
    }


    private function updateSuggestedResource(array $resource)
    {
        $action = 'update_suggested_resource';
        try {
            $result = $this->update($resource);

            // If encounters an error during creation
            if ($result instanceof MessageBag) {
                $this->setResponse($action, $result->all(), 'error');
            } else {
                $this->setResponse($action, $result);
            }
        } catch (\Exception $e) {
            $this->setResponse($action, $e->getMessage(), 'error');
        }
    }

    public function sendClientAccess()
    {
        $filtered = array_filter($this->client_contacts, function ($contact) {
            return in_array($contact['email'], $this->client_access);
        });

        $client_access = array_map(function ($contact) {
            $contact['user'] = ['can_login' => true, 'id' => 0];
            return $contact;
        }, $filtered);
        
        ClientService::addContactUser($client_access);
        
        return $this;
    }
}
