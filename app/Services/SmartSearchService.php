<?php

namespace App\Services;

use App\Repositories\Employee\EmployeeRepository;
use App\Repositories\Skill\SkillRepository;
use App\Repositories\Client\ClientRepository;

use App\Criterias\Employee\SearchByNameOrFullName;
use App\Criterias\Employee\EmployeeOnly;
use App\Criterias\Employee\WithStatus;
use App\Criterias\Employee\Select;

use App\Criterias\Employee\FilterByStatus;
use App\Criterias\Skill\SearchSkill;
use App\Criterias\Client\SearchByCompanyName;

use League\Fractal\Resource\Collection;
use App\Transformers\AssetTransformer;
use App\Services\EmployeeService;
use App\Services\ClientService;

use App\Models\Employee;
use App\Models\Status;

class SmartSearchService
{
    protected $query;
    public $result;

    public function __construct($query, $limit)
    {
        $this->query = $query;
        $this->limit = $limit; // Limit of items per resource;
        $this->result = [];
        $this->activeStatuses = [
            'New',
            'Hired',
            'Probationary',
            'Regular'
        ];
    }

    public static function run($args)
    {
        $service = new self($args['query'], $args['limit']);
        if (!empty($args['query'])) {
            $service->searchMultipleResources();
        }
        return $service;
    }

    public function searchMultipleResources()
    {
        $employeeRepository = $this->employeeFilter();

        $skillRepository = $this->skillFilter();

        $clientRepository = $this->clientFilter();

        $collection = array_merge(
            $employeeRepository->paginate($this->limit)->items(),
            $clientRepository->paginate($this->limit)->items(),
            $skillRepository->paginate($this->limit)->items()
        );

        $this->result = $this->serializeCollection($collection);
    }

    private function clientFilter()
    {
        $clientRepository = \App::make(ClientRepository::class);
        $clientRepository->pushCriteria(new SearchByCompanyName($this->query));

        return $clientRepository;
    }

    private function skillFilter()
    {
        $skillRepository = \App::make(SkillRepository::class);
        $skillRepository->pushCriteria(new SearchSkill($this->query));

        return $skillRepository;
    }

    private function employeeFilter()
    {
        $statusIds = Status::whereIn('name', $this->activeStatuses)->get()->modelKeys();
        $employeeRepository = \App::make(EmployeeRepository::class);
        $employeeRepository->pushCriteria(new SearchByNameOrFullName($this->query));
        $employeeRepository->pushCriteria(new EmployeeOnly());
        $employeeRepository->pushCriteria(new FilterByStatus($statusIds)); //probationary & regular

        $employeeRepository->pushCriteria(new WithStatus());
        $employeeRepository->pushCriteria(new Select());

        return $employeeRepository;
    }

    private function serializeCollection($collection)
    {
        return collect($collection)->map(function ($model) {
            return [
                'id' => $model->id,
                'class' => class_basename($model),
                'name' => $this->getSearchName($model),
                'photo' => $this->getPhoto($model)
            ];
        });
    }

    private function getSearchName($model)
    {
        $searchName = '';
        switch (class_basename($model)) {
            case 'Employee':
                $searchName = $model->getName();
                break;
            case 'Client':
                $searchName = $model->company;
                break;
            case 'Skill':
                $searchName = $model->name;
                break;
        }
        return $searchName;
    }

    private function getPhoto($model)
    {
        $photo = '';
        switch (class_basename($model)) {
            case 'Employee':
                $photo = ['data' => array_filter([EmployeeService::getEmployeeLatestPhoto($model, 'thumbnail')])]; //Just return the latest
                break;
            case 'Client':
                $photo = ['data' => array_filter([ClientService::getClientLatestPhoto($model, 'thumbnail')])];
                break;
            case 'Skill':
                $photo = '';
                break;
        }
        return $photo;
    }
}
