<?php

namespace App\Services;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Shared\File;

use App\Models\JobcodeAssignment;

class TimesheetExportService
{
    public static function export()
    {
        $spreadsheet = app(Spreadsheet::class);
        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->setCellValue('A1', 'Report generated ' . date('F d, Y h:i A T'));

        $content = self::collection()->groupBy('jobcode_id')->toArray();
        $currentRow = 3;

        foreach ($content as $resources) {
            $total = count($resources);
            $client = $resources[0]['client'] . " ({$total})";
            $worksheet->setCellValueByColumnAndRow(1, $currentRow, $client);
            $worksheet->getStyleByColumnAndRow(1, $currentRow)->applyFromArray([
                'font' => ['bold' => true],
            ]);
            $currentRow++;

            foreach ($resources as $resource) {
                $worksheet->setCellValueByColumnAndRow(1, $currentRow, $resource['employee']);
                $currentRow++;
            }

            $currentRow++;
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="timesheet.xlsx"');
        $writer->save("php://output");
    }

    public static function collection()
    {
        return JobcodeAssignment::select(
            'jobcode_assignments.id AS id',
            'jobcode_assignments.user_id AS user_id',
            'jobcode_assignments.jobcode_id AS jobcode_id',
            \DB::raw("CONCAT(tu.last_name, ', ', tu.first_name) AS employee"),
            'tj.name AS client'
        )
        ->leftJoin('timesheet_users AS tu', 'tu.id', '=', 'jobcode_assignments.user_id')
        ->leftJoin('timesheet_jobcodes AS tj', 'tj.id', '=', 'jobcode_assignments.jobcode_id')
        ->where('jobcode_assignments.active', 1)
        ->orderBy('tj.name')
        ->orderBy('tu.last_name')
        ->get();
    }
}
