<?php


namespace App\Services\ThirdParty;

use Carbon\Carbon;
// use App\Services\HackerRankService;
use Illuminate\Support\Facades\Log;
use Dingo\Api\Http\Request;
use App\Models\HackerRankTest;
use App\Jobs\SaveHackerRankResultToDBJob;
use App\Jobs\SaveHackerRankTestToDBJob;


class HackerRank extends APIService
{
    /*
     * Tsheets API
     *
     * @var string
     */
    const BASE_URI = 'https://www.hackerrank.com';

    /*
     * The class constructor
     *
     * @param string $startDate
     * @param string $endDate
     * @param bool   $onlyModified
     * @return void
     */
    public function __construct()
    {

        $this->baseUri = env('3RDPARTY_HACKERRANK_ENDPOINT', self::BASE_URI);
        $this->setHeaders(['Authorization' => 'Bearer ' . env('3RDPARTY_HACKERRANK_APIKEY', '')]); // @todo: default value must be empty once debugging is done

        parent::__construct();
        
    }

    public function getAllTests()
    {
        $page_end = false;
        $final_result = array();
        $limit = 100;
        $offset = 0;
        $ctr = 0;

        $response = $this->client->request('GET', '/x/api/v3/tests?limit='.$limit.'&offset='.$offset, ['verify' => false]);
        $results = json_decode($this->getResponse());

        while (!$page_end) {
            if ($ctr > 0) {
                $res2 = $this->client->request('GET', $temp_res, ['verify' => false]);
                $results2 = json_decode($this->getResponse());

                foreach($results2->data as $test) {
                    $result = array(
                        'id' => $test->id,
                        'unique_id' => $test->unique_id,
                        'name' => $test->name,
                        'tag'  => $test->tags
                    );
                    $final_result[] = $result;
                }
                
                $temp_res = str_replace($this->baseUri, '',$results2->next);

                if ($results2->next == '') {
                    $page_end = true;
                } 

            } else {
                foreach($results->data as $test) {
                    $result = array(
                        'id' => $test->id,
                        'unique_id' => $test->unique_id,
                        'name' => $test->name,
                        'tag'  => $test->tags
                    );
                    $final_result[] = $result;
                }

                $temp_res = str_replace($this->baseUri, '',$results->next);

                if ($results->next == '') {
                    $page_end = true;
                }
            }
            $ctr++;
        }
        return $final_result;
    }

    public function getAllTestResultsByKeyword($keyword)
    {
        $final_result = array();
        $limit = 100;
        $offset = 0;
        $tests = HackerRankTest::all();

        foreach($tests as $test) {
            $result = $this->getTestResultsTestByIdAndKeyword($test->test_id, $keyword);
            $final_result = array_merge($final_result, $result);
        }
        return $final_result;
    }

    public function getAllTestResultsByTestId($testId)
    {
        $page_end = false;
        $final_result = array();
        $limit = 100;
        $offset = 0;
        $ctr = 0;
        

        $res = $this->client->request('GET', '/x/api/v3/tests/'.$testId.'/candidates/?limit='.$limit.'&offset='.$offset, ['verify' => false]);
        $results = json_decode($this->getResponse());

        while (!$page_end) {
            if ($ctr > 0) {

                $res2 = $this->client->request('GET', $temp_res, ['verify' => false]);
                $results2 = json_decode($this->getResponse());

                foreach($results2->data as $result) {
                    $final_result[] = $result;
                }

                $temp_res = str_replace($this->baseUri, '',$results2->next);

                if ($results2->next == '') {
                    $page_end = true;
                }              
            } else {

                foreach($results->data as $result) {
                    $final_result[] = $result;
                }

                $temp_res = str_replace($this->baseUri, '',$results->next);

                if ($results->next == '') {
                    $page_end = true;
                }
            }
            $ctr++;
        }
        return $final_result;
    }

    public function checkTestsNoData()
    {
        $hackerTests = HackerRankTest::all();

        if (count($hackerTests) <= 0) {
            return true;
        }
        return false;
    }

    public function getAllTestResults() 
    {
        $final_result = array();
        $limit = 100;
        $offset = 0;

        $tests = HackerRankTest::all();

        foreach($tests as $test) {
            $result = $this->getAllTestResultsByTestId($test->test_id);
            $final_result = array_merge($final_result, $result);
        }
        return $final_result;
    }

    public function getTestResultsTestByIdAndKeyword($id, $keyword)
    {
        $page_end = false;
        $final_result = array();
        $limit = 100;
        $offset = 0;
        $ctr = 0;
        

        $res = $this->client->request('GET', '/x/api/v3/tests/'.$id.'/candidates/search?search='.$keyword.'&limit='.$limit.'&offset='.$offset, ['verify' => false]);
        $results = json_decode($this->getResponse());

        while (!$page_end) {
            if ($ctr > 0) {

                $res2 = $this->client->request('GET', $temp_res, ['verify' => false]);
                $results2 = json_decode($this->getResponse());

                foreach($results2->data as $result) {
                    $final_result[] = $result;
                }

                $temp_res = str_replace($this->baseUri, '',$results2->next);

                if ($results2->next == '') {
                    $page_end = true;
                }              
            } else {

                foreach($results->data as $result) {
                    $final_result[] = $result;
                }

                $temp_res = str_replace($this->baseUri, '',$results->next);

                if ($results->next == '') {
                    $page_end = true;
                }
            }
            $ctr++;
        }
 
        return $final_result;
    }
    /**
     * Executes the retrieving -> saving of all Hacker Test Result data until the page no longer returns data
     */
    public function retrieveAndStoreTestResults()
    {
        set_time_limit(0); //We cannot determine the exact time it spends fetching records from hacker ranks.
        try {
            $all_result = $this->getAllTestResults();

            foreach($all_result as $data_result) {
                $this->storeTestResult($data_result);
            }
        } catch (Exception $e) {
            return Log::info("Failed to process Hacker Rank Api on '". Carbon::now() ."'");
        }
    }
    /**
     * Executes the retrieving -> saving of all Hacker Test data until the page no longer returns data
     */
    public function retrieveAndStoreTests()
    {
        set_time_limit(0); //We cannot determine the exact time it spends fetching records from hacker ranks.
        try {
            $all_result = $this->getAllTests();
            $existingTest = HackerRankTest::all();
            $testIdToDelete = '';
            foreach($existingTest as $testExists) {
                $notfound = false;
                foreach($all_result as $data_result) {
                    if ($testExists->test_id == $data_result) {
                        $notfound = true;
                    }
                }
                if(!$notfound) {
                    $testIdToDelete = $testExists->id;
                }
            }
            if(!empty($testIdToDelete)) {
                $qryResult = HackerRankTest::find($testIdToDelete);
                $qryResult->delete();
            }
            foreach($all_result as $data_result) {
               $this->storeTest($data_result);
            }
        } catch (Exception $e) {
            return Log::info("Failed to process Hacker Rank Api on '". Carbon::now() ."'");
        }
    }


    /**
     * Saves the Hacker Test Result API data to the database
     */
    private function storeTestResult($data_result)
    {
        try {
            SaveHackerRankResultToDBJob::dispatch($data_result,true);
        } catch (Exception $e) {
            return Log::info("Failed to save Hacker Rank Test Result Data.");
        }
    }

    /**
     * Saves the Hacker Test API data to the database
     */
    private function storeTest($data_result)
    {
        try {
            SaveHackerRankTestToDBJob::dispatch($data_result,true);
        } catch (Exception $e) {
            return Log::info("Failed to save Hacker Rank Test Data.");
        }
    }

}
