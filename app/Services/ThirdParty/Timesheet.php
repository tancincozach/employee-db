<?php

namespace App\Services\ThirdParty;

use Carbon\Carbon;
use App\Jobs\GenerateRawTimesheets;
use App\Jobs\GenerateRawJobcodeAssignments;

use App\Models\Process;

use App\Repositories\Timesheet\TimesheetRepository;
use App\Criterias\Timesheet\ByJobcode;

// Weekly
use App\Criterias\Timesheet\Weekly;

// Monthly
use App\Criterias\Timesheet\Monthly;
use App\Criterias\Timesheet\ByMonthYear;

class Timesheet extends APIService
{
    /*
     * Timesheets API base uri
     *
     * @var string
     */
    const BASE_URI = 'https://rest.tsheets.com/api/v1/';

    /*
     * Authorization token
     *
     * @var string
     */
    protected $token;

    public function __construct()
    {
        parent::__construct([
            'base_uri' => self::BASE_URI
        ]);

        $this->token = getenv('API_TSHEETS_AUTH_BEARER');
    }

    /*
     * Retrieve timesheets per page
     *
     * @param array $query
     *
     * @return array
     */
    public function retrievePagedTimesheets(array $query)
    {
        try {
            $response = $this->client->request('GET', 'timesheets', [
                'headers' => [
                    'Authorization' => 'Bearer ' . ($this->token ?? ''),
                    'Accept'        => 'application/json',
                ],
                'query' => $query,
            ]);

            $result = json_decode($response->getBody(), true);
            $timesheets = [];

            if (!empty($result['results']['timesheets'])) {
                $timesheets = [
                    'timesheets' => $result['results']['timesheets'],
                    'jobcodes'   => $result['supplemental_data']['jobcodes'],
                    'users'      => $result['supplemental_data']['users'],
                ];
            }

            return $timesheets;
        } catch (Exception $e) {
            return Log::info('Failed to retrieve timesheets on' . Carbon::now());
        }
    }

    /*
     * Retrieve timesheets
     *
     * @param array $query
     *
     * @return array
     */
    public function retrieveTimesheets(array $query)
    {
        $process = app(Process::class);
        $process->start('Manual Timesheets Generation', json_encode($query));

        $page = 1;

        while (!empty($raw = $this->retrievePagedTimesheets($query))) {
            dispatch(new GenerateRawTimesheets($raw));

            $page++;
            $query['page'] = $page;
        }

        $process->finish();
    }

    /*
     * Get process running
     *
     * @return Collection
     */
    public static function getProcessRunning()
    {
        return Process::where('name', 'Manual Timesheets Generation')->latest()->first();
    }

    /*
     * Retrieve jobcode assignments per page
     *
     * @param array $query
     *
     * @return array
     */
    public function retrievePagedJobcodeAssignments(array $query)
    {
        try {
            $response = $this->client->request('GET', 'jobcode_assignments', [
                'headers' => [
                    'Authorization' => 'Bearer ' . ($this->token ?? ''),
                    'Accept'        => 'application/json',
                ],
                'query' => $query,
            ]);

            $result = json_decode($response->getBody(), true);
            $jobcodeAssignments = [];

            if (!empty($result['results']['jobcode_assignments'])) {
                $jobcodeAssignments = $result['results']['jobcode_assignments'];
            }

            return $jobcodeAssignments;
        } catch (Exception $e) {
            return Log::info('Failed to retrieve jobcode assignments on' . Carbon::now());
        }
    }

    /*
     * Retrieve jobcode assignments
     *
     * @param array $query
     *
     * @return array
     */
    public function retrieveJobcodeAssignments(array $query)
    {
        $page = 1;

        while (!empty($raw = $this->retrievePagedJobcodeAssignments($query))) {
            dispatch(new GenerateRawJobcodeAssignments($raw));

            $page++;
            $query['page'] = $page;
        }
    }

    public static function getWeekly(array $params)
    {
        $jobcodeId = $params['job_code_id'];
        $start = $params['start'];
        $end = $params['end'];
        $timesheetRepository = \App::make(TimesheetRepository::class);
        $timesheetRepository->pushCriteria(new Weekly($jobcodeId, $start, $end));
        return $timesheetRepository->get();
    }

    public static function getMonthly(array $params)
    {
        // Required filters
        $jobcodeId = $params['job_code_id'];
        $month = $params['month'];
        $year = $params['year'];

        $timesheetRepository = \App::make(TimesheetRepository::class);

        $timesheetRepository->pushCriteria(new ByJobcode([$jobcodeId]));
        $timesheetRepository->pushCriteria(new Monthly());
        $timesheetRepository->pushCriteria(new ByMonthYear($month, $year));

        return $timesheetRepository->get();
    }
}
