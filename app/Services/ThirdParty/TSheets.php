<?php

namespace App\Services\ThirdParty;

use Carbon\Carbon;
use App\Services\TSheetParserService;
use Dingo\Api\Http\Request;
use App\Jobs\SaveParsedToDBJob;
use Illuminate\Support\Facades\Log;
use App\Models\Process;

class TSheets extends APIService
{
    /*
     * Tsheets API
     *
     * @var string
     */
    const BASE_URI = 'https://rest.tsheets.com/api/v1/';

    /*
     * The starting date
     *
     * @var string
     */
    protected $startDate;

    /*
     * The ending date
     * @var string
     */
    protected $endDate;

    /*
     * Only modified flag
     *
     * @var bool
     */
    protected $onlyModified;

    /*
     * The class constructor
     *
     * @param string $startDate
     * @param string $endDate
     * @param bool   $onlyModified
     * @return void
     */
    public function __construct($startDate, $endDate, $onlyModified = true)
    {
        $this->onlyModified = $onlyModified;
        $this->startDate    = $startDate;
        $this->endDate      = $endDate;

        parent::__construct([
            'base_uri' => self::BASE_URI
        ]);
    }

    /*
     * Get any timesheets
     *
     * @param int $page
     * @return mixed
     */
    public function getAny($page = 1)
    {
        $query = [
            'start_date' => $this->startDate,
            'end_date'   => $this->endDate,
            'page'       => $page,
        ];

        return $this->requestClient($query);
    }

    /*
     * Get modified timesheets
     *
     * @param int $page
     * @return mixed
     */
    public function getModified($page = 1)
    {
        $startDate = Carbon::createFromFormat('Y-m-d H:i:s',
            $this->startDate.' 00:00:00'
        )->format('c');

        $endDate = Carbon::createFromFormat('Y-m-d H:i:s',
            $this->endDate.' 23:59:59'
        )->format('c');

        $query = [
            'modified_since'  => $startDate,
            'modified_before' => $endDate,
            'page'            => $page,
        ];

        return $this->requestClient($query);
    }

    public function requestClient($query)
    {
        try {
            $authorization = getenv('API_TSHEETS_AUTH_BEARER');
            $response = $this->client->request('GET', 'timesheets', [
                'headers' => [
                    'Authorization' => 'Bearer ' . ($authorization ?? ''),
                    'Accept'        => 'application/json',
                ],
                'query' => $query,
            ]);

            $result = json_decode($response->getBody(), true);

        } catch (Exception $e) {
            return Log::info("Failed to retrieve TSheets Api on '". Carbon::now() . "'  Page Number - '" . $page ."'");
        }

        return !empty($result['results']['timesheets']) ? $result : null;
    }

    /*
     * Get timesheets
     *
     * @param int $page
     * @return array
     */
    public function getTimesheets($page = 1)
    {
        if ($this->onlyModified) {
            return $this->getModified($page);
        }

        return $this->getAny($page);
    }


    /**
     * Executes the retrieving -> format -> saving of all TSheets data until the page no longer returns data
     */
    public function retrieveAndStore()
    {
        set_time_limit(0); //We cannot determine the exact time it spends fetching records from tsheets.
        try {
            $page = 1;
            $desc = $this->startDate . ' to ' . $this->endDate;
            $process = app(Process::class);
            $process->start('Weekly Reports Generation', $desc);

            while (!empty($rawTimesheet = $this->getTimesheets($page))) {
                $formattedTimesheet = $this->formatTimesheet($rawTimesheet);
                $this->storeTimesheet($formattedTimesheet);
                $page++;
            }

            $process->finish();
        } catch (Exception $e) {
            return Log::info("Failed to process TSheets Api on '". Carbon::now() ."'");
        }
    }

    /**
     * Format the TSheet API data needed by the TSheetParserService (Format like the csv file)
     */
    private function formatTimesheet($rawData)
    {
        $formattedData = [[]];

        try {
            foreach ($rawData['results']['timesheets'] as $info) {
                // proceed to next loop if jobcode id not found
                if (empty($rawData['supplemental_data']['jobcodes'][$info['jobcode_id']])) continue;

                $jobcode = $rawData['supplemental_data']['jobcodes'][$info['jobcode_id']]['name'];

                $sheet = array(
                    $rawData['supplemental_data']['users'][$info['user_id']]['email'],                                                  // 0    Username
                    $rawData['supplemental_data']['users'][$info['user_id']]['payroll_id'],                                             // 1    Payroll ID
                    $rawData['supplemental_data']['users'][$info['user_id']]['first_name'],                                             // 2    First Name
                    $rawData['supplemental_data']['users'][$info['user_id']]['last_name'],                                              // 3    Last Name
                    '0',                                                                                                                // 4    Number
                    '-',                                                                                                                // 5    Group
                    date('Y-m-d', strtotime($info['date'])),                                                                            // 6    Local Date
                    date('D', strtotime($info['start'])),                                                                               // 7    Local Day
                    $jobcode == 'Paid Time Off' ? '' : date('Y-m-d h:i:s A', strtotime($info['start'])),                                // 8    Local Start Time
                    $jobcode == 'Paid Time Off' ? '' : date('Y-m-d h:i:s A', strtotime($info['end'])),                                  // 9    Local End Time
                    $info['tz'],                                                                                                        // 10   Timezone
                    $info['duration'] / 3600 ,                          //Change TSheets API to Return Working Hours in Decimal Form instead of <Hours>.<Minutes>
                    //gmdate('H.i', $info['duration']),                                                                                 // 11   Hours
                    $jobcode,                                                                                                           // 12   Jobcode
                    '',                                                                                                                 // 13   Billable
                    '',                                                                                                                 // 14   Class
                    '',                                                                                                                 // 15   Service Item
                    $info['location'],                                                                                                  // 16   Location
                    '',                                                                                                                 // 17   Notes
                    'unapproved',                                                                                                       // 18   Approved Status
                    $info['id']                                                                                                         // 19   Timesheet ID
                );
                array_push($formattedData, $sheet);
            }
        } catch (Exception $e) {
            return Log::info("Failed to parse TSheets API data.'");
        }

        return $formattedData;
    }

    /**
     * Saves the TSheet API data to the database
     */
    private function storeTimesheet($timesheet)
    {
        try {
            $tsheetParser = new TSheetParserService(new Request(), null, [], [], true);

            $tsheetParser->setUnparsedData($timesheet);
            $tsheetParser->callDataParser();

            SaveParsedToDBJob::dispatch(
                $tsheetParser->getParsedDataToBeSaved(),
                true
            );
        } catch (Exception $e) {
            return Log::info("Failed to save TSheets Data.");
        }
    }

    public static function weeklyReportRunning()
    {
        return Process::where('name', 'Weekly Reports Generation')
            ->latest()->first();
    }
}
