<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Client;
use App\Models\ClientProject;
use App\Models\TimesheetJobcode;
use App\Models\ClientTimesheetJobcode;
use App\Models\Employee;
use App\Models\TimesheetUser;
use App\Models\EmployeeTimesheetUser;

class TimesheetMapService
{
    /*
     * Client active status
     *
     * @var integer
     */
    const ACTIVE = 0;

    public function execute()
    {
        $this->mapJobcodes();
        $this->mapEmployees();
    }

    /*
     * Map clients to timesheet jobcodes
     *
     * @return void
     */
    public function mapJobcodes()
    {
        $jobcodes = TimesheetJobcode::all();

        foreach ($jobcodes as $key => $value) {
            $client = Client::select('id')->where('company', $value['name'])
                ->orWhere('company', 'LIKE', '%' . $value['name'] . '%')->first();

            $clientProject = ClientProject::select('client_id')->where('project_name', $value['name'])
                ->orWhere('project_name', 'LIKE', '%' . $value['name'] . '%')->first();

            $clientId = $client->id ?? $clientProject->client_id ?? null;
            $data = [
                'client_id'            => $clientId,
                'timesheet_jobcode_id' => $value['id'],
            ];

            ClientTimesheetJobcode::updateOrCreate(
                ['timesheet_jobcode_id' => $value['id']],
                $data
            );
        }
    }

    /*
     * Map clients to timesheet jobcodes
     *
     * @return void
     */
    public function mapEmployees()
    {
        $timesheetUsers = TimesheetUser::all();

        foreach ($timesheetUsers as $key => $value) {
            $employee = Employee::select('employees.id', 'users.email')
                ->leftJoin('users', 'employees.user_id', '=', 'users.id')
                ->whereNotNull('employees.employee_no')
                ->whereNotNull('employees.user_id')
                ->where('users.email', $value['username'])
                ->orWhere('users.email', 'LIKE', '%' . $value['username'] . '%')
                ->orWhere('users.email', $value['email'])
                ->orWhere('users.email', 'LIKE', '%' . $value['email'] . '%')
                ->first();

            $employeeId = $employee->id ?? null;
            $data = [
                'employee_id'       => $employeeId,
                'timesheet_user_id' => $value['id'],
            ];

            EmployeeTimesheetUser::updateOrCreate(
                ['timesheet_user_id' => $value['id']],
                $data
            );
        }
    }
}
