<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\WeeklyFloorAnswer;
use App\Models\WeeklyFloorReport;

class WeeklyFloorReportService
{

    public static function completeSave($request)
    {
        $answers = $request->except('client_id', 'created_by', 'updated_by');

        $report = $request->only('client_id', 'created_by', 'updated_by');
        
        DB::beginTransaction();

        $weekly_report = WeeklyFloorReport::create($report);

        if (empty($weekly_report)) {
            DB::rollback();
        }

        //dd($weekly_report->id);

        foreach ($answers as $key => $value) 
        {
            $weekly_ans = WeeklyFloorAnswer::create([
                'weekly_floor_report_id' => $weekly_report->id,
                'weekly_floor_question_id' => $key,
                'weekly_floor_answer' => empty($value) ? ' ' : $value ,
            ]);

            if (empty($weekly_ans)) {
                DB::rollback();
            }
        }

        DB::commit();
        
        return true;
    }
}
