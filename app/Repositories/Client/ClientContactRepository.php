<?php

namespace App\Repositories\Client;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\ClientContact;
use App\Validators\ClientContactValidator;
use Dingo\Api\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Services\EmailQueueService;
use App\Models\EmailQueue;
use App\Models\User;
use App\Models\UserVerification;
use App\Models\ACL\Role;
use App\Models\ACL\UserRole;
use App\Models\ACL\ResourceRolePermission;
use App\Models\ACL\ResourceUserRolePermission;
use Carbon\Carbon;

class ClientContactRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return ClientContactValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ClientContact::class;
    }
}
