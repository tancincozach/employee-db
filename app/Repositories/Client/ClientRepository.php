<?php

namespace App\Repositories\Client;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\Client;
use App\Models\ClientContact;
use App\Models\EmailQueue;
use App\Services\EmailQueueService;
use PharIo\Manifest\Email;
use Dingo\Api\Http\Request;
use App\Validators\ClientValidator;
use App;

class ClientRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return ClientValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Client::class;
    }

    public function getClientWithResources()
    {
        $results = $this->model->with(
            [
                'resources' => function($query) {
                    $query
                        ->join('employees', 'employees.id', '=', 'employee_client_projects.employee_id')
                        ->select('employees.*')
                        ->orderBy('employees.first_name');
                },
            ])
            ->orderBy('clients.company')
            ->get();

        return $results;
    }

    /*
     * Get timezone based on id
     *
     * @param integer $id
     *
     * @return string
     */
    public function getTimezone($id)
    {
        $model = $this->model->find($id);

        return $model ? $model->timezone : '';
    }

    public function sendPartnerNotification($clientId, $clientEmail)
    {
        $client = Client::find($clientId);
        $contactArray = array();
        $clientContacts= ClientContact::where('client_id', $clientId)->get();
        $contactCtr = 0;
        $signatory = 'Yes';

        foreach($clientContacts as $rowContact) {
            if(empty($rowContact->contact_no)) {
                $contactArray['signatory_name'] = $rowContact->name;
                $contactArray['signatory_email'] = $rowContact->email;
            } else {
                $contactArray['contact_no'] = $rowContact->contact_no;
                $contactArray['name'] = $rowContact->name;
                $contactArray['email'] = $rowContact->email;
            }
            $contactCtr++;
        }

        $contact = (Object) $contactArray;

        if ($contactCtr > 1) {
            $signatory = 'No';
        }
         // Email functionality block
         $to = [
            'ccabang@fullscale.io',
            'reynaldo@fullscale.io',
            'july@fullscale.io',
            'rnacu@fullscale.io',
        ];

        if (App::isLocal()) {
            $to = [
                'b402117dd4-8e12ba@inbox.mailtrap.io',
            ];
        }

        $bcc = []; // Only add email here if for testing. Please keep this empty when done testing

        if (strtolower(trim(App::Environment())) == 'prod') {
            $to = [
                'sales@fullscale.io',
            ];
            $bcc = array_merge($bcc, [
                'ccabang@fullscale.io',
                'reynaldo@fullscale.io',
                'july@fullscale.io',
                'rnacu@fullscale.io',
                'deco@fullscale.io',
                'darrell@fullscale.io'
            ]);
        }

        $arrExec = [
            'company' => $client->company,
            'location' => $client->location,
            'phone' => $contact->contact_no,
            'contact_name' => $contact->name,
            'email' => $contact->email,
            'signatory' => $signatory,
            'signatory_name' => !empty($contact->signatory_name) ? $contact->signatory_name : '',
            'signatory_email' => !empty($contact->signatory_email) ? $contact->signatory_email : '',
            'company_do' => ucwords($client->company_do),
            'referred_by' => $client->referred_by
        ];

        $emailExec = new EmailQueue();
        $emailExec->status_id = 0;
        $emailExec->status_desc = "Pending Email";
        $emailExec->subject = ucwords($client->company) . " has answered the Agency Survey";
        $emailExec->send_to = implode($to, ',');
        $emailExec->bcc = implode($bcc, ',');
        $emailExec->body = EmailQueueService::renderTemplate('exec-new-partner', $arrExec);
        $emailExec->save();

        if (App::isLocal()) {
            $to = [
                'b402117dd4-8e12ba@inbox.mailtrap.io',
            ];
        }

        if (strtolower(trim(App::Environment())) == 'prod') {
            $to = [
                $clientEmail,
            ];

            $bcc = array_merge($bcc, [
                'ccabang@fullscale.io',
                'reynaldo@fullscale.io',
                'july@fullscale.io',
                'rnacu@fullscale.io',
                'deco@fullscale.io',
                'darrell@fullscale.io'
            ]);
        }

        $emailClient = new EmailQueue();
        $emailClient->status_id = 0;
        $emailClient->status_desc = "Pending Email";
        $emailClient->subject = "Thanks! We Received Your Information";
        $emailClient->send_to = implode($to, ',');
        $emailClient->bcc = implode($bcc, ',');
        $emailClient->body = EmailQueueService::renderTemplate('new-partner-thank-you-notification',$arrExec);
        $emailClient->save();

        $client->welcome_sent = 1;
        $client->save();
    }
}
