<?php

namespace App\Repositories\User;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\User;
use App\Validators\UserValidator;

class UserRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return UserValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    public function generateEmployeeId ($username)
    {
        $result = $this->model->select('employees.id')
                            ->leftJoin('employees', 'users.id', '=', 'employees.user_id')
                            ->where('users.email', $username)
                            ->whereNull('employees.deleted_at')
                            ->first();

        return $result['id'];
    }

    public function generateEmployeeIdByAlternateEmail ($email)
    {
        $result = $this->model->select('employees.id')
                            ->leftJoin('employees', 'users.id', '=', 'employees.user_id')
                            ->where('employees.email', $email)
                            ->whereNull('employees.deleted_at')
                            ->first();

        return $result['id'];
    }
}
