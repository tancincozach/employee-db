<?php

namespace App\Repositories\User;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\UserTimezone;
use App\Validators\UserTimezoneValidator;

class UserTimezoneRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return UserTimezoneValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserTimezone::class;
    }

    /*
     * Get zone name by user
     *
     * @param integer $userId
     *
     * @return string
     */
    public function getZoneName($userId)
    {
        $item = $this->model->where('user_id', $userId)->first();

        return $item ? $item->zone->zone_name : '';
    }
}
