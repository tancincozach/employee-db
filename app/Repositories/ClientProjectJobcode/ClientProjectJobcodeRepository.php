<?php
namespace App\Repositories\ClientProjectJobcode;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\ClientProjectJobcode;
use App\Models\Client;
use App\Models\ClientProject;
use App\Validators\ClientProjectJobcodeValidator;

/**
 * Interface ClientProjectJobCodeRepository.
 *
 * @package namespace App\Repositories;
 */
class ClientProjectJobcodeRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return ClientProjectJobcodeValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ClientProjectJobcode::class;
    }

    public function generateJobCodeId ($jobcode) 
    {        
        $result = $this->model->select('id')->where('jobcode', $jobcode)->first();

        if (isset($result)){
            return $result['id'];
        } else {
            $clientId = null;
            $projectId = null;

            if (!in_array($jobcode, ClientProjectJobcode::NON_WORK_JOBCODES)) {
                $clientModel = new Client;
                $clientId = $clientModel->select('id')
                                        ->where('company', $jobcode)
                                        ->orWhere('company', 'like', '%'.$jobcode.'%')->first();

                if (!isset($clientId)) {
                    $projectModel = new ClientProject;
                    $projectId = $projectModel->select('client_id')
                                            ->where('project_name', $jobcode)
                                            ->orWhere('project_name', 'like', '%'.$jobcode.'%')->first();
                }
            }

            return $this->model->insertGetId([
                                    'client_id' => $clientId['id'] ?? $projectId['client_id'] ?? null,
                                    'jobcode'   => $jobcode
                                ]);
        } 
    }

    public function getPTOHolidayJobcodeId ()
    {
        $ptoHolidays = [];
        $pto = $this->model->select('id')->where('jobcode', 'Paid Time Off')->first();

        if (!empty($pto['id'])) {
            array_push($ptoHolidays, $pto['id']);
        }

        $holidayIds = $this->model->where('is_holiday', 1)->pluck('id')->toArray();

        if (!empty($holidayIds)) {
            array_push($ptoHolidays, $holidayIds);
        }

        return $ptoHolidays;
    }

    public function getJobcodeIdByClientId ($clientId) 
    {
        $result = $this->model->where('client_id', $clientId)->pluck('id')->toArray();

        return $result;
    }

    public function getNonWorkJobcodeIds ()
    {
        return $this->model->whereIn('jobcode', ClientProjectJobcode::NON_WORK_JOBCODES)->pluck('id')->toArray();
    } 
}
