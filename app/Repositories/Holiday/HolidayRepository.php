<?php

namespace App\Repositories\Holiday;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\Holiday;
use App\Validators\HolidayValidator;

class HolidayRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return HolidayValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Holiday::class;
    }

    /*
     * Checks if the holiday with this date is already deleted
     *
     * @param string $date
     *
     * @return bool
     */
    public function checkIfTrashed($date)
    {
        return $this->makeModel()
            ->onlyTrashed()
            ->where('date', $date)
            ->count() > 0 ? true : false;
    }

    /*
     * Restore deleted holiday based on date
     *
     * @param string $date
     *
     * @return bool
     */
    public function restore($date)
    {
        return $this->makeModel()->where('date', $date)->restore();
    }

    /*
     * Get the holiday id based on date
     *
     * @param string $date
     *
     * @return integer
     */
    public function getId($date)
    {
        $model = $this->makeModel()->where('date', $date)->first();

        return !empty($model) ? $model->id : 0;
    }
}
