<?php
namespace App\Repositories\WeeklyReport;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\WeeklyReport;
use App\Validators\WeeklyReportValidator;
use App\Models\WeeklyReportBatchViewable;
use Illuminate\Database\Eloquent\Collection;
use App\Models\ClientProjectJobcode;

/**
 * Class WeeklyReportRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class WeeklyReportRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return WeeklyReportValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return WeeklyReport::class;
    }

    public static function getWorkingHourPercentage($workingHrs)
    {
        return number_format(($workingHrs / WeeklyReport::TOTAL_WORKING_MONTHLY_HOURS) * 100, 1);
    }

    public function filterViewableReports ($clientId, $jobcodeIds, $employeeIds)
    {
        $reportViewable = (new WeeklyReportBatchViewable)->getTable();

        return $this->model->whereIn('jobcode_id', $jobcodeIds)
                            ->whereIn('employee_id', $employeeIds)
                            ->whereRaw('log_date >= (SELECT MIN(' . $reportViewable . '.start_date) FROM ' . $reportViewable . ' WHERE ' . $reportViewable . '.client_id = '.$clientId.' AND '. $reportViewable .'.is_posted=1)')
                            ->whereRaw('log_date <= (SELECT MAX(' . $reportViewable . '.end_date) FROM ' . $reportViewable . ' WHERE ' . $reportViewable . '.client_id='.$clientId.' AND '.$reportViewable .'.is_posted=1)');
    }

    // public function getEmployeeIdsByClientProject ($projectIds)
    // {
    //     $employeeIds = $this->model->distinct()->whereIn('client_project_id', $projectIds)->pluck('employee_id');

    //     return $employeeIds->toArray();
    // }

    public function getJobcodeValue ($projectIds)
    {
        $data = $this->model->select('jobcode')->whereIn('client_project_id', $projectIds)->first();

        return $data->jobcode ?? null;
    }

    public function getEmployeeIdsByJobcodeId ($jobcodeId, $start, $end)
    {
        $employeeIds = $this->model->distinct()->whereIn('jobcode_id', $jobcodeId)
                                                ->whereBetween('log_date', [$start, $end])
                                                ->pluck('employee_id');

        return $employeeIds->toArray();
    }

    /*
     * Add totals per project
     *
     * @param Collection $data
     * @param bool $exclude Exclude non project jobcodes
     *
     * @return Collection
     */
    public function withTotalHours(Collection $data, $exclude = true)
    {
        $arr =  $data->groupBy('jobcode_id')->map(function ($item) {
            return [
                'jobcode'   => ClientProjectJobcode::getJobcodeById($item->first()->jobcode_id),
                'total_hrs' => number_format(collect($item)->sum('working_hrs'), 2),
            ];
        })->values();

        $filtered = collect($arr)->filter(function ($item) {
            return !in_array($item['jobcode'], ClientProjectJobcode::NON_WORK_JOBCODES);
        });

        return $filtered;
    }

    /*
     * Get jobcodes
     *
     * @param integer $employeeId
     * @param string  $startDate
     * @param string  $endDate
     *
     * @return array
     */
    public function getJobcodes($employeeId, $startDate, $endDate)
    {
        $collection = $this->model->distinct()->select('jobcode_id', 'jobcode')
            ->leftJoin('client_project_jobcodes', 'client_project_jobcodes.id', '=', 'weekly_reports.jobcode_id')
            ->where('employee_id', $employeeId)
            ->where('log_date', '>=', $startDate)
            ->where('log_date', '<=', $endDate)
            ->get();

        return $collection->filter(function ($item) {
            return !in_array($item['jobcode'], ClientProjectJobcode::NON_WORK_JOBCODES);
        })->toArray();
    }
}
