<?php

namespace App\Repositories\ClientTeambuilderBucket;

use App;
use App\Validators\ClientTeambuilderBucketValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\ClientTeambuilderBucket;
use App\Models\Client;
use App\Models\EmailQueue;
use App\Services\EmailQueueService;
use PharIo\Manifest\Email;
use Dingo\Api\Http\Request;
use App\Events\ClientSelectedResources;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ClientTeambuilderBucketRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return ClientTeambuilderBucketValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ClientTeambuilderBucket::class;
    }

    public function getExistingId ($clientId, $empId)
    {
        $bucket = $this->model->where('client_id', $clientId)
                                ->where('employee_id', $empId)
                                ->first();

        return ($bucket != null) ? $bucket['id'] : 0;
    }

    public function isAdminSuggested ($id)
    {
        $bucket = $this->model->where('id', $id)->first();

        return ($bucket != null) ? $bucket['is_suggested_admin'] : false;
    }

    public function updateBucketResource ($resources, $sendNotification = false, $clientId = 0)
    {
        $message = 'success';
        try {
            foreach ($resources as $row) {
                if(array_key_exists('client_teambuilder_bucket_id', $row)) {
                    $model = $this->makeModel()->findOrFail($row['client_teambuilder_bucket_id']);
                } else {
                    $model = new ClientTeamBuilderBucket;
                    $model->client_id = $row['client_id'];
                    $model->employee_id = $row['employee_id'];
                    $model->job_position_id = $row['job_position_id'];
                }

                $model->status = $row['status'];
                $model->save();
            }
        } catch(ModelNotFoundException $e) {
            $message = $e->getMessage();
        }

        // Send notification here...
        if ($sendNotification) {
            $client = Client::find($clientId);
            event(new ClientSelectedResources($client));
        }

        return $message;
    }

    public function sendSelectedTalentsEmail($clientId, $clientEmail)
    {
        $client = Client::find($clientId);

         // Email functionality block
         $to = [
            'ccabang@fullscale.io',
            'reynaldo@fullscale.io',
            'july@fullscale.io',
            'rnacu@fullscale.io',
        ];
        $bcc = []; // Only add email here if for testing. Please keep this empty when done testing

        if (strtolower(trim(App::Environment())) == 'prod') {
            $to = [
                'deco@fullscale.io',
                'sales@fullscale.io',
            ];
            $bcc = array_merge($bcc, [
                'ccabang@fullscale.io',
                'reynaldo@fullscale.io',
                'july@fullscale.io',
                'rnacu@fullscale.io',
            ]);
        }

        $link = env('APP_URL') . '/client/' . $clientId . '/profile';
        $arrExec = [
            'link' => $link,
            'client' => $client->company,
            'suggested' => $client->employeeSelected->where('status', 'suggested'),
            'selected' => $client->employeeSelected->where('status', 'selected')
        ];

        $emailExec = new EmailQueue();
        $emailExec->status_id = 0;
        $emailExec->status_desc = "Pending Email";
        $emailExec->subject = $client->company . " Preferred Resource";
        $emailExec->send_to = implode($to, ',');
        $emailExec->bcc = implode($bcc, ',');
        $emailExec->body = EmailQueueService::renderTemplate('exec-selected-resource-client', $arrExec);
        $emailExec->save();

        $emailClient = new EmailQueue();
        $emailClient->status_id = 0;
        $emailClient->status_desc = "Pending Email";
        $emailClient->subject = "Thanks we received your preferred resource";
        $emailClient->send_to = $clientEmail;
        $emailClient->bcc = implode($bcc, ',');
        $emailClient->body = view('emails.client-selected-resource-notification')->render();
        $emailClient->save();
    }
}

