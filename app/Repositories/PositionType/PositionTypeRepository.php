<?php

namespace App\Repositories\PositionType;

use App\Models\PositionType;
use App\Validators\PositionTypeValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class PositionTypeRepository
 * @package App\Repositories\Feedback
 */
class PositionTypeRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return PositionTypeValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return PositionType::class;
    }
}
