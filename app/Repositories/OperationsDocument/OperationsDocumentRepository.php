<?php

namespace App\Repositories\OperationsDocument;

use App\Models\OperationsDocument;
use App\Validators\OperationsDocumentValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

class OperationsDocumentRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return OperationsDocumentValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return OperationsDocument::class;
    }
}
