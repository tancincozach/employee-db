<?php

namespace App\Repositories\PerformEvalQuestion;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\PerformEvalQuestion;
use App\Validators\PerformEvalQuestion\PerformEvalQuestionValidator as QuestionValidator;

/**
 * Class PerformEvalQuestionRepository.
 *
 * @package namespace App\Repositories\PerformEvalQuestion;
 */
class PerformEvalQuestionRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PerformEvalQuestion::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return QuestionValidator::class;
    }
}
