<?php

namespace App\Repositories\PerformEvalQuestion;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\PerformEvalResponse;
use App\Validators\PerformEvalQuestion\PerformEvalResponseValidator as ResponseValidator;

/**
 * Class PerformEvalResponseRepository.
 *
 * @package namespace App\Repositories\PerformEvalQuestion;
 */
class PerformEvalResponseRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PerformEvalResponse::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return ResponseValidator::class;
    }
}
