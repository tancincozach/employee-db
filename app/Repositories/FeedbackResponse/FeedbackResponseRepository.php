<?php

namespace App\Repositories\FeedbackResponse;

use App\Models\FeedbackResponse;
use App\Validators\FeedbackResponseValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class FeedbackResponseRepository
 * @package App\Repositories\FeedbackCategory
 */
class FeedbackResponseRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return FeedbackResponseValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return FeedbackResponse::class;
    }
}
