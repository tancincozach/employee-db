<?php

namespace App\Repositories\Skill;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\Skill;
use App\Validators\SkillValidator;
use App\Models\ClientTeambuilderBucket;
use App\Models\AllQuestionChoice;

class SkillRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return SkillValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Skill::class;
    }

    public function getTechnologyChoices($tech)
    {   
        $techChoices = [];

        foreach($tech as $description) {
            if (!in_array($description, ClientTeambuilderBucket::TECHNOLOGY_CHOICES_CATEGORIES)) {
                continue;
            } else {
                if ($description == 'Developers') {
                    $result = AllQuestionChoice::where('all_question_id', AllQuestionChoice::TECHNOLOGY_CHOICES_ID)
                                                ->pluck('description')
                                                ->toArray();
                } else {
                    $result = $this->model->join('skill_categories', 'skill_categories.skill_id', '=', 'skills.id')
                                        ->whereRaw("skill_categories.category_id IN (SELECT id FROM categories WHERE Name LIKE '"."%".$description."%')")
                                        ->distinct()
                                        ->orderBy('name')
                                        ->pluck('name')
                                        ->toArray();
                }

                array_push($techChoices, array($description => $result));
            } 
        }

        return $techChoices;
    }
}
