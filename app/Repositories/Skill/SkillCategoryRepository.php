<?php

namespace App\Repositories\Skill;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\SkillCategory;

class SkillCategoryRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SkillCategory::class;
    }

    public function saveSkillCategory($skillId, $categories, $removedCategories = [])
    {
        if (!empty($removedCategories)) {
            $this->model->whereIn('id', $removedCategories)->delete();    
        }

        foreach ($categories as $category) {
            self::firstOrCreate(
                ['skill_id' => $skillId, 'category_id' => $category['key']]
            );   
        }
    }
}
