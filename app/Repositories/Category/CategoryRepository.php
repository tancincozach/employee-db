<?php

namespace App\Repositories\Category;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\Category;
use App\Validators\CategoryValidator;
use App\Models\SkillCategory;

class CategoryRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return CategoryValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    public function isExistCategory($name, $resourceType) {
        $result = $this->model->withTrashed()
                            ->where('name', $name)
                            ->where('resource_type', Category::RESOURCE_TYPES[((int)$resourceType) - 1])
                            ->first();

        if ($result !== null) {
            if ($result['deleted_at']) {
                $this->model->withTrashed()
                            ->find($result['id'])
                            ->restore();
            }
            return $result['id'];
        } else {
            return 0;
        }
    }

    
}
