<?php

namespace App\Repositories\Characteristic;

use App\Models\Characteristic;
use App\Validators\CharacteristicValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class CharacteristicRepository
 * @package App\Repositories\Feedback
 */
class CharacteristicRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return CharacteristicValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return Characteristic::class;
    }
}
