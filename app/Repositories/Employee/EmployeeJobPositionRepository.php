<?php

namespace App\Repositories\Employee;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\EmployeeJobPosition;
use App\Validators\EmployeeJobPositionValidator;
use Illuminate\Support\Str;

class EmployeeJobPositionRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return EmployeeJobPositionValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmployeeJobPosition::class;
    }

    /*
     * Get positions based on employee id
     *
     * @param integer $employeeId
     *
     * @return string
     */
    public function getPositions($employeeId)
    {
        $level = ['', 'Junior', 'Mid', 'Senior'];

        $collection = $this->model->where('employee_id', $employeeId)
            ->leftJoin('job_positions', 'employee_job_positions.position_id', '=', 'job_positions.id')
            ->select('job_title', 'level', 'employee_job_positions.id')
            ->orderBy('level')
            ->get();

        $collection = $collection->map(function ($item) use ($level) {
            $index = $item->level ?? 0;
            return trim($level[$index] . ' ' . $item->job_title);
        });

        $position = $collection->reduce(function ($carry, $item) {
            return $item . ' / ' . $carry;
        });

        $position = Str::replaceLast(' / ', '', $position);

        return !empty($position) ? $position : 'Unassigned';
    }
}
