<?php

namespace App\Repositories\WeeklyReportBatchViewable;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\WeeklyReportBatchViewable;
use App\Validators\WeeklyReportBatchViewableValidator;
use Carbon\Carbon;
use App\Models\ClientProjectJobcode;

/**
 * Class WeeklyReportBatchViewableRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class WeeklyReportBatchViewableRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return WeeklyReportBatchViewableValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return WeeklyReportBatchViewable::class;
    }

    public function saveReport($request) 
    {
        $model = new WeeklyReportBatchViewable;

        $model->client_id = $request['client_id'];

        $jobcodeId = ClientProjectJobcode::setClient(
          $request['jobcode_id'],
          $request['client_id']
        );
        
        $model->jobcode_id = $request['jobcode_id'];
        $model->start_date = Carbon::parse($request['start_date']);
        $model->end_date = Carbon::parse($request['end_date']);
        $model->is_posted = $request['is_posted'];
        $model->posted_date = $request['posted_date'];
        $model->posted_by_user_id = $request['posted_by_user_id'];
        $model->posted_version = $request['posted_version'];

        $model->save($request);
    }
}
