<?php

namespace App\Repositories\HackerRankTest;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\Skill;
use App\Models\HackerRankTest;
use App\Models\SkillHackerRankTest;
use App\Validators\HackerRankTestValidator;

class HackerRankTestRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return HackerRankTestValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HackerRankTest::class;
    }

    public function getSkillIdInTag($tag)
    {
        $skillTag = Skill::where('name', $tag)->first();
        if ($skillTag) {
            return $skillTag->id;
        }

        return false;
    }
   
}
