<?php

namespace App\Repositories\WeeklyFloorQuestion;

use App\Models\WeeklyFloorQuestion;
use App\Validators\WeeklyFloorQuestionValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class WeeklyFloorQuestionRepository
 * @package App\Repositories\FeedbackCategory
 */
class WeeklyFloorQuestionRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return WeeklyFloorQuestionValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return WeeklyFloorQuestion::class;
    }
}



