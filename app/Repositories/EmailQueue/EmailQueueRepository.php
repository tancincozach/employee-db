<?php

namespace App\Repositories\EmailQueue;

use App\Models\EmailQueue;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

class EmailQueueRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmailQueue::class;
    }

    public function fetchForSending()
    {
        return $this->model->where(['status_id' => 0],['sent_at' => null])->limit(config('email.limit', 50))->get();
    }

    public function markAsSent($emailContent)
    {
        $this->model->find($emailContent->id)->update(['status_id' => 1, 'status_desc' => 'Successfully sent', 'sent_at' => date("Y-m-d H:i:s")]);
    }

    /**
     * Mark email as error
     *
     * @param        $emailContent
     * @param string $error
     */
    public function markAsError($emailContent, $error = '')
    {
        $this->model->find($emailContent->id)->update([
            'status_id'   => 2,
            'status_desc' => !empty($error) && is_string($error) ? trim($error) : 'Error occurred while sending',
            'sent_at'     => date("Y-m-d H:i:s"),
        ]);
    }
}
