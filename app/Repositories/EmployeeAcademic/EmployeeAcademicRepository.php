<?php

namespace App\Repositories\EmployeeAcademic;

use App\Models\EmployeeAcademic;
use App\Validators\EmployeeAcademicValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class EmployeeAcademicRepository
 * @package App\Repositories\Feedback
 */
class EmployeeAcademicRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return EmployeeAcademicValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return EmployeeAcademic::class;
    }
}
