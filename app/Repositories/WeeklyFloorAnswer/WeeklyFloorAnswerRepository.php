<?php

namespace App\Repositories\WeeklyFloorAnswer;

use App\Models\WeeklyFloorAnswer;
use App\Validators\WeeklyFloorAnswerValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class WeeklyFloorAnswerRepository
 * @package App\Repositories\WeeklyFloorAnswer
 */
class WeeklyFloorAnswerRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return WeeklyFloorAnswerValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return WeeklyFloorAnswer::class;
    }
}



