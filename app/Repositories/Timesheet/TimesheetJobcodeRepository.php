<?php

namespace App\Repositories\Timesheet;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\TimesheetJobcode;
use App\Validators\TimesheetJobcodeValidator;

class TimesheetJobcodeRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return TimesheetJobcodeValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TimesheetJobcode::class;
    }

    /*
     * Get non-regular jobcodes
     * e.g Paid Time Off, Rest/Lunch Break
     *
     * @return array
     */
    public function getNonRegular()
    {
        return $this->model->select('id')
            ->where('type', '<>', 'regular')
            ->pluck('id')
            ->toArray();
    }
}
