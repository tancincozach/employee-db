<?php

namespace App\Repositories\Timesheet;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\TimesheetUserValidator;

use App\Models\TimesheetUser;
use App\Models\ClientTimesheetJobcode;
use App\Models\JobcodeAssignment;

class TimesheetUserRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return TimesheetUserValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TimesheetUser::class;
    }

    /*
     * Get user ids based on client
     *
     * @param integet $clientId
     *
     * @return array
     */
    public function getIdsByClient($clientId)
    {
        $userIds = [];
        $mapping = ClientTimesheetJobcode::where('client_id', $clientId)->first();

        if (!empty($mapping)) {
            $jobcodeId = $mapping->timesheet_jobcode_id;
            $userIds = JobcodeAssignment::where('jobcode_id', $jobcodeId)->pluck('user_id');
            $userIds = $userIds->toArray();
        }

        return $userIds;
    }
}
