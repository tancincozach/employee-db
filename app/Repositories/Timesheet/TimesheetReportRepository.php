<?php

namespace App\Repositories\Timesheet;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\TimesheetReport;

use Illuminate\Support\Collection;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class TimesheetReportRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TimesheetReport::class;
    }

    /*
     * Get associated employee jobcodes within the specified time period.
     *
     * @param integer $employeeId
     * @param string  $startDate
     * @param string  $endDate
     *
     * @return array
     */
    public function getAssociatedJobcodes($employeeId, $startDate, $endDate)
    {
        $query = $this->model->distinct()
            ->select(
                'timesheet_jobcode_id AS id',
                \DB::raw("IFNULL(client_name, IFNULL(timesheet_jobcode_name, '')) AS name")
            )
            ->where('timesheet_user_id', $employeeId)
            ->whereBetween('log_date', [$startDate, $endDate])
            ->where('jobcode_type', 'regular')
            ->get();

        return $query->toArray();
    }

    /*
     * Get employee's jobcode total hours within the specified time period.
     *
     * @param integer $employeeId
     * @param string  $startDate
     * @param string  $endDate
     * @param array   $jobcodeIds
     * @param integer $clientId
     *
     * @return array
     */
    public function getJobcodeTotalHours($employeeId, $startDate, $endDate, $jobcodeIds = [], $clientId = 0)
    {
        $query = $this->model
            ->select(
                \DB::raw("IFNULL(client_name, IFNULL(timesheet_jobcode_name, '')) AS name"),
                \DB::raw("SUM(working_hrs) AS total_hours")
            )
            ->where('timesheet_user_id', $employeeId)
            ->whereBetween('log_date', [$startDate, $endDate])
            ->where('jobcode_type', 'regular');

        if (!empty($clientId)) {
            $query = $query->where('client_id', $clientId);
        }

        if (!empty($jobcodeIds)) {
            $query = $query->whereIn('timesheet_jobcode_id', $jobcodeIds);
        }

        $query = $query->groupBy('timesheet_jobcode_name', 'client_name')
            ->orderBy('name')
            ->get();

        return $query->toArray();
    }

    /*
     * Format timesheets for weekly reports viewing
     *
     * @param Collection $timesheets
     * @param integer    $year
     * @param array      $range
     *
     * @return array
     */
    public function weekly(Collection $timesheets, $year, $range = [])
    {
        $byWeek = $timesheets->groupBy('week');

        $raw = $byWeek->map(function ($week, $key) {
            return $week->groupBy('timesheet_user_id');
        })->toArray();

        $defaultTitle = date('M d', strtotime($range[0])) . ' - ';
        $defaultTitle .= date('M d', strtotime($range[1]));

        $data = [
            'title'   => $defaultTitle,
            'headers' => [],
            'data'    => [],
        ];

        foreach ($raw as $key => $value) {
            $row = [];

            foreach ($value as $userId => $detail) {
                $hours = [];
                $user['employee'] = $detail[0]['employee'];
                $weekTotal = 0;

                foreach ($detail as $d) {
                    $total = (double) $d['day_total'];
                    $weekTotal += $total;

                    $hours[] = [
                        'date'   => $d['log_date'],
                        'total'  => (double) $d['day_total'],
                        'type'   => $d['type'],
                    ];
                }

                // push data if date is not found
                $week = $detail[0]['week'];
                $week = (int) $week + 1;
                $weekRange = $this->period($week, $year)['period'];
                $periodDates = [];

                $periodDates = array_map(function ($item) {
                    return $item->format('Y-m-d');
                }, $weekRange->toArray());

                $diff = array_values(
                    array_diff($periodDates, array_column($hours, 'date'))
                );

                foreach ($diff as $diffDate) {
                    $hours[] = [
                        'date'   => $diffDate,
                        'total'  => 0,
                        'type'   => 'regular',
                    ];
                }

                array_multisort(array_column($hours, 'date'), SORT_ASC, $hours);

                // find duplicate date, e.g employee having a half day pto

                // get dates that are duplicate
                $countValues = array_count_values(array_column($hours, 'date'));
                $duplicateDates = array_keys(array_filter($countValues, function ($value) {
                    return $value > 1;
                }));

                // save unique values
                $uniqueValues = array_filter($hours, function ($value) use ($duplicateDates) {
                    return !in_array($value['date'], $duplicateDates);
                });

                // save duplicate values
                $duplicateValues = array_filter($hours, function ($value) use ($duplicateDates) {
                    return in_array($value['date'], $duplicateDates);
                });

                // merge and format the duplicate values
                $mergedDuplicates = [];

                foreach ($duplicateValues as $dup) {
                    $mergedDuplicates[$dup['date']][] = [
                        'total'  => (double) $dup['total'],
                        'type'   => $dup['type'],
                    ];
                }

                // add the merged duplicates to the unique values
                foreach ($mergedDuplicates as $mergedKey => $merged) {
                    $uniqueValues[] = [
                        'date' => $mergedKey,
                        'data' => $merged,
                    ];
                }

                // sort the final values
                $uniqueValues = array_values($uniqueValues);
                array_multisort(array_column($uniqueValues, 'date'), SORT_ASC, $uniqueValues);

                $user['data'] = $uniqueValues;
                $user['total'] = (double) number_format($weekTotal, 2);

                $row[] = $user;
            }

            $period = $this->period((int)$key + 1, $year);
            $startDate = $period['start'];
            $endDate = $period['end'];
            $period = $period['period'];

            $headers = [];

            foreach ($period as $p) {
                $headers[] = [
                    'date'       => $p->format('Y-m-d'),
                    'day'        => $p->format('D'),
                    'formatted'  => $p->format('M d'),
                ];
            }

            $title = date('M d', strtotime($startDate));
            $title .= ' - ' . date('M d', strtotime($endDate));

            // sort row by employee
            array_multisort(array_column($row, 'employee'), SORT_ASC, $row);


            $data = [
                'title'   => $title,
                'headers' => $headers,
                'data'    => $row,
            ];
        }

        return $data;
    }

    /*
     * Get period by week
     *
     * @param integer $week
     * @param integer $year
     *
     * @return mixed
     */
    public function period($week, $year)
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        $date = Carbon::now();
        $date->setISODate($year, $week);
        $startDate = $date->startOfWeek()->format('Y-m-d');
        $endDate = $date->endOfWeek()->format('Y-m-d');
        $period = CarbonPeriod::create($startDate, $endDate);

        return [
            'start'  => $startDate,
            'end'    => $endDate,
            'period' => $period,
        ];
    }
}
