<?php

namespace App\Repositories\Timesheet;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\EmployeeTimesheetUser;
use App\Validators\EmployeeTimesheetUserValidator;

class EmployeeTimesheetUserRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return EmployeeTimesheetUserValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmployeeTimesheetUser::class;
    }

    /*
     * Get timesheet user id by employee id
     *
     * @param integer $id
     *
     * @return integer
     */
    public function getTimesheetUserId($id)
    {
        $data = $this->model->where('employee_id', $id)->first();

        return $data ? $data->timesheet_user_id : 0;
    }
}
