<?php

namespace App\Repositories\Timesheet;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\TimesheetValidator;

use App\Models\Timesheet;
use App\Models\ClientTimesheetJobcode;

use Illuminate\Support\Collection;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class TimesheetRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return TimesheetValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Timesheet::class;
    }

    /*
     * Get associated employee jobcodes within the specified time period.
     *
     * @param integer $employeeId
     * @param string  $startDate
     * @param string  $endDate
     *
     * @return array
     */
    public function getAssociatedJobcodes($employeeId, $startDate, $endDate)
    {
        $query = $this->model->distinct()
            ->select(
                'timesheets.jobcode_id AS id',
                'tj.name AS name'
            )
            ->leftJoin('timesheet_jobcodes AS tj', 'timesheets.jobcode_id', '=', 'tj.id')
            ->where('timesheets.user_id', $employeeId)
            ->whereBetween('timesheets.date', [$startDate, $endDate])
            ->where('tj.type', 'regular')
            ->get();

        return $query->toArray();
    }

    /*
     * Get employee's jobcode total hours within the specified time period.
     *
     * @param integer $employeeId
     * @param string  $startDate
     * @param string  $endDate
     * @param array   $jobcodeIds
     * @param integer $clientId
     *
     * @return array
     */
    public function getJobcodeTotalHours($employeeId, $startDate, $endDate, $jobcodeIds = [], $clientId = 0)
    {
        $query = $this->model
            ->select(
                'tj.name AS name',
                \DB::raw("SUM(ROUND(timesheets.duration / 3600, 2)) AS total_hours")
            )
            ->leftJoin('timesheet_jobcodes AS tj', 'timesheets.jobcode_id', '=', 'tj.id')
            ->leftJoin('client_timesheet_jobcodes AS ctj', 'tj.id', '=', 'ctj.timesheet_jobcode_id')
            ->leftJoin('clients AS c', 'ctj.client_id', '=', 'c.id')
            ->where('timesheets.user_id', $employeeId)
            ->whereBetween('timesheets.date', [$startDate, $endDate])
            ->where('tj.type', 'regular');

        if (!empty($clientId)) {
            $query = $query->where('c.id', $clientId);
        }

        if (!empty($jobcodeIds)) {
            $query = $query->whereIn('timesheets.jobcode_id', $jobcodeIds);
        }

        $query = $query->groupBy('tj.name', 'c.company')
            ->orderBy('tj.name')
            ->get();

        return $query->toArray();
    }

    /*
     * Format timesheets for weekly reports viewing
     *
     * @param Collection $timesheets
     * @param integer    $year
     * @param array      $range
     *
     * @return array
     */
    public function weekly(Collection $timesheets, $year, $range = [])
    {
        $raw = $timesheets->groupBy('timesheet_user_id');

        $defaultTitle = date('M d', strtotime($range[0])) . ' - ';
        $defaultTitle .= date('M d', strtotime($range[1]));

        $data = [
            'title'   => $defaultTitle,
            'headers' => [],
            'data'    => [],
        ];

        if ($raw->isEmpty()) {
            return $data;
        }

        foreach ($raw as $detail) {
            $hours = [];
            $lastIndex = count($detail) - 1;
            $user['employee'] = $detail[$lastIndex]['employee'];
            $weekTotal = 0;

            foreach ($detail as $d) {
                $total = (double) $d['day_total'];
                $weekTotal += $total;

                $hours[] = [
                    'date'   => $d['log_date'],
                    'total'  => (double) $d['day_total'],
                    'type'   => $d['type'],
                ];
            }

            // push data if date is not found
            $dateFromRange = Carbon::createFromFormat('Y-m-d', $range[1]);
            $week = $detail[$lastIndex]['week'];
            $week = $dateFromRange->weekOfYear;
            $week = (int) $week;

            $yearFromEndDate = Carbon::createFromFormat('Y-m-d', $range[1])->year;

            if ($year !== $yearFromEndDate) {
                $year = $yearFromEndDate;
            }

            $weekRange = $this->period($week, $year)['period'];

            $periodDates = [];

            $periodDates = array_map(function ($item) {
                return $item->format('Y-m-d');
            }, $weekRange->toArray());

            $diff = array_values(
                array_diff($periodDates, array_column($hours, 'date'))
            );

            foreach ($diff as $diffDate) {
                $hours[] = [
                    'date'   => $diffDate,
                    'total'  => 0,
                    'type'   => 'regular',
                ];
            }

            array_multisort(array_column($hours, 'date'), SORT_ASC, $hours);

            // find duplicate date, e.g employee having a half day pto

            // get dates that are duplicate
            $countValues = array_count_values(array_column($hours, 'date'));
            $duplicateDates = array_keys(array_filter($countValues, function ($value) {
                return $value > 1;
            }));

            // save unique values
            $uniqueValues = array_filter($hours, function ($value) use ($duplicateDates) {
                return !in_array($value['date'], $duplicateDates);
            });

            // save duplicate values
            $duplicateValues = array_filter($hours, function ($value) use ($duplicateDates) {
                return in_array($value['date'], $duplicateDates);
            });

            // merge and format the duplicate values
            $mergedDuplicates = [];

            foreach ($duplicateValues as $dup) {
                $mergedDuplicates[$dup['date']][] = [
                    'total'  => (double) $dup['total'],
                    'type'   => $dup['type'],
                ];
            }

            // add the merged duplicates to the unique values
            foreach ($mergedDuplicates as $mergedKey => $merged) {
                $uniqueValues[] = [
                    'date' => $mergedKey,
                    'data' => $merged,
                ];
            }

            // sort the final values
            $uniqueValues = array_values($uniqueValues);
            array_multisort(array_column($uniqueValues, 'date'), SORT_ASC, $uniqueValues);

            $user['data'] = $uniqueValues;
            $user['total'] = (double) number_format($weekTotal, 2);

            $row[] = $user;
        }

        $year = Carbon::createFromFormat('Y-m-d', $range[1])->year;
        $period = $this->period((int)$week, $year);
        $startDate = $period['start'];
        $endDate = $period['end'];
        $period = $period['period'];

        $headers = [];

        foreach ($period as $p) {
            $headers[] = [
                'date'       => $p->format('Y-m-d'),
                'day'        => $p->format('D'),
                'formatted'  => $p->format('M d'),
            ];
        }

        $title = date('M d', strtotime($startDate));
        $title .= ' - ' . date('M d', strtotime($endDate));

        // sort row by employee
        array_multisort(array_column($row, 'employee'), SORT_ASC, $row);


        $data = [
            'title'   => $title,
            'headers' => $headers,
            'data'    => $row,
        ];

        return $data;
    }

    /*
     * Get period by week
     *
     * @param integer $week
     * @param integer $year
     *
     * @return mixed
     */
    public function period($week, $year)
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        $date = Carbon::now();
        $date->setISODate($year, $week);
        $startDate = $date->startOfWeek()->format('Y-m-d');
        $endDate = $date->endOfWeek()->format('Y-m-d');
        $period = CarbonPeriod::create($startDate, $endDate);

        return [
            'start'  => $startDate,
            'end'    => $endDate,
            'period' => $period,
        ];
    }

    /*
     * Get jobcode id based on client
     *
     * @param integer $clientId
     *
     * @return integer;
     */
    public function getJobcodeIdByClient($clientId)
    {
        $jobcodeId = 0;

        $mapping = ClientTimesheetJobcode::where('client_id', $clientId)->first();

        if (!empty($mapping)) {
            $jobcodeId = $mapping->timesheet_jobcode_id;
        }

        return $jobcodeId;
    }

    /*
     * Get last updated timestamp based on the supplied parameters
     *
     * @param integer $userId
     * @param integer $jobcodeId
     * @param integer $year
     * @param integer $month
     *
     * @return Carbon
     */
    public function getLastUpdated($userId, $jobcodeId, $year, $month)
    {
        $item = $this->model->select('updated_at')
            ->where('user_id', $userId)
            ->where('jobcode_id', $jobcodeId)
            ->whereYear('date', $year)
            ->whereMonth('date', $month)
            ->latest('updated_at')
            ->first();

        return $item ? $item->updated_at : '';
    }
}
