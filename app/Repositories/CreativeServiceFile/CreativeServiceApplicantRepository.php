<?php

namespace App\Repositories\CreativeServiceFile;

use App\Models\CreativeServiceFile;
use App\Validators\CreativeServiceFileValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class CreativeServiceFileRepository
 * @package App\Repositories\CreativeServiceFile
 */
class CreativeServiceFileRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return CreativeServiceFileValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return CreativeServiceFile::class;
    }
}
