<?php

namespace App\Repositories\CreativeServiceApplicant;

use App\Models\CreativeServiceApplicant;
use App\Validators\CreativeServiceApplicantValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class CreativeServiceApplicantRepository
 * @package App\Repositories\CreativeService
 */
class CreativeServiceApplicantRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return CreativeServiceApplicantValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return CreativeServiceApplicant::class;
    }
}
