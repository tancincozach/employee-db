<?php
namespace App\Repositories\ClientOnboardingChecklist;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\ClientOnboardingChecklist;
use App\Validators\ClientOnboardingChecklistValidator;

/**
 * Interface ClientPreferredTeamRepository.
 *
 * @package namespace App\Repositories;
 */
class ClientOnboardingChecklistRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return ClientOnboardingChecklistValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ClientOnboardingChecklist::class;
    }
}
