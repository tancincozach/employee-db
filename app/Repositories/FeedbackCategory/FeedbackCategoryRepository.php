<?php

namespace App\Repositories\FeedbackCategory;

use App\Models\FeedbackCategory;
use App\Validators\FeedbackCategoryValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class FeedbackCategoryRepository
 * @package App\Repositories\FeedbackCategory
 */
class FeedbackCategoryRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return FeedbackCategoryValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return FeedbackCategory::class;
    }
}
