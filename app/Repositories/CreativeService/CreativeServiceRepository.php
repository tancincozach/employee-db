<?php

namespace App\Repositories\CreativeService;

use App\Models\CreativeService;
use App\Validators\CreativeServiceValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class CreativeServiceRepository
 * @package App\Repositories\CreativeService
 */
class CreativeServiceRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return CreativeServiceValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return CreativeService::class;
    }
}
