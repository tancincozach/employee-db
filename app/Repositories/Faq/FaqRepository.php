<?php

namespace App\Repositories\Faq;

use App\Models\Faq;
use App\Validators\FaqValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

class FaqRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return FaqValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return Faq::class;
    }
}
