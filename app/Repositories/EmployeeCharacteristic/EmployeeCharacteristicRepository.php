<?php

namespace App\Repositories\EmployeeCharacteristic;

use App\Models\EmployeeCharacteristic;
use App\Validators\EmployeeCharacteristicValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class EmployeeCharacteristicRepository
 * @package App\Repositories\Feedback
 */
class EmployeeCharacteristicRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return EmployeeCharacteristicValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return EmployeeCharacteristic::class;
    }
}
