<?php

namespace App\Repositories\Feedback;

use App\Models\Feedback;
use App\Validators\FeedbackValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class FeedbackRepository
 * @package App\Repositories\FeedbackCategory
 */
class FeedbackRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return FeedbackValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return Feedback::class;
    }
}
