<?php

namespace App\Repositories\HackerRankTestResult;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Models\HackerRankTestResult;
use App\Validators\HackerRankTestResultValidator;

class HackerRankTestResultRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return HackerRankTestResultValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HackerRankTestResult::class;
    }

    
}
