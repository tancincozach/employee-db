<?php

namespace App\Repositories\WeeklyFloorReport;

use App\Models\WeeklyFloorReport;
use App\Validators\WeeklyFloorReportValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class WeeklyFloorReportRepository
 * @package App\Repositories\FeedbackCategory
 */
class WeeklyFloorReportRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return WeeklyFloorReportValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return WeeklyFloorReport::class;
    }
}



