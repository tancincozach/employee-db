<?php

namespace App\Repositories\EmployeePositionType;

use App\Models\EmployeePositionType;
use App\Validators\EmployeePositionTypeValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class EmployeePositionTypeRepository
 * @package App\Repositories\PositionType
 */
class EmployeePositionTypeRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $cacheExcept = [];

    /**
     * @return string|null
     */
    public function validator()
    {
        return EmployeePositionTypeValidator::class;
    }

    /**
     * @return string
     */
    public function model()
    {
        return EmployeePositionType::class;
    }
}
