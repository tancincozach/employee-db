<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use App\Models\WeeklyFloorReport;
use App\Models\WeeklyFloorQuestion;
use App\Models\WeeklyFloorAnswer;
use App\Models\ClientContact;
use App\Models\Client;
use App\Models\Employee;
use App\Helpers\MailHelper;
use App;
# use App\Services\EmployeeReportAttachmentService;

class WeeklyFloorReports extends Mailable
{
    use Queueable, SerializesModels;

    public $weeklyFloorReport;
    public $weeklyFloorData;
    public $weeklyFloorQuestion;
    public $weeklyFloorAnswer;
    public $employee;
    public $client;
    public $template;
    public $fileAttachments;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($weeklyFloorReport, array $data = [])
    {
        $this->weeklyFloorReport = $weeklyFloorReport;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->employee = Employee::where('user_id', $this->weeklyFloorReport->created_by)->first();
        $this->client = Client::where('id', $this->weeklyFloorReport->client_id)->first();
        $this->weeklyFloorReport->company = $this->client->company;
        $this->weeklyFloorReport->week =  date('n/d',strtotime('monday this week', strtotime($this->weeklyFloorReport->created_at)));

        $this->processSubject();

        if ($this->employee) {
            $email = $this->employee->user->email;
            $bcc = [];
            if (strtolower(trim(App::Environment())) == 'prod') {
                $bcc = array_merge($bcc, [$email]);
            } else {
                $bcc = array_merge($bcc, [
                    $email,
                    'ccabang@fullscale.io',
                    'reynaldo@fullscale.io',
                    'july@fullscale.io',
                    'rnacu@fullscale.io'
                ]);
                if (App::isLocal()) {
                    $bcc = [];
                    $bcc = array_merge($bcc, ['b402117dd4-8e12ba@inbox.mailtrap.io']);
                }
            }

            return $this->from("donotreply@fullscale.io")
            ->subject($this->processSubject())
            ->replyTo($email)
            ->bcc($bcc)
            ->view("emails.weekly-floor-report-notification"); 
        } else {
            $bcc = array_merge($bcc, [
                'ccabang@fullscale.io',
                'reynaldo@fullscale.io',
                'july@fullscale.io',
                'rnacu@fullscale.io'
            ]);
            return $this->from("donotreply@fullscale.io")
            ->subject($this->processSubject())
            ->bcc($bcc)
            ->view("emails.weekly-floor-report-notification");
        }
    }
    private function processSubject() {
        $this->employee = Employee::where('user_id', $this->weeklyFloorReport->created_by)->first();
        
        $name = '';
        $str = "Weekly Floor Report - ".$this->weeklyFloorReport->company;
        if ($this->employee) {
            $name = $this->employee->first_name ." ". 
            substr($this->employee->last_name,0,1) .".";
            $str .= " by ".$name; 
        }
        return $str; 
    }
}