<?php

namespace App\Mail;

use App\Helpers\MailHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * FeedbackMail constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @return FeedbackMail
     */
    public function build()
    {
        $from = [
            'address' => ! empty($this->data['employee']['user']) ? $this->data['employee']['user']['email'] : 'noreply@fullscale.io',
            'name' => ! empty($this->data['employee']) ? $this->data['employee']['first_name'] . ' ' . $this->data['employee']['last_name'] : 'Anonymous'
        ];

        return $this->from($from['address'], $from['name'])
            ->subject('Feedback - ' . MailHelper::prependEnvNameIfNecessary($this->data['feedbackCategory']['name']))
            ->replyTo($from['address'])
            ->view('emails.feedback');
    }
}
