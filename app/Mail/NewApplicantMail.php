<?php

namespace App\Mail;

use App\Helpers\MailHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewApplicantMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * NewApplicantMail constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {  
        $this->data = $data;
    }

    /**
     * @return NewApplicantMail
     */
    public function build()
    {
        $from = [
            'address' => 'noreply@fullscale.io',
            'name' =>  'Applicant On Boarding'
        ];

        return $this->from($from['address'], $from['name'])
            ->bcc('july@fullscale.io', 'reynaldo@fullscale.io') //This portion is for temporary only and testing purposes.
            ->subject('New Applicant - ' . MailHelper::prependEnvNameIfNecessary( $this->data['applicant']['first_name'] . ' ' . $this->data['applicant']['last_name']))
            ->replyTo($from['address'])
            ->view('emails.new-applicant');
    }
}
