<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use App\Models\JobPosition;
use App\Models\EmployeeJobPosition;
use App\Models\EmployeeReportsFile;
use App\Helpers\MailHelper;
use App\Services\EmployeeReportAttachmentService;
class DailyReport extends Mailable
{
    use Queueable, SerializesModels;

    public $employeeReport;
    public $employee;
    public $template;
    public $fileAttachments;
    public $position;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($employeeReport)
    {
        $this->employeeReport = $employeeReport;
    }

    public function setEmployeeAndTemplate($employee, $template) {
        $this->employee = $employee;
        $this->template = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $path = 'app/public/daily-reports/';
        if($this->employee == null)
            $this->employee = $this->employeeReport->employee;
        if($this->template == null)
            $this->template = $this->employeeReport->reportTemplate;

        if (isset($this->employeeReport->id)) {
            //dd($this->employeeReport->id);
            $this->fileAttachments = EmployeeReportsFile::where('employee_reports_id', $this->employeeReport->id)->get();
        }

        $employeePosition = EmployeeJobPosition::where('employee_id', $this->employee->id)->first();
        
        if($employeePosition != null)
            $this->position = JobPosition::where('id', $employeePosition->position_id)->first()->job_title;

        $maildata =  $this->from('noreply@fullscale.io', 'Full Scale Reports')
                    ->subject(MailHelper::prependEnvNameIfNecessary($this->employeeReport->subject))
                    ->replyTo($this->employee->user->email)
                    ->bcc($this->employee->user->email)
                    ->view("emails.".$this->template->view_file);
                    
        if (!empty($this->fileAttachments)) {
               foreach($this->fileAttachments as $attachfile) {
                   $attachmentFile = storage_path($path . $attachfile->file);
                   $options = ['as' => $attachfile->original_file, 'mime' => $this->get_mime_type($attachfile->file)];
                   $maildata->attach((string) $attachmentFile, $options);
               }
        }
   
        return $maildata;                    
    }
    public function get_mime_type($filename) {
        $idx = explode( '.', $filename );
        $count_explode = count($idx);
        $idx = strtolower($idx[$count_explode-1]);
    
        $mimet = array( 
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
    
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
    
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
    
            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
    
            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
    
            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',
    
    
            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
    
        if (isset( $mimet[$idx] )) {
         return $mimet[$idx];
        } else {
         return 'application/octet-stream';
        }
     }
}
