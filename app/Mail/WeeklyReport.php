<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\ClientTimesheetJobcode;
use App\Services\ThirdParty\Timesheet as TimesheetService;

class WeeklyReport extends Mailable
{
    use Queueable, SerializesModels;
    public $client;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($client = null)
    {
        $this->client = $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $code = ClientTimesheetJobcode::where('client_id', $this->client->id)->first();
        $monday = date( 'Y-m-d', strtotime( 'monday last week'  )  );
        $saturday = date( 'Y-m-d', strtotime( 'saturday last week'  )  );

        $result = [];

        if($code) {
            $job_code = $code->timesheet_jobcode_id;

            $weekly = TimesheetService::getWeekly(['job_code_id'=>$job_code, 'start'=> $monday, 'end' => $saturday]);
            $monthly = TimesheetService::getMonthly(['job_code_id'=>$job_code, 'month'=>date('m'), 'year'=>date('Y')]);

            $this->preformat_monthly($result, $monthly);

            $this->preformat_weekly($result, $weekly);
        }

        $arrExec = ['rows' => $result, 'client_name' => $this->client->company,
                    'start' => $monday, 'end' => $saturday];

        return $this->from("donotreply@fullscale.io")
            ->subject("Weekly Timeclock Report " . $monday . " - " . $saturday)
            ->view("emails.weekly_client_report", $arrExec);
    }

    private function preformat_monthly(&$result, $data) {
        foreach($data as $d) {
            $result[$d->user_id] = ['monthly_hours' => $d->rendered,
                                    'target' => $d->target,
                                    'percentage' => $d->percentage,
                                    'name' => $d->employee];
        }
    }

    private function preformat_weekly(&$result, $data) {
        foreach($data as $d) {
            if(!isset($result[$d->timesheet_user_id]['weekly_hours']))
                $result[$d->timesheet_user_id]['weekly_hours'] = 0.0;

            $result[$d->timesheet_user_id]['weekly_hours'] += $d->day_total;
        }
    }
}
