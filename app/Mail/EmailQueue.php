<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\EmailQueueService;
use App\Helpers\MailHelper;

class EmailQueue extends Mailable
{
    use Queueable, SerializesModels;

    public $emailContents;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($emailContents)
    {
        $this->emailContents = $emailContents;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->emailContents->sender_email,
                !empty($this->emailContents->sender_name) ? trim($this->emailContents->sender_name) : 'Full Scale')
            ->subject(MailHelper::prependEnvNameIfNecessary($this->emailContents->subject))
            ->html($this->emailContents->body);
    }
}
