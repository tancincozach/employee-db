<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\JobcodeAssignment;
use App\Models\TimesheetJobcode;
use App\Models\TimesheetUser;

class GenerateRawJobcodeAssignments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /*
     * Jobcode assignment query
     *
     * @var array
     */
    protected $query;

    /*
     * Jobcode assignment raw
     *
     * @var array
     */
    protected $raw;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $raw)
    {
        $this->raw = $raw;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $jobcodeAssignments = $this->raw;

        foreach ($jobcodeAssignments as $key => $value) {
            $userIdExists    = !empty(TimesheetUser::find($value['user_id']));
            $jobcodeIdExists = !empty(TimesheetJobcode::find($value['jobcode_id']));

            if ($userIdExists && $jobcodeIdExists) {
                JobcodeAssignment::firstOrCreate(
                    ['id' => $value['id']],
                    [
                        'id'         => $value['id'],
                        'user_id'    => $value['user_id'],
                        'jobcode_id' => $value['jobcode_id'],
                        'active'     => $value['active'],
                    ]
                );
            }
        }
    }
}
