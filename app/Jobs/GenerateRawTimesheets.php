<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Timesheet;
use App\Models\TimesheetJobcode;
use App\Models\TimesheetUser;

use App\Services\ThirdParty\Timesheet as TimesheetService;

class GenerateRawTimesheets implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /*
     * Timesheet query
     *
     * @var array
     */
    protected $query;

    /*
     * Timesheet query
     *
     * @var array
     */
    protected $raw;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $raw)
    {
        $this->raw = $raw;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $timesheets = $this->raw;

        foreach ($timesheets['jobcodes'] as $key => $value) {
            TimesheetJobcode::firstOrCreate(
                ['id' => $value['id']],
                [
                    'id'              => $value['id'],
                    'name'            => $value['name'],
                    'parent_id'       => $value['parent_id'],
                    'assigned_to_all' => $value['assigned_to_all'],
                    'active'          => $value['active'],
                    'type'            => $value['type'],
                    'has_children'    => $value['has_children'],
                    'last_modified'   => $value['last_modified'],
                    'created'         => $value['created'],
                ]
            );
        }

        foreach ($timesheets['users'] as $key => $value) {
            TimesheetUser::firstOrCreate(
                ['id' => $value['id']],
                [
                    'id'                => $value['id'],
                    'first_name'        => $value['first_name'],
                    'last_name'         => $value['last_name'],
                    'active'            => $value['active'],
                    'employee_number'   => $value['employee_number'],
                    'username'          => $value['username'],
                    'email'             => $value['email'],
                    'mobile_number'     => $value['mobile_number'],
                    'hire_date'         => $value['hire_date'],
                    'term_date'         => $value['term_date'],
                    'last_modified'     => $value['last_modified'],
                    'last_active'       => $value['last_active'],
                    'created'           => $value['created'],
                    'company_name'      => $value['company_name'],
                    'profile_image_url' => $value['profile_image_url'],
                    'pto_balances'      => json_encode($value['pto_balances']),
                ]
            );
        }

        foreach ($timesheets['timesheets'] as $key => $value) {
            $userIdExists    = !empty(TimesheetUser::find($value['user_id']));
            $jobcodeIdExists = !empty(TimesheetJobcode::find($value['jobcode_id']));

            if ($userIdExists && $jobcodeIdExists) {
                $data = [
                    'id'            => $value['id'],
                    'user_id'       => $value['user_id'],
                    'jobcode_id'    => $value['jobcode_id'],
                    'start'         => date('Y-m-d H:i:s', strtotime($value['start'])),
                    'end'           => date('Y-m-d H:i:s', strtotime($value['end'])),
                    'duration'      => $value['duration'],
                    'date'          => $value['date'],
                    'tz'            => $value['tz'],
                    'tz_str'        => $value['tz_str'],
                    'type'          => $value['type'],
                    'location'      => $value['location'],
                    'on_the_clock'  => $value['on_the_clock'],
                    'notes'         => $value['notes'],
                    'last_modified' => $value['last_modified'],
                ];

                if ('manual' == $value['type']) {
                    unset($data['start']);
                    unset($data['end']);
                }

                Timesheet::updateOrCreate(
                    ['id' => $value['id']],
                    $data
                );
            }
        }
    }
}
