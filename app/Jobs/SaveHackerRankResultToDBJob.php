<?php
namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\HackerRankTestResult\HackerRankTestResultRepository;
use App\Repositories\HackerRankTest\HackerRankTestRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\Employee\EmployeeRepository;
use App\Repositories\Employee\EmployeeSkillRepository;


class SaveHackerRankResultToDBJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $fetchdataArray;
    protected $viaAPI;
    const DATE_FORMAT = 'Y-m-d H:i:s';
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fetchdataArray, $viaAPI = false)
    {
        $this->fetchdataArray = $fetchdataArray;
        $this->viaAPI = $viaAPI;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        HackerRankTestRepository $hackerRankTestRepo,
        HackerRankTestResultRepository $hackerRankTestResultRepo,
        UserRepository $userRepository,
        EmployeeRepository $employeeRepository,
        EmployeeSkillRepository $employeeSkillRepository
    ) {
        $this->hackerRankTestRepo = $hackerRankTestRepo;
        $this->hackerRankTestResultRepo = $hackerRankTestResultRepo;
        $this->userRepository = $userRepository;
        $this->employeeRepository = $employeeRepository;
        $this->employeeSkillRepository = $employeeSkillRepository;

        $testResultData = $this->fetchdataArray;
        $employeeId = $this->userRepository->generateEmployeeId($testResultData->email);

        if(empty($employeeId)) {
            $employeeId = $this->userRepository->generateEmployeeIdByAlternateEmail($testResultData->email);
        }
        if (!empty($employeeId)) {
            $hackerTestInfo =  $this->hackerRankTestRepo->findByField('test_id', $testResultData->test)->first();
           
            if ($hackerTestInfo->skill_id > 0) {
                $employeeSkills = $this->employeeSkillRepository->findWhere(['employee_id' => $employeeId]);
                if ((sizeof($employeeSkills) > 0)) {
                    $empSkillId = 0;
                    $noTopSkillCtr = 0;
                    $empSkillRate = 0;

                    foreach($employeeSkills as $empSkill) {
                        if ($hackerTestInfo->skill_id == $empSkill->skill_id) {
                            $empSkillId = $empSkill->id;
                            $empSkillRate = $empSkill->proficiency;
                            break;
                        }
                    }

                    if ($empSkillId > 0) {
                        $skillProficiency = $testResultData->percentage_score / 10;
                        if ($empSkillRate > $skillProficiency) {
                            $skillProficiency = $empSkillRate;
                        }
                        if ($skillProficiency > 5) {
                            $skillResult = $this->employeeSkillRepository->update(
                                [
                                    'skill_id'   => $hackerTestInfo->skill_id,
                                    'employee_id'   => $employeeId,
                                    'proficiency' => $skillProficiency,
                                    'checked_top_skill'   => 1
                                ],
                                $empSkillId
                            );
                        }
                    } else {
                        $skillProficiency = $testResultData->percentage_score / 10;
                        if ($empSkillRate > $skillProficiency) {
                            $skillProficiency = $empSkillRate;
                        }
                        if ($skillProficiency > 5) {
                            $skillResult = $this->employeeSkillRepository->create(
                                [
                                    'skill_id'   => $hackerTestInfo->skill_id,
                                    'employee_id'   => $employeeId,
                                    'proficiency' => $skillProficiency,
                                    'checked_top_skill'   => 1
                                ]
                            );
                        }
                    }
                } else {
                    if (sizeof($employeeSkills) <= 0) {
                        $skillProficiency = $testResultData->percentage_score / 10;
                        if ($skillProficiency > 5) {
                            $skillResult = $this->employeeSkillRepository->create(
                                [
                                    'skill_id'   => $hackerTestInfo->skill_id,
                                    'employee_id'   => $employeeId,
                                    'proficiency' => $skillProficiency,
                                    'checked_top_skill'   => 1
                                ]
                            );
                        }
                    }
                }
            }
            
            $result = $this->hackerRankTestResultRepo->updateOrCreate(
                [   
                    'result_id' => $testResultData->id
                ],
                [
                    'test_id'   => $testResultData->test,
                    'employee_id'   => $employeeId,
                    'email_used' => $testResultData->email,
                    'raw_score'   => (($testResultData->score) ? $testResultData->score : 0),
                    'percent_score'   => (($testResultData->percentage_score) ? $testResultData->percentage_score : 0),
                    'published'   => true,
                    'date_time_start_taken' => date(self::DATE_FORMAT, strtotime($testResultData->attempt_starttime)),
                    'date_time_end_taken' => date(self::DATE_FORMAT, strtotime($testResultData->attempt_endtime))
                ]
            );
        }
    }
}
