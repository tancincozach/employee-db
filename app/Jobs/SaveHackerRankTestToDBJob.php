<?php
namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\HackerRankTest\HackerRankTestRepository;


class SaveHackerRankTestToDBJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $fetchdataArray;
    protected $viaAPI;
    const VIRGIL_DETAILS = ['hackrank_email'=>'virgill@fullscale.io', 'employee_id'=>38];
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fetchdataArray, $viaAPI = false)
    {
        $this->fetchdataArray = $fetchdataArray;
        $this->viaAPI = $viaAPI;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        HackerRankTestRepository $hackerRankTestRepo
    ) {
        $this->hackerRankTestRepo = $hackerRankTestRepo;
        $rowData = $this->fetchdataArray;
        $currentTest = $this->hackerRankTestRepo->model()::where('test_id', $rowData['id'])->first();
        $skillId = 0;
        $tempSkill = '';
        $skillFound = 0; // flagger if the tag is being set.
        if (count($rowData['tag']) > 0) {
            foreach($rowData['tag'] as $tag) {
                $tempSkill = $this->hackerRankTestRepo->getSkillIdInTag($tag);

                if (!empty($tempSkill)) {
                    $skillId = $tempSkill;
                    $skillFound = 1;
                }
            }
        }

 
        $this->hackerRankTestRepo->updateOrCreate(
            [   
                'test_id'   => $rowData['id']
            ],
            [
                'name' => $rowData['name'],
                'unique_id' => $rowData['unique_id'],
                'skill_id' => $skillId,
                'has_tag' => $skillFound
            ]);
        
          
    }
}
