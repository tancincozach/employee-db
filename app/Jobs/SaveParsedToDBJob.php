<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\ClientProjectJobcode\ClientProjectJobcodeRepository;
use App\Repositories\WeeklyReport\WeeklyReportRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\Employee\EmployeeRepository;
use App\Criterias\ClientProjectJobcode\JobcodeMapping;
use App\Criterias\User\SearchByEmail;
use App\Criterias\Employee\FilterByUserId;

class SaveParsedToDBJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $reportArray;
    protected $viaAPI;

    /*
     * List of emails used in tsheets that is different in rocks email.
     * key is employee email
     * value is employee id
     *
     * @var array
     */
    protected $alternateEmails = [
        'virgill@fullscale.io'    => 38,
        'irynes@fullscale.io'     => 39,
        'dpueblo@fullscale.io'    => 268,
        'agcaballes@fullscale.io' => 732,
    ];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($reportArray, $viaAPI = false)
    {
        $this->reportArray = $reportArray;
        $this->viaAPI = $viaAPI;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        WeeklyReportRepository $weeklyReportRepo,
        ClientProjectJobcodeRepository $clientProjectRepo,
        UserRepository $userRepository,
        EmployeeRepository $employeeRepository
    ) {
        $this->clientProjectRepo = $clientProjectRepo;
        $this->weeklyReportRepo = $weeklyReportRepo;
        $this->userRepository = $userRepository;
        $this->employeeRepository = $employeeRepository;
        $employeeResult = '';

        foreach ($this->reportArray as $rowProject) {
            foreach ($rowProject as $rowEmployee) {
                foreach ($rowEmployee as $key => $value) {
                    $date = $value;

                    $jobcodeId = $this->clientProjectRepo->generateJobCodeId($date['jobcode_id']);
                    $employeeId = $this->userRepository->generateEmployeeId($date['employee_username']);

                    foreach ($this->alternateEmails as $key => $value) {
                        if ($key == $date['employee_username']) {
                            $employeeId = $value;
                        }
                    }

                    if (!isset($employeeId)) {
                        continue;
                    }

                    $date['jobcode_id'] = $jobcodeId;
                    $date['employee_id'] = $employeeId;

                    if (true === $this->viaAPI && !empty($date['timesheet_id'])) {
                        $weeklyReport = $this->weeklyReportRepo
                            ->model()::where('timesheet_id', $date['timesheet_id'])->first();

                        if (!empty($weeklyReport)) {
                            $weeklyReport->delete();
                        }
                    }

                    $result = $this->weeklyReportRepo->create($date);

                    if (!$result) {
                        //mail me in case issue occurs
                        // mail('kcandelario@fullscale.io', 'Error on Save DB', json_encode($date));
                    }
                }
                unset($employeeResult);
            }
        }
    }
}
