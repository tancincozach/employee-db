<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */ protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\Models\Employee' => 'App\Policies\EmployeePolicy',
        'App\Models\Applicant' => 'App\Policies\ApplicantPolicy',
        'App\Models\JobPosition' => 'App\Policies\PositionPolicy',
        'App\Models\Skill' => 'App\Policies\SkillPolicy',
        'App\Models\User' => 'App\Policies\UserPolicy',
        'App\Models\Department' => 'App\Policies\DepartmentPolicy',
        'App\Models\Address' => 'App\Policies\AddressPolicy',
        'App\Models\Asset' => 'App\Policies\AssetPolicy',
        'App\Models\ClientContact' => 'App\Policies\ClientContactPolicy',
        'App\Models\Client' => 'App\Policies\ClientPolicy',
        'App\Models\ClientProject' => 'App\Policies\ClientProjectPolicy',
        'App\Models\ClientProjectStatus' => 'App\Policies\ClientProjectStatusPolicy',
        'App\Models\ContactPerson' => 'App\Policies\ContactPersonPolicy',
        'App\Models\Contact' => 'App\Policies\ContactPolicy',
        'App\Models\Country' => 'App\Policies\CountryPolicy',
        'App\Models\EducationalAttainment' => 'App\Policies\EducationalAttainmentPolicy',
        'App\Models\EmployeeChecklist' => 'App\Policies\EmployeeChecklistPolicy',
        'App\Models\EmployeeClientProject' => 'App\Policies\EmployeeClientProjectPolicy',
        'App\Models\EmployeeDependent' => 'App\Policies\EmployeeDependentPolicy',
        'App\Models\EmployeeEducation' => 'App\Policies\EmployeeEducationPolicy',
        'App\Models\EmployeeInterest' => 'App\Policies\EmployeeInterestPolicy',
        'App\Models\EmployeeJobPosition' => 'App\Policies\EmployeeJobPositionPolicy',
        'App\Models\EmployeeLanguage' => 'App\Policies\EmployeeLanguagePolicy',
        'App\Models\EmployeeLocation' => 'App\Policies\EmployeeLocationPolicy',
        'App\Models\EmployeeOtherDetail' => 'App\Policies\EmployeeOtherDetailPolicy',
        'App\Models\EmployeeOtherSkill' => 'App\Policies\EmployeeOtherSkillPolicy',
        'App\Models\EmployeePortfolio' => 'App\Policies\EmployeePortfolioPolicy',
        'App\Models\EmployeeReport' => 'App\Policies\EmployeeReportPolicy',
        'App\Models\EmployeeSkill' => 'App\Policies\EmployeeSkillPolicy',
        'App\Models\EmployeeSpouse' => 'App\Policies\EmployeeSpousePolicy',
        'App\Models\EmployeeStatus' => 'App\Policies\EmployeeStatusPolicy',
        'App\Models\EmployeeWorkShift' => 'App\Policies\EmployeeWorkShiftPolicy',
        'App\Models\GovernmentAgency' => 'App\Policies\GovernmentAgencyPolicy',
        'App\Models\GovernmentId' => 'App\Policies\GovernmentIdPolicy',
        'App\Models\Language' => 'App\Policies\LanguagePolicy',
        'App\Models\NavMenu' => 'App\Policies\NavMenuPolicy',
        'App\Models\PreEmploymentChecklist' => 'App\Policies\PreEmploymentChecklistPolicy',
        'App\Models\Report' => 'App\Policies\ReportPolicy',
        'App\Models\ReportTemplate' => 'App\Policies\ReportTemplatePolicy',
        'App\Models\ACL\Resource' => 'App\Policies\ResourcePolicy',
        'App\Models\ACL\ResourceRolePermission' => 'App\Policies\ResourceRolePermissionPolicy',
        'App\Models\ACL\ResourceUserRolePermission' => 'App\Policies\ResourceUserRolePermissionPolicy',
        'App\Models\ACL\Role' => 'App\Policies\RolePolicy',
        'App\Models\Status' => 'App\Policies\StatusPolicy',
        'App\Models\ACL\UserRole' => 'App\Policies\UserRolePolicy',
        'App\Models\WorkExperience' => 'App\Policies\WorkExperiencePolicy',
        'App\Models\WorkShift' => 'App\Policies\WorkShiftPolicy',
        'App\Models\WeeklyReportBatch' => 'App\Policies\WeeklyReportBatchPolicy',
        'App\Models\WeeklyReportBatchDetail' => 'App\Policies\WeeklyReportBatchDetailPolicy',
        'App\Models\WeeklyReport' => 'App\Policies\WeeklyReportPolicy',
        'App\Models\ClientProjectJobcode' => 'App\Policies\ClientProjectJobcodePolicy',
        'App\Models\ClientFeedback\EmailRecipient' => 'App\Policies\EmailRecipientPolicy',
        'App\Models\ClientFeedback\EmailTemplate' => 'App\Policies\EmailTemplatePolicy',
        'App\Models\ClientFeedback\ProjectSurvey' => 'App\Policies\ProjectSurveyPolicy',
        'App\Models\ClientFeedback\QuestionCategory' => 'App\Policies\QuestionCategoryPolicy',
        'App\Models\ClientFeedback\Questionnaire' => 'App\Policies\QuestionnairePolicy',
        'App\Models\ClientFeedback\Question' => 'App\Policies\QuestionPolicy',
        'App\Models\ClientFeedback\SurveyResponse' => 'App\Policies\SurveyResponsePolicy',
        'App\Models\ClientFeedback\SurveySent' => 'App\Policies\SurveySentPolicy',
        'App\Models\WorkFromHome\WorkFromHomeRequest' => 'App\Policies\WorkFromHomeRequestPolicy',
        'App\Models\AllQuestion' => 'App\Policies\AllQuestion\QuestionPolicy',
        'App\Models\AllQuestionResponse' => 'App\Policies\AllQuestion\QuestionResponsePolicy',
        'App\Models\Zone' => 'App\Policies\ZonePolicy',
        'App\Models\ClientPreferredTeam' => 'App\Policies\ClientPreferredTeamPolicy',
        'App\Models\WorkLocation' => 'App\Policies\WorkLocationPolicy',
        'App\Models\ReferralType' => 'App\Policies\ReferralTypePolicy',
        'App\Models\ActivityLog' => 'App\Policies\ActivityLogPolicy',
        'App\Models\RouteWizard' => 'App\Policies\RouteWizardPolicy',
        'App\Models\ClientTeambuilderBucket' => 'App\Policies\ClientTeambuilderBucketPolicy',
        'App\Models\Notification' => 'App\Policies\NotificationPolicy',
        'App\Models\ClientOnboardingChecklist' => 'App\Policies\ClientOnboardingChecklistPolicy',
        'App\Models\Category' => 'App\Policies\CategoryPolicy',
        'App\Models\Holiday' => 'App\Policies\HolidayPolicy',
        'App\Models\HackerRankTestResult' => 'App\Policies\HackerRankTestResultPolicy',
        'App\Models\HackerRankTest' => 'App\Policies\HackerRankTestPolicy',
        'App\Models\Timesheet' => 'App\Policies\TimesheetPolicy',
        'App\Models\TimesheetJobcode' => 'App\Policies\TimesheetJobcodePolicy',
        'App\Models\TimesheetUser' => 'App\Policies\TimesheetUserPolicy',
        'App\Models\ClientTimesheetJobcode' => 'App\Policies\ClientTimesheetJobcodePolicy',
        'App\Models\EmployeeTimesheetUser' => 'App\Policies\EmployeeTimesheetUserPolicy',
        'App\Models\TimesheetReport' => 'App\Policies\TimesheetReportPolicy',
        'App\Models\TimesheetJobcodeAssignment' => 'App\Policies\TimesheetJobcodeAssignmentPolicy',
        'App\Models\FeedbackCategory' => 'App\Policies\FeedbackCategoryPolicy',
        'App\Models\Feedback' => 'App\Policies\FeedbackPolicy',
        'App\Models\FeedbackResponse' => 'App\Policies\FeedbackResponsePolicy',
        'App\Models\JobcodeAssignment' => 'App\Policies\JobcodeAssignmentPolicy',
        'App\Models\CreativeService' => 'App\Policies\CreativeServicePolicy',
        'App\Models\CreativeServiceApplicant' => 'App\Policies\CreativeServiceApplicantPolicy',
        'App\Models\CreativeServiceFile' => 'App\Policies\CreativeServiceFilePolicy',
        'App\Models\UserTimezone' => 'App\Policies\UserTimezonePolicy',
        'App\Models\EmployeeReportsFile' => 'App\Policies\EmployeeReportsFilePolicy',
        'App\Models\Characteristic' => 'App\Policies\CharacteristicPolicy',
        'App\Models\EmployeeAcademic' => 'App\Policies\EmployeeAcademicPolicy',
        'App\Models\EmployeeCharacteristic' => 'App\Policies\EmployeeCharacteristicPolicy',
        'App\Models\EmployeePositionType' => 'App\Policies\EmployeePositionTypePolicy',
        'App\Models\PositionType' => 'App\Policies\PositionTypePolicy',
        'App\Models\OperationsDocument' => 'App\Policies\OperationsDocumentPolicy',
        'App\Models\WeeklyFloorReport' => 'App\Policies\WeeklyFloorReportPolicy',
        'App\Models\WeeklyFloorQuestion' => 'App\Policies\WeeklyFloorQuestionPolicy',
        'App\Models\WeeklyFloorAnswer' => 'App\Policies\WeeklyFloorAnswerPolicy',
        'App\Models\Faq' => 'App\Policies\FaqPolicy',
        'App\Models\PerformEvalQuestion' => 'App\Policies\PerformEvalQuestionPolicy',
        'App\Models\PerformEvalResponse' => 'App\Policies\PerformEvalResponsePolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // define token scopes for passport
        // $this->tokenScopes();

        // passport routes
        Passport::routes();
    }

    /**
     * Define scopes for Passport Token
     *
     * @return void
     */
    private function tokenScopes()
    {
        Passport::tokensCan([
            'manage-employees' => 'Manage Employees'
        ]);
    }
}
