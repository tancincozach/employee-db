<?php

namespace App\Providers;

use App\Events\FeedbackEvent;
use App\Listeners\FeedbackListener;
use App\Events\NewApplicantEvent;
use App\Listeners\NewApplicantListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use App\Events\RetrieveTimeSheetEvent;
use App\Events\GenerateTimesheetEvent;
use App\Events\GenerateJobcodeAssignmentEvent;

use App\Listeners\RetrieveTimeSheetListener;
use App\Listeners\AdminNotification;
use App\Listeners\GenerateTimesheetListener;
use App\Listeners\GenerateJobcodeAssignmentListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        RetrieveTimeSheetEvent::class => [
            RetrieveTimeSheetListener::class,
        ],
        GenerateTimesheetEvent::class => [
            GenerateTimesheetListener::class,
        ],
        GenerateJobcodeAssignmentEvent::class => [
            GenerateJobcodeAssignmentListener::class,
        ],
        FeedbackEvent::class => [
            FeedbackListener::class,
        ],
        NewApplicantEvent::class => [
            NewApplicantListener::class,
        ],
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        AdminNotification::class,
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
