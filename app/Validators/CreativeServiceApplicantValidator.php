<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

/**
 * Class CreativeServiceApplicantValidator
 * @package App\Validators
 */
class CreativeServiceApplicantValidator extends LaravelValidator
{
    /**
     * CreativeServiceApplicantValidator constructor.
     * @param Factory $validator
     */
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'full_name' => 'required',
                'email' => 'required',
            ],
            ValidatorInterface::RULE_UPDATE => [
                'full_name' => 'required',
                'email' => 'required',
            ]
        ]);
    }
}
