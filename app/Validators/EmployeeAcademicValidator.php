<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

/**
 * Class EmployeeAcademicValidator
 * @package App\Validators
 */
class EmployeeAcademicValidator extends LaravelValidator
{
    /**
     * EmployeeAcademicValidator constructor.
     * @param Factory $validator
     */
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [],
            ValidatorInterface::RULE_UPDATE => []
        ]);
    }
}
