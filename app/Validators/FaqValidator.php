<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

class FaqValidator extends LaravelValidator
{
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'question' => 'required|unique:faqs,question',
                'answer' => 'required:faqs,answer'
            ],
            ValidatorInterface::RULE_UPDATE => [
                'question' => 'required:faqs,question',
                'answer' => 'required:faqs,answer'
            ]
        ]);
    }
}
