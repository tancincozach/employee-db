<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Support\Facades\Request;

class WeeklyReportBatchViewableValidator extends LaravelValidator
{
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'client_id'             => 'required',
                'jobcode'               => 'required',
                'start_date'            => 'required',
                'end_date'              => 'required',
                'posted_by_user_id'     => 'required'
            ],
            ValidatorInterface::RULE_UPDATE => [
                
            ]
        ]);
    }
}
