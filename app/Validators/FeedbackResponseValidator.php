<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

/**
 * Class FeedbackResponseValidator
 * @package App\Validators
 */
class FeedbackResponseValidator extends LaravelValidator
{
    /**
     * FeedbackResponseValidator constructor.
     * @param Factory $validator
     */
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'feedback_id' => 'required',
                'user_id' => 'required',
                'response' => 'required',
            ],
            ValidatorInterface::RULE_UPDATE => [
                'feedback_id' => 'required',
                'user_id' => 'required',
                'response' => 'required',
            ]
        ]);
    }
}
