<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

/**
 * Class EmployeeCharacteristicValidator
 * @package App\Validators
 */
class EmployeeCharacteristicValidator extends LaravelValidator
{
    /**
     * EmployeeCharacteristicValidator constructor.
     * @param Factory $validator
     */
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'employee_id' => 'required',
                'characteristic_id' => 'required',
                'rate' => 'required',
            ],
            ValidatorInterface::RULE_UPDATE => [
                'employee_id' => 'required',
                'characteristic_id' => 'required',
                'rate' => 'required',
            ]
        ]);
    }
}
