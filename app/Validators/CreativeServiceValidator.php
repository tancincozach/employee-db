<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

/**
 * Class CreativeServiceValidator
 * @package App\Validators
 */
class CreativeServiceValidator extends LaravelValidator
{
    /**
     * CreativeServiceValidator constructor.
     * @param Factory $validator
     */
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'company' => 'required',
                'overview' => 'required',
                'project' => 'required',
                'purpose' => 'required',
                'needed_at' => 'required',
            ],
            ValidatorInterface::RULE_UPDATE => [
                'company' => 'required',
                'overview' => 'required',
                'project' => 'required',
                'purpose' => 'required',
                'needed_at' => 'required',
            ]
        ]);
    }
}
