<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

/**
 * Class CreativeServiceFileValidator
 * @package App\Validators
 */
class CreativeServiceFileValidator extends LaravelValidator
{
    /**
     * CreativeServiceApplicantValidator constructor.
     * @param Factory $validator
     */
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [],
            ValidatorInterface::RULE_UPDATE => []
        ]);
    }
}
