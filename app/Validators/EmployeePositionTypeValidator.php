<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

/**
 * Class EmployeePositionTypeValidator
 * @package App\Validators
 */
class EmployeePositionTypeValidator extends LaravelValidator
{
    /**
     * EmployeePositionTypeValidator constructor.
     * @param Factory $validator
     */
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'employee_id' => 'required',
                'position_type_id' => 'required',
                'experience' => 'required',
            ],
            ValidatorInterface::RULE_UPDATE => [
                'employee_id' => 'required',
                'position_type_id' => 'required',
                'experience' => 'required',
            ]
        ]);
    }
}
