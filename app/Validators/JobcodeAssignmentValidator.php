<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Support\Facades\Request;

class JobcodeAssignmentValidator extends LaravelValidator
{
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'user_id'    => 'required',
                'jobcode_id' => 'required',
            ],
            ValidatorInterface::RULE_UPDATE => [
                'user_id'    => 'required',
                'jobcode_id' => 'required',
            ]
        ]);
    }
}
