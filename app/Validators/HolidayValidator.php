<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Support\Facades\Request;

class HolidayValidator extends LaravelValidator
{
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'name' => 'required',
                'date' => 'required|unique:holidays,date',
                'type' => 'required',
            ],
            ValidatorInterface::RULE_UPDATE => [
                'name' => 'required',
                'date' => 'required|unique:holidays,date',
                'type' => 'required',
            ]
        ]);
    }
}
