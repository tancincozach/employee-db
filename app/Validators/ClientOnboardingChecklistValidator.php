<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Support\Facades\Request;

class ClientOnboardingChecklistValidator extends LaravelValidator
{
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'date_occurred' => 'required'
            ],
            ValidatorInterface::RULE_UPDATE => [
                'date_occurred' => 'required'
            ]
        ]);
    }
}
