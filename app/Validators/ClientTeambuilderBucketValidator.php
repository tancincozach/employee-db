<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class ClientTeambuilderBucketValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'client_id' => 'required|exists:clients,id',
            'employee_id' => 'required|exists:employees,id',
            'job_position_id' => 'required|exists:job_positions,id',
            'status' => 'required|in:suggested,selected,declined,assigned'
        ],
        ValidatorInterface::RULE_UPDATE => [
        ]
    ];
}
