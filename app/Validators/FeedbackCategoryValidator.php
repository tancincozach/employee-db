<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

/**
 * Class FeedbackCategoryValidator
 * @package App\Validators
 */
class FeedbackCategoryValidator extends LaravelValidator
{
    /**
     * FeedbackCategoryValidator constructor.
     * @param Factory $validator
     */
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'category' => 'required',
            ],
            ValidatorInterface::RULE_UPDATE => [
                'category' => 'required',
            ]
        ]);
    }
}
