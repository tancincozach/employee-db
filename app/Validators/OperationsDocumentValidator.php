<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

class OperationsDocumentValidator extends LaravelValidator
{
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'name' => 'required',
                'url' => 'required'
            ],
            ValidatorInterface::RULE_UPDATE => [
                'name' => 'required',
                'url' => 'required'
            ]
        ]);
    }
}
