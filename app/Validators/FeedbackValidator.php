<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

/**
 * Class FeedbackValidator
 * @package App\Validators
 */
class FeedbackValidator extends LaravelValidator
{
    /**
     * FeedbackCategoryValidator constructor.
     * @param Factory $validator
     */
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'category_id' => 'required',
                'feedback' => 'required',
            ],
            ValidatorInterface::RULE_UPDATE => [
                'category_id' => 'required',
                'feedback' => 'required',
            ]
        ]);
    }
}
