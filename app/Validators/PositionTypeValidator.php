<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

/**
 * Class PositionTypeValidator
 * @package App\Validators
 */
class PositionTypeValidator extends LaravelValidator
{
    /**
     * FeedbackCategoryValidator constructor.
     * @param Factory $validator
     */
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'name' => 'required',
            ],
            ValidatorInterface::RULE_UPDATE => [
                'name' => 'required',
            ]
        ]);
    }
}
