<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;
use Illuminate\Contracts\Validation\Factory;

class UserTimezoneValidator extends LaravelValidator
{
    public function __construct(Factory $validator)
    {
        parent::__construct($validator);

        $this->setRules([
            ValidatorInterface::RULE_CREATE => [
                'user_id' => 'sometimes|required',
                'zone_id' => 'sometimes|required',
            ],
            ValidatorInterface::RULE_UPDATE => [
                'user_id' => 'sometimes|required',
                'zone_id' => 'sometimes|required',
            ]
        ]);
    }
}
