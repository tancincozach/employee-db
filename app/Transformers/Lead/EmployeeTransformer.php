<?php

namespace App\Transformers\Lead;

use App\Models\Employee;
use App\Transformers\EmployeeTransformer as BaseTransformer;
use App\Helpers\EmployeeAvailabilityHelper;

class EmployeeTransformer extends BaseTransformer
{
    public function transform(Employee $employee)
    {
        $data = parent::transform($employee);

        // hide unnecessary data
        unset(
            $data['department'],
            $data['email'],
            $data['employee_no'],
            $data['contact_no'],
            $data['initial'],
            $data['date_of_birth'],
            $data['civil_status'],
            $data['date_hired'],
            $data['regularization_date'],
            $data['intellicare_id_no'],
            $data['status'],
            $data['user_id'],
            $data['unique_str'],
            $data['preferredTeams'],
            $data['workLocation']
        );


        $additional = [
            'profile_picture' => $this->profilePicture($employee->photo),
            'available'       => $this->isAvailable($employee->id),
        ];

        return array_merge($data, $additional);
    }

    public function profilePicture($employeePhoto)
    {
        $url = url('api/asset?path=');
        $profile = $employeePhoto->where('type', 1)->sortByDesc('id')->first();

        return $profile ? $url . $profile->path : '';
    }

    public function isAvailable($id)
    {
        $helper = app(EmployeeAvailabilityHelper::class);

        return $helper->setId($id)->isAvailable();
    }
}
