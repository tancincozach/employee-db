<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Models\NotificationUser;

class NotificationUserTransformer extends TransformerAbstract
{
    public function transform(NotificationUser $notificationUser)
    {
        return [
            'notification_id' => $notificationUser->notification_id ?? null,
            'user_id'         => (int)$notificationUser->user_id ?? null,
            'read_at'         => $notificationUser->read_at ?? null,
        ];
    }
}
