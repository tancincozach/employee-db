<?php

namespace App\Transformers\Timesheet;

use League\Fractal\TransformerAbstract;
use App\Models\TimesheetJobcodeAssignment;

class TimesheetEmployeeNamesTransformer extends TransformerAbstract
{
    public function transform(TimesheetJobcodeAssignment $timesheetAssignment)
    {
        return [
            'id'   => $this->num($timesheetAssignment->timesheet_user_id),
            'name' => $this->str($this->name($timesheetAssignment)),
        ];
    }

    private function str($str)
    {
        return $str ?? '';
    }

    private function num($num)
    {
        return $num ? (int) $num : 0;
    }

    private function name($timesheetAssignment)
    {
        if ($timesheetAssignment->employee_id) {
            return $timesheetAssignment->employee_name;
        }

        return $timesheetAssignment->timesheet_user_name;
    }
}
