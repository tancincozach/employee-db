<?php

namespace App\Transformers\Timesheet;

use League\Fractal\TransformerAbstract;
use App\Models\ClientTimesheetJobcode;

use App\Traits\FilterTrait;

class MappedClientTransformer extends TransformerAbstract
{
    use FilterTrait;

    public function transform(ClientTimesheetJobcode $mapping)
    {
        return [
            'id'                   => $this->num($mapping->id),
            'timesheet_jobcode_id' => $this->num($mapping->timesheet_jobcode_id),
            'timesheet_jobcode'    => $this->str($mapping->timesheet_jobcode),
            'client_id'            => $this->num($mapping->client_id),
            'client'               => $this->str($mapping->client),
            'created_at'           => $this->str($mapping->created_at),
            'updated_at'           => $this->str($mapping->updated_at),
            'deleted_at'           => $this->str($mapping->deleted_at),
        ];
    }
}
