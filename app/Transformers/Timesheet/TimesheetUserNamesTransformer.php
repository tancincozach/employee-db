<?php

namespace App\Transformers\Timesheet;

use League\Fractal\TransformerAbstract;
use App\Models\TimesheetUser;

use App\Traits\FilterTrait;

class TimesheetUserNamesTransformer extends TransformerAbstract
{
    use FilterTrait;

    public function transform(TimesheetUser $user)
    {
        return [
            'id'   => $this->num($user->id),
            'name' => $this->name($user),
        ];
    }

    public function name($user)
    {
        $name = ucwords(trim($user->last_name . ', ' . $user->first_name));

        return $this->str($name);
    }
}
