<?php

namespace App\Transformers\Timesheet;

use League\Fractal\TransformerAbstract;
use App\Models\Timesheet;
use App\Traits\FilterTrait;

class WeeklyTransformer extends TransformerAbstract
{
    use FilterTrait;

    public function transform(Timesheet $timesheet)
    {
        return [
            'timesheet_user_id' => $this->num($timesheet->timesheet_user_id),
            'log_date'          => $this->str($timesheet->log_date),
            'week'              => $this->num($timesheet->week),
            'employee'          => $this->str($timesheet->employee),
            'client'            => $this->str($timesheet->client),
            'type'              => $this->str($timesheet->type),
            'day_total'         => $this->str($timesheet->day_total),
        ];
    }
}
