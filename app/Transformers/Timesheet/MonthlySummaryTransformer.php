<?php

namespace App\Transformers\Timesheet;

use League\Fractal\TransformerAbstract;
use App\Traits\FilterTrait;
use Illuminate\Support\Facades\Auth;

use App\Models\Timesheet;
use App\Repositories\Employee\EmployeeJobPositionRepository;
use App\Repositories\Timesheet\TimesheetRepository;
use App\Repositories\User\UserTimezoneRepository;

class MonthlySummaryTransformer extends TransformerAbstract
{
    use FilterTrait;

    protected $year;
    protected $month;
    protected $clientTz;

    public function transform(Timesheet $timesheet)
    {
        return [
            'id'           => $this->num($timesheet->id),
            'user_id'      => $this->num($timesheet->user_id),
            'jobcode_id'   => $this->num($timesheet->jobcode_id),
            'job_position' => $this->position($timesheet->id),
            'employee'     => $this->str($timesheet->employee),
            'client'       => $this->str($timesheet->client),
            'rendered'     => $this->dec($timesheet->rendered),
            'target'       => $this->num($timesheet->target),
            'percentage'   => $this->dec($timesheet->percentage),
            'last_updated' => $this->getLastUpdated($timesheet),
        ];
    }

    public function position($employeeId)
    {
        $job_position = app(EmployeeJobPositionRepository::class);

        return $this->str($job_position->getPositions($employeeId));
    }

    public function getLastUpdated($timesheet)
    {
        $lastUpdated = app(TimesheetRepository::class)->getLastUpdated(
            $timesheet->user_id,
            $timesheet->jobcode_id,
            $this->year,
            $this->month
        );

        $userTimezone = app(UserTimezoneRepository::class);
        $userTimezone = $userTimezone->getZoneName(Auth::id());

        return app(DailyTrackerTransformer::class)->timezone(
            $lastUpdated,
            $userTimezone,
            'Y-m-d h:i A T'
        );
    }

    public function setYearMonth($year, $month)
    {
        $this->year = $year;
        $this->month = $month;
    }

    public function setClientTz($tz)
    {
        $this->clientTz = $tz;
    }
}
