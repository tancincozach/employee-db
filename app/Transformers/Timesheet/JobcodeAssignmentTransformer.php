<?php

namespace App\Transformers\Timesheet;

use League\Fractal\TransformerAbstract;
use App\Models\JobcodeAssignment;

use App\Traits\FilterTrait;

class JobcodeAssignmentTransformer extends TransformerAbstract
{
    use FilterTrait;

    public function transform(JobcodeAssignment $jobcode)
    {
        return [
            'id'         => $this->num($jobcode->id),
            'user_id'    => $this->num($jobcode->user_id),
            'jobcode_id' => $this->num($jobcode->jobcode_id),
            'active'     => $this->str($jobcode->active),
        ];
    }
}
