<?php

namespace App\Transformers\Timesheet;

use League\Fractal\TransformerAbstract;
use App\Models\TimesheetJobcode;

use App\Traits\FilterTrait;

class TimesheetJobcodeNamesTransformer extends TransformerAbstract
{
    use FilterTrait;

    public function transform(TimesheetJobcode $jobcode)
    {
        return [
            'id'   => $this->num($jobcode->id),
            'name' => $this->str($jobcode->name),
        ];
    }
}
