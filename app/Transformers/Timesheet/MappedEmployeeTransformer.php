<?php

namespace App\Transformers\Timesheet;

use League\Fractal\TransformerAbstract;
use App\Models\TimesheetUser;

use App\Traits\FilterTrait;

class MappedEmployeeTransformer extends TransformerAbstract
{
    use FilterTrait;

    public function transform(TimesheetUser $user)
    {
        return [
            'id'                  => $this->num($user->id),
            'timesheet_user_id'   => $this->num($user->timesheet_user_id),
            'employee_id'         => $this->num($user->employee_id),
            'timesheet_user_name' => $this->str($user->timesheet_user_name),
            'employee_name'       => $this->str($user->employee_name),
            'created_at'          => $this->str($user->created_at),
            'updated_at'          => $this->str($user->updated_at),
            'deleted_at'          => $this->str($user->deleted_at),
        ];
    }
}
