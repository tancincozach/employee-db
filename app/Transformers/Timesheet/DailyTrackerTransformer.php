<?php

namespace App\Transformers\Timesheet;

use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Auth;

use App\Models\Timesheet;
use App\Traits\FilterTrait;
use App\Models\UserType;
use App\Repositories\User\UserTimezoneRepository;

class DailyTrackerTransformer extends TransformerAbstract
{
    use FilterTrait;

    /*
     * Timezone for non-regular jobcodes. e.g PTO, Rest/Lunch Break
     * if converted to client timezone
     * @var string
     */
    const DEFAULT_TZ = 'America/Chicago';

    /*
     * We will store the client timezone
     * if the current user is a client
     *
     * @var string
     */
    protected $clientTz = '';

    /*
     * Convert timezone
     *
     * @var boolean
     */
    protected $convertTz = false;

    public function transform(Timesheet $timesheet)
    {
        $dates = $this->dates($timesheet);

        $default = [
            'id'            => $this->num($timesheet->id),
            'user_id'       => $this->num($timesheet->user_id),
            'tz_str'        => $this->str($timesheet->tz_str),
            'client_tz'     => $this->str($timesheet->client_tz),
            'duration'      => $this->num($timesheet->duration),
            'company'       => $this->str($timesheet->company),
            'client_id'     => $this->num($timesheet->client_id),
            'jobcode_type'  => $this->str($timesheet->jobcode_type),
            'working_hrs'   => $this->dec($timesheet->working_hrs),
            'hrs_formatted' => $this->str(gmdate("H:i", $timesheet->duration)),
        ];

        if ($timesheet->type == 'manual') {
            $dates['start'] = '';
            $dates['end'] = '';
            $default['working_hrs'] = '';
        }

        return array_merge($default, $dates);
    }

    private function dec($num)
    {
        return $num ? number_format($num, 2) : 0;
    }

    public function timezone($date, $tz, $format)
    {
        if (empty($tz)) {
            $tz = self::DEFAULT_TZ;
        }

        $date = new \DateTime($date);
        $date = $date->setTimezone(new \DateTimeZone($tz));

        return $this->str($date->format($format));
    }

    private function dates($timesheet)
    {
        $currentUser = Auth::user();
        $userTimezone = app(UserTimezoneRepository::class);
        $userTimezone = $userTimezone->getZoneName($currentUser->id);

        // set timezone to user preferred timezone
        $dates = [
            'log_date'     => $this->timezone($timesheet->log_date, $userTimezone, 'F d, Y'),
            'day'          => $this->timezone($timesheet->log_date, $userTimezone, 'D'),
            'start'        => $this->timezone($timesheet->start, $userTimezone, 'h:i A T'),
            'end'          => $this->timezone($timesheet->end, $userTimezone, 'h:i A T'),
            'last_updated' => $this->timezone($timesheet->last_updated, $userTimezone, 'Y-m-d h:i A T'),
        ];

        $isClient = $currentUser->user_type_id == UserType::CLIENT;

        // If the current user is a client
        if ($isClient && $this->convertTz) {
            $dates = [
                'log_date'     => $this->timezone($timesheet->log_date, $timesheet->tz_str, 'F d, Y'),
                'day'          => $this->timezone($timesheet->log_date, $timesheet->tz_str, 'D'),
                'start'        => $this->timezone($timesheet->start, $timesheet->tz_str, 'h:i A T'),
                'end'          => $this->timezone($timesheet->end, $timesheet->tz_str, 'h:i A T'),
                'last_updated' => $this->timezone($timesheet->last_updated, $timesheet->tz_str, 'Y-m-d h:i A T'),
            ];
        }

        // else
        if (!$isClient && $this->convertTz) {
            $dates = [
                'log_date'     => $this->timezone($timesheet->log_date, $this->clientTz, 'F d, Y'),
                'day'          => $this->timezone($timesheet->log_date, $this->clientTz, 'D'),
                'start'        => $this->timezone($timesheet->start, $this->clientTz, 'h:i A T'),
                'end'          => $this->timezone($timesheet->end, $this->clientTz, 'h:i A T'),
                'last_updated' => $this->timezone($timesheet->last_updated, $this->clientTz, 'Y-m-d h:i A T'),
            ];
        }

        return $dates;
    }

    public function setClientTz($tz = '')
    {
        $this->clientTz = $tz;
    }

    public function convertTz($flag = true)
    {
        $this->convertTz = $flag;
    }
}
