<?php

namespace App\Transformers;

use App\Models\OperationsDocument;
use League\Fractal\TransformerAbstract;

class OperationsDocumentTransformer extends TransformerAbstract
{
    public function transform(OperationsDocument $doc)
    {
        return ['id' => $doc->id,
                'name' => $doc->name,
                'url' => $doc->url,
                'updated_by_user' => $doc->updated_by_user,
                'deleted_at' => $doc->deleted_at];
    }
}
