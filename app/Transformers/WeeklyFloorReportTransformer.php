<?php

namespace App\Transformers;

use App\Models\WeeklyFloorReport;
use League\Fractal\TransformerAbstract;

/**
 * Class WeeklyFloorReportTransformer
 * @package App\Transformers
 */
class WeeklyFloorReportTransformer extends TransformerAbstract
{
    /**
     * @param WeeklyFloorReport $WeeklyFloorReport
     * @return array
     */
    public function transform(WeeklyFloorReport $weeklyFloorReport)
    {
        return $weeklyFloorReport->toArray();
    }
}
