<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Skill;

class SkillTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'categories'
    ];

    public function transform(Skill $skill)
    {
        return [
            'id'          => (int)$skill->id ?? null,
            'name'        => $skill->name ?? '',
            'description' => $skill->description ?? '',
            'color'       => $skill->color ?? '',
            'deleted_at'  => $skill->deleted_at ?? ''
        ];
    }

    /**
     * Include Skill Categories
     *
     * @param  Skill $skill
     * @return Collection
     */
    public function includeCategories(Skill $skill)
    {
        return $this->collection($skill->categories, new SkillCategoryTransformer());
    }

}
