<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\WeeklyReport;
use App\Models\Zone;
use App\Models\ClientProjectJobcode;
use App\Services\TSheetParserService;
use App\Repositories\WeeklyReport\WeeklyReportRepository;
use App\Repositories\Holiday\HolidayRepository;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Employee\EmployeeJobPositionRepository;
use App\Helpers\RocksHelper;
use Carbon\Carbon;

class WeeklyReportTransformer extends TransformerAbstract
{
    protected $otherDetails;
    protected $reportFilter;
    protected $employeeWorkingReport;
    protected $rawFilter;
    protected $startDate;
    protected $endDate;
    protected $isWeekly;
    protected $isPostedReport;
    protected $clientId;
    protected $jobcodeIds;
    protected $jobcodeId;
    protected $currentMonth;
    protected $currentYear;

    public function transform(WeeklyReport $report)
    {
        if ($this->otherDetails || $this->reportFilter || $this->employeeWorkingReport) {
            $job_position = app(EmployeeJobPositionRepository::class);
            $jobcode = $report->clientProjectJobcode->jobcode;
            $jobcode_id = $report->clientProjectJobcode->id;

            $result = [
                'year'          => date("Y", strtotime($report->log_date)),
                'month'         => date("m", strtotime($report->log_date)),
                'employee_id'   => $report->employee_id,
                'employee'      => $report->employee ? $report->employee->getName() : 'FULL SCALE',
                'jobcode'       => $jobcode,
                'jobcode_id'    => $jobcode_id,
                'job_position'  => $job_position->getPositions($report->employee_id),
            ];

            if ($this->otherDetails) {
                $result['workLogs'] = TsheetParserService::formatWorkLogs($result);
                $result['headers'] = TsheetParserService::getDateHeaders($result['workLogs']);
            } elseif ($this->reportFilter) {
                if ($jobcode == 'Paid Time Off') {
                    $newJobcode = ClientProjectJobcode::getJobcodeById($this->jobcodeId);
                    $result['jobcode'] = $newJobcode ?: 'Paid Time Off';
                    $result['jobcode_id'] = $newJobcode ? $this->jobcodeId : $jobcode_id;
                }
                if ($this->isPostedReport) { // Client's View
                    $result['weekly_headers'] = TsheetParserService::getHeadersByMonth($this->currentYear, $this->currentMonth, $this->clientId);
                    $result['weekly_content'] = TsheetParserService::getWeeklyContent($result['weekly_headers'], $result, $this->jobcodeIds);
                } else { // Admin's View
                    $result['weekly_headers'] = TsheetParserService::getHeadersByMonth($this->currentYear, $this->currentMonth, $this->clientId);
                    $result['weekly_content'] = TsheetParserService::getWeeklyContent($result['weekly_headers'], $result, $this->jobcodeIds);
                }
            } elseif ($this->employeeWorkingReport) {
                $result['working_hrs'] = number_format($report->working_hrs, 2);

                $result['total_working_hrs'] = WeeklyReport::TOTAL_WORKING_MONTHLY_HOURS;
                $result['total_working_percentage'] = WeeklyReportRepository::getWorkingHourPercentage($result['working_hrs']);
                $result['id'] = $report->id;
            }

            return $result;
        } elseif ($this->rawFilter) {
            $clientTimezone = [];

            $timezone = Zone::DEFAULT_TIMEZONE;
            if ($report->clientProjectJobcode->client_id) {
                $timezone = $report->clientProjectJobcode->client->timezone;
            } else if ($this->clientId) {
                $timezone = app(ClientRepository::class)->getTimezone($this->clientId);
                $timezone = $timezone ?? Zone::DEFAULT_TIMEZONE;
            }

            $clientTimezone['client_timezone'] = $report->convertToClientTimezone($timezone);

            $jobcode_id = $report->clientProjectJobcode->id;
            $jobcode = $report->clientProjectJobcode->jobcode;
            $accumulated_hours = 0;
            if (!in_array($jobcode, ClientProjectJobcode::NON_WORK_JOBCODES)) {
                $accumulated_hours = number_format($report->working_hrs, 2);
            }

            $holiday = app(HolidayRepository::class);
            $is_holiday = $holiday->getId($report->log_date);
            $start_date = Carbon::parse($report->start_date)->format('Y-m-d');
            $log_date = Carbon::parse($report->log_date)->format('Y-m-d');
            $is_holiday = $is_holiday && ($log_date != $start_date);

            $reports = [
                'id'                => (int)$report->id,
                'jobcode_id'        => $jobcode_id,
                'jobcode'           => $jobcode,
                'log_date'          => $report->log_date,
                'start_date'        => $report->start_date,
                'end_date'          => $report->end_date,
                'hours'             => number_format($report->working_hrs, 2),
                'accumulated_hours' => $accumulated_hours,
                'is_holiday'        => $is_holiday,
            ];

            return array_merge($reports, $clientTimezone);
        } else {
            return [
                date("Y", strtotime($report->log_date)) =>
                date("m", strtotime($report->log_date))
            ];
        }
    }

    public function setOtherDetails($option = false)
    {
        $this->otherDetails = $option;
    }

    public function setFilterReport($option = false)
    {
        $this->reportFilter = $option;
    }

    public function setRawReport($option = false)
    {
        $this->rawFilter = $option;
    }

    public function setEmployeeWorkingReport($option = false)
    {
        $this->employeeWorkingReport = $option;
    }

    public function setMonthlyReport($clientId, $jobcodeIds, $option = false, $jobcodeId = 0)
    {
        $this->clientId = $clientId;
        $this->jobcodeIds = $jobcodeIds;
        $this->isPostedReport = $option;
        $this->jobcodeId = $jobcodeId;
    }

    /*
     * ClientId setter
     *
     * @param integet $clientId
     *
     * @return void
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /*
     * Set current month and year
     *
     * @param integer $month
     * @param interger $year
     *
     * return @void
     */
    public function setCurrentMonthAndYear($month, $year)
    {
        $this->currentMonth = $month;
        $this->currentYear = $year;
    }
}
