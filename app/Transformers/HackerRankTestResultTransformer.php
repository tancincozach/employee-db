<?php

namespace App\Transformers;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

use App\Models\HackerRankTestResult;

class HackerRankTestResultTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'hackerRankTest'
    ];

    public function transform(HackerRankTestResult $hackerTestResult)
    {

        return [
            'id'   => (int)$hackerTestResult->id ?? null,
            'result_id' => (int)$hackerTestResult->result_id ?? null,
            'test_id' => (int)$hackerTestResult->test_id ?? null,
            'test_name' => $hackerTestResult->test_name ?? '',
            'skill_name' => $hackerTestResult->skill_name ?? '',
            'fullname' => $hackerTestResult->fullname ?? '',
            'employee_id' => (int)$hackerTestResult->employee_id ?? null,
            'score' => $hackerTestResult->raw_score ?? null,
            'emp_status' => $hackerTestResult->statname ?? null,
            'percent_score' => $hackerTestResult->percent_score ?? null,
            'published' => $hackerTestResult->published ?? false,
            'takenstart' => $hackerTestResult->dtstart_taken ?? null,
            'takenend' => $hackerTestResult->dtend_taken ?? null,
            'test_url' => $hackerTestResult->test_url ?? null
        ];
    }

    /**
     * Include HackerRanks Test Results
     *
     * @param  HackerRankTestResult $hackerRankTestResult
     * @return Collection
     */
    public function includeHackerRankTest(HackerRankTestResult $hackerRankTestResult)
    {
        return $this->item($hackerRankTestResult->hackerRankTest, new HackerRankTestTransformer());
    }    
}
