<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\UserTimezone;

class UserTimezoneTransformer extends TransformerAbstract
{
    public function transform(UserTimezone $userTimezone)
    {
        return [
            'id'         => (int) $userTimezone->id,
            'user_id'    => (int) $userTimezone->user_id,
            'zone_id'    => (int) $userTimezone->zone_id,
            'email'      => $userTimezone->user->email ?? '',
            'timezone'   => $userTimezone->zone->zone_name ?? '',
            'created_at' => $userTimezone->created_at,
            'updated_at' => $userTimezone->updated_at,
        ];
    }
}
