<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\TimesheetReport;

class TimesheetReportTransformer extends TransformerAbstract
{
    protected $timezone;

    public function transform(TimesheetReport $timesheet)
    {
        $this->timezone = $timesheet->client_timezone;

        return [
            'id'                     => $this->num($timesheet->id),
            'timesheet_user_id'      => $this->num($timesheet->timesheet_user_id),
            'timesheet_user'         => $this->num($timesheet->timesheet_user),
            'timesheet_jobcode_id'   => $this->num($timesheet->timesheet_jobcode_id),
            'timesheet_jobcode_name' => $this->str($timesheet->timesheet_jobcode_name),
            'client_id'              => $this->num($timesheet->client_id),
            'client_name'            => $this->str($timesheet->client_name),
            'employee_id'            => $this->num($timesheet->employee_id),
            'employee'               => $this->employee($timesheet),
            'email'                  => $this->str($timesheet->email),
            'log_date'               => $this->str($timesheet->log_date),
            'day'                    => $this->str(date('D', strtotime($timesheet->log_date))),
            'start'                  => $this->str($timesheet->start),
            'end'                    => $this->str($timesheet->end),
            'duration'               => $this->num($timesheet->duration),
            'working_hrs'            => $this->dec($timesheet->working_hrs),
            'tz'                     => $this->str($timesheet->tz),
            'tz_str'                 => $this->str($timesheet->tz_str),
            'type'                   => $this->str($timesheet->type),
            'location'               => $this->str($timesheet->location),
            'on_the_clock'           => $this->num($timesheet->on_the_clock),
            'notes'                  => $this->str($timesheet->notes),
            'last_modified'          => $this->str($timesheet->last_modified),
            'assigned_to_all'        => $this->num($timesheet->assigned_to_all),
            'jobcode_active'         => $this->num($timesheet->jobcode_active),
            'jobcode_type'           => $this->str($timesheet->jobcode_type),
            'user_active'            => $this->num($timesheet->user_active),
            'timesheet_username'     => $this->str($timesheet->timesheet_username),
            'timesheet_email'        => $this->str($timesheet->timesheet_email),
            'user_last_active'       => $this->str($timesheet->user_last_active),
            'pto_balances'           => $this->json($timesheet->pto_balances),
            'user_created'           => $this->str($timesheet->user_created),
            'client_timezone'        => $this->str($timesheet->client_timezone),
            'client_log_date'        => $this->timezone($timesheet->log_date, 'F d, Y'),
            'client_day'             => $this->timezone($timesheet->log_date, 'D'),
            'client_start'           => $this->timezone($timesheet->start, 'h:i A T'),
            'client_end'             => $this->timezone($timesheet->end, 'h:i A T'),
            'created_at'             => $this->str($timesheet->created_at),
            'updated_at'             => $this->str($timesheet->updated_at),
            'deleted_at'             => $this->str($timesheet->deleted_at),
        ];
    }

    private function str($str)
    {
        return $str ?? '';
    }

    private function num($num)
    {
        return $num ? (int) $num : 0;
    }

    private function dec($num)
    {
        return $num ? (float) $num : 0;
    }

    private function json($num)
    {
        return $num ? json_decode($num, true) : '';
    }

    private function employee($employee)
    {
        return ucwords(trim(
            $employee->last_name . ', ' . $employee->first_name . ' '
            . $employee->middle_name . ' ' . $employee->ext
        ));
    }

    public function timezone($date, $format)
    {
        if (empty($this->timezone)) {
            return '';
        }

        $date = new \DateTime($date);
        $date = $date->setTimezone(new \DateTimeZone($this->timezone));

        return $this->str($date->format($format));
    }
}
