<?php

namespace App\Transformers;

use App\Models\CreativeService;
use League\Fractal\TransformerAbstract;

/**
 * Class FeedbackCategoryTransformer
 * @package App\Transformers
 */
class CreativeServiceTransformer extends TransformerAbstract
{
    /**
     * @param CreativeService $creativeService
     * @return array
     */
    public function transform(CreativeService $creativeService)
    {
        $jsonDecodedProject = json_decode($creativeService->project);
        $creativeService->project = implode(", ", $jsonDecodedProject);

        $jsonDecodedPurpose = json_decode($creativeService->purpose);
        $creativeService->purpose = implode(", ", $jsonDecodedPurpose);
        return $creativeService->toArray();
    }
}
