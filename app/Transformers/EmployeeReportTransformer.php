<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\EmployeeReport;

class EmployeeReportTransformer extends TransformerAbstract
{

    public function transform(EmployeeReport $empReport)
    {
        return [
            'id'             => (int)$empReport->id,
            'employee'       => $empReport->employee,
            'clientProject'  => $empReport->clientProject,
            'reportTemplate' => $empReport->reportTemplate,
            'subject'        => $empReport->subject,
            'content'        => $this->stringCleanup($empReport->content),
            'todo'           => $this->stringCleanup($empReport->todo),
            'roadblocks'     => $this->stringCleanup($empReport->roadblocks),
            'remarks'        => $this->stringCleanup($empReport->remarks),
            'send_to'        => $empReport->send_to,
            'sent'           => $empReport->sent,
            'report_date'    => $empReport->report_date,
            'created_at'     => $empReport->created_at->format('Y-m-d H:i:s'),
        ];
    }

    public function stringCleanup($str)
    {
        return trim(nl2br($str));
    }
}
