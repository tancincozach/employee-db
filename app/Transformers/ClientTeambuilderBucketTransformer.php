<?php

namespace App\Transformers;

use App\Models\ClientTeambuilderBucket;
use League\Fractal\TransformerAbstract;
use App\Services\EmployeeService;

class ClientTeambuilderBucketTransformer extends TransformerAbstract
{
    protected $getData;
    protected $defaultIncludes = [
        /* using this kind of includes as when we use with 
           there is no data sample employee->data is desired
            output is employee only */
        'employee',
    ];

    public function setGetData($arr)
    {
        $this->getData = $arr;
    }

    public function transform(ClientTeambuilderBucket $clientTeambuilderBucket)
    {
        if (empty($this->getData)) {
            $status_caption = $clientTeambuilderBucket->status;
            if (strtoupper($clientTeambuilderBucket->status) == strtoupper('selected')) {
                $status_caption = 'Saved';
            }
            return [
                'client_teambuilder_bucket_id'  => $clientTeambuilderBucket->id ?? null,
                'first_name'                    => $clientTeambuilderBucket->employee->first_name ?? '',
                'middle_name'                   => $clientTeambuilderBucket->employee->middle_name ?? '',
                'last_name'                     => $clientTeambuilderBucket->employee->last_name ?? '',
                'photo'                         => $clientTeambuilderBucket->employee->photo ?? [],
                'job_position_id'               => $clientTeambuilderBucket->job_position_id ?? null,
                'positions'                     => EmployeeService::getEmployeePositions($clientTeambuilderBucket->employee),
                'status'                        => $clientTeambuilderBucket->status ?? '',
                'status_caption'                => $status_caption,
                'is_suggested_admin'            => $clientTeambuilderBucket->is_suggested_admin ?? 0,
                'profile_url'                   => EmployeeService::getEmployeeProfileURL($clientTeambuilderBucket->employee),
                'skills'                        => $clientTeambuilderBucket->employee->skills ?? [],
                'client_id'                     => $clientTeambuilderBucket->client->id ?? null,
                'client_company'                => $clientTeambuilderBucket->client->company ?? '',
                'client_project_id'             => $clientTeambuilderBucket->clientProject->id ?? null,
                'client_project_name'           => $clientTeambuilderBucket->clientProject->project_name ?? '',
                'update_by_user'                => $clientTeambuilderBucket->getUpdatedByUser()
            ];
        }

        // apply dynamic return
        $result = [];
        foreach ($this->getData as $col) {
            $result[$col] = $clientTeambuilderBucket->$col;
        }
        return $result;
    }

    /**
     * Include Employee
     *
     * @param  ClientTeambuilderBucket $clientTeambuilderBucket
     * @return Collection
     */
    public function includeEmployee(ClientTeambuilderBucket $clientTeambuilderBucket)
    {
        $employeeTransformer = new EmployeeTransformer();
        $employeeTransformer->setDefaultIncludes(['skills', 'photo', 'positions', 'employeeStatuses']);

        return $this->item($clientTeambuilderBucket->employee, $employeeTransformer);
    }
}
