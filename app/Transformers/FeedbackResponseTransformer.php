<?php

namespace App\Transformers;

use App\Models\FeedbackResponse;
use League\Fractal\TransformerAbstract;

/**
 * Class FeedbackResponseTransformer
 * @package App\Transformers
 */
class FeedbackResponseTransformer extends TransformerAbstract
{
    /**
     * @param FeedbackResponse $feedbackResponse
     * @return array
     */
    public function transform(FeedbackResponse $feedbackResponse)
    {
        return $feedbackResponse->toArray();
    }
}
