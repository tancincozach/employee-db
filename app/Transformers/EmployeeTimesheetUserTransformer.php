<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\EmployeeTimesheetUser;

class EmployeeTimesheetUserTransformer extends TransformerAbstract
{
    public function transform(EmployeeTimesheetUser $employeeTimesheetUser)
    {
        return [
            'id'                      => $this->num($employeeTimesheetUser->id),
            'employee_id'             => $this->num($employeeTimesheetUser->employee_id),
            'timesheet_user_id'       => $this->num($employeeTimesheetUser->timesheet_user_id),
            'created_at'              => $this->str($employeeTimesheetUser->created_at),
            'updated_at'              => $this->str($employeeTimesheetUser->updated_at),
            'deleted_at'              => $this->str($employeeTimesheetUser->deleted_at),
        ];
    }

    private function str($str)
    {
        return $str ?? '';
    }

    private function num($num)
    {
        return $num ? (int) $num : 0;
    }
}
