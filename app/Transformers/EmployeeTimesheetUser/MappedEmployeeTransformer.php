<?php

namespace App\Transformers\EmployeeTimesheetUser;

use League\Fractal\TransformerAbstract;
use App\Models\EmployeeTimesheetUser;

use App\Traits\FilterTrait;

class MappedEmployeeTransformer extends TransformerAbstract
{
    use FilterTrait;

    public function transform(EmployeeTimesheetUser $user)
    {
        return [
            'id'                => $this->num($user->id),
            'timesheet_user_id' => $this->num($user->timesheet_user_id),
            'timesheet_user'    => $this->str($user->timesheet_user),
            'employee_id'       => $this->num($user->employee_id),
            'employee'          => $this->str($user->employee),
            'created_at'        => $this->str($user->created_at),
            'updated_at'        => $this->str($user->updated_at),
            'deleted_at'        => $this->str($user->deleted_at),
        ];
    }
}
