<?php

namespace App\Transformers;

use App\Models\EmployeeAcademic;
use League\Fractal\TransformerAbstract;

/**
 * Class EmployeeAcademicTransformer
 * @package App\Transformers
 */
class EmployeeAcademicTransformer extends TransformerAbstract
{
    /**
     * @param EmployeeAcademic $employeeAcademic
     * @return array
     */
    public function transform(EmployeeAcademic $employeeAcademic)
    {
        return $employeeAcademic->toArray();
    }
}
