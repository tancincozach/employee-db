<?php

namespace App\Transformers;

use App\Models\EmployeeCharacteristic;
use League\Fractal\TransformerAbstract;

/**
 * Class EmployeeCharacteristicTransformer
 * @package App\Transformers
 */
class EmployeeCharacteristicTransformer extends TransformerAbstract
{
    /**
     * @param EmployeeCharacteristic $employeeCharacteristic
     * @return array
     */
    public function transform(EmployeeCharacteristic $employeeCharacteristic)
    {
        return $employeeCharacteristic->toArray();
    }
}
