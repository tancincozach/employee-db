<?php

namespace App\Transformers;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

use App\Models\HackerRankTest;
use App\Models\Skill;

class HackerRankTestTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'skill'
    ];

    public function transform(HackerRankTest $hackerRankTest)
    {
        return [
            'id'   => (int)$hackerRankTest->id ?? null,
            'test_id' => (int)$hackerRankTest->test_id ?? null,
            'unique_id' => $hackerRankTest->unique_id ?? '',
            'name' => $hackerRankTest->name ?? '',
            'test_url' => $hackerRankTest->test_url ?? ''
        ];
    }
    /**
     * Include HackerRanks Test Results
     *
     * @param  HackerRankTestResult $hackerRankTestResult
     * @return Collection
     */
    public function includeSkill(HackerRankTest $hackerRankTest)
    {
        return !is_null($hackerRankTest->skill) ? $this->item($hackerRankTest->skill, new SkillTransformer()) : null;
    }      

}
