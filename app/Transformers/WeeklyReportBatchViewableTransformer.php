<?php
namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\WeeklyReportBatchViewable;
use App\Services\AssetService;

class WeeklyReportBatchViewableTransformer extends TransformerAbstract
{
    public function transform(WeeklyReportBatchViewable $batchViewable)
    {
        return [
            'id'                    => $batchViewable->id,
            'client_id'             => $batchViewable->client_id,
            'client_project_id'     => $batchViewable->client_project_id,
            'jobcode'               => $batchViewable->jobcode,
            'start_date'            => $batchViewable->start_date,
            'end_date'              => $batchViewable->end_date,
            'posted_by_user_id'     => $batchViewable->user->user_name,
            'posted_version'        => $batchViewable->posted_version,
            'remarks'               => $batchViewable->remarks,
        ];
    }
}
