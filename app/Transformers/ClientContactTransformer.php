<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ClientContact;

class ClientContactTransformer extends TransformerAbstract
{
    public function transform(ClientContact $contact)
    {
        return [
            'id'         => (int)$contact->id ?? null,
            'title'       => $contact->title ?? '',
            'name'       => $contact->name ?? '',
            'email'      => $contact->email ?? '',
            'contact_no' => $contact->contact_no ?? '',
            'is_signatory' => $contact->is_signatory ?? '',
            'user'       => $contact->user ?? '',
            'client_id'       => $contact->client_id ?? ''
        ];
    }
}
