<?php

namespace App\Transformers;

use App\Models\WeeklyFloorAnswer;
use League\Fractal\TransformerAbstract;

/**
 * Class WeeklyFloorAnswerTransformer
 * @package App\Transformers
 */
class WeeklyFloorAnswerTransformer extends TransformerAbstract
{
    /**
     * @param WeeklyFloorAnswer $weeklyFloorAnswer
     * @return array
     */
    public function transform(WeeklyFloorAnswer $weeklyFloorAnswer)
    {
        return $weeklyFloorAnswer->toArray();
    }
}
