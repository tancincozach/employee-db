<?php

namespace App\Transformers;

use App\Models\Feedback;
use League\Fractal\TransformerAbstract;

/**
 * Class FeedbackTransformer
 * @package App\Transformers
 */
class FeedbackTransformer extends TransformerAbstract
{
    /**
     * @param Feedback $feedback
     * @return array
     */
    public function transform(Feedback $feedback)
    {
        return $feedback->toArray();
    }
}
