<?php

namespace App\Transformers;

use App\Models\EmployeePositionType;
use League\Fractal\TransformerAbstract;

/**
 * Class EmployeePositionTypeTransformer
 * @package App\Transformers
 */
class EmployeePositionTypeTransformer extends TransformerAbstract
{
    /**
     * @param EmployeePositionType $employeePositionType
     * @return array
     */
    public function transform(EmployeePositionType $employeePositionType)
    {
        return $employeePositionType->toArray();
    }
}
