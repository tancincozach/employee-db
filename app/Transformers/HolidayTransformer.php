<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Holiday;

class HolidayTransformer extends TransformerAbstract
{
    public function transform(Holiday $holiday)
    {
        return [
            'id'   => (int)$holiday->id ?? null,
            'name' => $holiday->name ?? '',
            'date' => $holiday->date ?? '',
            'type' => $holiday->type ?? '',
        ];
    }
}
