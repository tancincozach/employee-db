<?php

namespace App\Transformers;

use App\Models\EmployeeReportsFile;
use League\Fractal\TransformerAbstract;

class EmployeeReportsFileTransformer extends TransformerAbstract
{
    public function transform(EmployeeReportsFile $employeeReportsFile)
    {
        return [
            'id'                     => $employeeReportsFile->id ?? null,
            'employee_reports_id'    => (int)$employeeReportsFile->employee_reports_id ?? null,
            'file'                   => $employeeReportsFile->file ?? '',
            'original_file'          => $employeeReportsFile->original_file ?? '',
            'created_at'             => $employeeReportsFile->created_at ?? null,
            'updated_at'             => $employeeReportsFile->updated_at ?? null,
        ];
    }
}
