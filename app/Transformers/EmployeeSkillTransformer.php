<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\EmployeeSkill;

class EmployeeSkillTransformer extends TransformerAbstract
{
    public function transform(EmployeeSkill $employeeSkill)
    {
        return [
            'id'                     => (int)$employeeSkill->id ?? null,
            'employee_id'            => (int)$employeeSkill->employee_id ?? null,
            'skill_id'               => (int)$employeeSkill->skill->id ?? null,
            'name'                   => $employeeSkill->skill->name ?? '',
            'description'            => $employeeSkill->skill->description ?? '',
            'color'                  => $employeeSkill->skill->color ?? '',
            'proficiency'            => $employeeSkill->proficiency ?? null,
            'years_of_experience'    => $employeeSkill->years_of_experience ?? null,
            'no_of_projects_handled' => $employeeSkill->no_of_projects_handled ?? null,
            'project_roles'          => $employeeSkill->project_roles ?? '',
            'checked_top_skill'      => $employeeSkill->checked_top_skill ?? ''
        ];
    }
}
