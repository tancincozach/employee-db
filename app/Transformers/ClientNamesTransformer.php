<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Client;

class ClientNamesTransformer extends TransformerAbstract
{
    public function transform(Client $client)
    {
        return [
            'id'   => (int) $client->id ?? 0,
            'company' => $client->company ?? '',
        ];
    }
}
