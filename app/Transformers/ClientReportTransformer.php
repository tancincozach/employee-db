<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Client;

class ClientReportTransformer extends TransformerAbstract
{
    public function transform(Client $client)
    {
        return [
            'id'            => (int)$client->id,
            'company_name'  => $client->company,
            'resources'     => $this->setResources($client->resources),
        ];
    }

    /**
     * This method will structure api response of resources
     * of a specific client
     *
     * @param array $resources
     *
     * @return Collection|Array
     */
    private function setResources($resources)
    {
        $employees = $resources
            ->map(function($resource, $key) {
                return empty($resource)
                    ? []
                    : [
                        'employee_id' => $resource->id,
                        'resource_name' => ucwords(trim($resource->first_name . ' ' . $resource->middle_name . ' ' . $resource->last_name . ' ' . $resource->ext)),
                    ];
            })
            ->toArray();

        return $employees ?? [];
    }
}
