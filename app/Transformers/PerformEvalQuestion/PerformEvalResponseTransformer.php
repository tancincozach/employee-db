<?php

namespace App\Transformers\PerformEvalQuestion;

use App\Models\PerformEvalResponse;
use League\Fractal\TransformerAbstract;

/**
 * Class PerformEvalResponseTransformer
 * @package App\Transformers
 */
class PerformEvalResponseTransformer extends TransformerAbstract
{
    /**
     * @param PerformEvalResponse $performEvalResponse
     * @return array
     */
    public function transform(PerformEvalResponse $performEvalResponse)
    {
        return $performEvalResponse->toArray();
    }
}