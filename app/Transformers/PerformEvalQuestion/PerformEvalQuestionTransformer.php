<?php

namespace App\Transformers\PerformEvalQuestion;

use App\Models\PerformEvalQuestion;
use League\Fractal\TransformerAbstract;

/**
 * Class PerformEvalQuestionTransformer
 * @package App\Transformers
 */
class PerformEvalQuestionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'criteria',
    ];

    /**
     * @param PerformEvalQuestion $performEvalQuestion
     * @return array
     */
    public function transform(PerformEvalQuestion $performEvalQuestion)
    {
        return $performEvalQuestion->toArray();
    }

    /**
     * Include Criterias
     *
     * @param  PerformEvalQuestion $performEvalQuestion
     * @return Collection
     */
    public function includeCriteria(PerformEvalQuestion $performEvalQuestion)
    {
        if (is_null($performEvalQuestion->criteria)) {
            return null;
        }

        return $this->collection($performEvalQuestion->criteria, new PerformEvalCriteriaTransformer());
    }
}