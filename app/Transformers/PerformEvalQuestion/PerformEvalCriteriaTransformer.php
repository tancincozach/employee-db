<?php

namespace App\Transformers\PerformEvalQuestion;

use App\Models\PerformEvalCriteria;
use League\Fractal\TransformerAbstract;

/**
 * Class PerformEvalCriteriaTransformer
 * @package App\Transformers
 */
class PerformEvalCriteriaTransformer extends TransformerAbstract
{
    /**
     * @param PerformEvalCriteria $performEvalCriteria
     * @return array
     */
    public function transform(PerformEvalCriteria $performEvalCriteria)
    {
        return $performEvalCriteria->toArray();
    }
}