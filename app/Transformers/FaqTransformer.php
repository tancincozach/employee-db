<?php

namespace App\Transformers;

use App\Models\Faq;
use League\Fractal\TransformerAbstract;

class FaqTransformer extends TransformerAbstract
{
    public function transform(Faq $faq)
    {
        return ['id' => $faq->id,
                'question' => $faq->question,
                'answer' => $faq->answer];
    }
}
