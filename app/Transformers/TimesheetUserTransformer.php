<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\TimesheetUser;

class TimesheetUserTransformer extends TransformerAbstract
{
    public function transform(TimesheetUser $timesheetUser)
    {
        return [
            'id'                => $this->num($timesheetUser->id),
            'first_name'        => $this->str($timesheetUser->first_name),
            'last_name'         => $this->str($timesheetUser->last_name),
            'active'            => $this->num($timesheetUser->active),
            'employee_number'   => $this->num($timesheetUser->employee_number),
            'username'          => $this->str($timesheetUser->username),
            'email'             => $this->str($timesheetUser->email),
            'mobile_number'     => $this->str($timesheetUser->mobile_number),
            'hire_date'         => $this->str($timesheetUser->hire_date),
            'term_date'         => $this->str($timesheetUser->term_date),
            'last_modified'     => $this->str($timesheetUser->last_modified),
            'last_active'       => $this->str($timesheetUser->last_active),
            'created'           => $this->str($timesheetUser->created),
            'company_name'      => $this->str($timesheetUser->company_name),
            'profile_image_url' => $this->str($timesheetUser->profile_image_url),
            'pto_balances'      => $this->json($timesheetUser->pto_balances),
            'created_at'        => $this->str($timesheetUser->created_at),
            'updated_at'        => $this->str($timesheetUser->updated_at),
            'deleted_at'        => $this->str($timesheetUser->deleted_at),
        ];
    }

    private function str($str)
    {
        return $str ?? '';
    }

    private function num($num)
    {
        return $num ? (int) $num : 0;
    }

    private function json($num)
    {
        return $num ? json_decode($num, true) : '';
    }
}
