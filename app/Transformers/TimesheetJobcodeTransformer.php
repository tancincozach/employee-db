<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\TimesheetJobcode;

class TimesheetJobcodeTransformer extends TransformerAbstract
{
    public function transform(TimesheetJobcode $timesheetJobcode)
    {
        return [
            'id'              => $this->num($timesheetJobcode->id),
            'name'            => $this->str($timesheetJobcode->name),
            'parent_id'       => $this->num($timesheetJobcode->parent_id),
            'assigned_to_all' => $this->num($timesheetJobcode->assigned_to_all),
            'active'          => $this->num($timesheetJobcode->active),
            'type'            => $this->str($timesheetJobcode->type),
            'has_children'    => $this->num($timesheetJobcode->has_children),
            'last_modified'   => $this->str($timesheetJobcode->last_modified),
            'created'         => $this->str($timesheetJobcode->created),
            'created_at'      => $this->str($timesheetJobcode->created_at),
            'updated_at'      => $this->str($timesheetJobcode->updated_at),
            'deleted_at'      => $this->str($timesheetJobcode->deleted_at),
        ];
    }

    private function str($str)
    {
        return $str ?? '';
    }

    private function num($num)
    {
        return $num ? (int) $num : 0;
    }
}
