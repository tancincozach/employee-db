<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class CategoryTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'skills'
    ];

    public function transform(Category $category)
    {
        return [
            'id'                     => (int)$category->id ?? null,
            'parent_id'              => (int)$category->parent_id ?? null,
            'name'                   => $category->name ?? '',
            'resource_type'          => $category->resource_type ?? null,
            'description'            => $category->description
        ];
    }

    /**
     * @param Category $category
     * @return \League\Fractal\Resource\Collection
     */
    public function includeSkills(Category $category)
    {
        return $this->collection($category->skills, new SkillTransformer());
    }
}
