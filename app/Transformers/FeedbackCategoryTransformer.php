<?php

namespace App\Transformers;

use App\Models\FeedbackCategory;
use League\Fractal\TransformerAbstract;

/**
 * Class FeedbackCategoryTransformer
 * @package App\Transformers
 */
class FeedbackCategoryTransformer extends TransformerAbstract
{
    /**
     * @param FeedbackCategory $feedbackCategory
     * @return array
     */
    public function transform(FeedbackCategory $feedbackCategory)
    {
        return $feedbackCategory->toArray();
    }
}
