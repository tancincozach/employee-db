<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ClientOnboardingChecklist;

class ClientOnboardingChecklistTransformer extends TransformerAbstract
{
    public function transform(ClientOnboardingChecklist $clientChecklist)
    {
        return [
            'id'                => (int)$clientChecklist->id,
            'client_id'         => $clientChecklist->client_id,
            'asset_id'          => $clientChecklist->asset_id,
            'step'              => $clientChecklist->step,
            'date_occurred'     => $clientChecklist->date_occurred,
            'client_signatory'  => $clientChecklist->client_signatory,
            'sent_by'           => $clientChecklist->sent_by,
            'sent_to'           => $clientChecklist->sent_to,
            'attendees'         => $clientChecklist->attendees,
            'client_feedback'   => $clientChecklist->client_feedback,
            'notes'             => $clientChecklist->notes,
            'asset'             => $clientChecklist->asset
        ];
    }
}
