<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\UserVerification;

class UserVerificationTransformer extends TransformerAbstract
{
    public function transform(UserVerification $user_verification)
    {
        return [
            'id'          => (int)$user_verification->id,
            'user_id'        => $user_verification->user_id,
            'verification_token' => $user_verification->verification_token,
            'verified_at'  => $user_verification->verified_at
        ];
    }
}
