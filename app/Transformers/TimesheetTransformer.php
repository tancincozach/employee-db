<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Timesheet;

class TimesheetTransformer extends TransformerAbstract
{
    public function transform(Timesheet $timesheet)
    {
        return [
            'id'            => $this->num($timesheet->id),
            'user_id'       => $this->num($timesheet->user_id),
            'jobcode_id'    => $this->num($timesheet->jobcode_id),
            'start'         => $this->str($timesheet->start),
            'end'           => $this->str($timesheet->end),
            'duration'      => $this->num($timesheet->duration),
            'date'          => $this->str($timesheet->date),
            'tz'            => $this->str($timesheet->tz),
            'tz_str'        => $this->str($timesheet->tz_str),
            'type'          => $this->str($timesheet->type),
            'location'      => $this->str($timesheet->location),
            'on_the_clock'  => $this->num($timesheet->on_the_clock),
            'notes'         => $this->str($timesheet->notes),
            'last_modified' => $this->str($timesheet->last_modified),
            'created_at'    => $this->str($timesheet->created_at),
            'updated_at'    => $this->str($timesheet->updated_at),
            'deleted_at'    => $this->str($timesheet->deleted_at),
        ];
    }

    private function str($str)
    {
        return $str ?? '';
    }

    private function num($num)
    {
        return $num ? (int) $num : 0;
    }
}
