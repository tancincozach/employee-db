<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\EmployeeTopSkill;

class EmployeeTopSkillTransformer extends TransformerAbstract
{
    public function transform(EmployeeTopSkill $employeeTopSkill)
    {
        return [
            'id'                     => (int)$employeeTopSkill->id ?? null,
            'employee_id'            => (int)$employeeTopSkill->employee_id ?? null,
            'skill_id'               => (int)$employeeTopSkill->skill_id ?? null,
            'skill_name'             => $employeeTopSkill->name ?? '',
            'proficiency'            => (int)$employeeTopSkill->proficiency ?? null,
            'skill_rank'             => (int)$employeeTopSkill->skill_rank ?? null,
        ];
    }
}
