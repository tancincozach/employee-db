<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Zone;
use App\Helpers\TimezoneHelper;

class ZoneTransformer extends TransformerAbstract
{
    public function transform(Zone $zone)
    {
        return [
            'id'   => (int)$zone->zone_id,
            'country_code' => $zone->country_code,
            'name' => $zone->zone_name,
            'text' =>  TimezoneHelper::getTimezoneCaption($zone->zone_name)
        ];
    }

    
}
