<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ClientTimesheetJobcode;

class ClientTimesheetJobcodeTransformer extends TransformerAbstract
{
    public function transform(ClientTimesheetJobcode $clientTimesheetJobcode)
    {
        return [
            'id'                      => $this->num($clientTimesheetJobcode->id),
            'client_id'               => $this->num($clientTimesheetJobcode->client_id),
            'timesheet_jobcode_id'    => $this->num($clientTimesheetJobcode->timesheet_jobcode_id),
            'created_at'              => $this->str($clientTimesheetJobcode->created_at),
            'updated_at'              => $this->str($clientTimesheetJobcode->updated_at),
            'deleted_at'              => $this->str($clientTimesheetJobcode->deleted_at),
        ];
    }

    private function str($str)
    {
        return $str ?? '';
    }

    private function num($num)
    {
        return $num ? (int) $num : 0;
    }
}
