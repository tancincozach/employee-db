<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Models\SkillCategory;

class SkillCategoryTransformer extends TransformerAbstract
{
    public function transform(SkillCategory $skillCategory)
    {
        return [
            'id'       => (int)$skillCategory->id ?? null,
            'skill'    => $skillCategory->skill->name ?? '',
            'category' => $skillCategory->category ?? null
        ];
    }
}
