<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Models\Notification;

class NotificationTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'users'
    ];

    public function transform(Notification $notification)
    {
        return [
            'id'      => $notification->id ?? null,
            'type'    => $notification->type ?? '',
            'data'    => json_decode($notification->data, true) ?? '',
            'created' => $notification->created_at->diffForHumans() ?? null,
        ];
    }

    public function includeUsers(Notification $notification)
    {
        return $this->collection($notification->users, new NotificationUserTransformer());
    }
}
