<?php

namespace App\Transformers;

use App\Models\CreativeServiceFile;
use League\Fractal\TransformerAbstract;

/**
 * Class CreativeServiceFileTransformer
 * @package App\Transformers
 */
class CreativeServiceFileTransformer extends TransformerAbstract
{
    /**
     * @param CreativeServiceFile $creativeServiceFile
     * @return array
     */
    public function transform(CreativeServiceFile $creativeServiceFile)
    {
        return $creativeServiceFile->toArray();
    }
}
