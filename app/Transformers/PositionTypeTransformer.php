<?php

namespace App\Transformers;

use App\Models\PositionType;
use League\Fractal\TransformerAbstract;

/**
 * Class PositionTypeTransformer
 * @package App\Transformers
 */
class PositionTypeTransformer extends TransformerAbstract
{
    /**
     * @param PositionType $positionType
     * @return array
     */
    public function transform(PositionType $positionType)
    {
        return $positionType->toArray();
    }
}
