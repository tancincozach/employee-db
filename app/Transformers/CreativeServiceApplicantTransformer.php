<?php

namespace App\Transformers;

use App\Models\CreativeServiceApplicant;
use League\Fractal\TransformerAbstract;

/**
 * Class CreativeServiceApplicantTransformer
 * @package App\Transformers
 */
class CreativeServiceApplicantTransformer extends TransformerAbstract
{
    /**
     * @param CreativeServiceApplicant $creativeServiceApplicant
     * @return array
     */
    public function transform(CreativeServiceApplicant $creativeServiceApplicant)
    {
        return $creativeServiceApplicant->toArray();
    }
}
