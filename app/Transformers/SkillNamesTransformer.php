<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Skill;

class SkillNamesTransformer extends TransformerAbstract
{
    public function transform(Skill $skill)
    {
        return [
            'id'          => (int)$skill->id ?? null,
            'name'        => $skill->name ?? '',
        ];
    }
}
