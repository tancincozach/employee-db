<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\EmployeeReport;

class EmployeeCalendarReportTransformer extends TransformerAbstract
{

    public function transform(EmployeeReport $empReport)
    {               
        $empArr= array();
        $empArr['id'] = $empReport->employee->id;
        $empArr['first_name'] = $empReport->employee->first_name;
        $empArr['last_name'] = $empReport->employee->last_name;
        unset($empReport->employee);
        return [
            'id'             => (int)$empReport->id,
            'employee'       => (object)$empArr,
            'report_date'    => $empReport->report_date,
        ];
    }

    public function stringCleanup($str)
    {
        return trim(nl2br($str));
    }
}
