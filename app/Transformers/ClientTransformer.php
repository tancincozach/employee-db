<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Client;
use App\Models\AllQuestionResponse;
use Illuminate\Support\Facades\Log;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;

class ClientTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'user'
    ];

    protected $defaultIncludes = [
        'contacts',
        'projects',
        'photo',
        'employeeSelected',
    ];

    protected $otherDetails;

    public function transform(Client $client)
    {
        $timezone = !empty($client->tz) ? $client->tz->zone_name : $client->timezone;
        $optionalInfo = [];

        if ($this->otherDetails) {
            $has_remote_team_experience = AllQuestionResponse::HasRemoteExp($client->id)->first();
            $optionalInfo = [
                'has_remote_team_experience'    => $has_remote_team_experience['all_question_choice_description'],
            ];
        }

        $default = [
            'id'                            => (int)$client->id ?? null,
            'company'                       => $client->company ?? '',
            'is_high_growth_client'         => $client->is_high_growth_client ?? null,
            'contract_signed'               => $client->contract_signed ?? null,
            'initial_deposit'               => $client->initial_deposit ?? null,
            'start_guide'                   => $client->start_guide ?? null,
            'first_week_check_up'           => $client->first_week_check_up ?? null,
            'team_emails_sent'              => $client->team_emails_sent ?? null,
            'first_month_check_up'          => $client->first_month_check_up ?? null,
            'status'                        => $client->status ?? null,
            'location'                      => $client->location ?? '',
            'timezone'                      => $timezone ?? '',
            'zone_id'                       => $client->zone_id ?? '',
            'notes'                         => $client->notes ?? '',
            'introductory_call'             => $client->introductory_call ?? null,
            'description'                   => $client->description ?? '',
            'is_partner'                    => $client->is_partner ?? 0,
            'company_do'                    => $client->company_do ?? '',
            'employee_spoken'               => $client->employee_spoken ?? '',
            'start_date'                    => $client->start_date ?? '',
            'projects_count'                => $client->projects_count ?? null,
            'resources_count'               => $client->resources_count ?? null,
            'created_at'                    => $client->created_at->format('m/d/Y')
        ];

        return array_merge($default, $optionalInfo);
    }

    public function setOtherDetails($option = false)
    {
        $this->otherDetails = $option;
    }

    /**
     * Include Client Contacts
     *
     * @param  Client $client
     * @return Collection
     */
    public function includeContacts(Client $client)
    {
        return $this->collection($client->contacts, new ClientContactTransformer());
    }

    /**
     * Include Client Team Builder Selected employee
     *
     * @param  Client $client
     * @return Collection
     */
    public function includeEmployeeSelected(Client $client)
    {
        return $this->collection($client->employeeSelected, new ClientTeambuilderBucketTransformer());
    }

    /**
     * Include Client Projects
     *
     * @param  Client $client
     * @return Collection
     */
    public function includeProjects(Client $client)
    {
        return $this->collection($client->projects, new ClientProjectTransformer());
    }

    /**
     * Include Photo
     *
     * @param Client $client
     * @return Collection
     */
    public function includePhoto(Client $client)
    {
        return $this->collection($client->photo, new AssetTransformer());
    }

    /**
     * Include User Account
     *
     * @param Client $client
     * @return Item
     */
    public function includeUser(Client $client)
    {
        $user = $client->user;

        if (is_null($user)) {
            return null;
        }

        return $this->item($user, new UserTransformer());
    }
}
