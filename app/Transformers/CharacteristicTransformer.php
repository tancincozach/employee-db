<?php

namespace App\Transformers;

use App\Models\Characteristic;
use League\Fractal\TransformerAbstract;

/**
 * Class CharacteristicTransformer
 * @package App\Transformers
 */
class CharacteristicTransformer extends TransformerAbstract
{
    /**
     * @param Characteristic $characteristic
     * @return array
     */
    public function transform(Characteristic $characteristic)
    {
        return $characteristic->toArray();
    }
}
