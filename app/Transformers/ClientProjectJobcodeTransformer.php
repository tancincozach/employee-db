<?php

namespace App\Transformers;

use App\Models\ClientProjectJobcode;
use League\Fractal\TransformerAbstract;

class ClientProjectJobcodeTransformer extends TransformerAbstract
{
    public function transform(ClientProjectJobcode $model)
    {
        return [
            'id'                => (int) $model->id,
            'client_id'         => $model->client_id,
            'jobcode'           => $model->jobcode,
            'clientProject'     => $model->clientProjects,
            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at,
            'deleted_at'        => $model->deleted_at
        ];
    }
}
