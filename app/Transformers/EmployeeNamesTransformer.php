<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Employee;

class EmployeeNamesTransformer extends TransformerAbstract
{
    public function transform(Employee $employee)
    {
        return [
            'id'   => (int) $employee->id ?? null,
            'name' => $employee->getName(),
        ];
    }
}
