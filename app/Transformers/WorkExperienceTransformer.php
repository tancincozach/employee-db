<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Models\WorkExperience;

class WorkExperienceTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'portfolios'
    ];

    public function transform(WorkExperience $experience)
    {
        return [
            'id'                 => (int)$experience->id ?? null,
            'employee_id'        => (int)$experience->employee_id ?? null,
            'job_title'          => $experience->job_title ?? '',
            'company_name'       => $experience->company_name ?? '',
            'start_date'         => Carbon::parse($experience->start_date)->format('Y-m-d'),
            'end_date'           => $experience->end_date ? Carbon::parse($experience->end_date)->format('Y-m-d') : null,
            'description'        => $experience->description ?? '',
            'reason_for_leaving' => $experience->reason_for_leaving ?? '',
            'current'            => $experience->current ?? '',
            'order'              => $experience->order ?? null
        ];
    }

    /**
     * Include Portfolios
     *
     * @param  WorkExperience $experience
     * @return Collection
     */
    public function includePortfolios(WorkExperience $experience)
    {
        return $this->collection($experience->employeePortfolios, new EmployeePortfolioTransformer());
    }
}
