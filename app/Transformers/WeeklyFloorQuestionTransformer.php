<?php

namespace App\Transformers;

use App\Models\WeeklyFloorQuestion;
use League\Fractal\TransformerAbstract;

/**
 * Class WeeklyFloorQuestionTransformer
 * @package App\Transformers
 */
class WeeklyFloorQuestionTransformer extends TransformerAbstract
{
    /**
     * @param WeeklyFloorQuestion $weeklyFloorQuestion
     * @return array
     */
    public function transform(WeeklyFloorQuestion $weeklyFloorQuestion)
    {
        return $weeklyFloorQuestion->toArray();
    }
}
