<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\RouteWizard;

class RouteWizardTransformer extends TransformerAbstract
{
    /**
     * Transform the RoleUse entity.
     *
     * @return json
     */
    public function transform(RouteWizard $route)
    {
        return [
            'id'                => $route->id,
            'group_id'          => $route->group_id,
            'icon'              => $route->icon,
            'route_name'        => $route->route_name,
            'sequence_order'    => $route->sequence_order,
            'route_uri'         => $route->route_uri,
            'caption'           => $route->caption,
            'goto_group_id'     => $route->goto_group_id
        ];
    }
}
