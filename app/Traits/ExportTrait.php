<?php

namespace App\Traits;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as ExcelWriter;
use PhpOffice\PhpSpreadsheet\Writer\Pdf as PdfWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv as CsvWriter;

/**
 * Export Trait
 *
 * Use this trait to convert your array of data into a desireable report file. This is used to
 * comply with our coding standard.
 *
 * @package App\Traits
 *
 */
trait ExportTrait
{
    /**
     * Export reports depending on the supplied data
     * @param array $query_result - result set from sql query
     * @param string $file_name - name for the file
     * @param string $file_type - xlsx, csv, pdf
     */
    public function export($query_result, $file_name, $file_type = 'xlsx')
    {
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->fromArray($query_result, null);
        $this->writeFile($file_type, $file_name, $spreadsheet);
    }

    protected function writeFile($file_type, $file_name, $spreadsheet)
    {
        switch ($file_type) {
            case 'xlsx':
                $writer = new ExcelWriter($spreadsheet);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                break;
            case 'pdf':
                $writer = new PdfWriter($spreadsheet);
                break;
            default:
                $writer = new CsvWriter($spreadsheet);
                header('Content-Type: text/csv');
                break;
        }

        header("Content-Disposition: attachment; filename={$file_name}");
        $writer->save("php://output");
    }
}
