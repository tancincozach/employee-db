<?php

namespace App\Traits;

use App\Models\BaseModel;
use Illuminate\Support\Str;

trait Uuids
{
    protected static function bootUuids()
    {
        static::creating(function (BaseModel $model) {
            if (! $model->getKey()) {
                $model->{$model->getKeyName()} = Str::uuid()->toString();
            }
        });
    }

    /**
     * Tells that the primary key is not incrementing
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Tells that the primary key type is string
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }
}
