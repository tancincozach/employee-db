<?php

namespace App\Traits;

trait ModelTrait
{
    /**
     * The model instance
     * 
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * The validation rules
     * 
     * @var array
     */
    protected $rules = [];

    /**
     * The action responses
     * 
     * @var array
     */
    protected $responses = [];

    /**
     * Get the primary key
     * 
     * @return string
     */
    protected function primaryKey()
    {
        return $this->model->getKeyName();
    }

    /**
     * Filter the field that has change
     * 
     * @param array $request
     * @param array $hiddenColumn
     * 
     * @return array
     */
    protected function filter(array $request, $hiddenColumn = [])
    {
        $result = [];
        $primary = $this->primaryKey();
        $columns = \Schema::getColumnListing($this->model->getTable());
        $data = $this->model->find($request[$primary])->makeVisible($hiddenColumn);
        foreach ($columns as $column) {
            if (isset($request[$column]) && $data->{$column} != $request[$column]) {
                $result[$column] = $request[$column];
            }
        }
        $result[$primary] = $data[$primary];
        return $result;
    }

    /**
     * Validates the request
     * 
     * @param array $request
     * 
     * @return mixed
     */
    protected function validate(array $request)
    {
        $validate = \Validator::make($request, $this->rules);
        return $validate->fails() ? $validate->errors() : true;
    }

    /**
     * Create a model record
     * 
     * @param array $request
     * 
     * @throws \Exception
     * 
     * @return mixed
     */
    protected function create(array $request)
    {
        try {
            $primary = $this->primaryKey();
            unset($request[$primary]);
            $valid = $this->validate($request);
            
            if (true === $valid) {
                $result = $this->model->create($request);
                return $result->{$primary};
            }
            return $valid;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Update a model record
     * 
     * @param array $request
     * 
     * @throws \Exception
     * 
     * @return mixed
     */
    protected function update(array $request)
    {
        try {
            $request = $this->filter($request);
            $valid = $this->validate($request);
            $primary = $this->primaryKey();
            
            if (true === $valid) {
                $model = $this->model->find($request[$primary]);
                foreach ($request as $field => $value) {
                    $model->{$field} = $value;
                }
                $model->save();
                return $request[$primary];
            }
            return $valid;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Set the action response
     * 
     * @param string $action
     * @param mixed  $response
     * @param string $result
     * 
     * @return void
     */
    protected function setResponse($action, $response, $result = 'success')
    {
        $this->responses[$action][$result] = $response;
    }

    /**
     * Get the action response
     * 
     * @return array
     */
    public function getResponses()
    {
        return $this->responses;
    }
}
