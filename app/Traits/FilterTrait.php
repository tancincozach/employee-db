<?php

namespace App\Traits;

trait FilterTrait
{
    public function str($str)
    {
        return $str ?? '';
    }

    public function num($num)
    {
        return $num ? (int) $num : 0;
    }

    public function dec($dec)
    {
        return $dec ? (double) $dec : 0;
    }
}
