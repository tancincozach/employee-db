<?php

namespace App\Traits;

use League\Fractal\Scope;

trait CheckNullResource
{
    /**
     * Check for null resource
     *
     * @param Scope $scope
     * @param string $includeName
     * @param mixed $data
     *
     * @return mixed
     */
    protected function callIncludeMethod(Scope $scope, $includeName, $data)
    {
        if (is_null($data->{$includeName})) {
            return false;
        }

        return parent::callIncludeMethod($scope, $includeName, $data);
    }
}
