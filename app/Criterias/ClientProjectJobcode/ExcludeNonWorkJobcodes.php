<?php

namespace App\Criterias\ClientProjectJobcode;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use App\Models\ClientProjectJobcode;

class ExcludeNonWorkJobcodes implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereNotIn('jobcode', ClientProjectJobcode::NON_WORK_JOBCODES)
                    ->where('is_holiday', '!=', 1);
    }
}
