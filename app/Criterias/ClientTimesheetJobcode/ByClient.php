<?php

namespace App\Criterias\ClientTimesheetJobcode;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ByClient implements CriteriaInterface
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('client_id', $this->id);
    }
}
