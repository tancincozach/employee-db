<?php

namespace App\Criterias\ClientTimesheetJobcode;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class WithMappedClient implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model
            ->select(
                'client_timesheet_jobcodes.id AS id',
                'client_timesheet_jobcodes.timesheet_jobcode_id AS timesheet_jobcode_id',
                'tj.name AS timesheet_jobcode',
                'client_timesheet_jobcodes.client_id AS client_id',
                'c.company AS client',
                'client_timesheet_jobcodes.created_at AS created_at',
                'client_timesheet_jobcodes.updated_at AS updated_at',
                'client_timesheet_jobcodes.deleted_at AS deleted_at'
            )
            ->leftJoin('timesheet_jobcodes AS tj', 'client_timesheet_jobcodes.timesheet_jobcode_id', '=', 'tj.id')
            ->leftJoin('clients AS c', 'client_timesheet_jobcodes.client_id', '=', 'c.id')
            ->orderBy('timesheet_jobcode');
    }
}
