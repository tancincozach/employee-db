<?php

namespace App\Criterias\ClientTimesheetJobcode;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class Unmapped implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereNull('client_id');
    }
}
