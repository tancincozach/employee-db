<?php

namespace App\Criterias\TimesheetJobcodeAssignment;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ByClient implements CriteriaInterface
{
    protected $ids;

    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('client_id', $this->ids);
    }
}
