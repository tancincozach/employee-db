<?php

namespace App\Criterias\TimesheetJobcodeAssignment;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class JobcodeAssignmentEmployees implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->distinct()
            ->select('timesheet_user_id', 'employee_id', 'employee_name', 'timesheet_user_name')
            ->whereNotNull('timesheet_user_id')
            ->orderBy('employee_name');
    }
}
