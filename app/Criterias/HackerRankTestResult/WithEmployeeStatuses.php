<?php

namespace App\Criterias\HackerRankTestResult;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class WithEmployeeStatuses implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->leftJoin('employee_statuses', function ($join) {
            $join->on('employee_statuses.employee_id', '=', 'hacker_rank_test_results.employee_id')
                        ->on('employee_statuses.id', '=', DB::raw('(SELECT id FROM employee_statuses WHERE employee_id = hacker_rank_test_results.employee_id ORDER BY created_at DESC LIMIT 0,1)'));
        })->leftJoin('statuses', 'statuses.id', '=', 'employee_statuses.status_id')
          ->whereNotIn('employee_statuses.status_id', [20, 21,22,7]);

        return $model;
    }
}