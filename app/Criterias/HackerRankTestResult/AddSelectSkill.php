<?php

namespace App\Criterias\HackerRankTestResult;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class AddSelectSkill implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->addSelect(DB::raw('skills.name AS skill_name'));
    }
}
