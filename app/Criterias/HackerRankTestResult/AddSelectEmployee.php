<?php

namespace App\Criterias\HackerRankTestResult;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class AddSelectEmployee implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->addSelect(DB::raw('(CONCAT(employees.first_name, " ", employees.last_name)) AS fullname'));
    }
}