<?php

namespace App\Criterias\HackerRankTestResult;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class AddSelect implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->addSelect(DB::raw('hacker_rank_test_results.*'));
    }
}
