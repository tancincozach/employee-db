<?php

namespace App\Criterias\HackerRankTestResult;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class WithSkills implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->leftJoin('skills', 'skills.id', '=', 'hacker_rank_tests', 'hacker_rank_tests.skill_id');
    }
}
