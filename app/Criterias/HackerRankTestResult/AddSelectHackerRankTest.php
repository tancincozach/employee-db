<?php

namespace App\Criterias\HackerRankTestResult;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class AddSelectHackerRankTest implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->addSelect(DB::raw('hacker_rank_tests.name AS test_name, hacker_rank_tests.skill_id AS skill_id, hacker_rank_tests.test_url AS test_url'));
    }
}
