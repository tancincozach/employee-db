<?php

namespace App\Criterias\HackerRankTestResult;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class WithHackerRankTest implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->leftJoin('hacker_rank_tests', 'hacker_rank_tests.test_id', '=', 'hacker_rank_test_results.test_id')
                     ->leftJoin('skills', function($join) {
                         $join->on('skills.id', '=', 'hacker_rank_tests.skill_id');
                     })
                     ->groupBy('hacker_rank_test_results.id');
    }
}
