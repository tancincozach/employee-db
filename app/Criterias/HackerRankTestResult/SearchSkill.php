<?php

namespace App\Criterias\HackerRankTestResult;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class SearchSkill implements CriteriaInterface
{
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }
    
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orWhere('skill_name', 'like', '%' . $this->term . '%');
    }
}
