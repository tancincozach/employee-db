<?php
namespace App\Criterias\ClientTeamBuilderBucket;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class FilterByStatus implements CriteriaInterface
{

    private $status;

    public function __construct($status)
    {
        $this->status = $status;
    }
    
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereIn('client_teambuilder_buckets.status', $this->status);

        return $model;
    }
}
