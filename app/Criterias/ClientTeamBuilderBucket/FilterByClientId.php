<?php
namespace App\Criterias\ClientTeamBuilderBucket;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class FilterByClientId implements CriteriaInterface
{
    private $client_id;

    public function __construct($client_id)
    {
        $this->client_id = $client_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('client_id', '=', $this->client_id);

        return $model;
    }
}
