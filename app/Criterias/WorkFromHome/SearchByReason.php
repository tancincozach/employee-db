<?php

namespace App\Criterias\WorkFromHome;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class SearchByReason implements CriteriaInterface
{
    /**
     * @var $term
     */
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('work_from_home_requests.reason', 'like', '%'. $this->term.'%');
    }
}
