<?php

namespace App\Criterias\WorkFromHome;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class SearchByDateReason implements CriteriaInterface
{
    /**
     * @var $month
     * @var $year
     */
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    public function apply($model, RepositoryInterface $repository)
    {
       
        $model->where(function($query) {
            $current_month = date('Y-m-d', strtotime($this->term));
            $next_month = date('Y-m-t', strtotime($this->term));
    
            $query->where('work_from_home_requests.reason', 'like', '%'. $this->term.'%')
                  ->orWhereBetween('start_date', [$current_month, $next_month])
                  ->orWhereBetween('end_date', [$current_month, $next_month]);
        });

        return $model;
    }
}
