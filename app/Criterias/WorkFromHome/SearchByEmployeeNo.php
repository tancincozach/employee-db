<?php

namespace App\Criterias\WorkFromHome;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchByEmployeeNo implements CriteriaInterface
{
    private $empNo;

    public function __construct($empNo)
    {
        $this->empNo = $empNo;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orWhere('employees.employee_no', 'LIKE', '%' . $this->empNo . '%');
    }
}
