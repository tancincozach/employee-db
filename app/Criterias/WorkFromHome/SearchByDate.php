<?php

namespace App\Criterias\WorkFromHome;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class SearchByDate implements CriteriaInterface
{
    /**
     * @var $month
     * @var $year
     */
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    public function apply($model, RepositoryInterface $repository)
    {
       
        $model->orWhere(function($query) {
            $current_month = date('Y-m-d', strtotime($this->term));
            $next_month = date('Y-m-t', strtotime($this->term));
    
            $query->whereBetween('start_date', [$current_month, $next_month])
                  ->orWhereBetween('end_date', [$current_month, $next_month]);
        });

        return $model;
    }
}
