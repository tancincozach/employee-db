<?php

namespace App\Criterias\WorkFromHome;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class FilterByDate implements CriteriaInterface
{
    /**
     * @var $month
     * @var $year
     */
    protected $month;
    protected $year;

    public function __construct($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $current_month = date('Y-m-d', strtotime($this->year."-".$this->month."-01"));
        $next_month = date('Y-m-d', strtotime($this->year."-".$this->month."-01 +1 month"));

        $model->whereBetween('start_date', [$current_month, $next_month]);
        $model->whereBetween('end_date', [$current_month, $next_month]);
        return $model;
    }
}
