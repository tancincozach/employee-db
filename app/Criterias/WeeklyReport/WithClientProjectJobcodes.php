<?php

namespace App\Criterias\WeeklyReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class WithClientProjectJobcodes implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->join('client_project_jobcodes', function($join)
        {
            $join->on('client_project_jobcodes.id', '=', 'weekly_reports.jobcode_id');
        });
    }
}
