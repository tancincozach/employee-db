<?php

namespace App\Criterias\WeeklyReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class GetWeeklyReportByEmployeeAndRange implements CriteriaInterface
{
    private $employeeId;
    private $startDate;
    private $endDate;

    public function __construct($employeeId, $startDate, $endDate)
    {
        $this->employeeId = $employeeId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('employee_id', $this->employeeId)
                        ->where('log_date', '>=', $this->startDate)
                        ->where('log_date', '<=', $this->endDate)
                        ->orderBy('log_date')
                        ->orderBy('start_date');
        return $model;
    }
}
