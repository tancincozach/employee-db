<?php

namespace App\Criterias\WeeklyReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class GetMonthlyReport implements CriteriaInterface
{
    private $startDate;
    private $endDate;
    private $jobcodeIds;

    public function __construct($startDate, $endDate, $jobcodeIds, $employeeIds)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->jobcodeIds = $jobcodeIds;
        $this->employeeIds = $employeeIds;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->leftJoin('employees', 'employees.id', 'weekly_reports.employee_id')
                        ->whereIn('weekly_reports.jobcode_id', $this->jobcodeIds)
                        ->whereBetween('weekly_reports.log_date', [$this->startDate, $this->endDate])
                        ->whereIn('weekly_reports.employee_id', $this->employeeIds)
                        ->whereNull('weekly_reports.deleted_at')
                        ->groupBy('weekly_reports.employee_username')
                        ->orderBy('employees.last_name');

        return $model;
    }
}
