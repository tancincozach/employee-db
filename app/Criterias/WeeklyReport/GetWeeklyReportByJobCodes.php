<?php

namespace App\Criterias\WeeklyReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class GetWeeklyReportByJobCodes implements CriteriaInterface
{
    private $jobcodes = [];

    public function __construct(array $jobcodes)
    {
        $this->jobcodes = $jobcodes;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('jobcode_id', $this->jobcodes);
    }
}
