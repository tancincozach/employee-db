<?php

namespace App\Criterias\WeeklyReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class GetWeeklyReportByEmployees implements CriteriaInterface
{
    private $employeeIds = [];

    public function __construct(array $employeeIds)
    {
        $this->employeeIds = $employeeIds;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('employee_id', $this->employeeIds);
    }
}
