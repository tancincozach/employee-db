<?php

namespace App\Criterias\WeeklyReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use App\Models\ClientProjectJobcode;

class WithoutLunchBreak implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        $lunchBreak = ClientProjectJobcode::where('jobcode', 'Rest/Lunch Break')->first();

        return $model->where('jobcode_id', '<>', $lunchBreak->id);
    }
}
