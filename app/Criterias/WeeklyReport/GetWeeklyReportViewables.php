<?php

namespace App\Criterias\WeeklyReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use App\Models\WeeklyReportBatchViewable;

class GetWeeklyReportViewables implements CriteriaInterface
{
    private $clientId;
    private $startDate;
    private $endDate;

    public function __construct($clientId, $startDate, $endDate)
    {
        $this->clientId = $clientId;
        $this->startDate = strval($startDate);
        $this->endDate = $endDate;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $reportViewable = (new WeeklyReportBatchViewable)->getTable();
        
        return $model->whereRaw("log_date >= (SELECT MIN(" . $reportViewable . ".start_date) FROM " . $reportViewable . " WHERE " . $reportViewable . ".client_id = ".$this->clientId. " AND ". $reportViewable .".is_posted=1 AND " .$reportViewable. ".start_date >= " . "'".$this->startDate."' AND ".$reportViewable.".deleted_at IS NULL)")
                    ->whereRaw("log_date <= (SELECT MAX(" . $reportViewable . ".end_date) FROM " . $reportViewable . " WHERE " . $reportViewable . ".client_id = ".$this->clientId. " AND ". $reportViewable .".is_posted=1 AND " .$reportViewable. ".end_date <= " . "'".$this->endDate."' AND ".$reportViewable.".deleted_at IS NULL)");
    }
}
