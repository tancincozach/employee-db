<?php

namespace App\Criterias\WeeklyReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use App\Models\WeeklyReportBatchViewable;

class GetByJobcodeId implements CriteriaInterface
{
    private $jobcodeIds;

    public function __construct($jobcodeIds)
    {
        $this->jobcodeIds = $jobcodeIds;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('jobcode_id', $this->jobcodeIds);
    }
}
