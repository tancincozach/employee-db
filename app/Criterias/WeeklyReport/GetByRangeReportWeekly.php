<?php

namespace App\Criterias\WeeklyReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class GetByRangeReportWeekly implements CriteriaInterface
{
    private $startDate;
    private $endDate;
    private $term;

    public function __construct($startDate, $endDate, $term)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->term = $term;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('log_date', '>=', $this->startDate)
            ->where('log_date', '<=', $this->endDate)
            ->where('jobcode', $this->term)
            ->whereNull('deleted_at')
            ->orderBy('employee_username');

        return $model;
        /**
         * SELECT CONCAT(e.first_name,' ', e.last_name) AS 'Team Member', SUM(wr.working_hrs) AS 'Total' FROM weekly_reports AS wr 
         *   LEFT JOIN employees AS e ON wr.employee_id = e.id
         *  WHERE wr.log_date >= '2019-04-01 00:00:00' AND wr.log_date <= '2019-04-30 00:00:00'   AND wr.jobcode = 'Atonix' AND wr.deleted_at IS NULL  GROUP BY wr.employee_id
         */
    }
}
