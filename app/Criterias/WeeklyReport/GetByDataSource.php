<?php

namespace App\Criterias\WeeklyReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class GetByDataSource implements CriteriaInterface
{
    private $dataSource;

    public function __construct($dataSource = 'api')
    {
        $this->dataSource = $dataSource;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('data_source', $this->dataSource);
    }
}
