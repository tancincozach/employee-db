<?php

namespace App\Criterias\WeeklyReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class GetWeeklyReportByMonthYear implements CriteriaInterface
{
    private $year;
    private $month;

    public function __construct($year, $month)
    {
        $this->year = $year;
        $this->month = $month;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->addSelect('*', DB::raw('SUM(working_hrs) as working_hrs'))
                        ->whereYear('log_date', $this->year)
                        ->whereMonth('log_date', $this->month)
                        ->leftJoin('employees', 'employees.id', 'weekly_reports.employee_id')
                        ->groupBy('jobcode_id', 'employee_id')
                        ->orderBy('employees.last_name')
                        ->orderBy('jobcode_id');

        return $model;
    }
}
