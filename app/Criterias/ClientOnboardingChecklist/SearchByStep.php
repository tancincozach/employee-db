<?php
namespace App\Criterias\ClientOnboardingChecklist;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchByStep implements CriteriaInterface
{
    private $step;

    public function __construct($step)
    {
        $this->step = $step;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('step', '=', $this->step);

        return $model;
    }
}
