<?php

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class WithEmployeeTopSkill implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->leftJoin('employee_top_skills', 'employee_top_skills.employee_id', '=', 'employees.id')
                     ->leftJoin('skills', 'skills.id', '=', 'employee_top_skills.skill_id');
    }
}
