<?php

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchByEmployeeTopThreeSkills implements CriteriaInterface
{
    private $skills;

    public function __construct($skills)
    {
        $this->skills = $skills;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereIn('skills.name', $this->skills);
                            
        return $model;
    }
}
