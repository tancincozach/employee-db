<?php

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class CebuEmployeeOnly implements CriteriaInterface
{
    private $excludeEmployeeNo;

    public function __construct($excludeEmployeeNo = ['FS-9999', 'FS-1999', 'FS-1002', 'FS-1001'])
    {
        $this->excludeEmployeeNo = $excludeEmployeeNo;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model
            ->whereNotIn('employee_no', $this->excludeEmployeeNo)
            ->whereNotNull('employee_no');
    }
}
