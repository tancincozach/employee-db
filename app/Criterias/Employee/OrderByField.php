<?php

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class OrderByField implements CriteriaInterface
{
    protected $field;
    protected $order;

    public function __construct($field, $order = 'ASC')
    {
        $this->field = $field;
        $this->order = $order;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orderBy($this->field, $this->order);
    }
}
