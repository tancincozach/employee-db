<?php

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use App\Models\Employee;
use DB;

class FindAvailableResource implements CriteriaInterface
{
    private $skills;

    public function __construct($skills)
    {
        $this->skills = $skills;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select('employees.*')
                ->leftJoin('employee_client_projects', function($join)
                    {
                        $join->on('employee_client_projects.employee_id', '=', 'employees.id')
                            ->where('employee_client_projects.deleted_at');
                    })
                ->leftJoin('client_projects', 'client_projects.id', '=', 'employee_client_projects.client_project_id')
                ->leftJoin('clients', 'clients.id', '=', 'client_projects.client_id')
                ->leftJoin('client_teambuilder_buckets', function($join)
                    {
                        $join->on('client_teambuilder_buckets.employee_id', '=', 'employees.id');
                    })
                ->leftJoin('employee_top_skills', 'employee_top_skills.employee_id', '=', 'employees.id')
                ->leftJoin('skills', 'skills.id', '=', 'employee_top_skills.skill_id')
                ->leftJoin('employee_job_positions', 'employee_job_positions.employee_id', '=', 'employees.id')
                ->leftJoin('job_positions', 'job_positions.id', '=', 'employee_job_positions.position_id')
                ->leftJoin('employee_statuses', function ($join) {
                    $join->on('employee_statuses.employee_id', '=', 'employees.id')
                        ->on('employee_statuses.id', '=', DB::raw('(SELECT id FROM employee_statuses WHERE employee_id = employees.id ORDER BY created_at DESC LIMIT 0,1)'));
                })
                ->leftJoin('statuses', 'statuses.id', '=', 'employee_statuses.status_id')
                ->whereNotNull('employee_no')
                ->whereIn('employee_statuses.status_id', Employee::ACTIVE_EMPLOYEE_STATUS)
                ->whereIn('skills.name', $this->skills)
                ->groupBy('employees.id')
                ->orderBy('proficiency', 'DESC');
                
    }
}
