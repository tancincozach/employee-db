<?php

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use App\Models\MultiprojectResource;
use App\Models\EmployeeClientProject;

class SearchBySkillAndUnassigned implements CriteriaInterface
{
    private $skillName;

    public function __construct($skillName)
    {
        $this->skillName = $skillName;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('skills.name', $this->skillName)
                    ->where(function($query){
                        $query->where(function($query1){
                            return $query1->whereNull('client_projects.id')
                                          ->orWhereIn('client_projects.id', EmployeeClientProject::AVAILABLE_EMPLOYEES_ASSIGNED);
                        })
                        ->orWhereIn('job_positions.id', MultiprojectResource::whereNotNull('job_position_id')->get()->pluck('job_position_id'));
                    });
                   
    }
}
