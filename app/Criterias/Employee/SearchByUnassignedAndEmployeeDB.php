<?php

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use App\Models\EmployeeClientProject;

class SearchByUnassignedAndEmployeeDB implements CriteriaInterface
{
    private $project;

    public function __construct($project)
    {
        $this->project = $project;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function ($query) {
            $query->whereNull('client_projects.project_name')
                  ->orWhereIn('client_projects.id', EmployeeClientProject::AVAILABLE_EMPLOYEES_ASSIGNED);
        });
    }
}
