<?php

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class FilterByClientId implements CriteriaInterface
{
    private $clientId;

    public function __construct($clientId)
    {
        $this->clientId = $clientId;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('clients.id', $this->clientId);
    }
}
