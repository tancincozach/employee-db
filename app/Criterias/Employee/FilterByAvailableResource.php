<?php

namespace App\Criterias\Employee;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use App\Models\Employee;

class FilterByAvailableResource implements CriteriaInterface
{
    private $skills;

    private $usedOr;

    /**
     * FilterByAvailableResource constructor.
     * @param $skills
     */
    public function __construct($skills, $usedOr)
    {
        $this->skills = $skills;
        $this->usedOr = $usedOr;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select('employees.*')
            ->leftJoin('employee_client_projects', function ($join) {
                $join->on('employee_client_projects.employee_id', '=', 'employees.id')
                    ->where('employee_client_projects.deleted_at');
            })
            ->leftJoin('client_projects', 'client_projects.id', '=', 'employee_client_projects.client_project_id')
            ->leftJoin('employee_skills', function ($join) {
                $join->on('employee_skills.employee_id', '=', 'employees.id')
                    ->where('employee_skills.proficiency', '>=', DB::raw(5))
                    ->where('employee_skills.checked_top_skill', '=', DB::raw(1));
            })
            ->leftJoin('employee_job_positions', 'employee_job_positions.employee_id', '=', 'employees.id')
            ->leftJoin('employee_statuses', function ($join) {
                $join->on('employee_statuses.employee_id', '=', 'employees.id')
                    ->on('employee_statuses.id', '=', DB::raw('(SELECT id FROM employee_statuses WHERE employee_id = employees.id ORDER BY created_at DESC LIMIT 0,1)'));
            })
            ->whereNotNull('employee_no')
            ->whereIn('employee_statuses.status_id', Employee::ACTIVE_EMPLOYEE_STATUS);
            

        if (!empty($this->skills)) {
            $model->whereIn('employee_skills.skill_id', $this->skills);
        }

        $model->groupBy('employees.id');
        
        if (!empty($this->skills)) {
            if (! $this->usedOr) {
                $model->having(DB::raw('COUNT(DISTINCT employee_skills.skill_id)'), '=', count($this->skills));
            }
        }

        $model->orderBy('employee_job_positions.level', 'DESC')
            ->orderBy('proficiency', 'DESC');

        return $model;
    }
}
