<?php

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use App\Models\EmployeeClientProject;

class FilterUnassignedEmployees implements CriteriaInterface
{
    private $clientId;

    public function __construct($clientId)
    {
        $this->clientId = $clientId;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function ($query) {
                    $query->whereNull('client_projects.project_name')
                        ->orWhereIn('client_projects.id', EmployeeClientProject::AVAILABLE_EMPLOYEES_ASSIGNED);
                })->where(function ($query) {
                    return $query->whereIn('client_teambuilder_buckets.client_id', $this->clientId)
                                ->orWhere('client_teambuilder_buckets.status', '!=', 'assigned')
                                ->orWhereNull('client_teambuilder_buckets.employee_id');
                });
                
    }
}
