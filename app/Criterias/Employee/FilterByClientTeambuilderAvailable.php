<?php

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class FilterByClientTeambuilderAvailable implements CriteriaInterface
{
    protected $clientId;

    public function __construct($clientId)
    {
        $this->clientId = $clientId;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function ($query) {
                return $query->where('client_teambuilder_buckets.client_id', $this->clientId)
                    ->whereNull('client_teambuilder_buckets.deleted_at')
                    ->orWhere('client_teambuilder_buckets.status', 'declined')
                    ->orWhereNull('client_teambuilder_buckets.employee_id');
            });
    }
}
