<?php

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchByIds implements CriteriaInterface
{
    protected $ids;

    public function __construct($ids = [])
    {
        $this->ids = $ids;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('employees.id', $this->ids);
    }
}
