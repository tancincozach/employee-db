<?php
/**
 * Created by PhpStorm.
 * User: raymund
 * Date: 6/4/19
 * Time: 7:07 PM
 */

namespace App\Criterias\Employee;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class WithClientTeambuilderBucket implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {

        return $model->leftJoin('client_teambuilder_buckets', function($join)
        {
            $join->on('client_teambuilder_buckets.employee_id', '=', 'employees.id');
        });
    }
}
