<?php

namespace App\Criterias\RouteWizard;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchByAll implements CriteriaInterface
{
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('group_id', 'like', '%' . $this->term . '%')
                        ->orWhere('icon', 'like', '%' . $this->term . '%')
                        ->orWhere('route_name', 'like', '%' . $this->term . '%')
                        ->orWhere('route_uri', 'like', '%' . $this->term . '%')
                        ->orWhere('sequence_order', 'like', '%' . $this->term . '%')
                        ->orWhere('goto_group_id', 'like', '%' . $this->term . '%');

        return $model;
    }
}
