<?php

namespace App\Criterias\EmployeeTimesheetUser;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class WithMappedEmployee implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model
            ->select(
                'employee_timesheet_users.id AS id',
                'employee_timesheet_users.timesheet_user_id AS timesheet_user_id',
                \DB::raw("CONCAT(tu.last_name, ', ', tu.first_name) AS timesheet_user"),
                'employee_timesheet_users.employee_id AS employee_id',
                \DB::raw("
                    CONCAT(
                        COALESCE(e.last_name, ''), ', ',
                        COALESCE(e.first_name, ''), ' ',
                        COALESCE(e.middle_name, ''), ' ',
                        COALESCE(e.ext, '')
                    ) AS employee
                "),
                'employee_timesheet_users.created_at AS created_at',
                'employee_timesheet_users.updated_at AS updated_at',
                'employee_timesheet_users.deleted_at AS deleted_at'
            )
            ->leftJoin('timesheet_users AS tu', 'employee_timesheet_users.timesheet_user_id', '=', 'tu.id')
            ->leftJoin('employees AS e', 'employee_timesheet_users.employee_id', '=', 'e.id')
            ->orderBy('timesheet_user');
    }
}
