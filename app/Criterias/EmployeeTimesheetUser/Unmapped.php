<?php

namespace App\Criterias\EmployeeTimesheetUser;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class Unmapped implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereNull('employee_id');
    }
}
