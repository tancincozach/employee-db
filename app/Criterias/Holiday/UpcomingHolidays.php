<?php

namespace App\Criterias\Holiday;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class UpcomingHolidays implements CriteriaInterface
{

    /*
     * Traverse through fields to search the keyword
     *
     * @param \Illuminate\Eloquent\Database\Model|Builder      $model
     * @param Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        return $model->where('date', '>=', date('Y-m-d'))->orderBy('date','asc');
    }
}
