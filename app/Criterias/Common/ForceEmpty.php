<?php

namespace App\Criterias\Common;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ForceEmpty implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('id', 0);
    }
}
