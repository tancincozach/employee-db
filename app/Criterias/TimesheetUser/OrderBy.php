<?php

namespace App\Criterias\TimesheetUser;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class OrderBy implements CriteriaInterface
{
    protected $col;
    protected $dir;

    public function __construct($col, $dir = 'asc')
    {
        $this->col = $col;
        $this->dir = $dir;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orderBy($this->col, $this->dir);
    }
}
