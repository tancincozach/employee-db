<?php

namespace App\Criterias\TimesheetUser;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class WithMappedEmployees implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model
            ->select(
                'etu.id AS id',
                'etu.timesheet_user_id AS timesheet_user_id',
                'etu.employee_id AS employee_id',
                \DB::raw("CONCAT(timesheet_users.last_name, ', ', timesheet_users.first_name) AS timesheet_user_name"),
                \DB::raw("
                    CASE
                        WHEN etu.employee_id IS NULL THEN '-- No corresponding employee --'
                        ELSE CONCAT(
                            COALESCE(e.last_name, ''), ', ',
                            COALESCE(e.first_name, ''), ' ',
                            COALESCE(e.middle_name, ''), ' ',
                            COALESCE(e.ext, ''))
                    END AS employee_name"),
                'timesheet_users.created_at AS created_at',
                'timesheet_users.updated_at AS updated_at',
                'timesheet_users.deleted_at AS deleted_at'
            )
            ->leftJoin('employee_timesheet_users AS etu', 'timesheet_users.id', '=', 'etu.timesheet_user_id')
            ->leftJoin('employees AS e', 'etu.employee_id', '=', 'e.id')
            ->orderBy('timesheet_users.last_name');
    }
}
