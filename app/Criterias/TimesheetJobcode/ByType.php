<?php

namespace App\Criterias\TimesheetJobcode;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ByType implements CriteriaInterface
{
    protected $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('type', $this->type);
    }
}
