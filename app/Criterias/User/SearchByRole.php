<?php

namespace App\Criterias\User;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

/**
 * Class SearchByRole
 * @package App\Criterias\User
 */
class SearchByRole implements CriteriaInterface
{
    private $role;

    private $withTrash;

    /**
     * SearchByRole constructor.
     * @param $role
     * @param bool $withTrash
     */
    public function __construct($role, $withTrash = true)
    {
        $this->role = $role;
        $this->withTrash = $withTrash;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->withTrash) {
            return $model->leftJoin('user_roles', 'users.id', '=', 'user_roles.user_id')
                ->where('user_roles.role_id', $this->role);
        } else {
            return $model->leftJoin('user_roles', 'users.id', '=', 'user_roles.user_id')
                ->where('user_roles.role_id', $this->role)
                ->whereNull('user_roles.deleted_at');
        }
    }
}
