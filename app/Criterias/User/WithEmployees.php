<?php

namespace App\Criterias\User;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class WithEmployees implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->addSelect( 'users.*',
            DB::raw('CONCAT(employees.last_name, employees.first_name) AS name')
        )->join('employees', 'employees.user_id', '=', 'users.id')->distinct()->where('users.user_name', '!=', '');
    }
}
