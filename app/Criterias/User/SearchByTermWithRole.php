<?php

namespace App\Criterias\User;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class SearchByTermWithRole implements CriteriaInterface
{
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function($query) {
            return $query->where('users.email', 'like', '%'. $this->term .'%')
                ->orWhere('users.user_name', 'like', '%'. $this->term .'%' )
                ->orWhere('employees.first_name', 'like', '%'. $this->term.'%')
                ->orWhere('employees.last_name', 'like', '%'. $this->term.'%')
                ->orWhere('employees.middle_name', 'like', '%'. $this->term.'%')
                ->orWhereRaw('REPLACE(CONCAT_WS("", `employees`.`first_name`, `employees`.`last_name`, `employees`.`middle_name`), " ", "") LIKE ?', $this->term)
                ->orWhereRaw('REPLACE(CONCAT_WS("", `employees`.`first_name`, `employees`.`middle_name`, `employees`.`last_name`), " ", "") LIKE ?', $this->term)
                ->orWhereRaw('REPLACE(CONCAT_WS("", `employees`.`last_name`, `employees`.`first_name`, `employees`.`middle_name`), " ", "") LIKE ?', $this->term)
                // added this condition to trap two words of their first name
                ->orWhereRaw('REPLACE(CONCAT_WS("", SUBSTRING_INDEX(`employees`.`first_name`, " ", 1), `employees`.`last_name`, `employees`.`middle_name`), " ", "") LIKE ?', $this->term)
                ->orWhereRaw('REPLACE(CONCAT_WS("", `employees`.`last_name`, SUBSTRING_INDEX(`employees`.`first_name`, " ", 1), `employees`.`middle_name`), " ", "") LIKE ?', $this->term)
                ->orWhere('roles.display_name', 'like', '%'. $this->term.'%')
                ->distinct();
        });
    }
}
