<?php

namespace App\Criterias\User;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class SearchByUsernameOrEmail implements CriteriaInterface
{
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orWhere(function($query) {
            return $query->where('users.email', 'like', '%'. $this->term .'%')
                ->orWhere('users.user_name', 'like', '%'. $this->term .'%' )
                ->distinct();
        });
    }
}
