<?php

namespace App\Criterias\User;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Support\Facades\DB;

class WithUpdatedBy implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->addSelect(DB::raw('users2.user_name AS updated_by'))
            ->join('users AS users2','users.updated_by_user_id', '=', 'users2.id');
    }
}
