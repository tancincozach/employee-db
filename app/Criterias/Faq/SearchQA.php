<?php

namespace App\Criterias\Faq;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchQA implements CriteriaInterface
{
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }
    
    public function apply($model, RepositoryInterface $repository)
    {
        $this->term = str_replace(' ', '%', $this->term);
        return $model->orWhere('faqs.question', 'like', '%' . $this->term . '%')
                     ->orWhere('faqs.answer', 'like', '%' . $this->term . '%');
    }
}
