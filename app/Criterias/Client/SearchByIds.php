<?php

namespace App\Criterias\Client;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchByIds implements CriteriaInterface
{
    private $ids;

    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereIn('id', $this->ids);

        return $model;
    }
}
