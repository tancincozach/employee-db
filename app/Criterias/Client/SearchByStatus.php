<?php

namespace App\Criterias\Client;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchByStatus implements CriteriaInterface
{
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    /**
     * CLIENT STATUS IDs
     * Active       - 0
     * End Contract - 1
     * Prospect     - 2
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('status', '=', $this->term);

        return $model;
    }
}
