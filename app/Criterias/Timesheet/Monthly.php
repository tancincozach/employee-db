<?php

namespace App\Criterias\Timesheet;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class Monthly implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model
            ->select(
                'e.id',
                'timesheets.user_id AS user_id',
                'timesheets.jobcode_id AS jobcode_id',
                'timesheets.tz_str AS tz_str',
                \DB::raw("
                    CASE
                        WHEN e.id IS NULL THEN CONCAT(tu.last_name, ', ', tu.first_name)
                        ELSE
                            CONCAT(
                                COALESCE(e.last_name, ''), ', ',
                                COALESCE(e.first_name, ''), ' ',
                                COALESCE(e.middle_name, ''), ' ',
                                COALESCE(e.ext, ''))
                            END
                    AS employee
                "),
                'tj.name AS client',
                \DB::raw('SUM(ROUND(timesheets.duration / 3600, 2)) AS rendered'),
                \DB::raw('168 AS target'),
                \DB::raw('ROUND((SUM(timesheets.duration / 3600) / 168) * 100, 2) AS percentage')
            )
            ->leftJoin('employee_timesheet_users AS etu', 'etu.timesheet_user_id', '=', 'timesheets.user_id')
            ->leftJoin('employees AS e', 'e.id', '=', 'etu.employee_id')
            ->leftJoin('timesheet_users AS tu', 'tu.id', '=', 'etu.timesheet_user_id')
            ->leftJoin('timesheet_jobcodes AS tj', 'tj.id', '=', 'timesheets.jobcode_id')
            ->groupBy('timesheets.jobcode_id', 'timesheets.user_id', 'e.id')
            ->orderBy('tu.last_name')
            ->orderBy('tj.name');
    }
}
