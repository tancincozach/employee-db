<?php

namespace App\Criterias\Timesheet;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ByLogDate implements CriteriaInterface
{
    protected $start;
    protected $end;

    public function __construct($start, $end = '')
    {
        $this->start = $start;
        $this->end = $end ?: $start;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereBetween('log_date', [$this->start, $this->end]);
    }
}
