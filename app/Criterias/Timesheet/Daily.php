<?php

namespace App\Criterias\Timesheet;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class Daily implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model
            ->select(
                'timesheets.id AS id',
                'timesheets.user_id AS user_id',
                'timesheets.date AS log_date',
                'timesheets.start AS start',
                'timesheets.end AS end',
                'timesheets.tz_str AS tz_str',
                'timesheets.type AS type',
                'timesheets.duration AS duration',
                'timesheets.updated_at AS last_updated',
                \DB::raw("ROUND(timesheets.duration / 3600, 2) AS working_hrs"),
                'tj.name AS company',
                'tj.type AS jobcode_type',
                'c.id AS client_id',
                \DB::raw("COALESCE(c.timezone, 'America/Chicago') AS client_tz")
            )
            ->leftJoin('timesheet_jobcodes AS tj', 'timesheets.jobcode_id', '=', 'tj.id')
            ->leftJoin('client_timesheet_jobcodes AS ctj', 'tj.id', '=', 'ctj.timesheet_jobcode_id')
            ->leftJoin('clients AS c', 'ctj.client_id', '=', 'c.id')
            ->orderBy('timesheets.date', 'desc')
            ->orderBy('timesheets.start', 'desc');
    }
}
