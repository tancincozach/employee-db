<?php

namespace App\Criterias\Timesheet;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ByMonthYear implements CriteriaInterface
{
    protected $month;
    protected $year;

    public function __construct($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereMonth('timesheets.date', $this->month)
            ->whereYear('timesheets.date', $this->year)
            ->where('tj.type', 'regular');
    }
}
