<?php

namespace App\Criterias\Timesheet;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ByClient implements CriteriaInterface
{
    protected $ids;

    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $ids = $this->ids;

        return $model->where(function ($query) use ($ids) {
            return $query->whereIn('client_id', $ids)
                ->orWhere('tj.type', '<>', 'regular');
        });
    }
}
