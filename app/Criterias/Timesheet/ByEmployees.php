<?php

namespace App\Criterias\Timesheet;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ByEmployees implements CriteriaInterface
{
    protected $ids;

    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('timesheet_user_id', $this->ids);
    }
}
