<?php

namespace App\Criterias\Timesheet;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

use App\Models\TimesheetJobcode;

class ExcludeLunchBreak implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        $exclude = TimesheetJobcode::select('id')
            ->where('type', 'unpaid_break')
            ->pluck('id')
            ->toArray();

        return $model->whereNotIn('jobcode_id', $exclude);
    }
}
