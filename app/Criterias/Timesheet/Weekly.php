<?php

namespace App\Criterias\Timesheet;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class Weekly implements CriteriaInterface
{
    /*
     * The id for Paid Time Off
     *
     * @var integer
     */
    const PTO = 13171368;

    protected $jobcodeId;
    protected $startDate;
    protected $endDate;

    public function __construct($jobcodeId, $startDate, $endDate)
    {
        $this->jobcodeId = $jobcodeId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $startDate = $this->startDate;
        $endDate = $this->endDate;
        $jobcodeId = $this->jobcodeId;

        return $model
            ->select(
                'timesheets.user_id AS timesheet_user_id',
                'timesheets.date AS log_date',
                \DB::raw('WEEK(timesheets.date) AS week'),
                \DB::raw("
                    CASE
                        WHEN e.id IS NULL THEN CONCAT(tu.last_name, ', ', tu.first_name)
                        ELSE CONCAT(
                            COALESCE(e.last_name, ''), ', ',
                            COALESCE(e.first_name, ''), ' ',
                            COALESCE(e.middle_name, ''), ' ',
                            COALESCE(e.ext, ''))
                     END AS employee
                "),
                'tj.name AS client',
                'tj.type AS type',
                \DB::raw("
                    CASE WHEN tj.id = ".self::PTO." THEN 0
                         ELSE ROUND(SUM(timesheets.duration / 3600), 2)
                    END AS day_total
                ")
            )

            ->leftJoin('timesheet_users AS tu', 'tu.id', '=', 'timesheets.user_id')
            ->leftJoin('employee_timesheet_users AS etu', 'etu.timesheet_user_id', '=', 'timesheets.user_id')
            ->leftJoin('employees AS e', 'e.id', '=', 'etu.employee_id')
            ->leftJoin('timesheet_jobcodes AS tj', 'tj.id', '=', 'timesheets.jobcode_id')
            ->leftJoin('client_timesheet_jobcodes AS ctj', 'ctj.timesheet_jobcode_id', '=', 'timesheets.jobcode_id')
            ->leftJoin('clients AS c', 'c.id', '=', 'ctj.client_id')

            ->whereBetween('timesheets.date', [$startDate, $endDate])
            ->whereIn('tj.type', ['regular', 'pto'])
            ->whereIn('timesheets.jobcode_id', [$jobcodeId, self::PTO])
            ->whereIn('timesheets.user_id', function ($query) use ($jobcodeId, $startDate, $endDate) {
                $query->distinct()
                    ->select('user_id')
                    ->from('timesheets AS t2')
                    ->leftJoin('timesheet_jobcodes AS tj2', 'tj2.id', '=', 't2.jobcode_id')
                    ->whereBetween('t2.date', [$startDate, $endDate])
                    ->whereIn('tj2.type', ['regular', 'pto'])
                    ->where('t2.jobcode_id', $jobcodeId);
            })
            ->groupBy('timesheets.user_id', 'timesheets.date', 'tj.id', 'e.id')
            ->orderBy('timesheets.date', 'DESC')
            ->orderBy('tu.last_name');
    }
}
