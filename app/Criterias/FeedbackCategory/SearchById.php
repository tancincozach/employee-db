<?php

namespace App\Criterias\FeedbackCategory;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchById implements CriteriaInterface
{
    protected $id;

    /**
     * SearchById constructor.
     * @param array $id
     */
    public function __construct(array $id = [])
    {
        $this->id = $id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('feedback_category.id', $this->id);
    }
}
