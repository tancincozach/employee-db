<?php

namespace App\Criterias\OperationsDocument;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class FilterByDocumentName implements CriteriaInterface
{
    private $docName;

    public function __construct($docName)
    {
        $this->docName = $docName;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function($query){
            return $query->where('operations_documents.name', 'like', '%' . $this->docName. '%');
        });
    }
}
