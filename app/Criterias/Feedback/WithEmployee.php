<?php

namespace App\Criterias\Feedback;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class WithEmployee implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->leftJoin('employees', 'employees.user_id', '=', 'feedback.user_id');
    }
}
