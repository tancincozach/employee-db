<?php

namespace App\Criterias\Feedback;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class AddSelectCategory implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->addSelect(DB::raw('feedback_category.name as catname'));
    }
}
