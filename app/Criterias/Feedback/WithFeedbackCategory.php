<?php

namespace App\Criterias\Feedback;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class WithFeedbackCategory implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->leftJoin('feedback_category', 'feedback_category.id', '=', 'feedback.category_id')
                     ->groupBy('feedback.id');
    }
}
