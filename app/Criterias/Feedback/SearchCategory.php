<?php

namespace App\Criterias\Feedback;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class SearchCategory implements CriteriaInterface
{
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }
    
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orWhere('feedback_category.name', 'like', '%' . $this->term . '%');
    }
}
