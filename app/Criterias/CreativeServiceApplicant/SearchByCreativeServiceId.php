<?php

namespace App\Criterias\CreativeServiceApplicant;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class SearchByCreativeServiceId implements CriteriaInterface
{
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }
    
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('creative_service_applicant.creative_service_id', 'like', '%' . $this->term . '%');
    }
}
