<?php

namespace App\Criterias\EmployeeReportsFile;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class FilterByEmployeeReportId implements CriteriaInterface
{
    private $employee_reports_id;

    public function __construct($employee_reports_id)
    {
        $this->employee_reports_id = $employee_reports_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('employee_reports_id', $this->employee_reports_id);
    }
}
