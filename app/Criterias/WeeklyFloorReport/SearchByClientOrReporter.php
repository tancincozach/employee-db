<?php

namespace App\Criterias\WeeklyFloorReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchByClientOrReporter implements CriteriaInterface
{
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select('weekly_floor_report.*')
            ->leftJoin('users', 'weekly_floor_report.created_by', '=', 'users.id')
            ->leftJoin('employees', 'users.id', '=', 'employees.user_id')
            ->leftJoin('clients', 'weekly_floor_report.client_id', '=', 'clients.id')
            ->where(function ($query) {
                return $query->where('employees.first_name', 'like', '%'.$this->term.'%')
                        ->orWhere('employees.last_name', 'like', '%'.$this->term.'%');
            })
            ->orWhere('clients.company', 'like', '%'.$this->term.'%');
    }
}
