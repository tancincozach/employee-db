<?php

namespace App\Criterias\WeeklyFloorReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class OrderByValue implements CriteriaInterface
{
    private $col;
    private $sort;

    public function __construct($column, $sorting = "desc")
    {
        $this->col = $column;
        $this->sort = $sorting;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orderBy($this->col, $this->sort);
    }
}
