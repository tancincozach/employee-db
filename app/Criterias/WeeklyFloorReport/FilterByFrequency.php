<?php

namespace App\Criterias\WeeklyFloorReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class FilterByFrequency implements CriteriaInterface
{
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $start = '';
        $end = '';
        switch ($this->term) {
            case '7days':
                $date = new \DateTime(date("Y-m-d"));
                $end = $date->format('Y-m-d');
                $date->modify('-7 day');
                $start = $date->format('Y-m-d');
                
                break;
            case 'previous-month':
                $start = date('Y-m-d', strtotime(date('Y-m')." -1 month"));
                $end = date("Y-m-t", strtotime($start));
                break;
            case 'current-month':
                $start = date('Y-m-01');
                $end = date('Y-m-t');
                break;
        }

        $frequency = [$start.' 00:00:00', $end.' 23:59:59'];

        return $model->whereBetween('weekly_floor_report.created_at', $frequency);
    }
}
