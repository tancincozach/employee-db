<?php

namespace App\Criterias\WeeklyFloorReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class FilterByCreatedId implements CriteriaInterface
{
    private $createdId;

    public function __construct($createdId)
    {
        $this->createdId = $createdId;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('weekly_floor_report.created_by', '=', $this->createdId);
    }
}
