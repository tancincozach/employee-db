<?php

namespace App\Criterias\CreativeService;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class SearchProject implements CriteriaInterface
{
    private $term;

    public function __construct($term)
    {
        $this->term = $term;
    }
    
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orWhere('creative_service.project', 'like', '%' . $this->term . '%');
    }
}
