<?php

namespace App\Criterias\CreativeService;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class WithCreativeServiceApplicant implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->leftJoin('creative_service_applicant', 'creative_service_applicant.creative_service_id', '=', 'creative_service.id')
                     ->where('creative_service_applicant.primary', 1)
                     ->groupBy('creative_service.id');
    }
}
