<?php

namespace App\Criterias\CreativeService;
namespace App\Criterias\CreativeService;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class AddSelectCreativeServiceApplicant implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->addSelect(DB::raw('creative_service_applicant.full_name as full_name, creative_service_applicant.email, creative_service_applicant.primary'));
    }
}
