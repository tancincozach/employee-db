<?php

namespace App\Criterias\CreativeService;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class AddSelectFields implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->addSelect(DB::raw('feedback.*, feedback.created_at as date_submitted, feedback.id as feedback_id'));
    }
}
