<?php

namespace App\Criterias\CreativeService;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class AddSelectCreativeService implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->addSelect(DB::raw('creative_service.*, creative_service.created_at AS date_submitted'));
    }
}
