<?php

namespace App\Criterias\Notification;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchByUser implements CriteriaInterface
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->leftJoin('notification_users', 'notifications.id', '=', 'notification_users.notification_id')
                    ->leftJoin('notification_flags', 'notification_flags.user_id', '=', 'notification_users.user_id')
                     ->where('notification_users.user_id', $this->id)
                     ->select(
                         'type',
                         'data',
                         'notification_users.created_at',
                         'notification_users.updated_at',
                         'notification_users.deleted_at',
                         'notification_id',
                         'notification_users.user_id',
                         'read_at',
                         'has_notification'
                     )
                     ->orderBy('created_at', 'desc');
    }
}
