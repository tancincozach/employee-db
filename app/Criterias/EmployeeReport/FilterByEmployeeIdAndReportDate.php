<?php

namespace App\Criterias\EmployeeReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;
class FilterByEmployeeIdAndReportDate implements CriteriaInterface
{
    
    public function __construct($employeeId,$reportDate)
    {
        $this->employeeId = $employeeId;
        $this->reportDate = $reportDate;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->leftJoin('employees', 'employees.id', '=', 'employee_reports.employee_id')
                       ->leftJoin('client_projects', 'client_projects.id', '=', 'employee_reports.client_project_id')
                       ->select(DB::raw('employee_reports.*','employees.*','client_projects.project_name','client_projects.id as project_id'))
                       ->where(function ($query) {
                         $query->where( 'employee_reports.employee_id', '=',  $this->employeeId)
                               ->where('employee_reports.report_date', '=',  $this->reportDate);
                    });

        return $model;
    }
}
