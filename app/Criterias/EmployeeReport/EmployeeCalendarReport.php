<?php

namespace App\Criterias\EmployeeReport;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;
class EmployeeCalendarReport implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->leftJoin('employees', 'employees.id', '=', 'employee_reports.employee_id')
                       ->leftJoin('client_projects', 'client_projects.id', '=', 'employee_reports.client_project_id')
                       ->select(DB::raw('employee_reports.*',
                                        'employees.id,employee.first_name,employee.last_name,employee.middle_name',
                                        'client_projects.project_name',
                                        'client_projects.id as project_id'));
        return $model;
    }
}
