<?php

namespace App\Criterias\HackerRankTest;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SearchByName implements CriteriaInterface
{
    /*
     * The keyword
     *
     * @var string
     */
    private $keyword;

    /*
     * The fields to search
     *
     * @var array
     */
    private $fields = [];

    /*
     * Class constructor
     *
     * @param  array $fields The fields to search
     * @return void
     */
    public function __construct($keyword, array $fields = ['name'])
    {
        $this->keyword = $keyword;
        $this->fields  = $fields;
    }

    /*
     * Traverse through fields to search the keyword
     *
     * @param \Illuminate\Eloquent\Database\Model|Builder      $model
     * @param Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $keyword = trim($this->keyword);
        $keyword = "%{$keyword}%";
        $searchFields = collect($this->fields);

        return $model->where(function ($query) use ($keyword, $searchFields) {
            $searchFields->each(function ($field) use ($query, $keyword) {
                $query->orWhere($field, 'like', $keyword);
            });
        });
    }
}
