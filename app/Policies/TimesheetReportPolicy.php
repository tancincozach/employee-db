<?php

namespace App\Policies;

use App\Models\User;
use App\Models\TimesheetReport;
use App\Policies\BasePolicy;

class TimesheetReportPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the timesheet reports.
     *
     * @param  \App\Models\User             $user
     * @param  \App\Models\TimesheetReport  $timesheetReport
     * @return mixed
     */
    public function view(User $user, TimesheetReport $timesheetReport)
    {
        return $this->isAllowed($user, 'timesheet_reports', 'can_view');
    }
}
