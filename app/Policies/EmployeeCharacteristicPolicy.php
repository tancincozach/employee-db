<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeCharacteristic;

/**
 * Class EmployeeCharacteristicPolicy
 * @package App\Policies
 */
class EmployeeCharacteristicPolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param EmployeeCharacteristic $employeeCharacteristic
     * @return bool
     */
    public function view(User $user, EmployeeCharacteristic $employeeCharacteristic)
    {
        return $this->isAllowed($user, 'employee_characteristic', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'employee_characteristic', 'can_add');
    }

    /**
     * @param User $user
     * @param EmployeeCharacteristic $employeeCharacteristic
     * @return bool
     */
    public function update(User $user, EmployeeCharacteristic $employeeCharacteristic)
    {
        return $this->isAllowed($user, 'employee_characteristic', 'can_edit');
    }

    /**
     * @param User $user
     * @param EmployeeCharacteristic $employeeCharacteristic
     * @return bool
     */
    public function delete(User $user, EmployeeCharacteristic $employeeCharacteristic)
    {
        return $this->isAllowed($user, 'employee_characteristic', 'can_delete');
    }

    /**
     * @param User $user
     * @param EmployeeCharacteristic $employeeCharacteristic
     * @return bool
     */
    public function restore(User $user, EmployeeCharacteristic $employeeCharacteristic)
    {
        return $this->isAllowed($user, 'employee_characteristic', 'can_edit');
    }

    /**
     * @param User $user
     * @param EmployeeCharacteristic $employeeCharacteristic
     * @return bool
     */
    public function forceDelete(User $user, EmployeeCharacteristic $employeeCharacteristic)
    {
        return $this->isAllowed($user, 'employee_characteristic', 'can_delete');
    }
}
