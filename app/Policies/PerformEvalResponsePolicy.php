<?php

namespace App\Policies;

use App\Models\User;
use App\Models\PerformEvalResponse;
use App\Policies\BasePolicy;

class PerformEvalResponsePolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the responses.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PerformEvalResponse $performEvalResponse
     * @return mixed
     */
    public function view(User $user, PerformEvalResponse $performEvalResponse)
    {
        return $this->isAllowed($user, 'perform_eval_response_api', 'can_view');
    }

    /**
     * Determine whether the user can create response.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'perform_eval_response_api', 'can_add');
    }

    /**
     * Determine whether the user can update the response.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PerformEvalResponse $performEvalResponse
     * @return mixed
     */
    public function update(User $user, PerformEvalResponse $performEvalResponse)
    {
        return $this->isAllowed($user, 'perform_eval_response_api', 'can_edit');
    }

    /**
     * Determine whether the user can delete the responses.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PerformEvalResponse $performEvalResponse
     * @return mixed
     */
    public function delete(User $user, PerformEvalResponse $performEvalResponse)
    {
        return $this->isAllowed($user, 'perform_eval_response_api', 'can_delete');
    }

    /**
     * Determine whether the user can restore the response.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PerformEvalResponse $performEvalResponse
     * @return mixed
     */
    public function restore(User $user, PerformEvalResponse $performEvalResponse)
    {
        return $this->isAllowed($user, 'perform_eval_response_api', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the response.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PerformEvalResponse $performEvalResponse
     * @return mixed
     */
    public function forceDelete(User $user, PerformEvalResponse $performEvalResponse)
    {
        return $this->isAllowed($user, 'perform_eval_response_api', 'can_delete');
    }
}