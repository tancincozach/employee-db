<?php

namespace App\Policies;

use App\Models\User;
use App\Models\JobcodeAssignment;
use App\Policies\BasePolicy;

class JobcodeAssignmentPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the jobcode assignments.
     *
     * @param  \App\Models\User               $user
     * @param  \App\Models\JobcodeAssignment  $jobcodeAssignment
     * @return mixed
     */
    public function view(User $user, JobcodeAssignment $jobcodeAssignment)
    {
        return $this->isAllowed($user, 'jobcode_assignments', 'can_view');
    }

    /**
     * Determine whether the user can create jobcode assignments.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'jobcode_assignments', 'can_add');
    }

    /**
     * Determine whether the user can update the jobcode assignments.
     *
     * @param  \App\Models\User               $user
     * @param  \App\Models\JobcodeAssignment  $jobcodeAssignment
     * @return mixed
     */
    public function update(User $user, JobcodeAssignment $jobcodeAssignment)
    {
        return $this->isAllowed($user, 'jobcode_assignments', 'can_edit');
    }

    /**
     * Determine whether the user can delete the jobcode assignments.
     *
     * @param  \App\Models\User               $user
     * @param  \App\Models\JobcodeAssignment  $jobcodeAssignment
     * @return mixed
     */
    public function delete(User $user, JobcodeAssignment $jobcodeAssignment)
    {
        return $this->isAllowed($user, 'jobcode_assignments', 'can_delete');
    }

    /**
     * Determine whether the user can restore the jobcode assignments.
     *
     * @param  \App\Models\User               $user
     * @param  \App\Models\JobcodeAssignment  $jobcodeAssignment
     * @return mixed
     */
    public function restore(User $user, JobcodeAssignment $jobcodeAssignment)
    {
        return $this->isAllowed($user, 'jobcode_assignments', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the jobcode assignments.
     *
     * @param  \App\Models\User               $user
     * @param  \App\Models\JobcodeAssignment  $jobcodeAssignment
     * @return mixed
     */
    public function forceDelete(User $user, JobcodeAssignment $jobcodeAssignment)
    {
        return $this->isAllowed($user, 'jobcode_assignments', 'can_delete');
    }
}
