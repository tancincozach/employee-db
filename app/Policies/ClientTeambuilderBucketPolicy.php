<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ClientTeambuilderBucket;
use App\Policies\BasePolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientTeambuilderBucketPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ClientTeambuilderBucket  $teambuilder
     * @return mixed
     */
    public function view(User $user, ClientTeambuilderBucket $teambuilder)
    {
        return $this->isAllowed($user, 'teambuilder_api', 'can_view');
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'teambuilder_api', 'can_add');
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ClientTeambuilderBucket  $teambuilder
     * @return mixed
     */
    public function update(User $user, ClientTeambuilderBucket $teambuilder)
    {
        return $this->isAllowed($user, 'teambuilder_api', 'can_edit');
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ClientTeambuilderBucket  $teambuilder
     * @return mixed
     */
    public function delete(User $user, ClientTeambuilderBucket $teambuilder)
    {
        return $this->isAllowed($user, 'teambuilder_api', 'can_delete');
    }

    /**
     * Determine whether the user can restore the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ClientTeambuilderBucket  $teambuilder
     * @return mixed
     */
    public function restore(User $user, ClientTeambuilderBucket $teambuilder)
    {
        return $this->isAllowed($user, 'teambuilder_api', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ClientTeambuilderBucket  $teambuilder
     * @return mixed
     */
    public function forceDelete(User $user, ClientTeambuilderBucket $teambuilder)
    {
        return $this->isAllowed($user, 'teambuilder_api', 'can_delete');
    }
}
