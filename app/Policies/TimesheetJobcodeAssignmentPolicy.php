<?php

namespace App\Policies;

use App\Models\User;
use App\Models\TimesheetJobcodeAssignment;
use App\Policies\BasePolicy;

class TimesheetJobcodeAssignmentPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the timesheet jobcode assignments.
     *
     * @param  \App\Models\User                        $user
     * @param  \App\Models\TimesheetJobcodeAssignment  $jobcodeAssignment
     * @return mixed
     */
    public function view(User $user, TimesheetJobcodeAssignment $jobcodeAssignment)
    {
        return $this->isAllowed($user, 'timesheet_jobcode_assignments', 'can_view');
    }
}
