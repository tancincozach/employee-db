<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CreativeServiceApplicant;

/**
 * Class CreativeServiceApplicantPolicy
 * @package App\Policies
 */
class CreativeServiceApplicantPolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param CreativeServiceApplicant $creativeServiceApplicant
     * @return bool
     */
    public function view(User $user, CreativeServiceApplicant $creativeServiceApplicant)
    {
        return $this->isAllowed($user, 'creative_service_applicant', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'creative_service_applicant', 'can_add');
    }

    /**
     * @param User $user
     * @param CreativeServiceApplicant $creativeServiceApplicant
     * @return bool
     */
    public function update(User $user, CreativeServiceApplicant $creativeServiceApplicant)
    {
        return $this->isAllowed($user, 'creative_service_applicant', 'can_edit');
    }

    /**
     * @param User $user
     * @param CreativeServiceApplicant $creativeServiceApplicant
     * @return bool
     */
    public function delete(User $user, CreativeServiceApplicant $creativeServiceApplicant)
    {
        return $this->isAllowed($user, 'creative_service_applicant', 'can_delete');
    }

    /**
     * @param User $user
     * @param CreativeServiceApplicant $creativeServiceApplicant
     * @return bool
     */
    public function restore(User $user, CreativeServiceApplicant $creativeServiceApplicant)
    {
        return $this->isAllowed($user, 'creative_service_applicant', 'can_edit');
    }

    /**
     * @param User $user
     * @param CreativeServiceApplicant $creativeServiceApplicant
     * @return bool
     */
    public function forceDelete(User $user, CreativeServiceApplicant $creativeServiceApplicant)
    {
        return $this->isAllowed($user, 'creative_service_applicant', 'can_delete');
    }
}
