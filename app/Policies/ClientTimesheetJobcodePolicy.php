<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ClientTimesheetJobcode;
use App\Policies\BasePolicy;

class ClientTimesheetJobcodePolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the client timesheet jobcodes.
     *
     * @param  \App\Models\User                    $user
     * @param  \App\Models\ClientTimesheetJobcode  $clientTimesheetJobcode
     * @return mixed
     */
    public function view(User $user, ClientTimesheetJobcode $clientTimesheetJobcode)
    {
        return $this->isAllowed($user, 'client_timesheet_jobcodes', 'can_view');
    }

    /**
     * Determine whether the user can create client timesheet jobcodes.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'client_timesheet_jobcodes', 'can_add');
    }

    /**
     * Determine whether the user can update the timesheet.
     *
     * @param  \App\Models\User                    $user
     * @param  \App\Models\ClientTimesheetJobcode  $clientTimesheetJobcode
     * @return mixed
     */
    public function update(User $user, ClientTimesheetJobcode $clientTimesheetJobcode)
    {
        return $this->isAllowed($user, 'client_timesheet_jobcodes', 'can_edit');
    }

    /**
     * Determine whether the user can delete the client timesheet jobcodes.
     *
     * @param  \App\Models\User                    $user
     * @param  \App\Models\ClientTimesheetJobcode  $clientTimesheetJobcode
     * @return mixed
     */
    public function delete(User $user, ClientTimesheetJobcode $clientTimesheetJobcode)
    {
        return $this->isAllowed($user, 'client_timesheet_jobcodes', 'can_delete');
    }

    /**
     * Determine whether the user can restore the client timesheet jobcodes.
     *
     * @param  \App\Models\User                    $user
     * @param  \App\Models\ClientTimesheetJobcode  $clientTimesheetJobcode
     * @return mixed
     */
    public function restore(User $user, ClientTimesheetJobcode $clientTimesheetJobcode)
    {
        return $this->isAllowed($user, 'client_timesheet_jobcodes', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the client timesheet jobcodes.
     *
     * @param  \App\Models\User                    $user
     * @param  \App\Models\ClientTimesheetJobcode  $clientTimesheetJobcode
     * @return mixed
     */
    public function forceDelete(User $user, ClientTimesheetJobcode $clientTimesheetJobcode)
    {
        return $this->isAllowed($user, 'client_timesheet_jobcodes', 'can_delete');
    }
}
