<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserTimezone;
use App\Policies\BasePolicy;

class UserTimezonePolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the user timezone.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\UserTimezone  $userTimezone
     * @return mixed
     */
    public function view(User $user, UserTimezone $userTimezone)
    {
        return $this->isAllowed($user, 'user_timezones', 'can_view');
    }

    /**
     * Determine whether the user can create user timezone.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'user_timezones', 'can_add');
    }

    /**
     * Determine whether the user can update the user timezone.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\UserTimezone  $userTimezone
     * @return mixed
     */
    public function update(User $user, UserTimezone $userTimezone)
    {
        return $this->isAllowed($user, 'user_timezones', 'can_edit');
    }

    /**
     * Determine whether the user can delete the user timezone.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\UserTimezone  $userTimezone
     * @return mixed
     */
    public function delete(User $user, UserTimezone $userTimezone)
    {
        return $this->isAllowed($user, 'user_timezones', 'can_delete');
    }

    /**
     * Determine whether the user can restore the user timezone.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\UserTimezone  $userTimezone
     * @return mixed
     */
    public function restore(User $user, UserTimezone $userTimezone)
    {
        return $this->isAllowed($user, 'user_timezones', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the user timezone.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\UserTimezone  $userTimezone
     * @return mixed
     */
    public function forceDelete(User $user, UserTimezone $userTimezone)
    {
        return $this->isAllowed($user, 'user_timezones', 'can_delete');
    }
}
