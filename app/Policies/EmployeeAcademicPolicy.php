<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeAcademic;

/**
 * Class EmployeeAcademicPolicy
 * @package App\Policies
 */
class EmployeeAcademicPolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param EmployeeAcademic $employeeAcademic
     * @return bool
     */
    public function view(User $user, EmployeeAcademic $employeeAcademic)
    {
        return $this->isAllowed($user, 'employee_academic', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'employee_academic', 'can_add');
    }

    /**
     * @param User $user
     * @param EmployeeAcademic $employeeAcademic
     * @return bool
     */
    public function update(User $user, EmployeeAcademic $employeeAcademic)
    {
        return $this->isAllowed($user, 'employee_academic', 'can_edit');
    }

    /**
     * @param User $user
     * @param EmployeeAcademic $employeeAcademic
     * @return bool
     */
    public function delete(User $user, EmployeeAcademic $employeeAcademic)
    {
        return $this->isAllowed($user, 'employee_academic', 'can_delete');
    }

    /**
     * @param User $user
     * @param EmployeeAcademic $employeeAcademic
     * @return bool
     */
    public function restore(User $user, EmployeeAcademic $employeeAcademic)
    {
        return $this->isAllowed($user, 'employee_academic', 'can_edit');
    }

    /**
     * @param User $user
     * @param EmployeeAcademic $employeeAcademic
     * @return bool
     */
    public function forceDelete(User $user, EmployeeAcademic $employeeAcademic)
    {
        return $this->isAllowed($user, 'employee_academic', 'can_delete');
    }
}
