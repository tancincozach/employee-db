<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeOtherDetail;

class EmployeeOtherDetailPolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param EmployeeOtherDetail $employeeOtherDetail
     * @return bool
     */
    public function view(User $user, EmployeeOtherDetail $employeeOtherDetail)
    {
        return $this->isAllowed($user, 'employee_other_detail', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'employee_other_detail', 'can_add');
    }

    /**
     * @param User $user
     * @param EmployeeOtherDetail $employeeOtherDetail
     * @return bool
     */
    public function update(User $user, EmployeeOtherDetail $employeeOtherDetail)
    {
        return $this->isAllowed($user, 'employee_other_detail', 'can_edit');
    }

    /**
     * @param User $user
     * @param EmployeeOtherDetail $employeeOtherDetail
     * @return bool
     */
    public function delete(User $user, EmployeeOtherDetail $employeeOtherDetail)
    {
        return $this->isAllowed($user, 'employee_other_detail', 'can_delete');
    }

    /**
     * @param User $user
     * @param EmployeeOtherDetail $employeeOtherDetail
     * @return bool
     */
    public function restore(User $user, EmployeeOtherDetail $employeeOtherDetail)
    {
        return $this->isAllowed($user, 'employee_other_detail', 'can_edit');
    }

    /**
     * @param User $user
     * @param EmployeeOtherDetail $employeeOtherDetail
     * @return bool
     */
    public function forceDelete(User $user, EmployeeOtherDetail $employeeOtherDetail)
    {
        return $this->isAllowed($user, 'employee_other_detail', 'can_delete');
    }
}
