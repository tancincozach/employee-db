<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ClientOnboardingChecklist;
use App\Policies\BasePolicy;

class ClientOnboardingChecklistPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the client onboarding checklist information.
     *
     * @param  \App\Models\User  $user
     * @param  \App\ClientOnboardingChecklist $ClientOnboardingChecklist
     * @return mixed
     */
    public function view(User $user, ClientOnboardingChecklist $ClientOnboardingChecklist)
    {
        return $this->isAllowed($user, 'client_onboarding_checklist_api', 'can_view');
    }

    /**
     * Determine whether the user can create client onboarding checklist information.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'client_onboarding_checklist_api', 'can_add');
    }

    /**
     * Determine whether the user can update the client onboarding checklist information.
     *
     * @param  \App\Models\User  $user
     * @param  \App\ClientOnboardingChecklist $ClientOnboardingChecklist
     * @return mixed
     */
    public function update(User $user, ClientOnboardingChecklist $ClientOnboardingChecklist)
    {
        return $this->isAllowed($user, 'client_onboarding_checklist_api', 'can_edit');
    }

    /**
     * Determine whether the user can delete the client onboarding checklist information.
     *
     * @param  \App\Models\User  $user
     * @param  \App\ClientOnboardingChecklist $ClientOnboardingChecklist
     * @return mixed
     */
    public function delete(User $user, ClientOnboardingChecklist $ClientOnboardingChecklist)
    {
        return $this->isAllowed($user, 'client_onboarding_checklist_api', 'can_delete');
    }

    /**
     * Determine whether the user can restore the client onboarding checklist information.
     *
     * @param  \App\Models\User  $user
     * @param  \App\ClientOnboardingChecklist $ClientOnboardingChecklist
     * @return mixed
     */
    public function restore(User $user, ClientOnboardingChecklist $ClientOnboardingChecklist)
    {
        return $this->isAllowed($user, 'client_onboarding_checklist_api', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the client onboarding checklist information.
     *
     * @param  \App\Models\User  $user
     * @param  \App\ClientOnboardingChecklist $ClientOnboardingChecklist
     * @return mixed
     */
    public function forceDelete(User $user, ClientOnboardingChecklist $ClientOnboardingChecklist)
    {
        return $this->isAllowed($user, 'client_onboarding_checklist_api', 'can_delete');
    }
}
