<?php

namespace App\Policies;

use App\Models\User;
use App\Models\PerformEvalQuestion;
use App\Policies\BasePolicy;

class PerformEvalQuestionPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the questions.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PerformEvalQuestion $performEvalQuestion
     * @return mixed
     */
    public function view(User $user, PerformEvalQuestion $performEvalQuestion)
    {
        return $this->isAllowed($user, 'perform_eval_question_api', 'can_view');
    }

    /**
     * Determine whether the user can create question.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'perform_eval_question_api', 'can_add');
    }

    /**
     * Determine whether the user can update the question.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PerformEvalQuestion $performEvalQuestion
     * @return mixed
     */
    public function update(User $user, PerformEvalQuestion $performEvalQuestion)
    {
        return $this->isAllowed($user, 'perform_eval_question_api', 'can_edit');
    }

    /**
     * Determine whether the user can delete the questions.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PerformEvalQuestion $performEvalQuestion
     * @return mixed
     */
    public function delete(User $user, PerformEvalQuestion $performEvalQuestion)
    {
        return $this->isAllowed($user, 'perform_eval_question_api', 'can_delete');
    }

    /**
     * Determine whether the user can restore the question.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PerformEvalQuestion $weeklyFloorAnswer
     * @return mixed
     */
    public function restore(User $user, PerformEvalQuestion $performEvalQuestion)
    {
        return $this->isAllowed($user, 'perform_eval_question_api', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the question.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PerformEvalQuestion $performEvalQuestion
     * @return mixed
     */
    public function forceDelete(User $user, PerformEvalQuestion $performEvalQuestion)
    {
        return $this->isAllowed($user, 'perform_eval_question_api', 'can_delete');
    }
}