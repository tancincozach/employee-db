<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Feedback;

/**
 * Class FeedbackPolicy
 * @package App\Policies
 */
class FeedbackPolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param Feedback $feedback
     * @return bool
     */
    public function view(User $user, Feedback $feedback)
    {
        return $this->isAllowed($user, 'feedback', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'feedback', 'can_add');
    }

    /**
     * @param User $user
     * @param Feedback $feedback
     * @return bool
     */
    public function update(User $user, Feedback $feedback)
    {
        return $this->isAllowed($user, 'feedback', 'can_edit');
    }

    /**
     * @param User $user
     * @param Feedback $feedback
     * @return bool
     */
    public function delete(User $user, Feedback $feedback)
    {
        return $this->isAllowed($user, 'feedback', 'can_delete');
    }

    /**
     * @param User $user
     * @param Feedback $feedback
     * @return bool
     */
    public function restore(User $user, Feedback $feedback)
    {
        return $this->isAllowed($user, 'feedback', 'can_edit');
    }

    /**
     * @param User $user
     * @param Feedback $feedback
     * @return bool
     */
    public function forceDelete(User $user, Feedback $feedback)
    {
        return $this->isAllowed($user, 'feedback', 'can_delete');
    }
}
