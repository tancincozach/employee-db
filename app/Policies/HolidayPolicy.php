<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Holiday;
use App\Policies\BasePolicy;

class HolidayPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the holiday.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\Holiday  $holiday
     * @return mixed
     */
    public function view(User $user, Holiday $holiday)
    {
        return $this->isAllowed($user, 'holidays', 'can_view');
    }

    /**
     * Determine whether the user can create holiday.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'holidays', 'can_add');
    }

    /**
     * Determine whether the user can update the holiday.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\Holiday  $holiday
     * @return mixed
     */
    public function update(User $user, Holiday $holiday)
    {
        return $this->isAllowed($user, 'holidays', 'can_edit');
    }

    /**
     * Determine whether the user can delete the holiday.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\Holiday  $holiday
     * @return mixed
     */
    public function delete(User $user, Holiday $holiday)
    {
        return $this->isAllowed($user, 'holidays', 'can_delete');
    }

    /**
     * Determine whether the user can restore the holiday.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\Holiday  $holiday
     * @return mixed
     */
    public function restore(User $user, Holiday $holiday)
    {
        return $this->isAllowed($user, 'holidays', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the holiday.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\Holiday  $holiday
     * @return mixed
     */
    public function forceDelete(User $user, Holiday $holiday)
    {
        return $this->isAllowed($user, 'holidays', 'can_delete');
    }
}
