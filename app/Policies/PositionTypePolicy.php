<?php

namespace App\Policies;

use App\Models\User;
use App\Models\PositionType;

/**
 * Class PositionTypePolicy
 * @package App\Policies
 */
class PositionTypePolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param PositionType $positionType
     * @return bool
     */
    public function view(User $user, PositionType $positionType)
    {
        return $this->isAllowed($user, 'position_type', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'position_type', 'can_add');
    }

    /**
     * @param User $user
     * @param PositionType $positionType
     * @return bool
     */
    public function update(User $user, PositionType $positionType)
    {
        return $this->isAllowed($user, 'position_type', 'can_edit');
    }

    /**
     * @param User $user
     * @param PositionType $positionType
     * @return bool
     */
    public function delete(User $user, PositionType $positionType)
    {
        return $this->isAllowed($user, 'position_type', 'can_delete');
    }

    /**
     * @param User $user
     * @param PositionType $positionType
     * @return bool
     */
    public function restore(User $user, PositionType $positionType)
    {
        return $this->isAllowed($user, 'position_type', 'can_edit');
    }

    /**
     * @param User $user
     * @param PositionType $positionType
     * @return bool
     */
    public function forceDelete(User $user, PositionType $positionType)
    {
        return $this->isAllowed($user, 'position_type', 'can_delete');
    }
}
