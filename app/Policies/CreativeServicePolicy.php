<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CreativeService;

/**
 * Class CreativeServicePolicy
 * @package App\Policies
 */
class CreativeServicePolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param CreativeService $creativeService
     * @return bool
     */
    public function view(User $user, CreativeService $creativeService)
    {
        return $this->isAllowed($user, 'creative_service', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'creative_service', 'can_add');
    }

    /**
     * @param User $user
     * @param CreativeService $creativeService
     * @return bool
     */
    public function update(User $user, CreativeService $creativeService)
    {
        return $this->isAllowed($user, 'creative_service', 'can_edit');
    }

    /**
     * @param User $user
     * @param CreativeService $creativeService
     * @return bool
     */
    public function delete(User $user, CreativeService $creativeService)
    {
        return $this->isAllowed($user, 'creative_service', 'can_delete');
    }

    /**
     * @param User $user
     * @param CreativeService $creativeService
     * @return bool
     */
    public function restore(User $user, CreativeService $creativeService)
    {
        return $this->isAllowed($user, 'creative_service', 'can_edit');
    }

    /**
     * @param User $user
     * @param CreativeService $creativeService
     * @return bool
     */
    public function forceDelete(User $user, CreativeService $creativeService)
    {
        return $this->isAllowed($user, 'creative_service', 'can_delete');
    }
}
