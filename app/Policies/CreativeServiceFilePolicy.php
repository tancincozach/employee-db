<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CreativeServiceFile;

/**
 * Class CreativeServiceFilePolicy
 * @package App\Policies
 */
class CreativeServiceFilePolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param CreativeServiceFile $creativeServiceFile
     * @return bool
     */
    public function view(User $user, CreativeServiceFile $creativeServiceFile)
    {
        return $this->isAllowed($user, 'creative_service_file', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'creative_service_file', 'can_add');
    }

    /**
     * @param User $user
     * @param CreativeServiceFile $creativeServiceFile
     * @return bool
     */
    public function update(User $user, CreativeServiceFile $creativeServiceFile)
    {
        return $this->isAllowed($user, 'creative_service_file', 'can_edit');
    }

    /**
     * @param User $user
     * @param CreativeServiceFile $creativeServiceFile
     * @return bool
     */
    public function delete(User $user, CreativeServiceFile $creativeServiceFile)
    {
        return $this->isAllowed($user, 'creative_service_file', 'can_delete');
    }

    /**
     * @param User $user
     * @param CreativeServiceFile $creativeServiceFile
     * @return bool
     */
    public function restore(User $user, CreativeServiceFile $creativeServiceFile)
    {
        return $this->isAllowed($user, 'creative_service_file', 'can_edit');
    }

    /**
     * @param User $user
     * @param CreativeServiceFile $creativeServiceFile
     * @return bool
     */
    public function forceDelete(User $user, CreativeServiceFile $creativeServiceFile)
    {
        return $this->isAllowed($user, 'creative_service_file', 'can_delete');
    }
}
