<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Employee;
use App\Models\HackerRankTest;
use App\Policies\BasePolicy;

class HackerRankTestPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the hackerRankTest.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\HackerRankTest  $hackerRankTest
     * @return mixed
     */
    public function view(User $user, HackerRankTest $hackerRankTest)
    {
        return $this->isAllowed($user, 'hacker_rank_tests', 'can_view');
    }

    /**
     * Determine whether the user can create hackerRankTest.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'hacker_rank_tests', 'can_add');
    }

    /**
     * Determine whether the user can update the hackerRankTest.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\HackerRankTest  $hackerRankTest
     * @return mixed
     */
    public function update(User $user, HackerRankTest $hackerRankTest)
    {
        return $this->isAllowed($user, 'hacker_rank_tests', 'can_edit');
    }

    /**
     * Determine whether the user can delete the hackerRankTest.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\HackerRankTest  $hackerRankTest
     * @return mixed
     */
    public function delete(User $user, HackerRankTest $hackerRankTest)
    {
        return $this->isAllowed($user, 'hacker_rank_tests', 'can_delete');
    }

    /**
     * Determine whether the user can restore the hackerRankTest.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\HackerRankTest  $hackerRankTest
     * @return mixed
     */
    public function restore(User $user, HackerRankTest $hackerRankTest)
    {
        return $this->isAllowed($user, 'hacker_rank_tests', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the hackerRankTest.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\HackerRankTest  $hackerRankTest
     * @return mixed
     */
    public function forceDelete(User $user, HackerRankTest $hackerRankTest)
    {
        return $this->isAllowed($user, 'hacker_rank_tests', 'can_delete');
    }
}
