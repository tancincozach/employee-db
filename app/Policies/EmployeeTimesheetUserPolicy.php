<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeTimesheetUser;
use App\Policies\BasePolicy;

class EmployeeTimesheetUserPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the employee timesheet users.
     *
     * @param  \App\Models\User                   $user
     * @param  \App\Models\EmployeeTimesheetUser  $employeeTimesheetUser
     * @return mixed
     */
    public function view(User $user, EmployeeTimesheetUser $employeeTimesheetUser)
    {
        return $this->isAllowed($user, 'employee_timesheet_users', 'can_view');
    }

    /**
     * Determine whether the user can create employee timesheet users.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'employee_timesheet_users', 'can_add');
    }

    /**
     * Determine whether the user can update the employee timesheet users.
     *
     * @param  \App\Models\User                   $user
     * @param  \App\Models\EmployeeTimesheetUser  $employeeTimesheetUser
     * @return mixed
     */
    public function update(User $user, EmployeeTimesheetUser $employeeTimesheetUser)
    {
        return $this->isAllowed($user, 'employee_timesheet_users', 'can_edit');
    }

    /**
     * Determine whether the user can delete the employee timesheet users.
     *
     * @param  \App\Models\User                   $user
     * @param  \App\Models\EmployeeTimesheetUser  $employeeTimesheetUser
     * @return mixed
     */
    public function delete(User $user, EmployeeTimesheetUser $employeeTimesheetUser)
    {
        return $this->isAllowed($user, 'employee_timesheet_users', 'can_delete');
    }

    /**
     * Determine whether the user can restore the employee timesheet users.
     *
     * @param  \App\Models\User                   $user
     * @param  \App\Models\EmployeeTimesheetUser  $employeeTimesheetUser
     * @return mixed
     */
    public function restore(User $user, EmployeeTimesheetUser $employeeTimesheetUser)
    {
        return $this->isAllowed($user, 'employee_timesheet_users', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the employee timesheet users.
     *
     * @param  \App\Models\User                   $user
     * @param  \App\Models\EmployeeTimesheetUser  $employeeTimesheetUser
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeTimesheetUser $employeeTimesheetUser)
    {
        return $this->isAllowed($user, 'employee_timesheet_users', 'can_delete');
    }
}
