<?php

namespace App\Policies;

use App\Models\User;
use App\Models\FeedbackResponse;

/**
 * Class FeedbackResponsePolicy
 * @package App\Policies
 */
class FeedbackResponsePolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param FeedbackResponse $feedbackResponse
     * @return bool
     */
    public function view(User $user, FeedbackResponse $feedbackResponse)
    {
        return $this->isAllowed($user, 'feedback_response', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'feedback_response', 'can_add');
    }

    /**
     * @param User $user
     * @param FeedbackResponse $feedbackResponse
     * @return bool
     */
    public function update(User $user, FeedbackResponse $feedbackResponse)
    {
        return $this->isAllowed($user, 'feedback_response', 'can_edit');
    }

    /**
     * @param User $user
     * @param FeedbackResponse $feedbackResponse
     * @return bool
     */
    public function delete(User $user, FeedbackResponse $feedbackResponse)
    {
        return $this->isAllowed($user, 'feedback_response', 'can_delete');
    }

    /**
     * @param User $user
     * @param FeedbackResponse $feedbackResponse
     * @return bool
     */
    public function restore(User $user, FeedbackResponse $feedbackResponse)
    {
        return $this->isAllowed($user, 'feedback_response', 'can_edit');
    }

    /**
     * @param User $user
     * @param FeedbackResponse $feedbackResponse
     * @return bool
     */
    public function forceDelete(User $user, FeedbackResponse $feedbackResponse)
    {
        return $this->isAllowed($user, 'feedback_response', 'can_delete');
    }
}
