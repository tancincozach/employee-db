<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Characteristic;

/**
 * Class CharacteristicPolicy
 * @package App\Policies
 */
class CharacteristicPolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param Characteristic $characteristic
     * @return bool
     */
    public function view(User $user, Characteristic $characteristic)
    {
        return $this->isAllowed($user, 'characteristic', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'characteristic', 'can_add');
    }

    /**
     * @param User $user
     * @param Characteristic $characteristic
     * @return bool
     */
    public function update(User $user, Characteristic $characteristic)
    {
        return $this->isAllowed($user, 'characteristic', 'can_edit');
    }

    /**
     * @param User $user
     * @param Characteristic $characteristic
     * @return bool
     */
    public function delete(User $user, Characteristic $characteristic)
    {
        return $this->isAllowed($user, 'characteristic', 'can_delete');
    }

    /**
     * @param User $user
     * @param Characteristic $characteristic
     * @return bool
     */
    public function restore(User $user, Characteristic $characteristic)
    {
        return $this->isAllowed($user, 'characteristic', 'can_edit');
    }

    /**
     * @param User $user
     * @param Characteristic $characteristic
     * @return bool
     */
    public function forceDelete(User $user, Characteristic $characteristic)
    {
        return $this->isAllowed($user, 'characteristic', 'can_delete');
    }
}
