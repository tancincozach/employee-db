<?php

namespace App\Policies;

use App\Models\User;
use App\Models\TimesheetUser;
use App\Policies\BasePolicy;

class TimesheetUserPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the timesheet users.
     *
     * @param  \App\Models\User           $user
     * @param  \App\Models\TimesheetUser  $timesheetUsers
     * @return mixed
     */
    public function view(User $user, TimesheetUser $timesheetUsers)
    {
        return $this->isAllowed($user, 'timesheet_users', 'can_view');
    }

    /**
     * Determine whether the user can create timesheet users.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'timesheet_users', 'can_add');
    }

    /**
     * Determine whether the user can update the timesheet users.
     *
     * @param  \App\Models\User           $user
     * @param  \App\Models\TimesheetUser  $timesheetUsers
     * @return mixed
     */
    public function update(User $user, TimesheetUser $timesheetUsers)
    {
        return $this->isAllowed($user, 'timesheet_users', 'can_edit');
    }

    /**
     * Determine whether the user can delete the timesheet users.
     *
     * @param  \App\Models\User           $user
     * @param  \App\Models\TimesheetUser  $timesheetUsers
     * @return mixed
     */
    public function delete(User $user, TimesheetUser $timesheetUsers)
    {
        return $this->isAllowed($user, 'timesheet_users', 'can_delete');
    }

    /**
     * Determine whether the user can restore the timesheet users.
     *
     * @param  \App\Models\User           $user
     * @param  \App\Models\TimesheetUser  $timesheetUsers
     * @return mixed
     */
    public function restore(User $user, TimesheetUser $timesheetUsers)
    {
        return $this->isAllowed($user, 'timesheet_users', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the timesheet users.
     *
     * @param  \App\Models\User           $user
     * @param  \App\Models\TimesheetUser  $timesheetUsers
     * @return mixed
     */
    public function forceDelete(User $user, TimesheetUser $timesheetUsers)
    {
        return $this->isAllowed($user, 'timesheet_users', 'can_delete');
    }
}
