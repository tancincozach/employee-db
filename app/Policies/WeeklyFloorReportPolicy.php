<?php

namespace App\Policies;

use App\Models\User;
use App\Models\WeeklyFloorReport;
use App\Policies\BasePolicy;

class WeeklyFloorReportPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the report.
     *
     * @param  \App\Models\User  $user
     * @param  \App\WeeklyFloorReport \$weeklyFloorReport
     * @return mixed
     */
    public function view(User $user, WeeklyFloorReport $weeklyFloorReport)
    {
        return $this->isAllowed($user, 'weekly_floor_report', 'can_view');
    }

    /**
     * Determine whether the user can create reports.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'weekly_floor_report', 'can_add');
    }

    /**
     * Determine whether the user can update the report.
     *
     * @param  \App\Models\User  $user
     * @param  \App\WeeklyFloorReport \$weeklyFloorReport
     * @return mixed
     */
    public function update(User $user, WeeklyFloorReport $weeklyFloorReport)
    {
        return $this->isAllowed($user, 'weekly_floor_report', 'can_edit');
    }

    /**
     * Determine whether the user can delete the report.
     *
     * @param  \App\Models\User  $user
     * @param  \App\WeeklyFloorReport \$weeklyFloorReport
     * @return mixed
     */
    public function delete(User $user, WeeklyFloorReport $weeklyFloorReport)
    {
        return $this->isAllowed($user, 'weekly_floor_report', 'can_delete');
    }

    /**
     * Determine whether the user can restore the report.
     *
     * @param  \App\Models\User  $user
     * @param  \App\WeeklyFloorReport \$weeklyFloorReport
     * @return mixed
     */
    public function restore(User $user, WeeklyFloorReport $weeklyFloorReport)
    {
        return $this->isAllowed($user, 'weekly_floor_report', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the report.
     *
     * @param  \App\Models\User  $user
     * @param  \App\WeeklyFloorReport \$weeklyFloorReport
     * @return mixed
     */
    public function forceDelete(User $user, WeeklyFloorReport $weeklyFloorReport)
    {
        return $this->isAllowed($user, 'weekly_floor_report', 'can_delete');
    }
}
