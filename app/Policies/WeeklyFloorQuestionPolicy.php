<?php

namespace App\Policies;

use App\Models\User;
use App\Models\WeeklyFloorQuestion;
use App\Policies\BasePolicy;

class WeeklyFloorQuestionPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the report.
     *
     * @param  \App\Models\User  $user
     * @param  \App\WeeklyFloorQuestion \$WeeklyFloorQuestion
     * @return mixed
     */
    public function view(User $user, WeeklyFloorQuestion $weeklyFloorQuestion)
    {
        return $this->isAllowed($user, 'weekly_floor_question', 'can_view');
    }

    /**
     * Determine whether the user can create reports.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'weekly_floor_question', 'can_add');
    }

    /**
     * Determine whether the user can update the report.
     *
     * @param  \App\Models\User  $user
     * @param  \App\WeeklyFloorQuestion \$weeklyFloorQuestion
     * @return mixed
     */
    public function update(User $user, WeeklyFloorQuestion $weeklyFloorQuestion)
    {
        return $this->isAllowed($user, 'weekly_floor_question', 'can_edit');
    }

    /**
     * Determine whether the user can delete the report.
     *
     * @param  \App\Models\User  $user
     * @param  \App\WeeklyFloorQuestion \$weeklyFloorQuestion
     * @return mixed
     */
    public function delete(User $user, WeeklyFloorQuestion $weeklyFloorQuestion)
    {
        return $this->isAllowed($user, 'weekly_floor_question', 'can_delete');
    }

    /**
     * Determine whether the user can restore the report.
     *
     * @param  \App\Models\User  $user
     * @param  \App\WeeklyFloorQuestion \$weeklyFloorQuestion
     * @return mixed
     */
    public function restore(User $user, WeeklyFloorQuestion $weeklyFloorQuestion)
    {
        return $this->isAllowed($user, 'weekly_floor_question', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the report.
     *
     * @param  \App\Models\User  $user
     * @param  \App\WeeklyFloorQuestion \$weeklyFloorQuestion
     * @return mixed
     */
    public function forceDelete(User $user, WeeklyFloorQuestion $weeklyFloorQuestion)
    {
        return $this->isAllowed($user, 'weekly_floor_question', 'can_delete');
    }
}
