<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Notification;
use App\Policies\BasePolicy;

class NotificationPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the notifications.
     *
     * @param  App\Models\User         $user
     * @param  App\Models\Notification $notification
     * @return mixed
     */
    public function view(User $user, Notification $notification)
    {
        return $this->isAllowed($user, 'notifications', 'can_view');
    }

    /**
     * Determine whether the user can create notifications.
     *
     * @param  App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'notifications', 'can_add');
    }

    /**
     * Determine whether the user can update the notification.
     *
     * @param  App\Models\User         $user
     * @param  App\Models\Notification $notification
     * @return mixed
     */
    public function update(User $user, Notification $notification)
    {
        return $this->isAllowed($user, 'notifications', 'can_edit');
    }

    /**
     * Determine whether the user can delete the notification.
     *
     * @param  App\Models\User         $user
     * @param  App\Models\Notification $notification
     * @return mixed
     */
    public function delete(User $user, Notification $notification)
    {
        return $this->isAllowed($user, 'notifications', 'can_delete');
    }

    /**
     * Determine whether the user can restore the notification.
     *
     * @param  App\Models\User         $user
     * @param  App\Models\Notification $notification
     * @return mixed
     */
    public function restore(User $user, Notification $notification)
    {
        return $this->isAllowed($user, 'notifications', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the notification.
     *
     * @param  App\Models\User         $user
     * @param  App\Models\Notification $notification
     * @return mixed
     */
    public function forceDelete(User $user, Notification $notification)
    {
        return $this->isAllowed($user, 'notifications', 'can_delete');
    }
}
