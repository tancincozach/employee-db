<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Timesheet;
use App\Policies\BasePolicy;

class TimesheetPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the timesheet.
     *
     * @param  \App\Models\User       $user
     * @param  \App\Models\Timesheet  $timesheet
     * @return mixed
     */
    public function view(User $user, Timesheet $timesheet)
    {
        return $this->isAllowed($user, 'timesheets', 'can_view');
    }

    /**
     * Determine whether the user can create timesheet.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'timesheets', 'can_add');
    }

    /**
     * Determine whether the user can update the timesheet.
     *
     * @param  \App\Models\User       $user
     * @param  \App\Models\Timesheet  $timesheet
     * @return mixed
     */
    public function update(User $user, Timesheet $timesheet)
    {
        return $this->isAllowed($user, 'timesheets', 'can_edit');
    }

    /**
     * Determine whether the user can delete the timesheet.
     *
     * @param  \App\Models\User       $user
     * @param  \App\Models\Timesheet  $timesheet
     * @return mixed
     */
    public function delete(User $user, Timesheet $timesheet)
    {
        return $this->isAllowed($user, 'timesheets', 'can_delete');
    }

    /**
     * Determine whether the user can restore the timesheet.
     *
     * @param  \App\Models\User       $user
     * @param  \App\Models\Timesheet  $timesheet
     * @return mixed
     */
    public function restore(User $user, Timesheet $timesheet)
    {
        return $this->isAllowed($user, 'timesheets', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the timesheet.
     *
     * @param  \App\Models\User       $user
     * @param  \App\Models\Timesheet  $timesheet
     * @return mixed
     */
    public function forceDelete(User $user, Timesheet $timesheet)
    {
        return $this->isAllowed($user, 'timesheets', 'can_delete');
    }
}
