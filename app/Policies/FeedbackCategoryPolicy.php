<?php

namespace App\Policies;

use App\Models\User;
use App\Models\FeedbackCategory;

/**
 * Class FeedbackCategoryPolicy
 * @package App\Policies
 */
class FeedbackCategoryPolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param FeedbackCategory $feedbackCategory
     * @return bool
     */
    public function view(User $user, FeedbackCategory $feedbackCategory)
    {
        return $this->isAllowed($user, 'feedback_category', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'feedback_category', 'can_add');
    }

    /**
     * @param User $user
     * @param FeedbackCategory $feedbackCategory
     * @return bool
     */
    public function update(User $user, FeedbackCategory $feedbackCategory)
    {
        return $this->isAllowed($user, 'feedback_category', 'can_edit');
    }

    /**
     * @param User $user
     * @param FeedbackCategory $feedbackCategory
     * @return bool
     */
    public function delete(User $user, FeedbackCategory $feedbackCategory)
    {
        return $this->isAllowed($user, 'feedback_category', 'can_delete');
    }

    /**
     * @param User $user
     * @param FeedbackCategory $feedbackCategory
     * @return bool
     */
    public function restore(User $user, FeedbackCategory $feedbackCategory)
    {
        return $this->isAllowed($user, 'feedback_category', 'can_edit');
    }

    /**
     * @param User $user
     * @param FeedbackCategory $feedbackCategory
     * @return bool
     */
    public function forceDelete(User $user, FeedbackCategory $feedbackCategory)
    {
        return $this->isAllowed($user, 'feedback_category', 'can_delete');
    }
}
