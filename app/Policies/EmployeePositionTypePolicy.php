<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeePositionType;

/**
 * Class EmployeePositionTypePolicy
 * @package App\Policies
 */
class EmployeePositionTypePolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param EmployeePositionType $employeePositionType
     * @return bool
     */
    public function view(User $user, EmployeePositionType $employeePositionType)
    {
        return $this->isAllowed($user, 'employee_position_type', 'can_view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'employee_position_type', 'can_add');
    }

    /**
     * @param User $user
     * @param EmployeePositionType $employeePositionType
     * @return bool
     */
    public function update(User $user, EmployeePositionType $employeePositionType)
    {
        return $this->isAllowed($user, 'employee_position_type', 'can_edit');
    }

    /**
     * @param User $user
     * @param EmployeePositionType $employeePositionType
     * @return bool
     */
    public function delete(User $user, EmployeePositionType $employeePositionType)
    {
        return $this->isAllowed($user, 'employee_position_type', 'can_delete');
    }

    /**
     * @param User $user
     * @param EmployeePositionType $employeePositionType
     * @return bool
     */
    public function restore(User $user, EmployeePositionType $employeePositionType)
    {
        return $this->isAllowed($user, 'employee_position_type', 'can_edit');
    }

    /**
     * @param User $user
     * @param EmployeePositionType $employeePositionType
     * @return bool
     */
    public function forceDelete(User $user, EmployeePositionType $employeePositionType)
    {
        return $this->isAllowed($user, 'employee_position_type', 'can_delete');
    }
}
