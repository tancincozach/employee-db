<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Employee;
use App\Models\HackerRankTestResult;
use App\Policies\BasePolicy;

class HackerRankTestResultPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the hackerRankTestResult.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\HackerRankTestResult  $hackerRankTestResult
     * @return mixed
     */
    public function view(User $user, HackerRankTestResult $hackerRankTestResult)
    {
        return $this->isAllowed($user, 'hacker_rank_results', 'can_view');
    }

    /**
     * Determine whether the user can create hackerRankTestResult.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'hacker_rank_results', 'can_add');
    }

    /**
     * Determine whether the user can update the hackerRankTestResult.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\HackerRankTestResult  $hackerRankTestResult
     * @return mixed
     */
    public function update(User $user, HackerRankTestResult $hackerRankTestResult)
    {
        return $this->isAllowed($user, 'hacker_rank_results', 'can_edit');
    }

    /**
     * Determine whether the user can delete the hackerRankTestResult.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\HackerRankTestResult  $hackerRankTestResult
     * @return mixed
     */
    public function delete(User $user, HackerRankTestResult $hackerRankTestResult)
    {
        return $this->isAllowed($user, 'hacker_rank_results', 'can_delete');
    }

    /**
     * Determine whether the user can restore the hackerRankTestResult.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\HackerRankTestResult  $hackerRankTestResult
     * @return mixed
     */
    public function restore(User $user, HackerRankTestResult $hackerRankTestResult)
    {
        return $this->isAllowed($user, 'hacker_rank_results', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the hackerRankTestResult.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\HackerRankTestResult  $hackerRankTestResult
     * @return mixed
     */
    public function forceDelete(User $user, HackerRankTestResult $hackerRankTestResult)
    {
        return $this->isAllowed($user, 'hacker_rank_results', 'can_delete');
    }
}
