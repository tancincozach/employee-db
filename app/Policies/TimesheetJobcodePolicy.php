<?php

namespace App\Policies;

use App\Models\User;
use App\Models\TimesheetJobcode;
use App\Policies\BasePolicy;

class TimesheetJobcodePolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the timesheet jobcodes.
     *
     * @param  \App\Models\User              $user
     * @param  \App\Models\TimesheetJobcode  $timesheetJobcodes
     * @return mixed
     */
    public function view(User $user, TimesheetJobcode $timesheetJobcodes)
    {
        return $this->isAllowed($user, 'timesheet_jobcodes', 'can_view');
    }

    /**
     * Determine whether the user can create timesheet jobcodes.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'timesheet_jobcodes', 'can_add');
    }

    /**
     * Determine whether the user can update the timesheet jobcodes.
     *
     * @param  \App\Models\User              $user
     * @param  \App\Models\TimesheetJobcode  $timesheetJobcodes
     * @return mixed
     */
    public function update(User $user, TimesheetJobcode $timesheetJobcodes)
    {
        return $this->isAllowed($user, 'timesheet_jobcodes', 'can_edit');
    }

    /**
     * Determine whether the user can delete the timesheet jobcodes.
     *
     * @param  \App\Models\User              $user
     * @param  \App\Models\TimesheetJobcode  $timesheetJobcodes
     * @return mixed
     */
    public function delete(User $user, TimesheetJobcode $timesheetJobcodes)
    {
        return $this->isAllowed($user, 'timesheet_jobcodes', 'can_delete');
    }

    /**
     * Determine whether the user can restore the timesheet jobcodes.
     *
     * @param  \App\Models\User              $user
     * @param  \App\Models\TimesheetJobcode  $timesheetJobcodes
     * @return mixed
     */
    public function restore(User $user, TimesheetJobcode $timesheetJobcodes)
    {
        return $this->isAllowed($user, 'timesheet_jobcodes', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the timesheet jobcodes.
     *
     * @param  \App\Models\User              $user
     * @param  \App\Models\TimesheetJobcode  $timesheetJobcodes
     * @return mixed
     */
    public function forceDelete(User $user, TimesheetJobcode $timesheetJobcodes)
    {
        return $this->isAllowed($user, 'timesheet_jobcodes', 'can_delete');
    }
}
