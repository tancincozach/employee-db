<?php

namespace App\Policies;

use App\Models\User;
use App\Models\OperationsDocument;

class OperationsDocumentPolicy extends BasePolicy
{

    /**
     * Determine whether the user can view the documents.
     *
     * @param  \App\Models\User  $user
     * @param  \App\OperationsDcouments $doc
     * @return mixed
     */
    public function view(User $user, OperationsDocument $doc)
    {
      return true; //set as true for all
    }

    /**
     * Determine whether the user can create documents.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAllowed($user, 'manage_operations', 'can_add');
    }

    /**
     * Determine whether the user can update the documents.
     *
     * @param  \App\Models\User  $user
     * @param  \App\OperationsDocument $doc
     * @return mixed
     */
    public function update(User $user, OperationsDocument $doc)
    {
        return $this->isAllowed($user, 'manage_operations', 'can_edit');
    }

    /**
     * Determine whether the user can delete the documents.
     *
     * @param  \App\Models\User  $user
     * @param  \App\OperationsDocument $doc
     * @return mixed
     */
    public function delete(User $user, OperationsDocument $doc)
    {
        return $this->isAllowed($user, 'manage_operations', 'can_delete');
    }

    /**
     * Determine whether the user can restore the faq.
     *
     * @param  \App\Models\User  $user
     * @param  \App\OperationsDocument $doc
     * @return mixed
     */
    public function restore(User $user, OperationsDocument $doc)
    {
        return $this->isAllowed($user, 'manage_operations', 'can_edit');
    }

    /**
     * Determine whether the user can permanently delete the faq.
     *
     * @param  \App\Models\User  $user
     * @param  \App\OperationsDocument $doc
     * @return mixed
     */
    public function forceDelete(User $user, OperationsDocument $doc)
    {
        return $this->isAllowed($user, 'manage_operations', 'can_delete');
    }
}
