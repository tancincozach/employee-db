<?php

namespace App\Listeners;

use App\Criterias\Employee\SearchByUser;
use App\Criterias\User\SearchByEmail;
use App\Criterias\User\SearchByRole;
use App\Events\NewApplicantEvent;
use App\Mail\NewApplicantMail;
use App\Repositories\Employee\EmployeeRepository;
use App\Repositories\FeedbackCategory\FeedbackCategoryRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Support\Facades\Mail;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class NewApplicantListener
 * @package App\Listeners
 */
class NewApplicantListener
{
    /**
     * The id for the Super Admin Role
     */
    private const SUPER_ADMIN_ROLE = 1;
    private const HUMAN_RESOURCE_ROLE = 6;

    protected $employeeRepository;

    protected $userRepository;

    /**
     * FeedbackListener constructor.
     * @param EmployeeRepository $employeeRepository
     * @param FeedbackCategoryRepository $feedbackCategoryRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        EmployeeRepository $employeeRepository,
        UserRepository $userRepository
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->userRepository = $userRepository;
    }

    public function onNewEmployeeCreated($applicantEvent)
    {
        //$this->userRepository->pushCriteria(new SearchByRole(self::SUPER_ADMIN_ROLE, false));
        //$users = $this->userRepository->get()->toArray();

        $users = array('rcanete@fullscale.io', 'rnacu@fullscale.io', 'keseo@fullscale.io');
        //$users = array('b402117dd4-8e12ba@inbox.mailtrap.io');
        $data = [
            'applicant' => $applicantEvent
        ];
        if ($users) {
            
            foreach ($users as $user) {
                Mail::to($user)->send(new NewApplicantMail($data));
                if(env('MAIL_HOST', false) == 'smtp.mailtrap.io'){
                    sleep(5);
                }
            }
        }
    }

    /**
     * @param NewApplicantEvent $newApplicantEvent
     * @throws RepositoryException
     */
    public function handle(NewApplicantEvent $newApplicantEvent)
    {
        $applicantEvent =  $newApplicantEvent->employee->getAttributes();
        $this->onNewEmployeeCreated($applicantEvent);
    }
}
