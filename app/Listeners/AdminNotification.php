<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Repositories\User\UserRepository;
use App\Criterias\User\SearchByRole;
use App\Services\NotificationService;

use App\Events\ClientOnboarded;
use App\Events\ClientSelectedResources;
use App\Events\NewEmployee;

class AdminNotification //implements ShouldQueue
{
    /**
     * The id for the superadmin role
     */
    private const SUPER_ADMIN_ROLE = 1;

    /**
     * The user model instance
     * 
     * @var \App\Repositories\User\UserRepository
     */
    protected $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;

        $this->user->pushCriteria(new SearchByRole(self::SUPER_ADMIN_ROLE));
    }

    /**
     * Handle client onboard event
     * 
     * @param \App\Events\ClientOnboarded $event
     * @return void
     */
    public function onClientOnboard($event)
    {
        $usersToNotify = $this->user->get();
        $type = get_class($event);
        $data = [
            'id'           => $event->client->id,
            'company'      => $event->client->company,
            'redirect_uri' => 'review-client-prospects',
        ];
        
        NotificationService::send($usersToNotify, $data, $type);
    }

    /**
     * Handle client has select preferred resource event
     * 
     * @param \App\Events\ClientSelectedResources $event
     * @return void
     */
    public function onSelectPreferredResource($event)
    {
        $usersToNotify = $this->user->get();
        $type = get_class($event);
        $data = [
            'id'      => $event->client->id,
            'company' => $event->client->company,
        ];
        
        NotificationService::send($usersToNotify, $data, $type);
    }

    /**
     * Handle employee created event
     * 
     * @param \App\Events\NewEmployee $event
     * @return void
     */
    public function onEmployeeCreated($event)
    {
        $usersToNotify = $this->user->get();
        $type = get_class($event);
        $data = [
            'id'       => $event->employee->id,
            'employee' => $event->employee->getName(),
        ];
        
        NotificationService::send($usersToNotify, $data, $type);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $thisClass = self::class;

        $events->listen(ClientOnboarded::class, 
            $thisClass.'@onClientOnboard');

        $events->listen(ClientSelectedResources::class, 
            $thisClass.'@onSelectPreferredResource');

        $events->listen(NewEmployee::class, 
            $thisClass.'@onEmployeeCreated');
    }
}
