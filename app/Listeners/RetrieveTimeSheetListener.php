<?php

namespace App\Listeners;

use App\Events\RetrieveTimeSheetEvent;
use App\Services\ExecService;

/**
 * Class RetrieveTimeSheetListener
 * @package App\Listeners
 */
class RetrieveTimeSheetListener
{
    /**
     * RetrieveTimeSheetListener constructor.
     */
    public function __construct() { }

    /**
     * Handle the event.
     *
     * @param  RetrieveTimeSheetEvent|object  $event
     * @return void
     */
    public function handle(RetrieveTimeSheetEvent $event)
    {
        // nohup prevents the command from being aborted automatically when you logout or exit the shell.
        new ExecService('cd ' . base_path() . ' && nohup php artisan timesheet:get -m ' . $event->getStartDate() . ' ' . $event->getEndDate());
    }
}
