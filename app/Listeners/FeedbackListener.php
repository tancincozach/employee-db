<?php

namespace App\Listeners;

use App\Criterias\Employee\SearchByUser;
use App\Criterias\FeedbackCategory\SearchById;
use App\Criterias\User\SearchByEmail;
use App\Criterias\User\SearchByRole;
use App\Events\FeedbackEvent;
use App\Mail\FeedbackMail;
use App\Repositories\Employee\EmployeeRepository;
use App\Repositories\FeedbackCategory\FeedbackCategoryRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Support\Facades\Mail;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class FeedbackListener
 * @package App\Listeners
 */
class FeedbackListener
{
    /**
     * The id for the Super Admin Role
     */
    private const SUPER_ADMIN_ROLE = 1;

    protected $employeeRepository;

    protected $feedbackCategoryRepository;

    protected $userRepository;

    /**
     * FeedbackListener constructor.
     * @param EmployeeRepository $employeeRepository
     * @param FeedbackCategoryRepository $feedbackCategoryRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        EmployeeRepository $employeeRepository,
        FeedbackCategoryRepository $feedbackCategoryRepository,
        UserRepository $userRepository
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->feedbackCategoryRepository = $feedbackCategoryRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param FeedbackEvent $feedbackEvent
     * @throws RepositoryException
     */
    public function handle(FeedbackEvent $feedbackEvent)
    {
        $feedback = $feedbackEvent->feedback->getAttributes();

        $employee = [];
        if (isset($feedback['user_id'])) {
            $this->employeeRepository->pushCriteria(new SearchByUser($feedback['user_id']));
            $employee = $this->employeeRepository->with(['user', 'positions'])->first()->toArray();
        }

        $this->feedbackCategoryRepository->pushCriteria(new SearchById([$feedback['category_id']]));
        $feedbackCategory = $this->feedbackCategoryRepository->first()->toArray();

        $this->userRepository->pushCriteria(new SearchByRole(self::SUPER_ADMIN_ROLE, false));
        $users = $this->userRepository->get()->toArray();

        $data = [
            'employee' => $employee,
            'feedbackCategory' => $feedbackCategory,
            'feedback' => $feedback,
        ];

        if ($users && $feedback) {
            foreach ($users as $user) {
                Mail::to($user['email'])->send(new FeedbackMail($data));
                if(env('MAIL_HOST', false) == 'smtp.mailtrap.io'){
                    sleep(3);
                }
            }
        }
    }
}
