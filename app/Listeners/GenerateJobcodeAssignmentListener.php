<?php

namespace App\Listeners;

use App\Events\GenerateJobcodeAssignmentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Services\ExecService;

class GenerateJobcodeAssignmentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GenerateJobcodeAssignmentEvent  $event
     * @return void
     */
    public function handle(GenerateJobcodeAssignmentEvent $event)
    {
        new ExecService($event->command);
    }
}
