<?php

namespace App\Listeners;

use App\Events\GenerateTimesheetEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Services\ExecService;

class GenerateTimesheetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GenerateTimesheetEvent  $event
     * @return void
     */
    public function handle(GenerateTimesheetEvent $event)
    {
        new ExecService($event->command);
    }
}
