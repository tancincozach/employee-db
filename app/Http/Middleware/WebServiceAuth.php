<?php

namespace App\Http\Middleware;

use App\Helpers\StatusCodeHelper;
use App\Models\ApiRequest;
use App\Models\ApiUser;
use Closure;

/**
 * Class WebServiceAuth
 *
 * @package App\Http\Middleware
 * @author Carmencito Berdon <cberdon@fullscale.io>
 */
class WebServiceAuth
{
    /**
     * API key
     * @var null
     */
    protected $apiKey = null;

    /**
     * API Secret
     * @var null
     */
    protected $apiSecret = null;

    /**
     * API Signature
     * @var null
     */
    protected $apiSignature = null;

    /**
     * Turns on/off debugging
     * @var bool
     */
    protected $debug = true;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->validateRequest()) {
            $response = response()->json([
                'data'        => [],
                'message'     => StatusCodeHelper::statusCode(401, true),
                'status_code' => 401,
            ]);

            // log request
            (new ApiRequest())->logRequest([
                'response' => $response,
            ]);

            return $response;
        }

        return $next($request);
    }

    /**
     * Validates API request based on provided details from header
     *
     * @return bool
     */
    protected function validateRequest() : bool
    {
        // check if required headers are provided for authentication
        if (empty(request()->header('x-fsrocks-date')) || empty(request()->header('authorization'))) {
            return false;
        }

        // get and clean up authorization header
        $authorization = trim(request()->header('authorization'));
        $authorization = preg_replace('/\s+/', ' ', $authorization);
        $authorization = str_ireplace('Basic', 'BASIC', $authorization);

        // the custom timestamp
        $fsDate = request()->header('x-fsrocks-date');

        // check if timestamp is in valid format
        if (! $this->isTimestampIsoValid($fsDate)) {
            true === $this->debug and \Log::info("Invalid timestamp '$fsDate'");
            return false;
        }

        // compute if request time is within the valid period
        $currentTimestamp = strtotime(gmdate('o-m-d\TH:i:s\Z'));
        $timestampValidityPeriod = 900;// 15 minutes validity period
        $fsDateTimestamp = strtotime($fsDate);

        // add current server time threshold for servers with unsynced time
        $currentTimestamp += 300; // 5 minutes

        // check if timestamp is within the valid time frame of access
        if ($fsDateTimestamp < ($currentTimestamp - $timestampValidityPeriod) || $fsDateTimestamp > $currentTimestamp) {
            true === $this->debug and \Log::info("Timestamp expired '$fsDate'");
            return false;
        }

        // retrieve access_key from authorization header
        list($authMode, $auth) = explode(' ', $authorization);
        list($this->apiKey, $this->apiSignature) = explode(':', base64_decode($auth));

        // get key details if available
        if (empty($keyDetails = (new ApiUser())->getKeyDetails($this->apiKey))) {
            true === $this->debug and \Log::info("Api key invalid '{$this->apiKey}'");
            return false;
        }

        // make sure we are getting a valid api secret string
        if (empty($this->apiSecret = $keyDetails->secret)) {
            true === $this->debug and \Log::info("Api secret invalid '{$this->apiSecret}'");
            return false;
        }

        // make this api key id globally available in the request
        request()->session()->put('api_user_id', $keyDetails->id);

        // check if authorization is valid based on rules set
        if ($authorization != $this->authentication(request()->method(), request()->decodedPath(), $fsDate)) {
            true === $this->debug and \Log::info("Validating authentication error '$authorization'");
            return false;
        }

        return true;
    }

    /**
     * Checks if time format is ISO 8601 valid
     *
     * @param $timestamp
     *
     * @return bool
     */
    protected function isTimestampIsoValid($timestamp) : bool
    {
        if (preg_match('/^' .
                '(\d{4})-(\d{2})-(\d{2})T' . // YYYY-MM-DDT ex: 2014-01-01T
                '(\d{2}):(\d{2}):(\d{2})' . // HH-MM-SS  ex: 17:00:00
                '(Z|((-|\+)\d{2}:\d{2}))' . // Z or +01:00 or -01:00
                '$/', $timestamp, $parts) == true) {
            try {
                new \DateTime($timestamp);
                return true;
            } catch (\Throwable $e) {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get the valid authentication based on request method and uri plus gmdate
     *
     * @param $requestMethod
     * @param $requestURI
     * @param $fsDate
     *
     * @return bool|string
     */
    protected function authentication($requestMethod, $requestURI, $fsDate)
    {
        // required parameters
        if (empty($this->apiKey) || empty($this->apiSignature)) {
            return false;
        }

        // correct laravel's "/" trimming
        $requestURI = '/' . ltrim($requestURI, '/');

        $messageToSign = utf8_encode("$requestMethod\n$fsDate\n$requestURI");
        $signature = sha1($messageToSign . $this->apiSecret);
        $auth = "BASIC " . base64_encode("{$this->apiKey}:$signature");

        return $auth;
    }
}
