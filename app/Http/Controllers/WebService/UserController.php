<?php

namespace App\Http\Controllers\WebService;

use App\Helpers\AuthHelper;
use App\Helpers\StatusCodeHelper;
use App\Models\ApiRequest;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    protected $apiRequest = null;

    function __construct(ApiRequest $apiRequest)
    {
        $this->apiRequest = $apiRequest;
    }

    public function validateToken(Request $request)
    {
        // get token to validate
        $token = $request->input('token', '');
        // default status code
        $errorCode = 200;
        // error code group
        $http = true;

        if (!empty($token)) {
            $tokenInfo = AuthHelper::getTokenUserInfo($token);

            // empty?
            empty($tokenInfo) and ($errorCode = 9001 and $http = false);

            $response = response()->json([
                'data'        => $tokenInfo,
                'message'     => StatusCodeHelper::statusCode($errorCode, $http),
                'status_code' => $errorCode,
            ]);

            // log to the database this request
            $this->apiRequest->logRequest([
                'response'    => $response,
            ]);

            return $response;
        }

        return response()->json([]);
    }
}
