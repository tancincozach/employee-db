<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\APIBaseController as BaseController;

use App\Repositories\Employee\EmployeeReportRepository;
use App\Validators\EmployeeReportValidator;
use App\Transformers\EmployeeReportTransformer;

use App\Criterias\EmployeeReport\SearchByEmployee;
use App\Criterias\EmployeeReport\SearchByEmployeeIds;
use App\Criterias\EmployeeReport\FilterByDate;
use Dingo\Api\Http\Request;
use App\Traits\ExportTrait;

class DailyReportController extends BaseController
{
    use ExportTrait;

    public function __construct(EmployeeReportRepository $repository, EmployeeReportValidator $validator, EmployeeReportTransformer $transformer)
    {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->validator = $validator;
    }

    public function summary(Request $request)
    {
        if ($request->get('employee_ids')) {
            $ids = explode(',', $request->get('employee_ids'));
            $this->repository->pushCriteria(new SearchByEmployeeIds($ids));
        }

        if ($request->get('start_date') && $request->get('end_date')) {
            $this->repository->pushCriteria(new FilterByDate($request->get('start_date'), $request->get('end_date')));
        }

        $results = $this->repository->get();

        $res = array(['Fullname', 'Id', 'Employee Id', 'Subject', 'Report Date']);

        foreach($results as $index => $value) {
          array_push($res, [
              $value['employee']->getName(),
              $value['employee']->id,
              $value['id'],
              $value['subject'],
              $value['report_date']
          ]);
        }

        $this->export($res, $request->get('filename'), 'csv');
    }

    private static function removeHtmlTags($str) {
        $str = preg_replace('#<[^>]+>#', ' ', $str);
        $str = preg_replace('/\n+|\t+|\s+|&nbsp;|&amp;/',' ', $str);
        return $str;
    }
}
