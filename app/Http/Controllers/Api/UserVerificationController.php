<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserVerification;
use Illuminate\Http\Request;

class UserVerificationController extends Controller
{
    public function __construct()
    {
        
    }

    /** $code is for the access code, and $i is for the user_id which is stored in the user_verifications table
     *  and $h is for the hash based on the $code and $i to ensure the url is unchanged
     */
    public function handleRequest($code, $i, $h)
    {
        if (strcasecmp($h,$this->generateHash($code,$i)) == 0) {
            if (UserVerification::resendVerification($code, $i)->count() == 1) {
                return view('user.password', [ 'isSuccess' => false, 'code' => $code, 'i' => $i, 'h' => $h ]);
            } else {
                return redirect('error');
            }
        } else {
            return redirect('error');
        }
    }

    /**
     * 
     */
    public function generateHash($code, $i)
    {
        return hash('sha256', $code . $i);
    }

    public function changePassword(Request $request)
    {
        //if hash is the same proceed to checking of user_id and access code in the db
        if (strcasecmp($request->h,$this->generateHash($request->code,$request->i)) == 0) {

            //if the user_id and access code is in the db proceed to update the verified_at column
            if (UserVerification::resendVerification($request->code, $request->i)->count() == 1) {
                UserVerification::resendVerification($request->code, $request->i)->update(['verified_at' => date("Y-m-d H:i:s")]);

                //Update the user password
                User::where('id','=', $request->i)->update(['password' => User::getPasswordHashValue($request->password), 'updated_by_user_id' => $request->i, 'is_verified' => 1]);
                return view('user.password', [ 'isSuccess' => true ]);
            } else {
                return redirect('error');
            }
        } else {
            return redirect('error');
        }
    }
}
