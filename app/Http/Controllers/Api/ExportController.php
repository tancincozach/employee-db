<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

use App\Services\TimesheetExportService;
use App\Services\WorkFromHomeExportService;

class ExportController extends BaseController
{
    /*
     * Export files
     *
     * @param Request $request
     *
     * @return void
     */
    public function export(Request $request)
    {
        if (filter_var($request->get('timesheet'), FILTER_VALIDATE_BOOLEAN))
            return TimesheetExportService::export();

        if (filter_var($request->get('wfrmonthly'), FILTER_VALIDATE_BOOLEAN))
            return WorkFromHomeExportService::export($this->getParsedDates($request), $request->get('employee_ids'));
    }

    private function getParsedDates(Request $request)
    {
        $current = time();
        $dt = new \DateTime();

        if (strcmp($request->get('report_date_filter_option'), 'monthly') == 0 ){
            if ($request->get('year')) {
                $dt->setDate($request->get('year'), date('m', $current), 1);
                $current = $dt->getTimestamp();
            }

            if ($request->get('month')) {
                $dt->setDate(date('Y', $current), $request->get('month'), 1);
                $current = $dt->getTimestamp();
                return [date('Y-m-01', $current), date('Y-m-t', $dt->modify('last day of this month')->getTimestamp())];
            }
        }else{
            if ($request->get('start_date') && $request->get('end_date')) {
                return [$request->get('start_date'), $request->get('end_date')];
            }
        }
        return [date('Y-m-01', $current), date('Y-m-t', $current)];
    }
}
