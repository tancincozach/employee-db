<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\Faq\FaqRepository;
use App\Transformers\FaqTransformer;
use App\Validators\FaqValidator;
use Dingo\Api\Http\Request;
use Prettus\Repository\Exceptions\RepositoryException;
use App\Criterias\Faq\SearchQA;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\Exceptions\ValidatorException;
/**
 * Class FaqController
 * @package App\Http\Controllers\Api
 */
class FaqController extends BaseController
{
    public function __construct(
        FaqRepository $repository,
        FaqValidator $validator,
        FaqTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request)
    {
        if($request->get('search'))
            $this->repository->pushCriteria(new SearchQA($request->get('search')));

        $limit = $request->get('take') ? $request->get('take') : config('repository.pagination.limit', 9999);

        if ($request->get('orderBy')) {
            if (is_array($request->get('orderBy'))) {
                foreach ($request->get('orderBy') as $sorting) {
                    $orderBy = $this->getOrderBy($sorting);

                    $this->setOrderBy($orderBy);
                }
            } else {
                $orderBy = $this->getOrderBy($request->get('orderBy'));

                $this->setOrderBy($orderBy);
            }
        }

        if ($request->get('disableDefaultIncludes')) {
            $this->transformer->setDefaultIncludes([]);
        }

        $result = $this->repository->paginate($limit);

        return $this->response
                    ->paginator($result, $this->transformer)
                    ->withHeader('Content-Range', $result->total());
    }
}
