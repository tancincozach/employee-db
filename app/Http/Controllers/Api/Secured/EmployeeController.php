<?php

namespace App\Http\Controllers\Api\Secured;

use App\Criterias\Common\ForceEmpty;
use App\Criterias\Employee\CebuEmployeeOnly;
use App\Criterias\Employee\EmployeeOnly;
use App\Criterias\Employee\FilterByAvailableResource;
use App\Criterias\Employee\FilterByClientTeambuilderAvailable;
use App\Criterias\Employee\FilterByNotAssignedToProject;
use App\Criterias\Employee\WithClientTeambuilderBucket;
use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Models\AllQuestionResponse;
use App\Models\Category;
use App\Models\Employee;
use App\Models\JobPosition;
use App\Models\Status;
use App\Repositories\Employee\EmployeeRepository;
use App\Services\CategorySkillHelper;
use App\Validators\EmployeeValidator;
use App\Transformers\EmployeeTransformer;
use App\Transformers\EmployeeNamesTransformer;
use App\Transformers\AvailableEmployeesTransformer;
use Dingo\Api\Http\Request;
use App\Criterias\Common\WithTrashed;
use App\Criterias\Employee\SearchByEmployeeNo;
use App\Criterias\Employee\SearchByNameOrFullName;
use App\Criterias\Employee\SearchBySkill;
use App\Criterias\Employee\WithEmployeeSkill;
use App\Criterias\Employee\SearchNotAssigned;
use App\Criterias\Employee\WithPosition;
use App\Criterias\Employee\FilterByJobTitle;
use App\Criterias\Employee\FilterByStatus;
use App\Criterias\Employee\FilterByProject;
use App\Criterias\Employee\FilterByLocation;
use App\Criterias\Employee\FilterByEmployeeNumber;
use App\Criterias\Employee\FilterByUnassigned;
use App\Criterias\Employee\FilterByName;
use App\Criterias\Employee\FilterByEmployeeStatus;
use App\Criterias\Employee\FilterByDepartment;
use App\Criterias\Employee\FilterByClient;
use App\Criterias\Employee\WithStatus;
use App\Criterias\Employee\WithDepartment;
use App\Criterias\Employee\Select;
use App\Criterias\Employee\SearchByUser;
use App\Criterias\Employee\ApplicantOnly;
use App\Criterias\Employee\EmployeeNoFilter;
use App\Criterias\Employee\FilterByUserId;
use App\Criterias\Employee\WithClientProject;
use App\Criterias\Employee\WithLocation;
use App\Criterias\Employee\SearchAll;
use App\Criterias\Employee\SearchAdvanced;
use App\Criterias\Employee\SearchBySkillAndUnassigned;
use App\Criterias\Employee\SearchByUnassignedAndEmployeeDB;
use App\Criterias\Employee\SearchByClientAndUnpreferred;
use App\Criterias\Employee\WithClientPreferredTeam;
use App\Criterias\Employee\SearchByJobTitles;
use App\Criterias\Employee\SearchByDeveloper;
use App\Criterias\Employee\FilterByUnassignedTeamBuilder;
use App\Criterias\Employee\FilterByClientId;
use App\Criterias\Employee\SearchByEmployeeTopThreeSkills;
use App\Criterias\Employee\WithEmployeeTopSkill;
use App\Criterias\Employee\FilterByAvailableEmployees;
use App\Criterias\Employee\FindAvailableResource;
use App\Criterias\Employee\FilterUnassignedEmployees;
use App\Criterias\Employee\OrderByProficiency;
use App\Criterias\Employee\OrderByField;
use App\Criterias\Employee\SelectFields;
use App\Events\NewEmployee;
use App\Events\NewApplicantEvent;
use App\Criterias\JobPosition\GetJobPositionByType;
use App\Models\ClientTeambuilderBucket;
use App\Models\EmployeeOtherDetail;
use App\Criterias\Employee\SearchByExactSkill;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends BaseController
{
    public function __construct(EmployeeRepository $repository, EmployeeValidator $validator, EmployeeTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function show($id)
    {
        // otherwise, authorize request
        $modelObj = app($this->repository->model());
        $this->authorize('view', $modelObj);

        if (request('disableDefaultIncludes')) {
            $this->transformer->setDefaultIncludes([]);
        }

        try {
            $result = $this->repository->find($id);
            if(!Auth::user()->can('view', $result->otherDetails->all())) {
              $result->otherDetails = [];
            }
            return $this->response->item($result, $this->transformer);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status_code' => 404,
                'message' => 'Can\'t find the requested resource.'
            ], 404);
        }
    }

    /**
     * @param Request $request
     * @return Resource
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        // filter by applicants/employees only
        if ($request->get('applicants')) {
            // add SearchByName criteria
            $this->repository->pushCriteria(new ApplicantOnly($request->get('applicants')));
        } else if ($request->get('resources')) {
            $this->repository->pushCriteria(new SearchNotAssigned($request->get('resources')));
        } else if ($request->get('cebuEmployeeOnly')) {
            $this->repository->pushCriteria(new CebuEmployeeOnly());
            $filter = !empty($request->get('filters')) ? explode('|', $request->get('filters')) : '';
            if (empty($request->get('search')) && empty($filter[0])) {
                $this->repository->pushCriteria(new FilterByStatus([16, 18, 19])); //probationary & regular
            }
        } else {
            $this->repository->pushCriteria(new EmployeeOnly($request->get('take')));
            $filter = !empty($request->get('filters')) ? explode('|', $request->get('filters')) : '';
            if (empty($request->get('search')) && empty($filter[0])) {
                $this->repository->pushCriteria(new FilterByStatus([16, 18, 19])); //probationary & regular
            }
        }

        if ($request->get('skills')) {
            if ($request->has('categories')) {
                $skills = CategorySkillHelper::getSkillNameByCategory($request->get('categories'));
            } else {
                $skills = $request->get('skills');

                if (count($skills) == 1) {
                    $this->repository->pushCriteria(new SearchByExactSkill(Arr::first($skills)));
                }
            }

            if($request->has('clientTeam')) {
                $this->transformer->setClientTeambuilderId($request->get('clientTeam'));
                $this->transformer->setClientTeamBuilderFetchOnly(['id','status', 'client_id', 'client_project_id', 'job_position_id']);
                $this->repository->pushCriteria(new WithClientTeambuilderBucket());

                if ($request->has('categories') && CategorSkillHelper::isUnassignedCategoryResourceType($request->get('categories'))) {
                    $this->repository->pushCriteria(new SearchBySkillAndUnassigned($skills));
                    $this->repository->pushCriteria(new FilterByUnassignedTeamBuilder($request->get('clientTeam')));
                }
            }

        } else if ($request->get('unassigned')) {
            $this->repository->pushCriteria(new SearchByUnassignedAndEmployeeDB($request->get('unassigned')));
        }

        // TODO: use teamBuilderFilter();
        if($request->get('developerSkills') && $request->get('clientTeam')){
            $developerSkills = explode(',', $request->get('developerSkills'));
            $this->repository->pushCriteria(new WithClientPreferredTeam());
            $this->repository->pushCriteria(new SearchByDeveloper());
            $this->repository->pushCriteria(new SearchBySkillAndUnassigned($developerSkills));
            $this->repository->pushCriteria(new SearchByClientAndUnpreferred($request->get('clientTeam')));
        }

        if($request->get('positions')){
            $positions = explode(',', $request->get('positions'));
            $this->repository->pushCriteria(new SearchByJobTitles($positions));
        }

        // TODO: remove advance search
        // see if we have searches
        if ($request->get('search')) {
            /*
            switch ($request->get('searchBy')) {
                case 'skill':
                    $this->repository->pushCriteria(new WithEmployeeSkill());
                    $this->repository->pushCriteria(new SearchBySkill($request->get('search')));
                    break;
                case 'empID':
                    $this->repository->pushCriteria(new SearchByEmployeeNo($request->get('search')));
                    break;
                default:
                    $this->repository->pushCriteria(new SearchByNameOrFullName($request->get('search')));
                    break;
            }*/

            //Splits the formatted search into an array for the name, project, skill and location
            $advSearchArr = preg_split('/<name>:|<project>:|<skill>:|<location>:|<client>:/', $request->get('search'));
            array_shift($advSearchArr);
            //If there is a format '<name>:<project>:<skill>:<location>:' in the search, then use the advance search
            //else use the general search
            if (count($advSearchArr) == 5) {
                //To separate and get the different values selected in the advanced search
                $nameArr = explode('$|$', str_replace([' ', ','], '', $advSearchArr[0]));
                $projectArr = explode('$|$', $advSearchArr[1]);
                $skillArr = explode('$|$', $advSearchArr[2]);
                $locationArr = explode('$|$', str_replace([' ', ','], '', $advSearchArr[3]));
                $clientArr = explode('$|$', $advSearchArr[4]);

                $this->repository->pushCriteria(new SearchAdvanced($nameArr, $projectArr, $skillArr, $locationArr, $clientArr));
            } else {
                $this->repository->pushCriteria(new SearchAll($request->get('search')));
            }
        }

        // see if we should be including those deleted employees
        if ($request->get('withTrashed')) {
            // include deleted employees
            $this->repository->pushCriteria(new WithTrashed());
        }

        // filter by users id
        if ($request->get('user_id')) {
            $this->repository->pushCriteria(new SearchByUser($request->get('user_id')));
        }

        // filter by job title
        if ($request->get('filters')) {
            $filter = explode('|', $request->get('filters'));

            switch ($filter[0]) {
                case 'applicant_list':
                    if ($filter[1] !== '0') {
                        $this->repository->pushCriteria(new FilterByJobTitle($filter[1]));
                    }
                    if ($filter[2] !== '0') {
                        $this->repository->pushCriteria(new FilterByStatus([$filter[2]]));
                    }
                    break;
                case 'employee_list':
                    if (!empty($filter[1])) {
                        $this->repository->pushCriteria(new FilterByEmployeeNumber($filter[1]));
                    }

                    if (!empty($filter[2])) {
                        $names = explode(", ", $filter[2]);
                        $name = explode(" ", $names[1]);
                        if (sizeof($name) > 1)
                            array_pop($name);
                        $firstName = implode(" ", $name);
                        $this->repository->pushCriteria(new FilterByName(trim($firstName), trim($names[0])));
                    }

                    if (!empty($filter[3])) {
                        if ($filter[3] !== 'Unassigned') {
                            $this->repository->pushCriteria(new FilterByProject($filter[3]));
                        } else {
                            $this->repository->pushCriteria(new FilterByUnassigned($filter[3]));
                        }
                    }

                    if (!empty($filter[4])) {
                        $location = explode(", ", $filter[4]);
                        $this->repository->pushCriteria(new FilterByLocation($location[0], $location[1]));
                    }

                    if (!empty($filter[5])) {
                        $this->repository->pushCriteria(new FilterByStatus([$filter[5]]));
                    }

                    if (!empty($filter[6])) {
                        $this->repository->pushCriteria(new FilterByClient($filter[6]));
                    }

                    // if ($filter[1] !== '0') {
                    //     $this->repository->pushCriteria(new FilterByJobTitle($filter[1]));
                    // }
                    // if ($filter[2] !== '0') {
                    //     $this->repository->pushCriteria(new FilterByDepartment($filter[2]));
                    // }
                    // if ($filter[3] !== '0') {
                    //     $empStatusFilter = ($filter[3] == '1')
                    //         ? $this->repository->pushCriteria(new FilterByEmployeeStatus('Assigned'))
                    //         : $this->repository->pushCriteria(new FilterByEmployeeStatus('Available'));
                    // }
                    break;
            }
        }


        // checks availability of employee no.
        if ($request->get('employeeNo')) {
            $this->repository->pushCriteria(new WithTrashed());
            $this->repository->pushCriteria(new EmployeeNoFilter($request->get('employeeNo')));
        }

        if ($request->get('user_id')) {
            $this->repository->pushCriteria(new FilterByUserId($request->get('user_id')));
        }

        if ($request->get('clientId')){
            $this->repository->pushCriteria(new FilterByClientId($request->get('clientId')));
        }

        // TODO: replace this to defaultCriteria()
        // default criteria
        $this->repository->pushCriteria(new WithDepartment());
        $this->repository->pushCriteria(new WithPosition());
        $this->repository->pushCriteria(new WithStatus());
        $this->repository->pushCriteria(new WithLocation());
        $this->repository->pushCriteria(new WithClientProject());
        $this->repository->pushCriteria(new WithEmployeeSkill());
        $this->repository->pushCriteria(new Select());

        return parent::index($request);
    }

    /**
     * Use for Advance Search
     *
     * @param Request $request
     * @return Collection;
     */
    public function advanceSearch(Request $request)
    {
        //Splits the formatted search into an array for the name, project, skill and location
        $advSearchArr = preg_split('/<name>:|<project>:|<skill>:|<location>:|<client>:/', $request->get('search'));

        // remove first empty element in array
        array_shift($advSearchArr);

        //To separate and get the different values selected in the advanced search
        $nameArr = explode('$|$', str_replace([' ', ','], '', $advSearchArr[Employee::ADV_SEARCH_NAME]));
        $projectArr = explode('$|$', $advSearchArr[Employee::ADV_SEARCH_PROJECT]);
        $skillArr = explode('$|$', $advSearchArr[Employee::ADV_SEARCH_SKILL]);
        $locationArr = explode('$|$', str_replace([' ', ','], '', $advSearchArr[Employee::ADV_SEARCH_LOCATION]));
        $clientArr = explode('$|$', $advSearchArr[Employee::ADV_SEARCH_CLIENT]);

        $this->repository->pushCriteria(new SearchAdvanced($nameArr, $projectArr, $skillArr, $locationArr, $clientArr));

        // load default criteria
        $this->defaultCriteria();

        return parent::index($request);
    }

    public function getAvailableEmployees(Request $request) {
        $resources = array();
        $skills = array();
        $selectedSkills = array();

        if($request->has('resourceTypes')) {
            $resources = explode(',', $request->get('resourceTypes'));
        }
        if($resources == [40,43]) {
           $getTesterCategories = Category::where('resource_type', 'Testers')->pluck('id');

           $usedOr = false;
           $failed = false;

           if (count($getTesterCategories) > 0) {
                foreach ($getTesterCategories as $category) {
                    $categories = CategorySkillHelper::getAllCategory([$category]);
                    $categorySkills = CategorySkillHelper::getAllSkillByCategory($categories);
                    $selectedSkills = array_intersect($selectedSkills, $categorySkills);
                }
            $skills = CategorySkillHelper::getAllSkillByCategory($getTesterCategories, $selectedSkills);
            $usedOr = true;
           } else {
                $failed = true;
           }

           if (! $failed) {
                $this->repository->pushCriteria(new FilterByAvailableResource($skills, $usedOr));
                $this->repository->pushCriteria(new FilterByAvailableEmployees());
            } else {
                $this->repository->pushCriteria(new ForceEmpty());
            }
        } else {
            $this->defaultFilter($request);

            $this->teamBuilderFilter($request);
            // load default criteria
            $this->defaultCriteria();
        }
        return parent::index($request);
    }

    /*
     * For lead generation specific available employees
     *
     * @param Request $request
     *
     * @return Resource
     */
    public function leadAvailableEmployees(Request $request)
    {
        $newRequest = $request;

        // type of resource ids
        $pm = '6,';
        $testers = '40,43,';
        $content = '33,34,35,';
        $creative = '30,31,49,55';
        $resourceTypes = $pm.$testers.$content.$creative;

        $params = [
            'clientTeam'    => [0],
            'include'       => 'skills,positions',
            'resourceTypes' => $resourceTypes,
        ];

        $newRequest->request->add($params);
        $newRequest->query->add($params);

        $this->transformer = app(AvailableEmployeesTransformer::class);
        $this->transformer->setDefaultIncludes([]);

        return $this->getAvailableEmployees($request);
    }

    /**
     * @param Request $request
     * @return Resource
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function availableEmployees(Request $request)
    {
        $this->defaultFilter($request);


        if ($request->has('build')) {
            $developerSkills = [];
            $isUnassignFilter = false;

            $this->transformer->setClientTeambuilderId($request->get('clientTeam'));
            // get only the status and client_id of clientbuilder
            $this->transformer->setClientTeamBuilderFetchOnly(['id','status', 'client_id', 'client_project_id', 'job_position_id']);

            // Find Available Resource
            if ($request->has('availables')) {
                $developerSkills = ($request->has('skills')) ? $request->get('skills')
                                : CategorySkillHelper::getSkillNameByCategory($request->get('categories'));

                $isUnassignFilter = CategorySkillHelper::inUnassignedSkillsCategory($developerSkills);
            } else if ($request->has('resourceTypes')) { // Smart Team Builder
                $this->repository->pushCriteria(new GetJobPositionByType($request->get('resourceTypes')));

                if ($isUnassignFilter = in_array($request->get('resourceTypes'), ClientTeambuilderBucket::FILTER_UNASSIGN_TEAMBUILDER)) {
                    $categorySkills = CategorySkillHelper::getSkillsByClient($request->get('clientTeam'), $request->get('additionalSkills'));

                    foreach ($categorySkills as $skill) {
                        if (!in_array($skill, $developerSkills)) {
                            $developerSkills[] = $skill;
                        }
                    }
                }
            }

            $this->repository->pushCriteria(new FindAvailableResource($developerSkills));

            if ($isUnassignFilter) {
                $this->repository->pushCriteria(new FilterUnassignedEmployees($request->get('clientTeam')));
            }
        } else {
            $this->teamBuilderFilter($request);
            // load default criteria
            $this->defaultCriteria();
        }

        return parent::index($request);
    }

    /**
     * @param Request $request
     * @return Resource
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function availableResource(Request $request)
    {
        $skills = $selectedSkills = $request->get('skills', []);
        $selectedCategories = $request->get('categories', []);

        $usedOr = false;
        $failed = false;
        if (count($selectedCategories) && count($selectedSkills)) {
            foreach ($selectedCategories as $category) {
                $categories = CategorySkillHelper::getAllCategory([$category]);
                $categorySkills = CategorySkillHelper::getAllSkillByCategory($categories);
                $result = array_intersect($selectedSkills, $categorySkills);
                if (count($result) != count($selectedSkills)) {
                    $failed = true;
                    break;
                }
            }
        } elseif (count($selectedCategories) && count($selectedSkills) == 0) {
            $skills = CategorySkillHelper::getAllSkillByCategory($selectedCategories, $selectedSkills);
            $usedOr = true;
        }

        if (! $failed) {
            $this->repository->pushCriteria(new FilterByAvailableResource($skills, $usedOr));
            $this->repository->pushCriteria(new FilterByAvailableEmployees());
        } else {
            $this->repository->pushCriteria(new ForceEmpty());
        }

        return parent::index($request);
    }

    /**
     * Default criteria for employee
     *
     * @return void;
     */
    private function defaultCriteria()
    {
        $this->repository->pushCriteria(new WithDepartment());
        $this->repository->pushCriteria(new WithPosition());
        $this->repository->pushCriteria(new WithStatus());
        $this->repository->pushCriteria(new WithLocation());
        $this->repository->pushCriteria(new WithClientProject());
        $this->repository->pushCriteria(new WithEmployeeSkill());
        $this->repository->pushCriteria(new Select());
    }

    /**
     * Default employees filter
     *
     * @return void;
     */
    private function defaultFilter(Request $request)
    {
        // filter by applicants/employees only
        if ($request->get('applicants')) {
            // add SearchByName criteria
            $this->repository->pushCriteria(new ApplicantOnly($request->get('applicants')));
        } else if ($request->get('resources')) {
            $this->repository->pushCriteria(new SearchNotAssigned($request->get('resources')));
        } else {
            $this->repository->pushCriteria(new EmployeeOnly($request->get('take')));
            $filter = !empty($request->get('filters')) ? explode('|', $request->get('filters')) : '';
            if (empty($request->get('search')) && empty($filter[0])) {
                $this->repository->pushCriteria(new FilterByStatus([16, 18, 19])); //probationary & regular
            }
        }
    }

    #region OLD Team Builder API
    /**
     * Default filter for team builder
     *
     * @return void;
     */
    private function teamBuilderFilter(Request $request)
    {
        $developerSkills = [];
        if ($request->get('clientTeam')) {
            // set client_id in client_teambuilder_bucket, to get only specific client
            $this->transformer->setClientTeambuilderId($request->get('clientTeam'));
            // get only the status and client_id of clientbuilder
            $this->transformer->setClientTeamBuilderFetchOnly(['id','status', 'client_id', 'client_project_id', 'job_position_id']);
            // filter by client teambuilder if status is `declined` or employee doesn't exist in client teambuilder.
            $this->repository->pushCriteria(new WithClientTeambuilderBucket());

            if (!$request->has('resourceTypes')) {
                // get all skills by category by developers only
                $categorySkills = CategorySkillHelper::getSkillsByClient($request->get('clientTeam'), $request->get('additionalSkills'));
                foreach ($categorySkills as $skill) {
                    if (!in_array($skill, $developerSkills)) {
                        $developerSkills[] = $skill;
                    }
                }
                $this->repository->pushCriteria(new SearchByDeveloper());
                $this->repository->pushCriteria(new SearchBySkillAndUnassigned($developerSkills));
                $this->repository->pushCriteria(new FilterByUnassignedTeamBuilder($request->get('clientTeam')));
            } else if($request->has('resourceTypes')) {
                $resources = explode(',', $request->get('resourceTypes'));
                $this->repository->pushCriteria(new SearchByJobTitles($resources));

                if($resources == [40,43]) {
                    $this->repository->pushCriteria(new SearchBySkillAndUnassigned($resources));
                    $this->repository->pushCriteria(new FilterByUnassignedTeamBuilder($request->get('clientTeam')));
                }
            }
        }
    }
    #endregion

    /**
     * Save new employee data.
     * Send notification for new employee created.
     *
     * @param Dingo\Api\Http\Request $request
     * @return Resource Item
     */
    public function store(Request $request)
    {
        $result = parent::store($request);
        $employee = $result->getOriginalContent();

        if (empty($employee->employee_no)) {
            event(new NewApplicantEvent($employee));
        } else {
            event(new NewEmployee($employee));
        }

        return $result;
    }

    /**
     * Update employee data.
     * Send notification for newly hired employee
     *
     * @param Dingo\Api\Http\Request $request
     * @param int $id
     * @return Resource Item
     */
    public function update(Request $request, $id)
    {
        $sendNotification = $request->get('sendNotification');
        unset($request['sendNotification']);

        $result = parent::update($request, $id);
        $employee = $result->getOriginalContent();

        if ($sendNotification) {
            event(new NewEmployee($employee));
        }

        return $result;
    }

    /**
     * 2 in 1 API that works for Smart Team Builder and Find Available Resource
     * for Client
     *
     * @param Dingo\Api\Http\Request $request
     *
     */
    private function filterAvailableEmployees (Request $request)
    {
        $developerSkills = [];
        $isUnassignFilter = false;

        if ($request->has('clientTeam')) {
            $this->repository->pushCriteria(new WithClientProject());
            // set client_id in client_teambuilder_bucket, to get only specific client
            $this->transformer->setClientTeambuilderId($request->get('clientTeam'));
            // get only the status and client_id of clientbuilder
            $this->transformer->setClientTeamBuilderFetchOnly(['id','status', 'client_id', 'client_project_id', 'job_position_id']);
            // filter by client teambuilder if status is `declined` or employee doesn't exist in client teambuilder.
            $this->repository->pushCriteria(new WithClientTeambuilderBucket());
            $this->repository->pushCriteria(new WithEmployeeTopSkill());

            // Client's Smart Team Builder Query...
            if ($request->has('resourceTypes')) {
                $this->repository->pushCriteria(new GetJobPositionByType($request->get('resourceTypes')));

                if ($isUnassignFilter = in_array($request->get('resourceTypes'), ClientTeambuilderBucket::FILTER_UNASSIGN_TEAMBUILDER)) {
                    $categorySkills = CategorySkillHelper::getSkillsByClient($request->get('clientTeam'), $request->get('additionalSkills'));

                    foreach ($categorySkills as $skill) {
                        if (!in_array($skill, $developerSkills)) {
                            $developerSkills[] = $skill;
                        }
                    }
                }
            } else if ($request->has('availables')) { // Client's Find Available Resources Query...
                $developerSkills = ($request->has('skills')) ? $request->get('skills')
                            : CategorySkillHelper::getSkillNameByCategory($request->get('categories'));

                $isUnassignFilter = CategorySkillHelper::inUnassignedSkillsCategory($developerSkills);
            }

            $this->repository->pushCriteria(new SearchByEmployeeTopThreeSkills($developerSkills));

            if ($isUnassignFilter && !empty($developerSkills)) {
                $this->repository->pushCriteria(new FilterByAvailableEmployees());
                $this->repository->pushCriteria(new FilterByUnassignedTeamBuilder($request->get('clientTeam')));
            }

            $this->repository->pushCriteria(new OrderByProficiency());
        }
    }

    public function getEmployeeNames(Request $request, EmployeeNamesTransformer $transformer)
    {
        $this->transformer = $transformer;

        if ($request->get('clientId')) {
            $this->repository->pushCriteria(new WithClientProject());
            $this->repository->pushCriteria(new FilterByClientId($request->get('clientId')));
        }

        $this->repository->pushCriteria(new EmployeeOnly());
        $this->repository->pushCriteria(new FilterByStatus([
            Status::HIRED,
            Status::PROBATIONARY,
            Status::REGULAR,
        ]));
        $this->repository->pushCriteria(new WithStatus());
        $this->repository->pushCriteria(new OrderByField('last_name'));
        $this->repository->pushCriteria(new Select());

        return parent::index($request);
    }
}
