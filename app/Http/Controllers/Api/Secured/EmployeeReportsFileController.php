<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\Employee\EmployeeReportsFileRepository;
use App\Transformers\EmployeeReportsFileTransformer;
use Dingo\Api\Http\Request;
use Illuminate\Api\Http\Response;
use App\Validators\EmployeeReportsFileValidator;
use App\Services\EmployeeReportAttachmentService;
use App\Criterias\EmployeeReportsFile\FilterByEmployeeReportId;


class EmployeeReportsFileController extends BaseController
{
    public function __construct(EmployeeReportsFileRepository $repository, EmployeeReportsFileValidator $validator, EmployeeReportsFileTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request)
    {
        if ($request->get('employee_reports_id')) {
            $this->repository->pushCriteria(new FilterByEmployeeReportId($request->get('employee_reports_id')));
        }
        return parent::index($request);
    }

    /**
     * Upload UploadService
     *
     * @param  Dingo\Api\Http\Request $request
     * @return Resource Item
     */
    public function store(Request $request)
    {
        try {

            $uploaded = new EmployeeReportAttachmentService($this->repository, $this->validator, $this->transformer);
            $result = $uploaded->getResult($request);

            return $this->response->item($result, $this->transformer);
        } catch (Exception $e) {
            return response()->json(
                [
                    'status_code' => 400,
                    'message' => $e->getMessageBag()
                ], 400
            );
        }
    }
}
