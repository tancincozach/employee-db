<?php

namespace App\Http\Controllers\Api\Secured;

use \Prettus\Validator\Contracts\ValidatorInterface;
use Dingo\Api\Http\Request;

use App\Http\Controllers\Api\APIBaseController as BaseController;

use App\Repositories\Timesheet\ClientTimesheetJobcodeRepository;
use App\Validators\ClientTimesheetJobcodeValidator;

use App\Transformers\ClientTimesheetJobcodeTransformer;
use App\Transformers\Timesheet\MappedClientTransformer;

use App\Criterias\ClientTimesheetJobcode\ByClient;
use App\Criterias\ClientTimesheetJobcode\WithMappedClient;
use App\Criterias\ClientTimesheetJobcode\Unmapped;

class ClientTimesheetJobcodeController extends BaseController
{
    public function __construct(
        ClientTimesheetJobcodeRepository $repository,
        ClientTimesheetJobcodeValidator $validator,
        ClientTimesheetJobcodeTransformer $transformer
    ) {
        $this->repository  = $repository;
        $this->validator   = $validator;
        $this->transformer = $transformer;
    }

    /**
     * Get list of timesheet jobcodes
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function index(Request $request)
    {
        if ($clientId = $request->get('clientId')) {
            $this->repository->pushCriteria(new ByClient($clientId));
        }

        if (filter_var($request->get('withMapping'), FILTER_VALIDATE_BOOLEAN)) {
            $this->repository->pushCriteria(new WithMappedClient());
            $this->transformer = app(MappedClientTransformer::class);
        }

        if (filter_var($request->get('unmapped'), FILTER_VALIDATE_BOOLEAN)) {
            $this->repository->pushCriteria(new Unmapped());
        }

        return parent::index($request);
    }
}
