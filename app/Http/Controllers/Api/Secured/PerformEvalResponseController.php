<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\PerformEvalQuestion\PerformEvalResponseRepository;
use App\Transformers\PerformEvalQuestion\PerformEvalResponseTransformer;
use App\Validators\PerformEvalQuestion\PerformEvalResponseValidator;
use Dingo\Api\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use App\Services\PerformEvalResponseService;

/**
 * Class PerformEvalResponseController
 * @package App\Http\Controllers\Api\Secured
 */
class PerformEvalResponseController extends BaseController
{
    /**
     * PerformEvalResponseController constructor.
     * @param PerformEvalResponseRepository $repository
     * @param PerformEvalResponseTransformer $transformer
     * @param PerformEvalResponseValidator $validator
     */
    public function __construct(
        PerformEvalResponseRepository $repository,
        PerformEvalResponseTransformer $transformer,
        PerformEvalResponseValidator $validator
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @return Resource
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        return parent::index($request);
    }

    /**
     * Handle POST request for the resource, saving new data
     *
     * @param Dingo\Api\Http\Request $request
     * @return Resource Item
     */
    public function store(Request $request)
    {
        $modelObj = app($this->repository->model());
        $this->authorize('create', $modelObj);

        try {
            $this->validator->with($request->all())->passesOrFail('create');
            $per = new PerformEvalResponseService();
            $response = $per->submitResponse($request->all());

            if (!$response) {
                throw new Exception('Internal Server Error', 500);
            }

            $bodyData = $per->setEmailData($request->all());
            $per->emailBodyContent = view('emails.performance-evaluation-notification', $bodyData)->render();

            // send email notification
            $per->sendEmail();

            return response()->json([
                'status_code' => 200,
                'message' => 'Successfully submitted.',
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'status_code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());
        }
    }
}