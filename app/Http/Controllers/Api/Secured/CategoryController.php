<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\Category\CategoryRepository;
use App\Validators\CategoryValidator;
use App\Transformers\CategoryTransformer;
use Dingo\Api\Http\Request;

class CategoryController extends BaseController
{
    public function __construct(CategoryRepository $repository, CategoryValidator $validator, CategoryTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function store (Request $request)
    {
        $isExistId = $this->repository->isExistCategory($request['name'], $request['resource_type']);

        return ($isExistId > 0) ? parent::update($request, $isExistId) : parent::store($request);
    }
}
