<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\Skill\SkillRepository;
use App\Validators\SkillValidator;
use App\Transformers\SkillTransformer;
use Dingo\Api\Http\Request;
use App\Criterias\Skill\SearchSkill;
use App\Criterias\Skill\SearchExactSkill;
use App\Criterias\Common\WithTrashed;
use App\Repositories\Skill\SkillCategoryRepository;
use App\Transformers\SkillNamesTransformer;

class SkillController extends BaseController
{
    public function __construct(SkillRepository $repository, SkillValidator $validator, SkillTransformer $transformer, SkillCategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
        $this->categoryRepository = $categoryRepository;
    }

    public function index(Request $request)
    {
        if ($request->get('search')) {
            $this->repository->pushCriteria(new SearchSkill($request->get('search')));
        }

        if($request->get('name') && $request->get('withTrashed')) {
            $this->repository->pushCriteria(new WithTrashed());
            $this->repository->pushCriteria(new SearchExactSkill($request->get('name')));
        }

        if ($request->get('namesOnly')) {
            $this->transformer = app(SkillNamesTransformer::class);
        }

        return parent::index($request);
    }

    public function store(Request $request)
    {
        $response = parent::store(new Request($request->except(['categories', 'removeCategories'])));

        $this->categoryRepository->saveSkillCategory($response->getOriginalContent()->id, $request['categories']);
    }

    /**
     * This assumes normal update or passively restore and update the previously soft-deleted record
     * @param Request $request
     * @param int $id
     * @return Resource
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update(Request $request, $id) {
        // We need to get this passive, that is - we need to consider those that's already soft-deleted
        $this->repository->pushCriteria(new WithTrashed());

        // we need to restore (set deleted_at to NULL) prior to updating
        $this->repository->find($id)->restore();

        // Call mom!
        $response = parent::update(new Request($request->except(['categories', 'removeCategories'])), $id);

        // Save Categories
        $this->categoryRepository->saveSkillCategory($id, $request['categories'], $request['removeCategories']);

        return $response;
    }


    /**
     * Getting technology choices in Client's Smart Team Builder
     */
    public function getTechnologyChoices(Request $request)
    {
        return $this->repository->getTechnologyChoices($request->all());
    }
}
