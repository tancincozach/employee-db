<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Controller as BaseController;
use Dingo\Api\Http\Request;
use App\Services\SmartSearchService;

class SmartSearchController extends BaseController
{

    public function index(Request $request)
    {
        $service = SmartSearchService::run([
            'query' => $request->get('q'),
            'limit' => 5 // Limit of items per resource;
        ]);

        return ['data' => $service->result];
    }
}
