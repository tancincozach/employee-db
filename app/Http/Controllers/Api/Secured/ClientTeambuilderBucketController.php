<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\ClientTeambuilderBucket\ClientTeambuilderBucketRepository;
use App\Validators\ClientTeambuilderBucketValidator;
use App\Transformers\ClientTeambuilderBucketTransformer;
use App\Criterias\ClientTeamBuilderBucket\FilterByStatus;
use App\Criterias\ClientTeamBuilderBucket\FilterByClientId;
use App\Services\ClientService;
use App\Services\CategorySkillHelper;
use Dingo\Api\Http\Request;
use App\Models\User;

class ClientTeambuilderBucketController extends BaseController
{
    public function __construct(ClientTeambuilderBucketRepository $repository, ClientTeambuilderBucketValidator $validator, ClientTeambuilderBucketTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }
    /**
     * Get list of resources pooled by Matt
     *
     * @return Collection
     */

    public function index(Request $request)
    {
        // $this->repository->pushCriteria(new FilterBySuggestedResource());

        if ($request->client_id) {
            $this->repository->pushCriteria(new FilterByClientId($request->client_id));
        }

        if ($request->status) {
            $this->repository->pushCriteria(new FilterByStatus(explode(',', $request->status)));
        }

        return parent::index($request);
    }

    public function saveSelectedAvailableResource(Request $request)
    {
        $selectedResource = $request->get('selectedEmployee');
        if ($request->get('client_id') && $request->get('client_email')) {
            $clientId = $request->get('client_id');
            $clientEmail = $request->get('client_email');
            $user = User::where('email', $clientEmail)->first();
        }

        foreach ($selectedResource as $resource) {
            $action = $resource['action'];
            $resource_data = $resource;
            unset($resource_data['action']);
            unset($resource_data['client_teambuilder_bucket_id']);
   
            if (empty($resource['client_teambuilder_bucket_id'])) {
                $id = $this->repository->getExistingId($resource['client_id'], $resource['employee_id']);
            } else {
                $id = $resource['client_teambuilder_bucket_id'];
            }

            $resource_data['updated_by_user_id'] = $user ? $user->id : null;

            if ($id > 0) {
                $clientBucket = $this->repository->find($id);
                $is_suggested = $clientBucket->is_suggested_admin;

                if ($is_suggested) {
                    if ($action == 'remove') {
                        if ($clientBucket->status == 'selected') {
                            $resource_data['is_suggested_admin'] = 0;
                            parent::update(new Request($resource_data), $id);
                        } else {
                            parent::destroy($id);
                        }
                    } else {
                        if ($action == 'select') {
                            $resource_data['status'] = 'selected';  
                        } else {
                            $resource_data['status'] = 'suggested';
                        }
                        parent::update(new Request($resource_data), $id);
                    }
                } else {
                   parent::destroy($id);
                }
            } else {
                $response = parent::store(new Request($resource_data));
            }
        }

        if ($clientId && $clientEmail) {
            $this->repository->sendSelectedTalentsEmail($clientId, $clientEmail);
        }
        return 'success';
    }

    public function saveSelectedResource(Request $request)
    {
        $payload = $request->get('payload');
        $sendNotification = $payload['sendNotification'] ?? false;
        $clientId = $payload['clientId'];

        unset($request['payload']);

        $this->repository->updateBucketResource($request->all(), $sendNotification, $clientId);
    }

    public function saveTeamBuilder(Request $request)
    {
        $suggestedResource = $request->get('suggestedResource');

        foreach ($suggestedResource as $suggested) {
            $id = $this->repository->getExistingId($suggested['client_id'], $suggested['employee_id']);

            if ($id > 0) {
                parent::update(new Request($suggested), $id);
            } else {
                parent::store(new Request($suggested));
            }
        }

        $updatedBucket = $request->get('updatedBucket');

        foreach ($updatedBucket as $updated) {
            if ($updated['status'] === 'selected') {
                parent::update(new Request($updated), $updated['id']);
            } else {
                parent::destroy($updated['id']);
            }
        }

        if (count($request->get('clientNotification')) > 0) {
            ClientService::sendTeamBuilderNotification($request->get('clientNotification'));
        }

        return 'success';
    }

    public function getOnboardingResults(Request $request)
    {
        return array(
            'service_need' => CategorySkillHelper::getServiceNeeded($request['id']),
            'tech_require' => CategorySkillHelper::getTechRequired($request['id'])
        );
    }
}
