<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Models\EmailQueue;
use App\Repositories\AllQuestion\QuestionResponseRepository;
use App\Services\EmailQueueService;
use App\Transformers\AllQuestion\ResponseTransformer as QuestionResponseTransformer;
use App\Validators\AllQuestion\QuestionResponseValidator;
use Dingo\Api\Http\Request;
use App\Criterias\AllQuestion\FilterByCategory;
use App\Criterias\AllQuestion\FilterByClient;
use App\Criterias\AllQuestion\FilterByFormLabel;
use PharIo\Manifest\Email;
use App\Events\ClientOnboarded;
use App\Models\Client;
use App;

class AllQuestionResponseController extends BaseController
{
    public function __construct(QuestionResponseRepository $repository, QuestionResponseValidator $validator, QuestionResponseTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request)
    {
        if ($request->get('filterBy')) {
            $filterBy = explode(',', $request->get('filterBy'));
            $filter   = explode(',', $request->get('filterVar'));

            foreach ($filterBy as $key => $value) {
                switch (strtolower(trim($value))) {
                    case 'category':
                        $this->repository->pushCriteria(new FilterByCategory($filter[$key]));// id or name
                        break;
                    case 'client':
                        $this->repository->pushCriteria(new FilterByClient($filter[$key]));
                        break;
                }
            }
        }

        if ($request->get('formLabel')) {
            $this->repository->pushCriteria(new FilterByClient($request->get('clientId')));
            $this->repository->pushCriteria(new FilterByFormLabel($request->get('formLabel')));
        }

        return parent::index($request);
    }

    /**
     * Handle POST request for the resource, saving new data
     *
     * @param Dingo\Api\Http\Request $request
     * @return Resource Item
     */
    public function store(Request $request)
    {
        $modelObj = app($this->repository->model());
        $this->authorize('create', $modelObj);

        try {
            $this->validator->with($request->all())->passesOrFail('create');
            $result = $this->repository->saveAllResponses($request->all());
            $result = collect($result);

            $this->sendEmails($result);

            $client = Client::find($result->first()->client_id);
            event(new ClientOnboarded($client));

            return $this->response->collection($result, $this->transformer);
        } catch (ValidatorException $e) {
            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag(),
            ], 400);
        }
    }

    private function sendEmails($result)
    {
        $emailAddressPrimaryContact = $result->filter(function($item) {
            return $item['all_question_form_label'] === "email_address_primary_contact";
        });

        $emailAddressPrimaryContact = $emailAddressPrimaryContact->values()->toArray();

        $body = [];
        $textFields = [
            'short_text',
            'long_text',
            'date',
            'number',
            'money',
        ];

        foreach ($result->toArray() as $key => $item)
        {
            $key = ucfirst(str_replace('_', ' ', $item['all_question_form_label']));

            if (strcasecmp($key, "Legal document") == 0) {
                $key = "Is primary contact the legal signatory?";
            }
            if (!empty($item['response']) && in_array($item['all_question_type'], $textFields)) {
                $body[$key] = $item['response'];
            } else {
                $body[$key][] = $item['all_question_choice_description'] ?? $item['response'] ?? null;
            }
        }

        // Email functionality block
        $to = [
            'ccabang@fullscale.io',
            'reynaldo@fullscale.io',
            'july@fullscale.io',
            'rnacu@fullscale.io',
        ];
        $bcc = []; // Only add email here if for testing. Please keep this empty when done testing

        if (strtolower(trim(App::Environment())) == 'prod') {
            $to = [
                'deco@fullscale.io',
                'sales@fullscale.io',
            ];
            $bcc = array_merge($bcc, [
                'ccabang@fullscale.io',
                'reynaldo@fullscale.io',
                'july@fullscale.io',
                'rnacu@fullscale.io',
            ]);
        }

        $body = $this->offsetAssoc($body, ['Remote team exp' => $body['Remote team exp']], 5);

        if($body['Remote team exp'][0] === 'Yes') {
            $body = $this->offsetAssoc($body, ['Remote team exp type' => $body['Remote team exp type']], 6);
            $body = $this->offsetAssoc($body, ['Remote team exp country' => $body['Remote team exp country']], 7);
        }

        $renderedBody = view('emails.onboarding-exec-notification', compact('body'))->render();

        // send to matt
        EmailQueue::create([
            'subject' => $body['Company name'].' has ONBOARDED',
            'send_to' => implode($to, ','),
            'body' => $renderedBody,
            'bcc' => implode($bcc, ','),
        ]);
        // send to client
        EmailQueue::create([
            'subject' => 'Thanks! We Received Your Information',
            'send_to' => $emailAddressPrimaryContact[0]['response'],
            'body' => view('emails.onboarding-client-notification', ['contact_name' => $body['Primary contact']])->render()
        ]);
    }

    function offsetAssoc($array, $values, $offset) {
        return array_slice($array, 0, $offset, true) + $values + array_slice($array, $offset, NULL, true);
    }
}
