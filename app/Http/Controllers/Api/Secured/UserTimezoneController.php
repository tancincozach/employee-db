<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;

use App\Repositories\User\UserTimezoneRepository;
use App\Validators\UserTimezoneValidator;
use App\Transformers\UserTimezoneTransformer;

use App\Criterias\UserTimezone\SearchByUserId;

class UserTimezoneController extends BaseController
{
    public function __construct(
        UserTimezoneRepository $repository,
        UserTimezoneValidator $validator,
        UserTimezoneTransformer $transformer
    ) {
        $this->repository  = $repository;
        $this->validator   = $validator;
        $this->transformer = $transformer;
    }

    /**
     * Get list of user timezones
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function index(Request $request)
    {
        if ($userId = $request->get('user_id')) {
            $this->repository->pushCriteria(new SearchByUserId($userId));
        }

        return parent::index($request);
    }
}
