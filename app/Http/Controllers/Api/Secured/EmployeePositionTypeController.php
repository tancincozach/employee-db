<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\EmployeePositionType\EmployeePositionTypeRepository;
use App\Transformers\EmployeePositionTypeTransformer;
use App\Validators\EmployeePositionTypeValidator;

/**
 * Class EmployeePositionTypeController
 * @package App\Http\Controllers\Api\Secured
 */
class EmployeePositionTypeController extends BaseController
{
    /**
     * EmployeePositionTypeController constructor.
     * @param EmployeePositionTypeRepository $repository
     * @param EmployeePositionTypeValidator $validator
     * @param EmployeePositionTypeTransformer $transformer
     */
    public function __construct(
        EmployeePositionTypeRepository $repository,
        EmployeePositionTypeValidator $validator,
        EmployeePositionTypeTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }
}
