<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\EmployeeCharacteristic\EmployeeCharacteristicRepository;
use App\Transformers\EmployeeCharacteristicTransformer;
use App\Validators\EmployeeCharacteristicValidator;

/**
 * Class EmployeeCharacteristicController
 * @package App\Http\Controllers\Api\Secured
 */
class EmployeeCharacteristicController extends BaseController
{
    /**
     * EmployeeCharacteristicController constructor.
     * @param EmployeeCharacteristicRepository $repository
     * @param EmployeeCharacteristicValidator $validator
     * @param EmployeeCharacteristicTransformer $transformer
     */
    public function __construct(
        EmployeeCharacteristicRepository $repository,
        EmployeeCharacteristicValidator $validator,
        EmployeeCharacteristicTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }
}
