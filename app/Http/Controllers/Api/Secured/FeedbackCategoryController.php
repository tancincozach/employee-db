<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\FeedbackCategory\FeedbackCategoryRepository;
use App\Transformers\FeedbackCategoryTransformer;
use App\Validators\FeedbackCategoryValidator;

/**
 * Class FeedbackCategoryController
 * @package App\Http\Controllers\Api\Secured
 */
class FeedbackCategoryController extends BaseController
{
    /**
     * FeedbackCategoryController constructor.
     * @param FeedbackCategoryRepository $repository
     * @param FeedbackCategoryValidator $validator
     * @param FeedbackCategoryTransformer $transformer
     */
    public function __construct(
        FeedbackCategoryRepository $repository,
        FeedbackCategoryValidator $validator,
        FeedbackCategoryTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }
}
