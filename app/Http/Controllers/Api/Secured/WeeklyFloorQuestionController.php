<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\WeeklyFloorQuestion\WeeklyFloorQuestionRepository;
use App\Transformers\WeeklyFloorQuestionTransformer;
use App\Validators\WeeklyFloorQuestionValidator;
use Dingo\Api\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

/**
 * Class WeeklyFloorQuestionController
 * @package App\Http\Controllers\Api\Secured
 */
class WeeklyFloorQuestionController extends BaseController
{
    /**
     * WeeklyFloorQuestionController constructor.
     * @param WeeklyFloorQuestionRepository $repository
     * @param WeeklyFloorQuestionTransformer $transformer
     * @param WeeklyFloorQuestionValidator $validator
     */
    public function __construct(
        WeeklyFloorQuestionRepository $repository,
        WeeklyFloorQuestionTransformer $transformer,
        WeeklyFloorQuestionValidator $validator
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @return Resource
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        return parent::index($request);
    }
}

