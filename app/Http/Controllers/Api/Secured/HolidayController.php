<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\Holiday\HolidayRepository;
use App\Validators\HolidayValidator;
use App\Transformers\HolidayTransformer;
use Dingo\Api\Http\Request;
use App\Criterias\Holiday\SearchByFields;
use App\Criterias\Holiday\UpcomingHolidays;
use \Prettus\Validator\Contracts\ValidatorInterface;

class HolidayController extends BaseController
{
    public function __construct(
        HolidayRepository $repository,
        HolidayValidator $validator,
        HolidayTransformer $transformer
    ) {
        $this->repository  = $repository;
        $this->validator   = $validator;
        $this->transformer = $transformer;
    }

    /**
     * GET list of resource
     *
     * @param  Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function index(Request $request)
    {
        if ($request->get('searchByDate')) {
            $keyword = $request->get('searchByDate');

            $this->repository->pushCriteria(
                new SearchByFields($keyword, ['date'])
            );
        }
        if ($request->get('searchBy')=='upcoming') {
            $this->repository->pushCriteria(new UpcomingHolidays());
        }

        return parent::index($request);
    }

    /**
     * Handle POST request for the resource, saving new data
     *
     * @param Dingo\Api\Http\Request $request
     * @return Resource Item
     */
    public function store(Request $request)
    {

        $modelObj = app($this->repository->model());
        $this->authorize('create', $modelObj);

        $date = $request->get('date');
        if ($this->repository->checkIfTrashed($date)) {
            $this->repository->restore($date);
            $id = $this->repository->getId($date);
            $result = $this->repository->update($request->all(), $id);

            return $this->response->item($result, $this->transformer);
        }

        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $result = $this->repository->create($request->all());

            return $this->response->item($result, $this->transformer);
        } catch (ValidatorException $e) {
            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag()
            ], 400);
        }
    }

    /**
     * PUT|PATCH update a resource
     *
     * @param Dingo\Api\Http\Request $request
     * @param int $id
     * @return Resource Item
     */
    public function update(Request $request, $id)
    {
        $modelObj = app($this->repository->model());
        $this->authorize('update', $modelObj);

        try {
            $this->validator->with($request->all())->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $result = $this->repository->update($request->all(), $id);

            return $this->response->item($result, $this->transformer);
        } catch (ValidatorException $e) {
            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag()
            ], 400);
        }
    }
}
