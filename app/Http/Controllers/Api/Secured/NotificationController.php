<?php

namespace App\Http\Controllers\Api\Secured;

use Dingo\Api\Http\Request;
use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\Notification\NotificationRepository;
use App\Validators\NotificationValidator;
use App\Transformers\NotificationTransformer;
use App\Criterias\Notification\SearchByUser;
use App\Services\NotificationService;

class NotificationController extends BaseController
{
    public function __construct(
        NotificationRepository $repository,
        NotificationValidator $validator,
        NotificationTransformer $transformer
    ) {
        $this->repository  = $repository;
        $this->validator   = $validator;
        $this->transformer = $transformer;
    }

    /**
     * GET formatted list of notifications
     *
     * @param Dingo\Api\Http\Request
     * @return mixed
     */
    public function index(Request $request)
    {
        $this->authorize('view', app($this->repository->model()));
        $user_id = $request->user()->id;
        
        $this->repository->pushCriteria(new SearchByUser($user_id));
        $result = $this->repository->get();
        $result = NotificationService::format($result);
        return $result;
    }

    /**
     * Mark as read notifications
     * 
     * @param Dingo\Api\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->authorize('update', app($this->repository->model()));
        $userId = $request->user()->id;

        if ($request->get('action') == 'open') {
            NotificationService::openNotification($userId);
        } else {
            $data = $request->get('data');
            $data = is_array($data) ? $data : [$data];
            NotificationService::markAsRead($data, $userId);
        }
    }
}
