<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\CreativeServiceApplicant\CreativeServiceApplicantRepository;
use App\Transformers\CreativeServiceApplicantTransformer;
use App\Validators\CreativeServiceApplicantValidator;
use App\Criterias\CreativeServiceApplicant\SearchByCreativeServiceId;

use Dingo\Api\Http\Request;


/**
 * Class CreativeServiceApplicantController
 * @package App\Http\Controllers\Api\Secured
 */
class CreativeServiceApplicantController extends BaseController
{
    /**
     * CreativeServiceApplicantController constructor.
     * @param CreativeServiceApplicantRepository $repository
     * @param CreativeServiceApplicantValidator $validator
     * @param CreativeServiceApplicantTransformer $transformer
     */
    public function __construct(
        CreativeServiceApplicantRepository $repository,
        CreativeServiceApplicantValidator $validator,
        CreativeServiceApplicantTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }
    
    public function index(Request $request)
    {
       // dd($request);
       if($request->get('creative_service_id')) {
            $this->repository->pushCriteria(new SearchByCreativeServiceId($request->get('creative_service_id')));
       }
       return parent::index($request);
    }
}
