<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Models\AllQuestion;
use App\Repositories\AllQuestion\QuestionRepository;
use App\Transformers\AllQuestion\QuestionTransformer;
use App\Validators\AllQuestion\QuestionValidator;
use App\Criterias\AllQuestion\ActiveOnly;
use App\Criterias\AllQuestion\FilterByCategory;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;

class AllQuestionController extends BaseController
{
    public function __construct(QuestionRepository $repository, QuestionValidator $validator, QuestionTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request)
    {
        $this->repository->pushCriteria(new ActiveOnly(true));
        $this->repository->orderBy('page');
        $this->repository->orderBy('display_sequence');

        if ($request->__isset('category_id')) {
            $this->repository->pushCriteria(new FilterByCategory($request->get('category_id')));
        }

        if (!empty($request->get('group'))) {
            // get parent response
            $response = parent::index($request);

            // get questions only
            $questions = $response->getOriginalContent()->toArray()['data'];

            // group by page
            $pages = [];
            foreach ($questions as $question) {
                $pages[$question['page']][] = $this->transformItem(AllQuestion::find($question['id']));
            }

            // // re-arrange index
            // $pages = array_values($pages);
            // comment for now, will rediscuss with the dev who change this. - Ralph 05/31/2019

            return response(['data' => $pages], 200);
        } else {
            return parent::index($request);
        }
    }

    private function transformItem($data)
    {
        $fractal = new Manager();
        $fractal->setSerializer(new ArraySerializer());
        return $fractal->createData((new Item($data, new QuestionTransformer)))->toArray();
    }
}
