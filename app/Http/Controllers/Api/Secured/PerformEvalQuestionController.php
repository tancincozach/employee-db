<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\PerformEvalQuestion\PerformEvalQuestionRepository;
use App\Transformers\PerformEvalQuestion\PerformEvalQuestionTransformer;
use App\Validators\PerformEvalQuestion\PerformEvalQuestionValidator;
use Dingo\Api\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

/**
 * Class PerformEvalQuestionController
 * @package App\Http\Controllers\Api\Secured
 */
class PerformEvalQuestionController extends BaseController
{
    /**
     * PerformEvalQuestionController constructor.
     * @param PerformEvalQuestionRepository $repository
     * @param PerformEvalQuestionTransformer $transformer
     * @param PerformEvalQuestionValidator $validator
     */
    public function __construct(
        PerformEvalQuestionRepository $repository,
        PerformEvalQuestionTransformer $transformer,
        PerformEvalQuestionValidator $validator
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @return Resource
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        return parent::index($request);
    }
}