<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\ClientOnboardingChecklist\ClientOnboardingChecklistRepository;
use App\Validators\ClientOnboardingChecklistValidator;
use App\Transformers\ClientOnboardingChecklistTransformer;
use Dingo\Api\Http\Request;
use App\Criterias\ClientOnboardingChecklist\SearchByClientId;
use App\Criterias\ClientOnboardingChecklist\SearchByStep;

class ClientOnboardingChecklistController extends BaseController
{
    public function __construct(ClientOnboardingChecklistRepository $repository, ClientOnboardingChecklistValidator $validator, ClientOnboardingChecklistTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

     /**
     *
     *
     * @return Collection
     */
    public function index(Request $request)
    {
        if ($request->get('client_id')) {
            $this->repository->pushCriteria(new SearchByClientId($request->get('client_id')));
        }

        if ($request->get('step')) {
            $this->repository->pushCriteria(new SearchByStep($request->get('step')));
        }
        return parent::index($request);
    }

    /**
     * Handle POST request for the resource, saving new data
     *
     * @param Dingo\Api\Http\Request $request
     * @return Resource Item
     */
    public function store(Request $request)
    {
        // we remove these request data, we don't store these in db
        $request = new Request($request->except(['asset']));
        return parent::store($request);
    }

    /**
     * PUT|PATCH update a resource
     *
     * @param Dingo\Api\Http\Request $request
     * @param int $id
     * @return Resource Item
     */
    public function update(Request $request, $id)
    {
        // we remove these request data, we don't store these in db
        $request = new Request($request->except(['asset']));
        return parent::update($request, $id);
    }

}
