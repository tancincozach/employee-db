<?php

namespace App\Http\Controllers\Api\Secured;

use \Prettus\Validator\Contracts\ValidatorInterface;
use Dingo\Api\Http\Request;

use App\Http\Controllers\Api\APIBaseController as BaseController;

use App\Repositories\Timesheet\TimesheetJobcodeRepository;
use App\Validators\TimesheetJobcodeValidator;
use App\Transformers\TimesheetJobcodeTransformer;
use App\Transformers\Timesheet\TimesheetJobcodeNamesTransformer;

use App\Criterias\TimesheetJobcode\ByType;

class TimesheetJobcodeController extends BaseController
{
    public function __construct(
        TimesheetJobcodeRepository $repository,
        TimesheetJobcodeValidator $validator,
        TimesheetJobcodeTransformer $transformer
    ) {
        $this->repository  = $repository;
        $this->validator   = $validator;
        $this->transformer = $transformer;
    }

    /**
     * Get list of timesheet jobcodes
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function index(Request $request)
    {
        if ($type = $request->get('type')) {
            $this->repository->pushCriteria(new ByType($type));
        }

        if (filter_var($request->get('namesOnly'), FILTER_VALIDATE_BOOLEAN)) {
            $this->transformer = app(TimesheetJobcodeNamesTransformer::class);
        }

        return parent::index($request);
    }

    /**
     * Disable creating data
     *
     * @param Dingo\Api\Http\Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable updating data
     *
     * @param Dingo\Api\Http\Request $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable deleting data
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }
}
