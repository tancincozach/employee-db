<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\WeeklyFloorAnswer\WeeklyFloorAnswerRepository;
use App\Transformers\WeeklyFloorAnswerTransformer;
use App\Validators\WeeklyFloorAnswerValidator;
use Dingo\Api\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

/**
 * Class WeeklyFloorAnswerController
 * @package App\Http\Controllers\Api\Secured
 */
class WeeklyFloorAnswerController extends BaseController
{
    /**
     * WeeklyFloorAnswerController constructor.
     * @param WeeklyFloorAnswerRepository $repository
     * @param WeeklyFloorAnswerTransformer $transformer
     * @param WeeklyFloorAnswerValidator $validator
     */
    public function __construct(
        WeeklyFloorAnswerRepository $repository,
        WeeklyFloorAnswerTransformer $transformer,
        WeeklyFloorAnswerValidator $validator
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @return Resource
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        return parent::index($request);
    }
}

