<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\CreativeService\CreativeServiceRepository;
use App\Transformers\CreativeServiceTransformer;
use App\Validators\CreativeServiceValidator;
use App\Criterias\CreativeService\SearchProject;
use App\Criterias\CreativeService\SearchPurpose;
use App\Criterias\CreativeService\SearchCompany;
use App\Criterias\CreativeService\CreativeService;
use App\Criterias\CreativeService\WithCreativeServiceApplicant;
use App\Criterias\CreativeService\AddSelectCreativeServiceApplicant;
use App\Criterias\CreativeService\AddSelectCreativeService;

use Dingo\Api\Http\Request;

/**
 * Class CreativeServiceController
 * @package App\Http\Controllers\Api\Secured
 */
class CreativeServiceController extends BaseController
{
    /**
     * CreativeServiceController constructor.
     * @param CreativeServiceRepository $repository
     * @param CreativeServiceValidator $validator
     * @param CreativeServiceTransformer $transformer
     */
    public function __construct(
        CreativeServiceRepository $repository,
        CreativeServiceValidator $validator,
        CreativeServiceTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request)
    {
        if ($request->get('search')) {
            $this->repository->pushCriteria(new SearchProject($request->get('search')));
            $this->repository->pushCriteria(new SearchPurpose($request->get('search')));
            $this->repository->pushCriteria(new SearchCompany($request->get('search')));
        }

        $this->repository->pushCriteria(new WithCreativeServiceApplicant());
        $this->repository->pushCriteria(new AddSelectCreativeServiceApplicant());
        $this->repository->pushCriteria(new AddSelectCreativeService());

        return parent::index($request);
    }
}
