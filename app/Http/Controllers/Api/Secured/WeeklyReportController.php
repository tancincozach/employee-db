<?php

namespace App\Http\Controllers\Api\Secured;

use App\Events\RetrieveTimeSheetEvent;
use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\WeeklyReport\WeeklyReportRepository;
use App\Repositories\Client\ClientProjectRepository;
use App\Repositories\WeeklyReportBatchViewable\WeeklyReportBatchViewableRepository;
use App\Repositories\ClientProjectJobcode\ClientProjectJobcodeRepository;
use App\Services\ExecService;
use App\Validators\WeeklyReportValidator;
use App\Transformers\WeeklyReportTransformer;
use Dingo\Api\Http\Request;
use App\Criterias\ClientProjectJobcode\ExcludeNonWorkJobcodes;
use App\Criterias\WeeklyReport\GetWeeklyReportByJobcode;
use App\Criterias\WeeklyReport\GetWeeklyReportByJobCodes;
use App\Criterias\WeeklyReport\GetWeeklyReportByEmployees;
use App\Criterias\WeeklyReport\GetWeeklyReportByProjectIdAndYear;
use App\Criterias\WeeklyReport\GetByRangeReport;
use App\Criterias\WeeklyReport\GetWeeklyReportByEmployeeAndRange;
use App\Criterias\WeeklyReport\GetMonthlyReport;
use App\Criterias\WeeklyReport\WithClientProjectJobcodes;
use App\Criterias\WeeklyReport\GetByJobcodeId;
use App\Criterias\WeeklyReport\GetByDataSource;
use App\Criterias\WeeklyReport\WithoutLunchBreak;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Arr;
use App\Services\TSheetParserService;
use App\Services\ThirdParty\TSheets;

use App\Criterias\Common\WithTrashed;
use App\Criterias\WeeklyReport\GetWeeklyReportByMonthYear;

class WeeklyReportController extends BaseController
{
    public function __construct(
        WeeklyReportRepository $repository,
        WeeklyReportValidator $validator,
        WeeklyReportTransformer $transformer,
        ClientProjectRepository $projectRepository,
        WeeklyReportBatchViewableRepository $viewableRepository,
        ClientProjectJobcodeRepository $jobcodeRepository
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
        $this->projectRepository = $projectRepository;
        $this->viewableRepository = $viewableRepository;
        $this->jobcodeRepository = $jobcodeRepository;
    }

    public function index(Request $request)
    {
        $jobcodesMeta = [];

        if (!empty($request->get('raw'))) {
            $this->transformer->setRawReport(true);
            $startDate = $request->get('startDate');
            $endDate = $request->get('endDate');

            if ($request->has('clientId')) {
                $jobcodes = array_merge(
                    $this->jobcodeRepository->getJobcodeIdByClientId($request->get('clientId')),
                    $this->jobcodeRepository->getNonWorkJobcodeIds(),
                    $this->jobcodeRepository->getPTOHolidayJobcodeId()
                );

                $this->transformer->setClientId($request->get('clientId'));
                $this->repository->pushCriteria(new GetByJobcodeId($jobcodes));

                $startDate = new \DateTime($startDate);
                $startDate->add(new \DateInterval('P1D'));
                $startDate = $startDate->format('Y-m-d');

                $endDate = new \DateTime($endDate);
                $endDate->add(new \DateInterval('P1D'));
                $endDate = $endDate->format('Y-m-d');
            }

            $this->repository->pushCriteria(new GetWeeklyReportByEmployeeAndRange(
                $request->get('employeeId'),
                $startDate,
                $endDate
            ));
            $this->repository->pushCriteria(new WithoutLunchBreak());

            $jobcodesMeta = $this->repository->getJobcodes(
                $request->get('employeeId'),
                $startDate,
                $endDate
            );

            if ($request->has('jobcodeIds')) {
                $jobcodes = array_merge(
                    $request->get('jobcodeIds'),
                    $this->jobcodeRepository->getNonWorkJobcodeIds(),
                    $this->jobcodeRepository->getPTOHolidayJobcodeId()
                );

                $this->repository->pushCriteria(new GetByJobcodeId($jobcodes));
            }
        } else {
            if (!empty($request->get('jobcode'))) {
                $this->repository->pushCriteria(new GetWeeklyReportByJobcode($request->get('jobcode')));
                $otherDetails = false;
                if (!empty($request->get('year'))) {
                    $monthNumeric = date_parse($request->get('month'));

                    $param = [
                        'jobcode'       => $request->get('jobcode'),
                        'year'          => $request->get('year'),
                        'month'         => is_numeric($request->get('month')) ? $request->get('month') : $monthNumeric['month']
                    ];
                    $otherDetails = true;
                    $this->repository->pushCriteria(new GetWeeklyReportByProjectIdAndYear($param));
                }
            }
            $this->transformer->setOtherDetails($otherDetails);
        }

        $this->repository->pushCriteria(new GetByDataSource());

        $total_hrs = $this->repository->withTotalHours($this->repository->get());
        $overall_total = (double) number_format($total_hrs->sum('total_hrs'), 2);

        $response = parent::index($request);
        $oldMeta = $response->getMeta();
        $meta = array_merge(
            $oldMeta,
            ['total_hrs' => $total_hrs],
            ['overall_total' => $overall_total],
            ['jobcodes' => $jobcodesMeta]
        );
        $response->setMeta($meta);

        return $response;
    }

    public function getTsheetReport(Request $request)
    {
        //Criteria
        $criteria = ($request['weekly'])
            ? new GetByRangeReport($request['start_date'], $request['end_date'], $request['job_code'])
            : new GetByRangeReportWeekly($request['start_date'], $request['end_date'], $request['job_code']);

        $this->repository->pushCriteria($criteria);

        //Transform
        $this->transformer->setReportParameters(array('start_date' => $request['start_date'], 'end_date' => $request['end_date'], 'weekly' => $request['weekly']));
        $this->transformer->setFilterReport(true);

        return parent::index($request);
    }

    public function getTsheetNow(Request $request)
    {
        $args = [ '--modified' => true ];
        if ($request->get('modified')) {
            $args = [
                '--modified' => $request->get('modified') === 'true' ? true : false
            ];
        }

        if ($request->get('startDate')) {
            $args['start-date'] = $request->get('startDate');
        }
        if ($request->get('endDate')) {
            $args['end-date'] = $request->get('endDate');
        }

        event(new RetrieveTimeSheetEvent($args['start-date'], $args['end-date']));

        return response()->json([
            'status_code' => 200,
            'message' => 'Override Weekly Report is now processing!',
        ], 200);
    }

    public function getClientMonthlyReport(Request $request)
    {
        $jobcodeId = ($request->has('posted'))
                    ? $this->jobcodeRepository->getJobcodeIdByClientId($request['client'])
                    : [(int) $request['id']];

        if (!empty($jobcodeId)) {
            $range = TSheetParserService::overrideMonthRange($request->get('month'), $request->get('year'));
            $startDate = $range['start']->format('Y-m-d');
            $endDate = $range['end']->format('Y-m-d');

            $jobcodes = [];
            $employeeIds = $this->repository->getEmployeeIdsByJobcodeId($jobcodeId, $startDate, $endDate);
            $otherJobcodes = $this->jobcodeRepository->getPTOHolidayJobcodeId();

            $jobcodes = array_merge($jobcodeId, $otherJobcodes);

            $this->repository->pushCriteria(new GetMonthlyReport($startDate, $endDate, $jobcodes, $employeeIds));
            $this->repository->pushCriteria(new GetByDataSource());

            if ($request->has('posted')) {
                $this->repository->filterViewableReports($request['client'], $jobcodes, $employeeIds);
            }

            $this->transformer->setMonthlyReport($request['client'], $jobcodes, $request->has('posted'), $jobcodeId[0]);
            $this->transformer->setFilterReport(true);
            $this->transformer->setCurrentMonthAndYear($request->get('month'), $request->get('year'));

            $result = $this->repository->paginate($request->get('take'));

            return $this->response
                ->paginator($result, $this->transformer)
                ->withHeader('Content-Range', $result->total());
        } else {
            return [];
        }
    }

    public function postReport(Request $request)
    {
        $this->viewableRepository->saveReport($request->all());
    }

    public function getEmployeeWorkingReport(Request $request)
    {
        $this->repository->pushCriteria(new GetWeeklyreportByMonthYear($request['year'], $request['month']));
        $this->repository->pushCriteria(new WithClientProjectJobcodes());
        $this->repository->pushCriteria(new ExcludeNonWorkJobcodes());
        $this->repository->pushCriteria(new GetByDataSource());

        $jobCodes = [];
        if ($request->has('client')) {
            // check if this client has no associated job code.
            $jobCode = $this->jobcodeRepository->getJobcodeIdByClientId($request->get('client'));

            if (empty($jobCode)) {
                return [];
            } else {
                $jobCodes[] = $jobCode;
            }
        }

        if ($request->get('jobcodes')) {
            $jobCodes = array_merge($jobCodes, $request->get('jobcodes'));
        }

        if (!empty($jobCodes)) {
            $this->repository->pushCriteria(new GetWeeklyReportByJobCodes($jobCodes));
        }

        if ($request->get('employeeIds')) {
            $employeeIds = $request->get('employeeIds');
            $this->repository->pushCriteria(new GetWeeklyReportByEmployees($employeeIds));
        }

        $this->transformer->setEmployeeWorkingReport(true);

        return parent::index($request);
    }

    public function overrideResponse(Request $request)
    {
        return TSheets::weeklyReportRunning();
    }
}
