<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\EmployeeAcademic\EmployeeAcademicRepository;
use App\Transformers\EmployeeAcademicTransformer;
use App\Validators\EmployeeAcademicValidator;

/**
 * Class EmployeeAcademicController
 * @package App\Http\Controllers\Api\Secured
 */
class EmployeeAcademicController extends BaseController
{
    /**
     * EmployeeAcademicController constructor.
     * @param EmployeeAcademicRepository $repository
     * @param EmployeeAcademicValidator $validator
     * @param EmployeeAcademicTransformer $transformer
     */
    public function __construct(
        EmployeeAcademicRepository $repository,
        EmployeeAcademicValidator $validator,
        EmployeeAcademicTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }
}
