<?php

namespace App\Http\Controllers\Api\Secured;

use Dingo\Api\Http\Request;

use App\Http\Controllers\Api\APIBaseController as BaseController;

use App\Repositories\Timesheet\TimesheetJobcodeAssignmentRepository;
use App\Transformers\Timesheet\TimesheetEmployeeNamesTransformer;

use App\Criterias\TimesheetJobcodeAssignment\JobcodeAssignmentEmployees;
use App\Criterias\TimesheetJobcodeAssignment\ByClient;

class TimesheetJobcodeAssignmentController extends BaseController
{
    public function __construct(
        TimesheetJobcodeAssignmentRepository $repository,
        TimesheetEmployeeNamesTransformer $transformer
    ) {
        $this->repository  = $repository;
        $this->transformer = $transformer;
    }

    /**
     * GET list of resource
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(new JobcodeAssignmentEmployees());

        if ($request->get('clientIds')) {
            $this->repository->pushCriteria(new ByClient($request->get('clientIds')));
        }

        return parent::index($request);
    }

    /**
     * Disable creating data
     *
     * @param Dingo\Api\Http\Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable updating data
     *
     * @param Dingo\Api\Http\Request $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable deleting data
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }
}
