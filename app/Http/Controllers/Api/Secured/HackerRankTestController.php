<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\HackerRankTest\HackerRankTestRepository;
use App\Repositories\Skill\SkillHackerRankTestsRepository;
use App\Validators\HackerRankTestValidator;
use App\Transformers\HackerRankTestTransformer;
use Dingo\Api\Http\Request;
use App\Criterias\HackerRankTest\SearchByName;
use \Prettus\Validator\Contracts\ValidatorInterface;
use App\Services\ThirdParty\HackerRank;

class HackerRankTestController extends BaseController
{
    public function __construct(
        HackerRankTestRepository $repository,
        HackerRankTestValidator $validator,
        HackerRankTestTransformer $transformer
    )
    {
        $this->repository  = $repository;
        $this->validator   = $validator;
        $this->transformer = $transformer;
    }

    /**
     * GET list of Hacker Rank Test Results
     *
     * @param  Dingo\Api\Http\Request
     * @return Test Results Collection
     */
    public function index(Request $request)
    {
        if ($request->get('search')) {
            $keyword = $request->get('search');

            $this->repository->pushCriteria(
                new SearchByName($keyword, ['name'])
            );
        }
        return parent::index($request);
    }

    /**
     * Handle Importing the Hacker Rank Test and Save the data
     *
     * @param Dingo\Api\Http\Request $request
     * @return Resource Item
     */

    public function fetchAndImportTests() {
        
        $modelObj = app($this->repository->model());
        $this->authorize('update', $modelObj);
        
        try {
            $hackerRank = new HackerRank;
            $fetchHackerTests = $hackerRank->getAllTests();

            foreach($fetchHackerTests as $rowTest) {

                $skillId = 0;
                $tempSkill = '';
                $skillFound = 0; // flagger if the tag is being set.
                if (count($rowTest['tag']) > 0) {
                    foreach($rowTest['tag'] as $tag) {
                        $tempSkill =$this->repository->getSkillIdInTag($tag);
        
                        if (!empty($tempSkill)) {
                            $skillId = $tempSkill;
                            $skillFound = 1;
                        }
                    }
                }
               $this->repository->updateOrCreate(
                    [   
                        'test_id'   => $rowTest['id']
                    ],
                    [
                        'name' => $rowTest['name'],
                        'unique_id' => $rowTest['unique_id'],
                        'skill_id' => $skillId,
                        'has_tag' => $skillFound
                    ]);

            }

        } catch (ValidatorException $e) {
            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag()
            ], 400);
        }
    }

    /**
     * Handle POST request for the resource, saving new data
     *
     * @param Dingo\Api\Http\Request $request
     * @return Resource Item
     */
    public function store(Request $request)
    {
        // $modelObj = app($this->repository->model());
        // $this->authorize('create', $modelObj);
        // $hackerRank = new HackerRank;

        // $fetchHackerTests = $hackerRank->getAllTests();

        // print_r($fetchHackerTests);exit;

    }
}
