<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\HackerRankTestResult\HackerRankTestResultRepository;
use App\Repositories\User\UserRepository;
use App\Validators\HackerRankTestResultValidator;
use App\Transformers\HackerRankTestResultTransformer;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Contracts\ValidatorInterface;
use App\Services\ThirdParty\HackerRank;
use App\Criterias\HackerRankTestResult\WithEmployee;
use App\Criterias\HackerRankTestResult\AddSelectEmployee;
use App\Criterias\HackerRankTestResult\WithHackerRankTest;
use App\Criterias\HackerRankTestResult\WithHackerRankTestResult;
use App\Criterias\HackerRankTestResult\WithSkills;
use App\Criterias\HackerRankTestResult\SearchEmployee;
use App\Criterias\HackerRankTestResult\SearchHackerRankTest;
use App\Criterias\HackerRankTestResult\SearchSkill;
use App\Criterias\HackerRankTestResult\AddSelectHackerRankTest;
use App\Criterias\HackerRankTestResult\AddSelect;
use App\Criterias\HackerRankTestResult\AddSelectStatus;
use App\Criterias\HackerRankTestResult\AddSelectSkill;
use App\Criterias\HackerRankTestResult\WithEmployeeStatuses;

class HackerRankTestResultController extends BaseController
{
    public function __construct(
        HackerRankTestResultRepository $repository,
        UserRepository $userRepository,
        HackerRankTestResultValidator $validator,
        HackerRankTestResultTransformer $transformer
    )
    {
        $this->repository  = $repository;
        $this->userRepository = $userRepository;
        $this->validator   = $validator;
        $this->transformer = $transformer;
    }

    /**
     * GET list of Hacker Rank Test Results
     *
     * @param  Dingo\Api\Http\Request
     * @return Test Results Collection
     */
    public function index(Request $request)
    {
        
        if ($request->get('search')) {
            $this->repository->pushCriteria(new SearchHackerRankTest($request->get('search')));
            $this->repository->pushCriteria(new SearchEmployee($request->get('search')));
            $this->repository->pushCriteria(new SearchSkill($request->get('search')));
        }
        $this->repository->pushCriteria(new AddSelectStatus());
        $this->repository->pushCriteria(new WithEmployeeStatuses());
        $this->repository->pushCriteria(new WithEmployee());
        $this->repository->pushCriteria(new AddSelectEmployee());
        $this->repository->pushCriteria(new AddSelectSkill());
        $this->repository->pushCriteria(new WithHackerRankTest()); 
        $this->repository->pushCriteria(new AddSelectHackerRankTest());
        $this->repository->pushCriteria(new AddSelect());

        return parent::index($request);
    }

    public function fetchAndImportTestResults(Request $request)
    {
        set_time_limit(0); //We cannot determine the exact time it spends fetching records from hacker ranks.
        $modelObj = app($this->repository->model());
        $this->authorize('update', $modelObj);

        try {
            $hackerRank = new HackerRank;

            if ($request->get('id')) {
                $fetchHackerTestResults = $hackerRank->getAllTestResultsByTestId($request->get('id'));
            } else {
                $fetchHackerTestResults = $hackerRank->getAllTestResults();
            }

            foreach($fetchHackerTestResults as $testResult) {
                $employeeId = $this->userRepository->generateEmployeeId($testResult->email);
                $checkTestResult = $this->repository->findByField('result_id', $testResult->id)->first();

                if (empty($checkTestResult) && !empty($employeeId)) {
                    $hackerTestResult = array(
                        'result_id' => $testResult->id,
                        'test_id'   => $testResult->test,
                        'employee_id'   => $employeeId,
                        'raw_score'   => $testResult->score,
                        'percent_score'   => $testResult->percentage_score,
                        'published'   => true,
                        'date_time_start_taken' => date('Y-m-d H:i:s', strtotime($testResult->attempt_starttime)),
                        'date_time_end_taken' => date('Y-m-d H:i:s', strtotime($testResult->attempt_endtime))
        
                    );

                    $result = $this->repository->create($hackerTestResult);

                }                
            }

            return response()->json([
                'status_code' => 200,
                'message' => 'Data Successfully fetched and saved!'
            ], 200);

        } catch (ValidatorException $e) {
            return response()->json([
                'status_code' => 400,
                'message' => $e->getMessageBag()
            ], 400);
        }        


    }
}
