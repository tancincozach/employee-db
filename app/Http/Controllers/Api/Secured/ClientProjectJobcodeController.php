<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\ClientProjectJobcode\ClientProjectJobcodeRepository;
use App\Validators\ClientProjectJobcodeValidator;
use App\Transformers\ClientProjectJobcodeTransformer;
use Dingo\Api\Http\Request;
use App\Criterias\ClientProjectJobcode\SearchClientProjectJobcode;
use \Prettus\Validator\Contracts\ValidatorInterface;
use App\Criterias\ClientProjectJobcode\ExcludeNonWorkJobcodes;

class ClientProjectJobcodeController extends BaseController
{
    public function __construct(ClientProjectJobcodeRepository $repository, ClientProjectJobcodeValidator $validator, ClientProjectJobcodeTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    /**
     * Get list of Job Positions
     *
     * @return Collection
     */
    public function index(Request $request)
    {
        // see if we have searches
        if ($request->get('search')) {
            $this->repository->pushCriteria(new SearchClientProjectJobcode($request->get('search')));
        }

        $this->repository->pushCriteria(new ExcludeNonWorkJobcodes());

        return parent::index($request);
    }

    public function store(Request $request)
    {
        return \App\Models\ClientProjectJobcode::where('id', $request->get('id'))
                ->update(['client_project_id' => $request->get('client_project_id')]);
    }
}
