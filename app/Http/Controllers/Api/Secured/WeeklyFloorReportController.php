<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\WeeklyFloorReport\WeeklyFloorReportRepository;
use App\Transformers\WeeklyFloorReportTransformer;
use App\Validators\WeeklyFloorReportValidator;
use App\Services\WeeklyFloorReportService;
use App\Criterias\WeeklyFloorReport\FilterByCreatedId;
use App\Criterias\WeeklyFloorReport\FilterByFrequency;
use App\Criterias\WeeklyFloorReport\OrderByValue;
use App\Criterias\WeeklyFloorReport\SearchByClientOrReporter;
use Dingo\Api\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

/**
 * Class WeeklyFloorReportController
 * @package App\Http\Controllers\Api\Secured
 */
class WeeklyFloorReportController extends BaseController
{
    /**
     * WeeklyFloorReportController constructor.
     * @param WeeklyFloorReportRepository $repository
     * @param WeeklyFloorReportTransformer $transformer
     * @param WeeklyFloorReportValidator $validator
     */
    public function __construct(
        WeeklyFloorReportRepository $repository,
        WeeklyFloorReportTransformer $transformer,
        WeeklyFloorReportValidator $validator
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @return Resource
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        if ($request->get('created_by')) {
            $this->repository->pushCriteria(new FilterByCreatedId($request->get('created_by')));
        }

        if ($request->get('frequency')) {
            $this->repository->pushCriteria(new FilterByFrequency($request->get('frequency')));
        }

        if ($request->get('search')) {
            $this->repository->pushCriteria(new SearchByClientOrReporter($request->get('search')));
        }

        $this->repository->pushCriteria(new OrderByValue('weekly_floor_report.created_at'));

        return parent::index($request);
    }

    public function completeSave(Request $request)
    {
        WeeklyFloorReportService::completeSave($request);
    }
}
