<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\Characteristic\CharacteristicRepository;
use App\Transformers\CharacteristicTransformer;
use App\Validators\CharacteristicValidator;

/**
 * Class CharacteristicController
 * @package App\Http\Controllers\Api\Secured
 */
class CharacteristicController extends BaseController
{
    /**
     * CharacteristicController constructor.
     * @param CharacteristicRepository $repository
     * @param CharacteristicValidator $validator
     * @param CharacteristicTransformer $transformer
     */
    public function __construct(
        CharacteristicRepository $repository,
        CharacteristicValidator $validator,
        CharacteristicTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }
}
