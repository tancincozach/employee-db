<?php

namespace App\Http\Controllers\Api\Secured;

use Dingo\Api\Http\Request;

use App\Http\Controllers\Api\APIBaseController as BaseController;

use App\Repositories\Timesheet\TimesheetReportRepository;
use App\Repositories\Timesheet\TimesheetJobcodeRepository;
use App\Repositories\Client\ClientRepository;

use App\Transformers\TimesheetReportTransformer;
use App\Transformers\Timesheet\DailyTrackerTransformer;
use App\Transformers\Timesheet\MonthlySummaryTransformer;
use App\Transformers\Timesheet\WeeklyTransformer;

use App\Criterias\Timesheet\ByEmployees;
use App\Criterias\Timesheet\ByLogDate;
use App\Criterias\Timesheet\ByJobcode;
use App\Criterias\Timesheet\ByClient;
use App\Criterias\Timesheet\ExcludeLunchBreak;
use App\Criterias\Timesheet\ByMonthYear;
use App\Criterias\TimesheetJobcodeAssignment\ByClient as ByCurrentClient;
use App\Criterias\Timesheet\Weekly;

use App\Events\GenerateTimesheetEvent;

use App\Services\ThirdParty\Timesheet;
use App\Services\TimesheetMapService;

class TimesheetReportController extends BaseController
{
    public function __construct(
        TimesheetReportRepository $repository,
        TimesheetReportTransformer $transformer
    ) {
        $this->repository  = $repository;
        $this->transformer = $transformer;
    }

    /**
     * Daily tracker resides in this function
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function index(Request $request)
    {
        // initialize meta values
        $jobcodesMeta = [];
        $jobcodesTotalHrsMeta = [];

        // Daily Tracker
        if ($request->get('daily')) {
            $jobcodesForTotal = [];

            // Required filters
            $employeeId = $request->get('employeeId');
            $startDate = $request->get('startDate');
            $endDate = $request->get('endDate');

            $this->transformer = app(DailyTrackerTransformer::class);
            $this->repository->pushCriteria(new ByEmployees([$employeeId]));
            $this->repository->pushCriteria(new ByLogDate($startDate, $endDate));

            // When the current user is a client
            // Initialize the client id
            $clientId = 0;
            if ($request->get('clientId')) {
                $clientId = $request->get('clientId');
                $client = app(ClientRepository::class);
                $this->transformer->setClientTz($client->getTimezone($clientId));
                $this->repository->pushCriteria(new ByClient([$clientId]));

                // As requirement, we will not show Rest/Lunch Break to clients.
                // Especially when employee has many clients handled.
                // e.g, Project Managers, QA
                $this->repository->pushCriteria(new ExcludeLunchBreak());
            }

            // This request is not avilable to clients.
            // We will not allow clients to view data to each other.
            if ($request->get('jobcodeIds')) {
                $jobcodeIds = $request->get('jobcodeIds');
                $jobcodeRepository = app(TimesheetJobcodeRepository::class);
                $jobcodes = array_merge($jobcodeIds, $jobcodeRepository->getNonRegular());
                $jobcodesForTotal = array_merge($jobcodesForTotal, $jobcodeIds);
                $this->repository->pushCriteria(new ByJobcode($jobcodes));
            }

            if ($convertTz = filter_var($request->get('convertTz'), FILTER_VALIDATE_BOOLEAN)) {
                $this->transformer->isClientTz($convertTz);
            }

            $jobcodesMeta = $this->repository->getAssociatedJobcodes(
                $employeeId,
                $startDate,
                $endDate
            );

            $jobcodesTotalHrsMeta = $this->repository->getJobcodeTotalHours(
                $employeeId,
                $startDate,
                $endDate,
                $jobcodesForTotal,
                $clientId
            );
        }

        // additional meta
        $response = parent::index($request);
        $overallTotal = number_format(array_sum(array_pluck($jobcodesTotalHrsMeta, 'total_hours')), 2);
        $meta = array_merge(
            $response->getMeta(),
            ['jobcodes'       => $jobcodesMeta],
            ['jobcodes_total' => $jobcodesTotalHrsMeta],
            ['overall_total'  => $overallTotal]
        );
        $response->setMeta($meta);

        return $response;
    }

    /**
     * Get monthly hours summary
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function monthly(Request $request)
    {
        // Required filters
        $month = $request->get('month');
        $year = $request->get('year');

        // Optional filters
        if ($employeeIds = $request->get('employeeIds')) {
            $this->repository->pushCriteria(new ByEmployees($employeeIds));
        }

        if ($jobcodeIds = $request->get('jobcodeIds')) {
            $this->repository->pushCriteria(new ByJobcode($jobcodeIds));
        }

        // If the current user is a client
        if ($clientId = $request->get('clientId')) {
            $this->repository->pushCriteria(new ByCurrentClient($clientId));
        }

        $this->transformer = app(MonthlySummaryTransformer::class);
        $this->repository->pushCriteria(new ByMonthYear($month, $year));

        return parent::index($request);
    }

    /**
     * Get weekly report
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function weekly(Request $request)
    {
        // required filters
        $jobcodeId = $request->get('jobcodeId');
        $start = $request->get('start');
        $end = $request->get('end');
        $year = $request->get('year');

        $this->repository->pushCriteria(new Weekly($jobcodeId, $start, $end));
        $this->transformer = app(WeeklyTransformer::class);

        return $this->repository->weekly($this->repository->get(), $year, [$start, $end]);
    }

    /*
     * Generate timesheets
     *
     * @param Dingo\Api\Http\Request $request
     *
     * @return void
     */
    public function generate(Request $request)
    {
        $command = 'cd ' . base_path() . ' && nohup php artisan timesheet:generate-raw';

        if ($ids = $request->get('ids')) {
            $ids = implode(',', $ids);
            $command .= ' --ids=' . $ids;
        }

        if ($jobcodeIds = $request->get('jobcodeIds')) {
            $jobcodeIds = implode(',', $jobcodeIds);
            $command .= ' --jobcode_ids=' . $jobcodeIds;
        }

        if ($userIds = $request->get('userIds')) {
            $userIds = implode(',', $userIds);
            $command .= ' --user_ids=' . $userIds;
        }

        if ($startDate = $request->get('startDate')) {
            $command .= ' --start_date=' . $startDate;
        }

        if ($endDate = $request->get('endDate')) {
            $command .= ' --end_date=' . $endDate;
        }

        if ($modifiedSince = $request->get('modifiedSince')) {
            $command .= ' --modified_since=' . $modifiedSince;
        }

        if ($modifiedBefore = $request->get('modifiedBefore')) {
            $command .= ' --modified_before=' . $modifiedBefore;
        }

        if ($jobcodeType = $request->get('jobcodeType')) {
            $command .= ' --jobcode_type=' . $jobcodeType;
        }

        if ($onTheClock = $request->get('onTheClock')) {
            $command .= ' --on_the_clock=' . $onTheClock;
        }

        event(new GenerateTimesheetEvent($command));

        return response()->json([
            'status_code' => 200,
            'message'     => 'Override Weekly Report is now processing!',
        ]);
    }

    /*
     * Map raw timesheets data
     *
     * @param TimesheetMapService $service
     *
     * @return void
     */
    public function map(TimesheetMapService $service)
    {
        $service->execute();
    }

    /*
     * Get process running
     *
     * @param Dingo\Api\Http\Request
     *
     * @return Collection
     */
    public function getGenerationResponse(Request $request)
    {
        return Timesheet::getProcessRunning();
    }

    /**
     * Disable creating data
     *
     * @param Dingo\Api\Http\Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable updating data
     *
     * @param Dingo\Api\Http\Request $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable deleting data
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }
}
