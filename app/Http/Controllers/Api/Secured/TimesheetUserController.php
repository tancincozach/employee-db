<?php

namespace App\Http\Controllers\Api\Secured;

use \Prettus\Validator\Contracts\ValidatorInterface;
use Dingo\Api\Http\Request;

use App\Http\Controllers\Api\APIBaseController as BaseController;

use App\Repositories\Timesheet\TimesheetUserRepository;
use App\Repositories\Timesheet\EmployeeTimesheetUserRepository;
use App\Validators\TimesheetUserValidator;
use App\Transformers\TimesheetUserTransformer;
use App\Transformers\Timesheet\TimesheetUserNamesTransformer;
use App\Transformers\Timesheet\MappedEmployeeTransformer;

use App\Criterias\TimesheetUser\WithMappedEmployees;
use App\Criterias\TimesheetUser\ByUserIds;
use App\Criterias\TimesheetUser\OrderBy;
use App\Criterias\TimesheetUser\ById;

class TimesheetUserController extends BaseController
{
    public function __construct(
        TimesheetUserRepository $repository,
        TimesheetUserValidator $validator,
        TimesheetUserTransformer $transformer
    ) {
        $this->repository  = $repository;
        $this->validator   = $validator;
        $this->transformer = $transformer;
    }

    /**
     * GET list of resource
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function index(Request $request)
    {
        if ($clientId = $request->get('clientId')) {
            $userIds = $this->repository->getIdsByClient($clientId);
            $this->repository->pushCriteria(new ByUserIds($userIds));
        }

        if (filter_var($request->get('namesOnly'), FILTER_VALIDATE_BOOLEAN)) {
            $this->transformer = app(TimesheetUserNamesTransformer::class);
        }

        if (filter_var($request->get('withMapping'), FILTER_VALIDATE_BOOLEAN)) {
            $this->repository->pushCriteria(new WithMappedEmployees());
            $this->transformer = app(MappedEmployeeTransformer::class);
        }

        if ($employeeId = $request->get('employeeId')) {
            $id = app(EmployeeTimesheetUserRepository::class)->getTimesheetUserId($employeeId);
            $this->repository->pushCriteria(new ById($id));
        }

        $this->repository->pushCriteria(new OrderBy('last_name'));

        return parent::index($request);
    }

    /**
     * Disable creating data
     *
     * @param Dingo\Api\Http\Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable updating data
     *
     * @param Dingo\Api\Http\Request $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable deleting data
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }
}
