<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\PositionType\PositionTypeRepository;
use App\Transformers\PositionTypeTransformer;
use App\Validators\PositionTypeValidator;

/**
 * Class PositionTypeController
 * @package App\Http\Controllers\Api\Secured
 */
class PositionTypeController extends BaseController
{
    /**
     * PositionTypeController constructor.
     * @param PositionTypeRepository $repository
     * @param PositionTypeValidator $validator
     * @param PositionTypeTransformer $transformer
     */
    public function __construct(
        PositionTypeRepository $repository,
        PositionTypeValidator $validator,
        PositionTypeTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }
}
