<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Repositories\RouteWizard\RouteWizardRepository;
use App\Validators\RouteWizardValidator;
use App\Transformers\RouteWizardTransformer;
use App\Criterias\RouteWizard\SearchByAll;

class RouteWizardController extends BaseController
{
    //
    public function __construct(RouteWizardRepository $repository, RouteWizardValidator $validator, RouteWizardTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request)
    {
        if ($request->get('search')) {
            $this->repository->pushCriteria(new SearchByAll($request->get('search')));
        }

        return parent::index($request);
    }
}
