<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\CreativeServiceFile\CreativeServiceFileRepository;
use App\Transformers\CreativeServiceFileTransformer;
use Dingo\Api\Http\Request;
use Illuminate\Api\Http\Response;
use App\Validators\CreativeServiceFileValidator;
use App\Services\UploadService;
use App\Criterias\CreativeServiceFile\SearchByCreativeServiceId;

/**
 * Class CreativeServiceFileController
 * @package App\Http\Controllers\Api\Secured
 */
class CreativeServiceFileController extends BaseController
{
    /**
     * CreativeServiceFileController constructor.
     * @param CreativeServiceFileRepository $repository
     * @param CreativeServiceFileValidator $validator
     * @param CreativeServiceFileTransformer $transformer
     */
    public function __construct(
        CreativeServiceFileRepository $repository,
        CreativeServiceFileValidator $validator,
        CreativeServiceFileTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request) {
        if ($request->get('creative_service_id')) {
            $this->repository->pushCriteria(new SearchByCreativeServiceId($request->get('creative_service_id')));
        }
        return parent::index($request);
    }

    public function getCreativeFile(Request $request) {
        if ($request->get('path')) {
            $uploaded = new UploadService($this->repository, $this->validator, $this->transformer);
            return $uploaded->getAsset($request->get('path'), ($request->get('fd') ? true : false));
        }
    }

    /**
     * Upload UploadService
     *
     * @param  Dingo\Api\Http\Request $request
     * @return Resource Item
     */
    public function store(Request $request)
    {
        try {
            $uploaded = new UploadService($this->repository, $this->validator, $this->transformer);
            $result = $uploaded->getResult($request);

            return $this->response->item($result, $this->transformer);
        } catch (Exception $e) {
            return response()->json(
                [
                    'status_code' => 400,
                    'message' => $e->getMessageBag()
                ], 400
            );
        }
    }
}
