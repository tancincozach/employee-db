<?php

namespace App\Http\Controllers\Api\Secured;

use \Prettus\Validator\Contracts\ValidatorInterface;
use Dingo\Api\Http\Request;

use App\Http\Controllers\Api\APIBaseController as BaseController;

use App\Repositories\Timesheet\JobcodeAssignmentRepository;
use App\Validators\JobcodeAssignmentValidator;
use App\Transformers\Timesheet\JobcodeAssignmentTransformer;

class JobcodeAssignmentController extends BaseController
{
    public function __construct(
        JobcodeAssignmentRepository $repository,
        JobcodeAssignmentValidator $validator,
        JobcodeAssignmentTransformer $transformer
    ) {
        $this->repository  = $repository;
        $this->validator   = $validator;
        $this->transformer = $transformer;
    }

    /**
     * Get list of resource
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function index(Request $request)
    {
        return parent::index($request);
    }

    /**
     * Disable creating data
     *
     * @param Dingo\Api\Http\Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable updating data
     *
     * @param Dingo\Api\Http\Request $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable deleting data
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }
}
