<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\FeedbackResponse\FeedbackResponseRepository;
use App\Transformers\FeedbackResponseTransformer;
use App\Validators\FeedbackResponseValidator;

/**
 * Class FeedbackResponseController
 * @package App\Http\Controllers\Api\Secured
 */
class FeedbackResponseController extends BaseController
{
    /**
     * FeedbackController constructor.
     * @param FeedbackResponseRepository $repository
     * @param FeedbackResponseValidator $validator
     * @param FeedbackResponseTransformer $transformer
     */
    public function __construct(
        FeedbackResponseRepository $repository,
        FeedbackResponseValidator $validator,
        FeedbackResponseTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }
}
