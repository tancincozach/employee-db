<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\Employee\EmployeeWorkShiftRepository;
use App\Validators\EmployeeWorkShiftValidator;
use App\Transformers\EmployeeWorkShiftTransformer;
use Dingo\Api\Http\Request;
use App\Models\WorkShift;
use App\Models\EmployeeWorkShift;

class EmployeeWorkShiftController extends BaseController
{
    public function __construct(EmployeeWorkShiftRepository $repository, EmployeeWorkShiftValidator $validator, EmployeeWorkShiftTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function store(Request $request)
    {
        if(!$this->has_workshifts($request))
            return parent::store($request);
        return null;
    }

    public function update(Request $request, $id)
    {
        if(!$this->has_workshifts($request, $id))
          return parent::update($request, $id);
        else
          return parent::destroy($id);
    }

    private function has_workshifts($request, $id = null)
    {
        $employee_id = $request->get('employee_id');
        $shift_id    = $request->get('shift_id');
        $start_time  = $request->get('start_time');
        $end_time    = $request->get('end_time');

        if($shift = WorkShift::find($shift_id)) {
            $start_time = $shift->start_time;
            $end_time   = $shift->end_time;
        }

        $result = EmployeeWorkShift::where('employee_id', $employee_id);

        if($id)
          $result->where('id', '!=', $id);

        $result->where(function($query) use ($shift_id, $start_time, $end_time) {

                if($shift_id)
                  $query->where('shift_id', $shift_id);

                if($start_time)
                    $query->orWhere('start_time', 'LIKE', '%' . $start_time . '%');

                if($end_time)
                    $query->orWhere('end_time', 'LIKE', '%' . $end_time . '%');

                return $query;
            });
        return $result->exists();
    }
}
