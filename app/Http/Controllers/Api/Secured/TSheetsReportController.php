<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Services\ThirdParty\TSheets;
use Carbon\Carbon;

class TSheetsReportController extends BaseController
{
    public function retrieveAndStoreReports()
    {
        $yesterday = Carbon::yesterday()->format('Y-m-d');
        $tsheets = new TSheets($yesterday, $yesterday, true);
        $tsheets->retrieveAndStore();
    }
}
