<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\Employee\EmployeeReportRepository;
use App\Transformers\EmployeeCalendarReportTransformer;
use App\Validators\EmployeeReportValidator;
use App\Criterias\EmployeeReport\SearchByEmployee;
use App\Criterias\EmployeeReport\SearchByEmployeeIDS;
use App\Criterias\EmployeeReport\EmployeeCalendarReport;
use App\Criterias\EmployeeReport\FilterByDate;
use App\Models\ReportTemplate;
use App\Mail\DailyReport;
use Dingo\Api\Http\Request;

class EmployeeCalendarReportController extends BaseController
{
    public function __construct(EmployeeReportRepository $repository, EmployeeReportValidator $validator, EmployeeCalendarReportTransformer $transformer)
    {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->validator = $validator;
    }
    /**
     * Get list of employee reports
     *
     * @return Collection
     */
    public function index(Request $request)
    {             
        if ($request->get('employee_id')) {
            if(strpos($request->get('employee_id'),',') !== false){
                $empIDS = explode(",",$request->get('employee_id'));
                $this->repository->pushCriteria(new SearchByEmployeeIDS($empIDS));
            }else{
                $this->repository->pushCriteria(new SearchByEmployee($request->get('employee_id')));                  
            }                
        }
        $this->repository->pushCriteria(new EmployeeCalendarReport());
        if ($request->get('report_date')) {
            $date = $request->get('report_date');
            if(strpos($date,':') !== false){
                $date_arr = explode(":",$date);
                $this->repository->pushCriteria(new FilterByDate($date_arr[0], $date_arr[1]));
            }else{
                $this->repository->pushCriteria(new FilterByDate($date, $date));
            }
        }              
        return parent::index($request);
    }

    public function preview(Request $request)
    {
        try {
            $report = new DailyReport($request);
            $employee = Employee::where('id', $request->employee_id)->first();
            $template = ReportTemplate::where('id', $request->report_template_id)->first();
            $report->setEmployeeAndTemplate($employee, $template);
            return ['preview' => $report->render()];
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status_code' => 404,
                'message' => 'Can\'t find the requested resource.'
            ], 404);
        }
    }
}

