<?php

namespace App\Http\Controllers\Api\Secured;

use App\Events\FeedbackEvent;
use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\Feedback\FeedbackRepository;
use App\Transformers\FeedbackTransformer;
use App\Validators\FeedbackValidator;
use App\Criterias\Feedback\WithEmployee;
use App\Criterias\Feedback\SearchEmployee;
use App\Criterias\Feedback\SearchCategory;
use App\Criterias\Feedback\WithFeedbackCategory;
use App\Criterias\Feedback\AddSelectFields;
use App\Criterias\Feedback\AddSelectCategory;
use App\Criterias\Feedback\AddSelectEmployee;
use Dingo\Api\Http\Request;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class FeedbackController
 * @package App\Http\Controllers\Api\Secured
 */
class FeedbackController extends BaseController
{
    /**
     * FeedbackController constructor.
     * @param FeedbackRepository $repository
     * @param FeedbackValidator $validator
     * @param FeedbackTransformer $transformer
     */
    public function __construct(
        FeedbackRepository $repository,
        FeedbackValidator $validator,
        FeedbackTransformer $transformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    /**
     * @param Request $request
     * @return Resource
     * @throws RepositoryException
     */
    public function index(Request $request)
    {
        if ($request->get('search')) {
            $this->repository->pushCriteria(new SearchCategory($request->get('search')));
            $this->repository->pushCriteria(new SearchEmployee($request->get('search')));
        }

        $this->repository->pushCriteria(new WithFeedbackCategory());
        $this->repository->pushCriteria(new WithEmployee());
        $this->repository->pushCriteria(new AddSelectFields());
        $this->repository->pushCriteria(new AddSelectCategory());
        $this->repository->pushCriteria(new AddSelectEmployee());

        return parent::index($request);
    }

    /**
     * @param Request $request
     * @return Resource
     */
    public function store(Request $request)
    {
        $result = parent::store($request);
        $feedback = $result->getOriginalContent();

        event(new FeedbackEvent($feedback));

        return $result;
    }

    /**
     * @param int $id
     * @return Resource
     * @throws RepositoryException
     */
    public function show($id)
    {
        $this->repository->pushCriteria(new WithEmployee());
        $this->repository->pushCriteria(new AddSelectFields());
        $this->repository->pushCriteria(new AddSelectEmployee());

        return parent::show($id);
    }
}
