<?php

namespace App\Http\Controllers\Api\Secured;

use Dingo\Api\Http\Request;
use App\Http\Controllers\Api\APIBaseController as BaseController;

use App\Repositories\Timesheet\TimesheetRepository;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Timesheet\TimesheetJobcodeRepository;

use App\Validators\TimesheetValidator;

use App\Transformers\TimesheetTransformer;
use App\Transformers\Timesheet\DailyTrackerTransformer;
use App\Transformers\Timesheet\MonthlySummaryTransformer;
use App\Transformers\Timesheet\WeeklyTransformer;

// Daily Tracker
use App\Criterias\Timesheet\Daily;
use App\Criterias\Timesheet\ByUserId;
use App\Criterias\Timesheet\ByDate;
use App\Criterias\Timesheet\ByClient;
use App\Criterias\Timesheet\ByJobcode;
use App\Criterias\Timesheet\ExcludeLunchBreak;

// Monthly
use App\Criterias\Timesheet\Monthly;
use App\Criterias\Timesheet\ByMonthYear;

// Weekly
use App\Criterias\Timesheet\Weekly;

use App\Events\GenerateTimesheetEvent;
use App\Events\GenerateJobcodeAssignmentEvent;
use App\Services\ThirdParty\Timesheet as TimesheetService;

class TimesheetController extends BaseController
{
    public function __construct(
        TimesheetRepository $repository,
        TimesheetValidator $validator,
        TimesheetTransformer $transformer
    ) {
        $this->repository  = $repository;
        $this->validator   = $validator;
        $this->transformer = $transformer;
    }

    /**
     * Daily tracker resides in this function
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function index(Request $request)
    {
        // initialize meta values
        $jobcodesMeta = [];
        $jobcodesTotalHrsMeta = [];

        if (filter_var($request->get('daily'), FILTER_VALIDATE_BOOLEAN)) {
            $jobcodesForTotal = [];

            // required filters
            $employeeId = $request->get('employeeId');
            $startDate = $request->get('startDate');
            $endDate = $request->get('endDate');

            $this->repository->pushCriteria(new Daily());
            $this->transformer = app(DailyTrackerTransformer::class);

            $this->repository->pushCriteria(new ByUserId([$employeeId]));
            $this->repository->pushCriteria(new ByDate($startDate, $endDate));

            // When the current user is a client
            // Initialize the client id
            $clientId = 0;
            if ($request->get('clientId')) {
                $clientId = $request->get('clientId');
                $client = app(ClientRepository::class);
                $this->transformer->setClientTz($client->getTimezone($clientId));
                $this->repository->pushCriteria(new ByClient([$clientId]));

                // As requirement, we will not show Rest/Lunch Break to clients.
                // Especially when employee has many clients handled.
                // e.g, Project Managers, QA
                $this->repository->pushCriteria(new ExcludeLunchBreak());
            }

            // This request is not avilable to clients.
            // We will not allow clients to view data to each other.
            if ($request->get('jobcodeIds')) {
                $jobcodeIds = $request->get('jobcodeIds');
                $jobcodeRepository = app(TimesheetJobcodeRepository::class);
                $jobcodes = array_merge($jobcodeIds, $jobcodeRepository->getNonRegular());
                $jobcodesForTotal = array_merge($jobcodesForTotal, $jobcodeIds);
                $this->repository->pushCriteria(new ByJobcode($jobcodes));
            }

            if ($convertTz = filter_var($request->get('convertTz'), FILTER_VALIDATE_BOOLEAN)) {
                $this->transformer->convertTz();
            }

            $jobcodesMeta = $this->repository->getAssociatedJobcodes(
                $employeeId,
                $startDate,
                $endDate
            );

            $jobcodesTotalHrsMeta = $this->repository->getJobcodeTotalHours(
                $employeeId,
                $startDate,
                $endDate,
                $jobcodesForTotal,
                $clientId
            );
        }

        // additional meta
        $response = parent::index($request);
        $overallTotal = number_format(array_sum(array_pluck($jobcodesTotalHrsMeta, 'total_hours')), 2);
        $meta = array_merge(
            $response->getMeta(),
            ['jobcodes'       => $jobcodesMeta],
            ['jobcodes_total' => $jobcodesTotalHrsMeta],
            ['overall_total'  => $overallTotal]
        );
        $response->setMeta($meta);

        return $response;
    }

    /**
     * Get monthly hours summary
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function monthly(Request $request)
    {
        // Required filters
        $month = $request->get('month');
        $year = $request->get('year');

        // Optional filters
        if ($employeeIds = $request->get('employeeIds')) {
            $this->repository->pushCriteria(new ByUserId($employeeIds));
        }

        if ($jobcodeIds = $request->get('jobcodeIds')) {
            $this->repository->pushCriteria(new ByJobcode($jobcodeIds));
        }

        $this->transformer = app(MonthlySummaryTransformer::class);
        $this->transformer->setYearMonth($year, $month);

        // If the current user is a client
        if ($clientId = $request->get('clientId')) {
            $jobcodeId = $this->repository->getJobcodeIdByClient($clientId);
            $this->repository->pushCriteria(new ByJobcode([$jobcodeId]));

            $client = app(ClientRepository::class);
            $this->transformer->setClientTz($client->getTimezone($clientId[0]));
        }

        $this->repository->pushCriteria(new Monthly());
        $this->repository->pushCriteria(new ByMonthYear($month, $year));

        return parent::index($request);
    }

    /**
     * Get weekly report
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function weekly(Request $request)
    {
        // required filters
        $jobcodeId = $request->get('jobcodeId');
        $start = $request->get('start');
        $end = $request->get('end');
        $year = $request->get('year');

        $this->repository->pushCriteria(new Weekly($jobcodeId, $start, $end));
        $this->transformer = app(WeeklyTransformer::class);

        return $this->repository->weekly($this->repository->get(), $year, [$start, $end]);
    }

    /*
     * Generate timesheets
     *
     * @param Dingo\Api\Http\Request $request
     *
     * @return void
     */
    public function generate(Request $request)
    {
        $command = 'cd ' . base_path() . ' && nohup php artisan timesheet:generate-raw';

        if ($ids = $request->get('ids')) {
            $ids = implode(',', $ids);
            $command .= ' --ids=' . $ids;
        }

        if ($jobcodeIds = $request->get('jobcodeIds')) {
            $jobcodeIds = implode(',', $jobcodeIds);
            $command .= ' --jobcode_ids=' . $jobcodeIds;
        }

        if ($userIds = $request->get('userIds')) {
            $userIds = implode(',', $userIds);
            $command .= ' --user_ids=' . $userIds;
        }

        if ($startDate = $request->get('startDate')) {
            $command .= ' --start_date=' . $startDate;
        }

        if ($endDate = $request->get('endDate')) {
            $command .= ' --end_date=' . $endDate;
        }

        if ($modifiedSince = $request->get('modifiedSince')) {
            $command .= ' --modified_since=' . $modifiedSince;
        }

        if ($modifiedBefore = $request->get('modifiedBefore')) {
            $command .= ' --modified_before=' . $modifiedBefore;
        }

        if ($jobcodeType = $request->get('jobcodeType')) {
            $command .= ' --jobcode_type=' . $jobcodeType;
        }

        if ($onTheClock = $request->get('onTheClock')) {
            $command .= ' --on_the_clock=' . $onTheClock;
        }

        event(new GenerateTimesheetEvent($command));

        return response()->json([
            'status_code' => 200,
            'message'     => 'Override Weekly Report is now processing!',
        ]);
    }

    /*
     * Map raw timesheets data
     *
     * @param TimesheetMapService $service
     *
     * @return void
     */
    public function map(TimesheetMapService $service)
    {
        $service->execute();
    }

    /*
     * Get process running
     *
     * @return Collection
     */
    public function getGenerationResponse()
    {
        return TimesheetService::getProcessRunning();
    }

    /*
     * Generate jobcode assignments
     *
     * @param Dingo\Api\Http\Request $request
     *
     * @return void
     */
    public function generateJobcodeAssignments(Request $request)
    {
        $command = 'cd ' . base_path() . ' && nohup php artisan timesheet:generate-assignments';

        if ($userIds = $request->get('userIds')) {
            $userIds = implode(',', $userIds);
            $command .= ' --user_ids=' . $userIds;
        }

        if ($type = $request->get('type')) {
            $command .= ' --type=' . $type;
        }

        if ($jobcodeId = $request->get('jobcodeId')) {
            $command .= ' --jobcode_id=' . $jobcodeId;
        }

        if ($active = $request->get('active')) {
            $command .= ' --active=' . $active;
        }

        if ($modifiedSince = $request->get('modifiedSince')) {
            $command .= ' --modified_since=' . $modifiedSince;
        }

        if ($modifiedBefore = $request->get('modifiedBefore')) {
            $command .= ' --modified_before=' . $modifiedBefore;
        }

        event(new GenerateJobcodeAssignmentEvent($command));

        return response()->json([
            'status_code' => 200,
            'message'     => 'Generating timesheet jobcode assignments!',
        ]);
    }

    /**
     * Disable creating data
     *
     * @param Dingo\Api\Http\Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable updating data
     *
     * @param Dingo\Api\Http\Request $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }

    /**
     * Disable deleting data
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        return response()->json([
            'status_code' => 500,
            'message'     => 'Bad request',
        ]);
    }
}
