<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;

use App\Repositories\Employee\EmployeeRepository;
use App\Transformers\Lead\EmployeeTransformer as EmployeeTransformer;
use App\Helpers\EmployeeAvailabilityHelper;
use App\Models\Employee;

use App\Criterias\Employee\SearchByIds;
use App\Criterias\Employee\EmployeeOnly;
use App\Criterias\Employee\FilterByStatus;
use App\Criterias\Employee\WithPosition;
use App\Criterias\Employee\WithStatus;
use App\Criterias\Employee\WithEmployeeSkill;
use App\Criterias\Employee\Select;

class LeadController extends BaseController
{
    public function __construct(
        EmployeeRepository $repository,
        EmployeeTransformer $transformer,
        EmployeeAvailabilityHelper $helper
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->helper = $helper;
    }

    /*
     * Leads generation specific retrieving of employees
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->has('available')) {
            $available = filter_var($request->get('available'), FILTER_VALIDATE_BOOLEAN);
            $this->repository->pushCriteria(new SearchByIds($this->helper->getIds($available)));
        } else {
            $this->repository->pushCriteria(new SearchByIds($this->helper->getDeployableIds()));
        }

        $this->repository->pushCriteria(new EmployeeOnly());
        $this->repository->pushCriteria(new FilterByStatus(Employee::ACTIVE_EMPLOYEE_STATUS));

        $this->repository->pushCriteria(new WithPosition());
        $this->repository->pushCriteria(new WithStatus());
        $this->repository->pushCriteria(new WithEmployeeSkill());
        $this->repository->pushCriteria(new Select());

        $this->transformer->setDefaultIncludes([]);

        return parent::index($request);
    }
}
