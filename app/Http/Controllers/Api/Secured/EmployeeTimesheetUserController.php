<?php

namespace App\Http\Controllers\Api\Secured;

use \Prettus\Validator\Contracts\ValidatorInterface;
use Dingo\Api\Http\Request;

use App\Http\Controllers\Api\APIBaseController as BaseController;

use App\Repositories\Timesheet\EmployeeTimesheetUserRepository;
use App\Validators\EmployeeTimesheetUserValidator;
use App\Transformers\EmployeeTimesheetUserTransformer;
use App\Transformers\EmployeeTimesheetUser\MappedEmployeeTransformer;

use App\Criterias\EmployeeTimesheetUser\WithMappedEmployee;
use App\Criterias\EmployeeTimesheetUser\Unmapped;

class EmployeeTimesheetUserController extends BaseController
{
    public function __construct(
        EmployeeTimesheetUserRepository $repository,
        EmployeeTimesheetUserValidator $validator,
        EmployeeTimesheetUserTransformer $transformer
    ) {
        $this->repository  = $repository;
        $this->validator   = $validator;
        $this->transformer = $transformer;
    }

    /**
     * Get list of timesheet jobcodes
     *
     * @param Dingo\Api\Http\Request
     * @return Resource Collection
     */
    public function index(Request $request)
    {

        if (filter_var($request->get('withMapping'), FILTER_VALIDATE_BOOLEAN)) {
            $this->repository->pushCriteria(new WithMappedEmployee());
            $this->transformer = app(MappedEmployeeTransformer::class);
        }

        if (filter_var($request->get('unmapped'), FILTER_VALIDATE_BOOLEAN)) {
            $this->repository->pushCriteria(new Unmapped());
        }

        return parent::index($request);
    }
}
