<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Models\ActivityLog;
use App\Repositories\Client\ClientRepository;
use App\Services\ActivityLogService;
use App\Validators\ClientValidator;
use App\Transformers\ClientTransformer;
use App\Transformers\ClientReportTransformer;
use Dingo\Api\Http\Request;
use App\Criterias\Client\SearchByCompanyName;
use App\Criterias\Client\SearchByReviewed;
use App\Criterias\Client\WithProjectCount;
use App\Criterias\Client\WithResourceCount;
use App\Criterias\Client\SearchByCompanyNameExact;
use App\Criterias\Client\SearchByStatus;
use App\Criterias\Client\SortByCreatedatAndStatus;
use App\Criterias\Client\AddSelectClientCompanyOnly;
use App\Repositories\Client\ClientContactRepository;
use App\Validators\ClientContactValidator;
use App\Transformers\ClientContactTransformer;
use App\Repositories\Client\ClientProjectRepository;
use App\Validators\ClientProjectValidator;
use App\Transformers\ClientProjectTransformer;
use App\Repositories\ClientTeambuilderBucket\ClientTeambuilderBucketRepository;
use App\Validators\ClientTeambuilderBucketValidator;
use App\Transformers\ClientTeambuilderBucketTransformer;
use App\Services\ClientReviewService;
use Illuminate\Support\Carbon;
use App\Criterias\Client\SearchByIds;
use App\Transformers\ClientNamesTransformer;

class ClientController extends BaseController
{
    public function __construct(
        ClientRepository $repository,
        ClientValidator $validator,
        ClientTransformer $transformer,
        ClientContactRepository $ccRepository,
        ClientContactValidator $ccValidator,
        ClientContactTransformer $ccTransformer,
        ClientProjectRepository $cpRepository,
        ClientProjectValidator $cpValidator,
        ClientProjectTransformer $cpTransformer,
        ClientTeambuilderBucketRepository $ctbrepository,
        ClientTeambuilderBucketValidator $ctbvalidator,
        ClientTeambuilderBucketTransformer $ctbtransformer
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
        $this->ccRepository = $ccRepository;
        $this->ccValidator = $ccValidator;
        $this->ccTransformer = $ccTransformer;
        $this->cpRepository = $cpRepository;
        $this->cpValidator = $cpValidator;
        $this->cpTransformer = $cpTransformer;
        $this->ctbrepository = $ctbrepository;
        $this->ctbvalidator = $ctbvalidator;
        $this->ctbtransformer = $ctbtransformer;
    }

    /**
     * Get list of clients
     *
     * @return Collection
     */
    public function index(Request $request)
    {
        // see if we have searches
        if ($request->get('search')) {
            // add SearchByCompanyName criteria
            $this->repository->pushCriteria(new SearchByCompanyName($request->get('search')));
        }

        if ($request->get('company')) {
            // add SearchByCompanyName criteria
            $this->repository->pushCriteria(new SearchByCompanyNameExact($request->get('company')));
        }

        if ($request->get('reviewed') == '0') {
            $this->repository->pushCriteria(new SearchByReviewed($request->get('reviewed')));
        }

        if ($request->get('status')) {
            $otherDetails = false;
            // add SearchByStatus criteria
            if ($request->get('status') == 'active') {
                $status = 0;
            }
            if ($request->get('status') == 'prospect') {
                $status = 2;
                $otherDetails = true;
            }
            $this->repository->pushCriteria(new SearchByStatus($status));
            $this->transformer->setOtherDetails($otherDetails);
        }

        if ($request->get('ids')) {
            $this->repository->pushCriteria(new SearchByIds($request->get('ids')));
        }

        // default criteria
        $this->repository->pushCriteria(new WithProjectCount());
        $this->repository->pushCriteria(new WithResourceCount());
        $this->repository->pushCriteria(new SortByCreatedatAndStatus());

        if ($request->get('namesOnly')) {
            $this->transformer = app(ClientNamesTransformer::class);
        }

        $response = parent::index($request);

        // log response if success
        if (
            $response->status() === config('const.STATUS_CODE.SUCCESS') &&
            !empty($request->get('take'))
        ) {
            ActivityLogService::log([
                'description' => 'viewed all clients.',
                'model' => $this->repository->model()
            ]);
        }

        return $response;
    }

    /**
     * Get list of clients
     *
     * @return Collection
     */
    public function clientReport(Request $request)
    {
        if (!is_null(request('user')) && 'me' === request('user')) {
            // does nothing
        } else {
            // otherwise, authorize request
            $modelObj = app($this->repository->model());
            $this->authorize('view', $modelObj);
        }

        try {
            $result = $this->repository->getClientWithResources();
            $response = $this->response->collection($result, new ClientReportTransformer());
        } catch (ModelNotFoundException $e) {
            $response =  response()->json([
                'status_code' => 404,
                'message' => $e->getMessageBag()
            ], 404);
        }

        // log response if success
        if (
            $response->status() === config('const.STATUS_CODE.SUCCESS') &&
            !empty($request->get('take'))
        ) {
            ActivityLogService::log([
                'description' => 'viewed the client report page.',
                'model' => $this->repository->model()
            ]);
        }

        return $response;
    }

    public function show($id)
    {
        // default criteria
        $this->repository->pushCriteria(new WithProjectCount());
        $this->repository->pushCriteria(new WithResourceCount());

        $response = parent::show($id);

        // log response if success
        if ($response->status() === config('const.STATUS_CODE.SUCCESS')) {
            ActivityLogService::log([
                'description' => 'viewed (' . $response->getOriginalContent()->company . ') client.',
                'model' => $this->repository->model()
            ]);
        }

        return $response;
    }

    /**
     * POST save a resource
     *
     * @param Dingo\Api\Http\Request $request
     * @return Resource Item
     */
    public function store(Request $request)
    {
        // we remove these request data, we don't store these in db
        $request = new Request($request->except(['contacts', 'projects_count', 'resources_count', 'photo', 'projects', 'key', 'name']));
        $response =  parent::store($request);

        // log response if success
        if ($response->status() === config('const.STATUS_CODE.SUCCESS')) {
            ActivityLogService::log([
                'description' => 'successfully added (' . $request->get('company') . ') client.',
                'model' => $this->repository->model()
            ]);
        } else if ($response->status() === config('const.STATUS_CODE.BAD_REQUEST')) {
            ActivityLogService::log([
                'description' => 'failed to add (' . $request->get('company') . ') client.',
                'log_type' => ActivityLog::LOG_TYPE_ERROR,
                'model' => $this->repository->model()
            ]);
        }

        return $response;
    }

    /**
     * PUT|PATCH update a resource
     *
     * @param Dingo\Api\Http\Request $request
     * @param int $id
     * @return Resource Item
     */
    public function update(Request $request, $id)
    {
        // we remove these request data, we don't store these in db
        $request = new Request($request->except(['contacts', 'projects_count', 'resources_count', 'photo', 'projects', 'key', 'name']));
        $response = parent::update($request, $id);

        // log response if success
        if ($response->status() === config('const.STATUS_CODE.SUCCESS')) {
            ActivityLogService::log([
                'description' => 'successfully updated (' . $request->get('company') . ') client.',
                'model' => $this->repository->model()
            ]);
        } else if ($response->status() === config('const.STATUS_CODE.BAD_REQUEST')) {
            ActivityLogService::log([
                'description' => 'failed to update (' . $request->get('company') . ') client.',
                'log_type' => ActivityLog::LOG_TYPE_ERROR,
                'model' => $this->repository->model()
            ]);
        }

        return $response;
    }

    public function wizardClientSetup(Request $request)
    {
        $request = $request->all();
        $request = [
            'client_details'      => $request['client_details']['client'],
            'client_contacts'     => $request['client_details']['contacts'],
            'project'             => $request['client_projects'],
            'suggested_resource'  => $request['suggested_resource'],
            'client_access'       => $request['profile_access'],
        ];

        $service = new ClientReviewService($request);
        $service->updateClientDetails()
            ->updateClientContacts()
            ->setupProject()
            ->assignSuggestedResource()
            ->sendClientAccess();
    }

    public function getCompanies(Request $request)
    {
        $this->transformer = app(ClientNamesTransformer::class);
        $this->repository->pushCriteria(new AddSelectClientCompanyOnly());

        return parent::index($request);
    }

    public function getRSSFeeds()
    {
        try {
            $xml = simplexml_load_file('https://fullscale.io/feed');
            $feeds = [];

            foreach ($xml->channel->item as $item) {
                $link = (string) $item->link;
                $description = (string) $item->description;
                $description = trim($description);
                $description = preg_replace('/<p>(The\spost.*)<\/p>/', '', $description);
                $ellipsis = '... ';
                $anchor =  '<a target="_blank" href="'.$link.'">Read More</a>';
                $description = preg_replace('/\[.*\]/', $ellipsis . $anchor, $description);
                $published_date = new Carbon((string) $item->pubDate);
                $published_date = $published_date->format('Y-m-d');
                $feed = [
                    'title'          => (string) $item->title,
                    'description'    => $description,
                    'link'           => $link,
                    'published_date' => $published_date,
                ];
                $feeds[] = $feed;
            }
            return $feeds;
        } catch(\Exception $e) {
            return [];
        }
    }
}
