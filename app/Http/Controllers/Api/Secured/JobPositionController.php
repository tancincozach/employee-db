<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use App\Repositories\JobPosition\JobPositionRepository;
use App\Validators\JobPositionValidator;
use App\Transformers\JobPositionTransformer;
use App\Criterias\JobPosition\SearchJob;
use App\Criterias\JobPosition\SearchExactJobPosition;
use App\Criterias\Common\WithTrashed;
use Dingo\Api\Http\Request;

class JobPositionController extends BaseController
{
    public function __construct(JobPositionRepository $repository, JobPositionValidator $validator, JobPositionTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    /**
     * Get list of Job Positions
     *
     * @return Collection
     */
    public function index(Request $request)
    {
        // see if we have searches
        if ($request->get('search')) {
            // add SearchByJobTitle criteria
            $this->repository->pushCriteria(new SearchJob($request->get('search')));
        }

        if($request->get('job_title') && $request->get('withTrashed')) {
            $this->repository->pushCriteria(new WithTrashed());
            $this->repository->pushCriteria(new SearchExactJobPosition($request->get('job_title')));
        }

        return parent::index($request);
    }

    /**
     * This assumes normal update or passively restore and update the previously soft-deleted record
     * @param Request $request
     * @param int $id
     * @return Resource
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update(Request $request, $id) {
        // We need to get this passive, that is - we need to consider those that's already soft-deleted
        $this->repository->pushCriteria(new WithTrashed());

        // we need to restore (set deleted_at to NULL) prior to updating
        $this->repository->find($id)->restore();

        // Call mom!
        return parent::update($request, $id);
    }
}
