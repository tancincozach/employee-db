<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use App\Models\NotificationUser;
use Illuminate\Support\Str;

class Notification extends BaseModel
{
    use Uuids;

    /**
     * Get the users for this notification
     */
    public function users()
    {
        return $this->hasMany(NotificationUser::class);
    }

    /**
     * Create notification
     * 
     * @param array $payload
     * @param string $type
     * @return Illuminate\Database\Eloquent\Collection
     */
    public static function createNotification(array $payload, $type)
    {
        $notification = new self();
        $notification->type = $type;
        $notification->data = json_encode($payload);
        $notification->save();

        return $notification;
    }
}
