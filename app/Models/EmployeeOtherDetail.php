<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeOtherDetail extends BaseModel
{
    public function referredBy()
    {
        return $this->belongsTo(Employee::class, 'referred_by_employee_id')
            ->withDefault([
                'first_name' => '',
                'last_name' => ''
            ]);
    }
}
