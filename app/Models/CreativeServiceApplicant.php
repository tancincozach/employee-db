<?php

namespace App\Models;

use App\Traits\Uuids;

/**
 * Class CreativeServiceApplicant
 * @package App\Models
 */
class CreativeServiceApplicant extends BaseModel
{
    use Uuids;

    protected $table = 'creative_service_applicant';
}
