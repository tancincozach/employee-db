<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerformEvalCriteria extends BaseModel
{
    use Uuids;

    protected $table = 'perform_eval_criteria';

    public function question()
    {
        return $this->belongsTo(PerformEvalQuestion::class, 'perform_eval_question_id');
    }
}