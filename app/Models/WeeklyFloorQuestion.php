<?php

namespace App\Models;
use App\Traits\Uuids;

use Illuminate\Database\Eloquent\SoftDeletes;

class WeeklyFloorQuestion extends BaseModel
{
    use Uuids;

    protected $table = 'weekly_floor_question';
}

