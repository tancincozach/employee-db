<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TimesheetUser extends BaseModel
{
    protected $guarded = [];

    public $incrementing = false;
}
