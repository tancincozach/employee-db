<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserVerification extends BaseModel
{
    public function scopeCheckUserVerification($query, $code, $i)
    {
        return $query->where('verification_token','=', $code)
            ->where('user_id','=', $i)
            ->whereNull('verified_at')
            ->whereRaw('expired_at >= NOW()');
    }

    /**
     * Check user token for resending verification.
     * Regardless if token is already verified and expired.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $token
     * @param int    $userId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeResendVerification($query, $token, $userId)
    {
        return $query->where('verification_token','=', $token)
            ->where('user_id','=', $userId);
    }

}
