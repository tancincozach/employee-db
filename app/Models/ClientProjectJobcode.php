<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientProjectJobcode extends BaseModel
{
    const NON_WORK_JOBCODES = [
        'Paid Time Off',
        'Rest/Lunch Break',
        'Utility/Maintenance',
        'Maternity Leave',
        'Pait Time Off',
        'Philippines Independence Day',
        'Election Day',
        'Good Friday',
        'Holiday',
        'Labor Day',
        'Maundy Thursday',
        'The Day of Valor',
        'National Heroes Day',
        'PTO Cebu FS',
        'Independence Day',
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /*
     * Set client id
     * @param int $id
     * @param int $clientId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function setClient($id, $clientId)
    {
        $model = self::find($id);
        $model->client_id = $clientId;
        $model->save();

        return $model;
    }

    /*
     * Get jobcode based on id specified
     *
     * @param integer $id
     *
     * @return string
     */
    public static function getJobcodeById($id)
    {
        return self::find($id)->jobcode;
    }
}
