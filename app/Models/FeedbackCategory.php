<?php

namespace App\Models;

use App\Traits\Uuids;

/**
 * Class FeedbackCategory
 * @package App\Models
 */
class FeedbackCategory extends BaseModel
{
    use Uuids;

    protected $table = 'feedback_category';
}
