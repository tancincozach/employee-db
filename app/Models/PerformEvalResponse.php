<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerformEvalResponse extends BaseModel
{
    use Uuids;

    protected $table = 'perform_eval_response';
}