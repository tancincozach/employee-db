<?php

namespace App\Models;

class ClientTeambuilderBucket extends BaseModel
{
    protected $table = 'client_teambuilder_buckets';

    const TECHNOLOGY_CHOICES_CATEGORIES = [
        'Developers', 'Mobile', 'Web', 
        'Testers', 'Manual', 'Automation'
    ];

    const FILTER_UNASSIGN_TEAMBUILDER = [
        'developer', 'tester'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function clientProject()
    {
        return $this->belongsTo(ClientProject::class);
    }

    public function jobPosition()
    {
        return $this->belongsTo(JobPosition::class);
    }

    public function updatedByUser()
    {
        return $this->belongsTo(User::class);
    }

    public function getUpdatedByUser()
    {
        $user = ['id' => null, 'user_name' => 'Rocks Admin', 'email' => null];

        if (!is_null($this->updated_by_user_id)) {
            $user['id'] = $this->updated_by_user_id;
            $user['user_name'] = $this->updatedByUser->user_name;
            $user['email'] = $this->updatedByUser->email;
        }

        $user['user_details'] = "(" . $user['user_name'] . ") " . $user['email'];
        return $user;
    }
}
