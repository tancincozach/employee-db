<?php

namespace App\Models;

class EmployeeTopSkill extends BaseModel
{
    protected $table = 'employee_skills';

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSkill($query)
    {
        return $query->leftJoin('skills', function ($join) {
            $join->on('skills.id', '=', 'employee_skills.skill_id');
        })
        ->where('employee_skills.checked_top_skill', 1);
    }
}
