<?php

namespace App\Models;

class UserType extends BaseModel
{
    /*
     * Client type id
     *
     * @var integer
     */
    const CLIENT = 2;
}
