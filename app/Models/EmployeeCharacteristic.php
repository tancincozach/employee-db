<?php

namespace App\Models;

use App\Traits\Uuids;

/**
 * Class EmployeeCharacteristic
 * @package App\Models
 */
class EmployeeCharacteristic extends BaseModel
{
    use Uuids;

    protected $table = 'employee_characteristic';
}
