<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Timesheet extends BaseModel
{
    protected $guarded = [];

    public $incrementing = false;
}
