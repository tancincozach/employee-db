<?php

namespace App\Models;

use App\Traits\Uuids;

use Illuminate\Database\Eloquent\Model;

class Faq extends BaseModel
{
    use Uuids;

    protected $table = 'faqs';
}
