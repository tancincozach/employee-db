<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TimesheetJobcode extends BaseModel
{
    protected $guarded = [];

    public $incrementing = false;
}
