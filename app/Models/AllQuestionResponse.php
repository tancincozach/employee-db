<?php

namespace App\Models;

class AllQuestionResponse extends BaseModel
{
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function scopeHasRemoteExp($query, $client_id)
    {
        return $query->where('client_id', '=', $client_id)
                    ->where('all_question_id', '=', '28');
    }
}
