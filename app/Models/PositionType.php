<?php

namespace App\Models;

use App\Traits\Uuids;

/**
 * Class PositionType
 * @package App\Models
 */
class PositionType extends BaseModel
{
    use Uuids;

    protected $table = 'position_type';
}
