<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class SkillCategory extends BaseModel
{
    protected $table = 'skill_categories';

    public function skill()
    {
        return $this->belongsTo(Skill::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
