<?php

namespace App\Models;

use App\Traits\Uuids;

/**
 * Class EmployeeAcademic
 * @package App\Models
 */
class EmployeeAcademic extends BaseModel
{
    use Uuids;

    protected $table = 'employee_academic';
}
