<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeeklyReportBatch extends BaseModel
{
    protected $guarded = [
        'create_at',
        'updated_at',
        'deleted_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function batchDetail()
    {
        return $this->hasMany(WeeklyReportBatchDetail::class);
    }

    public function asset()
    {
        return $this->hasOne(Asset::class);
    }

    public static function createDummy()
    {
        $data = [
            'id' => 1,
            'filename' => 'Generated-Dummy',
            'weekly_start_date' => date('Y-m-d H:i:s'),
            'weekly_end_date' => date('Y-m-d H:i:s'),
            'created_by_user_id' => 1,
            'updated_by_user_id' => 1
        ];
        (new self())->create($data);
    }

    public static function isEmpty()
    {
        $instance = new self();
        $returned = $instance->get()->count();
        return ($returned <= 0);
    }
}
