<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends BaseModel
{
    public function categories()
    {
        return $this->hasMany(SkillCategory::class);
    }

    public function hackerRankTests()
    {
        return $this->belongsToMany(HackerRankTest::class, 'skill_hacker_rank_tests', 'hacker_rank_tests_id', 'skill_id');
    }
}
