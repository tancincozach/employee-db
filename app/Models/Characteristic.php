<?php

namespace App\Models;

use App\Traits\Uuids;

/**
 * Class Characteristic
 * @package App\Models
 */
class Characteristic extends BaseModel
{
    use Uuids;

    protected $table = 'characteristic';
}
