<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ApiUser extends Model
{
    use SoftDeletes;

    /**
     * Mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'owner_name',
        'key',
        'secret',
        'allowed_ip',
    ];

    /**
     * Generates API owner and its key and secret
     *
     * @param       $ownerName
     * @param array $allowedIp
     *
     * @return bool
     */
    public function generateOwner($ownerName, array $allowedIp = [])
    {
        return $this->create([
            'owner_name' => (string)trim($ownerName),
            'key'        => Str::uuid()->getHex(),
            'secret'     => str_random(64),
            'allowed_ip' => json_encode($allowedIp),
        ]);
    }

    /**
     * Retrieves key details
     *
     * @param $key The key to search
     *
     * @return mixed
     */
    public function getKeyDetails($key)
    {
        // check if key exists
        return $this->where('key', $key)->first();
    }
}
