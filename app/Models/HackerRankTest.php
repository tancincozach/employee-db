<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class HackerRankTest extends BaseModel
{
    protected $table = 'hacker_rank_tests';
    /**
     *  HackerRankTestResult
     */
    public function hackerRankTestResults()
    {
        return $this->hasMany(HackerRankTestResult::class);
    }
    /**
     *  Skill
     */
    public function skill()
    {
        return $this->belongsTo(Skill::class);
    }
    
}
