<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class HackerRankTestResult extends BaseModel
{

    public function hackerRankTest()
    {
        return $this->belongsTo(HackerRankTest::class, 'test_id', 'test_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function skill()
    {
        return $this->belongsTo(Skill::class);
    }

}
