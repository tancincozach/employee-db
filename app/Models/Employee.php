<?php

namespace App\Models;

use App\Models\WorkFromHome\WorkFromHomeRequest;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Employee extends BaseModel
{
    const ADV_SEARCH_NAME = 0;
    const ADV_SEARCH_PROJECT = 1;
    const ADV_SEARCH_SKILL = 2;
    const ADV_SEARCH_LOCATION = 3;
    const ADV_SEARCH_CLIENT = 4;

    const ACTIVE_EMPLOYEE_STATUS = ['16', '18', '19'];

    /**
     * Get Full Name
     */
    public function getName()
    {
        return ucwords(trim($this->last_name . ', ' . $this->first_name . ' ' . $this->middle_name . ' ' . $this->ext));
    }

    /**
     * Get Full Name with first name firsts
     */
    public function getFullNameFirst()
    {
        return ucwords(trim($this->first_name . ' ' . $this->last_name . ' ' . $this->ext));
    }

    /**
     * Get Initial Name (short)
     */
    public function getInitial()
    {
        return ucwords(trim($this->first_name . ' ' . substr($this->last_name, 0, 1)));
    }

    /**
     * @todo: try to use this experimentally - Charm
     * @param $query
     *
     * @return mixed
     */
    public function scopeWithTopSkills($query)
    {
        return $query->leftJoin('employee_top_skills', 'employee_top_skills.employee_id', 'employees.id')
            ->leftJoin('skills', 'skills.id', '=', 'employee_top_skills.skill_id')
            ->orderBy('employee_top_skills.proficiency', 'desc')
            ->withTrashed();
    }

    /**
     * User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Active position
     */
    public function positions()
    {
        return $this->hasMany(EmployeeJobPosition::class)->with('position');
    }

    /**
     * Interests
     */
    public function interests()
    {
        return $this->hasMany(EmployeeInterest::class);
    }

    /**
     * Government IDs
     */
    public function governmentIds()
    {
        return $this->hasMany(GovernmentId::class);
    }

    /**
     * Work Shift
     */
    public function shift($all=false)
    {
        $result = $this->hasMany(EmployeeWorkShift::class);
        return $all ? $result : $result->orderBy('updated_at', 'desc')->limit(2);
    }

    /**
     * Contact Person
     */
    public function contactPerson()
    {
        return $this->hasOne(ContactPerson::class);
    }

    /**
     * Address
     */
    public function address()
    {
        return $this->hasMany(Address::class);
    }

    /**
     * Photo
     */
    public function photo()
    {
        return $this->hasMany(Asset::class, 'assetable_id');
    }

    /**
     * Applicant PDF
     */
    public function pdf()
    {
        return $this->hasMany(Asset::class, 'cv');
    }

    /**
     * Experience
     */
    public function workExperiences()
    {
        // return $this->hasMany(WorkExperience::class)->orderBy('order', 'asc');
        return $this->hasMany(WorkExperience::class)->orderBy('start_date', 'desc');
    }
    /**
     * Skill
     */
    public function skills()
    {
        return $this->hasMany(EmployeeSkill::class)->orderBy('proficiency', 'desc')->with('skill');
    }
    /**
     * Skill
     */
    public function topSkills()
    {
        return $this->hasMany(EmployeeTopSkill::class)->orderBy('proficiency', 'desc')->skill()->withTrashed();
    }

    /**
     * Language
     */
    public function languages()
    {
        return $this->hasMany(EmployeeLanguage::class);
    }

    public function educations()
    {
        return $this->hasMany(EmployeeEducation::class)->orderBy('order', 'asc');
    }

    /**
     * Portfolio
     */
    public function portfolios()
    {
        return $this->hasMany(EmployeePortfolio::class);
    }

    /**
     * Department
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    /**
     * Employee Statuses
     */
    public function employeeStatuses()
    {
        return $this->hasMany(EmployeeStatus::class)->orderBy('id', 'desc');
    }

    /**
     * Employee Spouse
     */
    public function spouse()
    {
        return $this->hasOne(EmployeeSpouse::class);
    }

    /**
     * Employee Dependent
     */
    public function dependents()
    {
        return $this->hasMany(EmployeeDependent::class)->orderBy('order', 'asc');
    }

    /**
     * Employee Contacts
     */
    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    /**
     * Employee Other Skills
     */
    public function otherSkills()
    {
        return $this->hasMany(EmployeeOtherSkill::class);
    }

    /**
     * Employee Other Details
     */
    public function otherDetails()
    {
        return $this->hasMany(EmployeeOtherDetail::class);
    }

    public function projects()
    {
        return $this->hasMany(EmployeeClientProject::class);
    }

    public function employeeReport()
    {
        return $this->hasMany(EmployeeReport::class);
    }

    public function location()
    {
        return $this->hasOne(EmployeeLocation::class);
    }

    /**
     * @return mixed
     */
    public function preferredTeams()
    {
        return $this->hasMany(ClientPreferredTeam::class)->withTrashed();
    }

    public function workLocation()
    {
        return $this->belongsTo(WorkLocation::class);
    }

    /**
     * @return HasMany
     */
    public function workFromHomeRequests()
    {
        return $this->hasMany(WorkFromHomeRequest::class);
    }

    public function clientTeambuilderBucket()
    {
        return $this->hasMany(ClientTeambuilderBucket::class);
    }

    /**
     * @return HasMany
     */
    public function hackerRankTestResult()
    {
        return $this->hasMany(HackerRankTestResult::class)->orderBy('percent_score', 'desc');
    }
}
