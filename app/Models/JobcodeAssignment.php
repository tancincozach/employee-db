<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class JobcodeAssignment extends BaseModel
{
    protected $guarded = [];

    public $incrementing = false;
}
