<?php

namespace App\Models;
use App\Traits\Uuids;

use Illuminate\Database\Eloquent\SoftDeletes;

class WeeklyFloorAnswer extends BaseModel
{
    use Uuids;

    protected $table = 'weekly_floor_answer';

    public function weeklyFloorQuestion()
    {
        return $this->belongsTo(WeeklyFloorQuestion::class);
    }
}

