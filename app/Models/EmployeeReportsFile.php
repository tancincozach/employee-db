<?php

namespace App\Models;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeReportsFile extends BaseModel
{
    use Uuids;

    protected $table = 'employee_reports_file';
    /**
     * Employee Report details
     */    
    public function employeeReport()
    {
        return $this->belongsTo(EmployeeReport::class);
    }
}
