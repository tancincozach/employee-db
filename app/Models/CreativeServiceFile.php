<?php

namespace App\Models;

use App\Traits\Uuids;

/**
 * Class CreativeServiceFile
 * @package App\Models
 */
class CreativeServiceFile extends BaseModel
{
    use Uuids;

    const PATH = 'creative-service';

    protected $table = 'creative_service_file';
}
