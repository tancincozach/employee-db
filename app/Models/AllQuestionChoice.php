<?php

namespace App\Models;

class AllQuestionChoice extends BaseModel
{
    // Developers by default
    const TECHNOLOGY_CHOICES_ID = 25;

    const LIMIT_TAKE_CHOICES = 20;

    public function question()
    {
        return $this->belongsTo(AllQuestion::class);
    }
}
