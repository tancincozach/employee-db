<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

use App\Helpers\RocksHelper;

class WeeklyReport extends BaseModel
{
    const JOBCODE_INDEX = 0;
    const PTO_INDEX = 1;
    const HOLIDAY_INDEX = 2;
    const TOTAL_WORKING_MONTHLY_HOURS = '168';

    public function weeklyReportBatchDetail()
    {
        return $this->belongsTo(WeeklyReportBatchDetail::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class)->withTrashed();
    }

    public function clientProjectJobcode()
    {
        return $this->belongsTo(ClientProjectJobcode::class, 'jobcode_id', 'id');
    }

    public function scopeGetWorklogs($query, $array = [])
    {
        return $query->addSelect(DB::raw('SUM(working_hrs) as working_hrs'))
            ->where('jobcode', '=', $array['jobcode'])
            ->whereDate('log_date', '<=', $array['endDate'])
            ->whereDate('log_date', '>=', $array['startDate'])
            ->where('employee_id', '=', $array['employee_id'])
            ->whereNull('deleted_at')
            ->groupBy('employee_id');
    }

    public function scopeGetWorkHrs($query, $array = [])
    {
        return $query->addSelect(DB::raw('SUM(working_hrs) as working_hrs, id'))
            ->where('jobcode', '=', $array['jobcode'])
            ->whereDate('log_date', '=', $array['log_date'])
            ->where('employee_id', '=', $array['employee_id'])
            ->whereNull('deleted_at')
            ->groupBy('log_date');
    }

    public function scopeGetByLogDate($query, $array = [])
    {
         return  $query->select(DB::raw('SUM(working_hrs) as working_hrs, jobcode_id, log_date,id'))
            ->whereDate('log_date', '=', $array['log_date'])
            ->where('employee_id', '=', $array['employee_id'])
            ->where('data_source', 'api')
            ->whereIn('jobcode_id', $array['jobcode_id'])
            ->whereNull('deleted_at')
            ->groupBy('jobcode_id')
            ->get();
    }

    /*
     * Convert data to client timezone
     *
     * @param string $timezone
     *
     * @return array
     */
    public function convertToClientTimezone($timezone)
    {
        $logDate =   new \DateTime($this->log_date);
        $startDate = new \DateTime($this->start_date);
        $endDate =   new \DateTime($this->end_date);

        $logDate = $logDate->setTimezone(new \DateTimeZone($timezone));
        $startDate = $startDate->setTimezone(new \DateTimeZone($timezone));
        $endDate = $endDate->setTimezone(new \DateTimeZone($timezone));

        return [
            'log_date'   => $logDate->format('F d, Y'),
            'day'        => $logDate->format('D'),
            'start_date' => $startDate->format('h:i A T'),
            'end_date'   => $endDate->format('h:i A T'),
        ];
    }
}
