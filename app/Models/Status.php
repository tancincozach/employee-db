<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends BaseModel
{
    const HIRED = 16;
    const PROBATIONARY = 18;
    const REGULAR = 19;

    public function employeeStatus()
    {
        return $this->hasMany(EmployeeStatus::class);
    }
}
