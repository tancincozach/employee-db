<?php

namespace App\Models;

use App\Traits\Uuids;

/**
 * Class Feedback
 * @package App\Models
 */
class Feedback extends BaseModel
{
    use Uuids;

    protected $table = 'feedback';
}
