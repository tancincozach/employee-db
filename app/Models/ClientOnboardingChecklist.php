<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Client;
use App\Models\Asset;

class ClientOnboardingChecklist extends BaseModel
{
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function asset()
    {
        return $this->hasOne(Asset::class,'id','asset_id');
    }

}
