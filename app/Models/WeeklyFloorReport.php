<?php

namespace App\Models;
use App\Traits\Uuids;

use Illuminate\Database\Eloquent\SoftDeletes;

class WeeklyFloorReport extends BaseModel
{
    use Uuids;

    protected $table = 'weekly_floor_report';

    public function answers()
    {
        return $this->hasMany(WeeklyFloorAnswer::class);
    }

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }
}
