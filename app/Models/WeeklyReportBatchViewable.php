<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeeklyReportBatchViewable extends BaseModel
{
    public function user()
    {
        return $this->belongsTo(User::class, 'posted_by_user_id', 'id');
    }

    public function scopeIsExistPostedReport ($query, $clientId, $startDate, $endDate)
    {
        return $query->where('client_id', $clientId)
                    ->whereDate('start_date', $startDate)
                    ->whereDate('end_date', $endDate)
                    ->where('is_posted', 1)
                    ->exists();
    }

    public function scopeGetDateRangeViewablesByClientId ($query, $clientId, $month) 
    {
        $dates = $query->select('start_date','end_date')->distinct()
                    ->whereMonth('start_date', $month)
                    ->whereMonth('end_date', $month)
                    ->where('client_id', $clientId)
                    ->where('is_posted', 1)
                    ->orderBy('start_date', 'DESC')->get();

        return $dates->toArray();
                    
    }
}
