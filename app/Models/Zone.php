<?php

namespace App\Models;

class Zone extends BaseModel
{
    const DEFAULT_TIMEZONE = 'America/Chicago';

    protected $primaryKey = 'zone_id';
    protected $table = 'zones';
}
