<?php

namespace App\Models;

class UserTimezone extends BaseModel
{
    /*
     * Get the user that owns the user timezone.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /*
     * Get the zone the owns  user timezone.
     */
    public function zone()
    {
        return $this->belongsTo('App\Models\Zone', 'zone_id', 'zone_id');
    }
}
