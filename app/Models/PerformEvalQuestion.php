<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerformEvalQuestion extends BaseModel
{
    protected $table = 'perform_eval_question';

    public function criteria()
    {
        return $this->hasMany(PerformEvalCriteria::class)->orderBy('sequence_no');
    }
}