<?php

namespace App\Models;

use App\Traits\Uuids;

/**
 * Class CreativeService
 * @package App\Models
 */
class CreativeService extends BaseModel
{
    use Uuids;

    protected $table = 'creative_service';
}
