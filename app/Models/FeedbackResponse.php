<?php

namespace App\Models;

use App\Traits\Uuids;

/**
 * Class FeedbackResponse
 * @package App\Models
 */
class FeedbackResponse extends BaseModel
{
    use Uuids;

    protected $table = 'feedback_response';
}
