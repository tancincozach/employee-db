<?php

namespace App\Models;

use App\Traits\Uuids;

/**
 * Class EmployeePositionType
 * @package App\Models
 */
class EmployeePositionType extends BaseModel
{
    use Uuids;

    protected $table = 'employee_position_type';
}
