<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationFlag extends BaseModel
{
    /**
     * Set user notification count
     * 
     * @param  int  $userId
     * @param  bool $reset
     * @return void
     */
    public static function setHasNotification($userId, $reset = false)
    {
        $model = self::where('user_id', $userId)->first(['has_notification']);
        $currentCount = $model ? $model->has_notification : 0;
        $hasNotification = $reset ? 0 : ++$currentCount;

        self::updateOrCreate(
            [ 'user_id' => $userId ],
            [ 'user_id' => $userId, 'has_notification' => $hasNotification ]
        );
    }
}
