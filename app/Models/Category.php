<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends BaseModel
{
    protected $table = 'categories';

    const RESOURCE_TYPES = [
        'Developers',
        'Testers',
        'Managers',
        'Creative'
    ];

    const UNASSIGN_ONLY_RESOURCE_TYPE = [
        'Developers',
        'Testers'
    ];

    public function skills()
    {
        return $this->belongsToMany(Skill::class, 'skill_categories');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }
}
