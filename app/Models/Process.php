<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Process extends BaseModel
{
    protected $table = 'process';

    /*
     * Start a process
     *
     * @param string $name
     * @param string $desc
     *
     * @return Process
     */
    public function start($name, $desc = '')
    {
        $this->name = $name;
        $this->description = $desc;
        $this->start_at = date('Y-m-d H:i:s');
        $this->save();

        return $this;
    }

    /*
     * Finish a process
     *
     * @return Process
     */
    public function finish()
    {
        $this->end_at = date('Y-m-d H:i:s');
        $this->status = 'inactive';
        $this->save();

        return $this;
    }
}
