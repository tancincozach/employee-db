<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeeklyReportBatchDetail extends BaseModel
{
    public function batch()
    {
        return $this->belongsTo(WeeklyReportBatch::class, 'weekly_report_batch_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'uploaded_by_user_id', 'id');
    }

    public function asset()
    {
        return $this->belongsTo(Asset::class, 'asset_id', 'id');
    }

    public function weeklyReport()
    {
        return $this->hasMany(WeeklyReport::class, 'id', 'weekly_report_batch_detail_id');
    }

    public static function createDummy()
    {
        $data = [
            'id' => 1,
            'weekly_report_batch_id' => 1,
            'file_version' => 1,
            'asset_id' => 1,
            'parsed_filename' => 'Generated-Dummy-Details',
            'remarks' => 'This is a generated dummy record',
            'status' => 'success',
            'uploaded_by_user_id' => 1
        ];
        (new self())->create($data);
    }

    public static function isEmpty()
    {
        $instance = new self();
        $returned = $instance->get()->count();
        return ($returned <= 0);
    }
}
