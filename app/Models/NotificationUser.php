<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationUser extends BaseModel
{
    /**
     * Assign notification to a user
     * 
     * @param string $notificationId
     * @param integer $userId
     * @return Illuminate\Database\Eloquent\Collection
     */
    public static function assignNotification($notificationId, $userId)
    {
        return self::create([
            'notification_id' => $notificationId,
            'user_id'         => $userId,
        ]);
    }

    /**
     * Mark as read notifications
     * 
     * @param array $notifications
     * @param integer $userId
     * @return void
     */
    public static function markAsRead(array $notifications, $userId)
    {
        self::where('user_id', $userId)
            ->whereIn('notification_id', $notifications)
            ->update(['read_at' => now()]);
    }

    /**
     * Check user notification count
     *
     * @param  int $userId
     * @return bool
     */
    public static function checkNotificationCount($userId)
    {
        $query = self::select(
            \DB::raw('COUNT(DISTINCT type) AS notification_count'),
            'notification_users.user_id',
            \DB::raw('IFNULL(notification_flags.has_notification, 0) AS has_notification')
        )
            ->leftJoin('notifications', 'notification_users.notification_id', '=', 'notifications.id')
            ->leftJoin('notification_flags', 'notification_users.user_id', '=', 'notification_flags.user_id')
            ->where('notification_users.user_id', $userId)
            ->groupBy('notification_users.user_id');
        $data = $query->first(['notification_count', 'has_notification']);

        return $data->notification_count == $data->has_notification;
    }
}
