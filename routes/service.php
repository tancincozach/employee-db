<?php

Route::group(['prefix' => 'user'], function () {
    Route::post('/validate-token', 'UserController@validateToken');
});
