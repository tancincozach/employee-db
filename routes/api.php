<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//   return $request->user();
//});

if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) {
    $api = app('Dingo\Api\Routing\Router');

    $api->version('v1', ['namespace' => 'App\Http\Controllers\Api'], function ($api) {
        // Authentication
        $api->post('/auth/token', 'AuthController@getToken');
        $api->post('/auth/check', 'AuthController@check');
        $api->get('/profile/{username}', 'EmployeeProfileController@show');

        // Client Survey
        $api->resource('survey', 'SurveyController');
        $api->post('/survey/tokenize/{link}', 'SurveyController@tokenize');
        $api->patch('/response/{id}', 'SurveyResponsesController@update');
        $api->get('/surveysubmitted/{id}', 'SurveyController@sendSurveySubmittedNotification');

        // applicant
        $api->post('/applicant/tokenize', 'ApplicantFormController@tokenize');
        $api->resource('applicant', 'ApplicantFormController');
        $api->get('asset', 'S3AssetController@getAsset');

        $api->get('export', 'ExportController@export');



        // Operations Documents
        $api->resource('operations-documents', 'Secured\OperationsDocumentController');

        $api->group(['prefix' => 'work-from-home'], function ($api) {
            $api->get('export', 'ExportController@export');
        });
        $api->get('daily-report-summary', 'EmployeeReportController@dailyReportSummary');

        // Faqs
        $api->resource('faq', 'FaqController');

        // Protected Routes (Requires Authentication)
        $api->group(['namespace' => 'Secured', 'middleware' => 'auth:api'], function ($api) {
            // Authentication
            $api->post('/auth/logout', 'AuthController@logout');
            $api->post('/preview', 'EmployeeReportController@preview');
            $api->post('/checkpass', 'UserController@checkpass');
            $api->post('/add-contact-user', 'ClientContactController@addContactUser');
            $api->post('/remove-contact-user', 'ClientContactController@removeLoginCredentials');

            // Users
            $api->resource('users', 'UserController');
            $api->get('update-global-password', 'UserController@updateGlobalPass');

            // Employees
            $api->group(['prefix' => 'employees'], function ($api) {
                $api->get('advance-search', 'EmployeeController@advanceSearch');
                $api->get('available', 'EmployeeController@availableEmployees');
                $api->get('get-availables', 'EmployeeController@getAvailableEmployees');
                $api->get('available-resource', 'EmployeeController@availableResource');
                $api->get('getEmployeeNames', 'EmployeeController@getEmployeeNames');
                
            });
            $api->resource('employees', 'EmployeeController');
            $api->resource('smart-searches', 'SmartSearchController');
            $api->resource('employee-positions', 'EmployeeJobPositionController');
            $api->resource('employee-skills', 'EmployeeSkillController');
            $api->resource('employee-reports-file', 'EmployeeReportsFileController');
            $api->resource('employee-interests', 'EmployeeInterestController');
            $api->resource('government-ids', 'GovernmentIdController');
            $api->resource('employee-shifts', 'EmployeeWorkShiftController');
            $api->resource('employee-languages', 'EmployeeLanguageController');
            $api->resource('employee-educations', 'EmployeeEducationController');
            $api->resource('contact-persons', 'ContactPersonController');
            $api->resource('employee-portfolios', 'EmployeePortfolioController');
            $api->resource('employee-reports', 'EmployeeReportController');            
            $api->resource('employee-calendar-reports', 'EmployeeCalendarReportController');            

            $api->resource('work-location', 'WorkLocationController');

            // Resource Allocation - temporarily set
            $api->resource('resource-allocation', 'EmployeeController');

            // Resource Allocation - temporarily set
            $api->resource('applicants', 'EmployeeController');

            // Skills
            $api->resource('skills', 'SkillController');

            // Skill Categories
            $api->resource('categories', 'CategoryController');

            // Government Agencies
            $api->resource('government-agencies', 'GovernmentAgencyController');

            // Work Shifts
            $api->resource('work-shifts', 'WorkShiftController');

            // Work Experience
            $api->resource('work-experience', 'WorkExperienceController');

            // Job Positions
            $api->resource('job-positions', 'JobPositionController');

            // Educational Attainments
            $api->resource('educational-attainments', 'EducationalAttainmentController');

            // Languages
            $api->resource('languages', 'LanguageController');

            // Contacts
            $api->resource('contacts', 'ContactController');

            // Addresss
            $api->resource('addresses', 'AddressController');

            // Countries
            $api->resource('countries', 'CountryController', ['only' => ['index', 'show']]);

            // Assets
            $api->resource('assets', 'AssetController');

            // Departments
            $api->resource('departments', 'DepartmentController');

            // Client Preferred Teams
            $api->resource('smart-team-builder', 'ClientPreferredTeamController');

            // Clients
            $api->resource('clients', 'ClientController');
            $api->get('client-reports', 'ClientController@clientReport');
            $api->get('client-companies', 'ClientController@getCompanies');

            // General Question Module
            $api->resource('all-questions', 'AllQuestionController');
            $api->resource('all-responses', 'AllQuestionResponseController');
            // End

            // Perform Evaluation Review Module
            $api->resource('perform-eval-questions', 'PerformEvalQuestionController');
            $api->resource('perform-eval-responses', 'PerformEvalResponseController');
            // End

            // Statuses
            $api->resource('status', 'StatusController');

            // Employee Status
            $api->resource('employee-statuses', 'EmployeeStatusController');

            // Hacker Rank Tests
            $api->group(['prefix' => 'hacker-rank-test'], function ($api) {
                $api->get('import', 'HackerRankTestController@fetchAndImportTests');
            });
            $api->resource('hacker-rank-test', 'HackerRankTestController');

            // Hacker Rank Test Results
            $api->group(['prefix' => 'hacker-rank-result'], function ($api) {
                $api->get('import', 'HackerRankTestResultController@fetchAndImportTestResults');
            });
            $api->resource('hacker-rank-result', 'HackerRankTestResultController');

            // Employee Spouse
            $api->resource('employee-spouse', 'EmployeeSpouseController');

            // Employee Dependent
            $api->resource('employee-dependent', 'EmployeeDependentController');

            // Employee Other Skill
            $api->resource('employee-other-skill', 'EmployeeOtherSkillController');

            // Employee Other Details
            $api->resource('employee-other-details', 'EmployeeOtherDetailController');

            // Employee Location
            $api->resource('employee-locations', 'EmployeeLocationController');

            // Client Contact
            $api->resource('client-contacts', 'ClientContactController');

            // Client Projects
            $api->resource('client-projects', 'ClientProjectController');

            // Client Projects Statuses
            $api->resource('client-project-status', 'ClientProjectStatusController');

            // Employee Client Projects
            $api->resource('employee-client-projects', 'EmployeeClientProjectController');

            // Reports
            $api->resource('reports', 'ReportController');

            // Report templates
            $api->resource('report-templates', 'ReportTemplateController');

            // Employee Checklist
            $api->resource('employee-checklist', 'EmployeeChecklistController');

            // Role
            $api->resource('roles', 'ACL\RoleController');

            // User Role
            $api->resource('role-users', 'ACL\RoleUserController');

            // Client Feedback Module
            $api->group(['namespace' => 'ClientFeedback', 'prefix' => 'feedback'], function ($api) {
                $api->resource('questionnaires', 'QuestionnaireController');
                $api->resource('question-categories', 'QuestionCategoriesController');
                $api->resource('questions', 'QuestionsController');
                $api->resource('email-templates', 'EmailTemplateController');
                $api->patch('project-surveys/{id}/manual-send', 'ProjectSurveysController@manualSend');
                $api->resource('project-surveys', 'ProjectSurveysController');
                $api->resource('survey-sent', 'SurveySentController');
                $api->resource('survey-responses', 'SurveyResponseController');
            });

            // Work Logs
            $api->resource('work-logs', 'TsheetController');

            // All Questions
            $api->resource('all-questions', 'AllQuestionController');

            // All Question Responses
            $api->resource('all-responses', 'AllQuestionResponseController');

            //Get Technology-Choices
            $api->get('technology-choices', 'SkillController@getTechnologyChoices');

            //Get Client Onboarding Results
            $api->get('onboarding-results', 'ClientTeambuilderBucketController@getOnboardingResults');

            // Client Project Reports
            $api->resource('client-project-reports', 'WeeklyReportController');

            // Client Project Jobcode
            $api->resource('client-project-jobcodes', 'ClientProjectJobcodeController');

            // Work From Home
            $api->resource('work-from-home', 'WorkFromHome\WorkFromHomeRequestController');
            // Ends here

            // Resource User Role Permission
            $api->resource('user-role-permissions', 'ACL\ResourceUserRolePermissionController');

            // Resource Role Permission
            $api->resource('role-permissions', 'ACL\ResourceRolePermissionController');

            // Resources
            $api->resource('resources', 'ACL\ResourceController');

            // User Role
            $api->resource('role-users', 'ACL\RoleUserController');

            // User Roles
            $api->resource('user-roles', 'ACL\UserRoleController');

            // TimeZONe
            $api->resource('timezone', 'TimeZoneController');

            //Referral Types
            $api->resource('referral-types', 'ReferralTypeController');

            // Notifications
            $api->group(['middleware' => 'nocache'], function ($api) {
                $api->resource('notifications', 'NotificationController');
            });

            // get logs
            $api->get('activity-logs', 'ActivityLogController@index');

            $api->resource('route-wizards', 'RouteWizardController');

            $api->post('wizard-client-setup', 'ClientController@wizardClientSetup');

            $api->post('save-assigned-resource', 'EmployeeClientProjectController@saveAssignedResource');

            // Client Teambuilder Bucket
            $api->group(['prefix' => 'team-builder'], function ($api) {
                $api->get('/', 'ClientTeambuilderBucketController@index');
                $api->post('/', 'ClientTeambuilderBucketController@saveSelectedResource');
                $api->post('save-talent', 'ClientTeambuilderBucketController@saveSelectedAvailableResource');
                $api->post('save-team-builder', 'ClientTeambuilderBucketController@saveTeamBuilder');
            });

            $api->get('rss-feeds', 'ClientController@getRSSFeeds');

            //Client Onboarding Checklist
            $api->resource('client-onboarding-checklist', 'ClientOnboardingChecklistController');

            $api->get('tsheet', 'TSheetsReportController@retrieveAndStoreReports');

            $api->get('tsheet-report', 'WeeklyReportController@getTsheetReport');

            $api->get('client-tsheet-report', 'WeeklyReportController@getClientMonthlyReport');

            $api->post('post-report', 'WeeklyReportController@postReport');

            $api->get('employee-working-report', 'WeeklyReportController@getEmployeeWorkingReport');

            $api->get('tsheet-report-override', 'WeeklyReportController@getTsheetNow');

            $api->get('override-response', 'WeeklyReportController@overrideResponse')->middleware('nocache');

            // Holidays
            $api->resource('holidays', 'HolidayController');

            // Timesheets
            $api->group(['prefix' => 'timesheets'], function ($api) {
                $api->resource('/', 'TimesheetController');
                $api->get('monthly', 'TimesheetController@monthly');
                $api->get('weekly', 'TimesheetController@weekly');
                $api->post('generate', 'TimesheetController@generate');
                $api->post('generate-assignments', 'TimesheetController@generateJobcodeAssignments');
                $api->get('generation-response', 'TimesheetController@getGenerationResponse')->middleware('nocache');
                $api->post('map', 'TimesheetController@map');
            });
            $api->resource('timesheet-jobcodes', 'TimesheetJobcodeController');
            $api->resource('timesheet-users', 'TimesheetUserController');
            $api->resource('client-timesheet-jobcodes', 'ClientTimesheetJobcodeController');
            $api->resource('employee-timesheet-users', 'EmployeeTimesheetUserController');
            $api->resource('timesheet-jobcode-assignments', 'TimesheetJobcodeAssignmentController');
            $api->resource('jobcode-assignments', 'JobcodeAssignmentController');

            // Feedback
            $api->resource('feedback-category', 'FeedbackCategoryController');
            $api->resource('feedback', 'FeedbackController');
            $api->resource('feedback-response', 'FeedbackResponseController');

            // Creative Service
            $api->resource('creative-service', 'CreativeServiceController');
            $api->resource('creative-service-applicant', 'CreativeServiceApplicantController');
            $api->resource('creative-service-file', 'CreativeServiceFileController');

            // Leads
            $api->group(['prefix' => 'leads'], function ($api) {
                $api->get('employees', 'LeadController@index');
            });

            // User Timezones
            $api->resource('user-timezones', 'UserTimezoneController');

            $api->resource('characteristic', 'CharacteristicController');
            $api->resource('employee-academic', 'EmployeeAcademicController');
            $api->resource('employee-characteristic', 'EmployeeCharacteristicController');
            $api->resource('employee-position-type', 'EmployeePositionTypeController');
            $api->resource('position-type', 'PositionTypeController');
            // Operations Documents
            $api->resource('operations-documents', 'OperationsDocumentController');
            //Weekly Floor Report
            $api->group(['prefix' => 'weekly-floor-report'], function ($api) {
                $api->resource('/', 'WeeklyFloorReportController');
                $api->post('save', 'WeeklyFloorReportController@completeSave');
            });

            //Weekly Floor Question
            $api->resource('weekly-floor-question', 'WeeklyFloorQuestionController');

            //Weekly Floor Answer
            $api->resource('weekly-floor-answer', 'WeeklyFloorAnswerController');
        });
    });
}
