<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <title>Daily Reports Status</title>
        <!--[if !mso]><!-- -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!--<![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </head>
    <body>

        <h2>Total Employees: {{ count($all_employees) }}</h2>
        <h2>Number of Employees who sent a Daily Report: {{ count($employees_submitted_yesterday) }}</h2>
        <h2>Employee(s) who did not submit a report ({{ count($employee_records) }}):</h2>
        <table border="1" cellpadding="5" cellspacing="0" width="100%">
            <tr>
                <th>Employee No.</th>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Status</th>
                <th>Position</th>
            </tr>
            @foreach ($employee_records as $employee)
                <tr>
                    <td>{{ $employee->employee_no }}</td>
                    <td>{{ $employee->last_name }}</td>
                    <td>{{ $employee->first_name }}</td>
                    <td>{{ $employee->status_name }}</td>
                    <td>{{ $employee->job_title }}</td>
                </tr>
            @endforeach
        </table>
    
    </body>
</html>
