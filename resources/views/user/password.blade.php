<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Full Scale Rocks!</title>
        <meta name="csrf-token" content="{{ csrf_token()  }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('fonts/line-awesome/css/line-awesome.min.css') }}" />
        <link rel="icon" href="{{ asset('images/fs-icon-150x150.png')  }}" size="32x32" />
        <link rel="icon" href="{{ asset('images/fs-icon')  }}" size="192x192" />
        <link rel="stylesheet" type="text/css" href="{{ asset('fonts/montserrat/styles.css') }}" />
        <link rel="stylesheet" href="{{ mix('/css/theme.css')  }}" />
    </head>
    <body class="ks-navbar-fixed ks-sidebar-default ks-sidebar-position-fixed ks-page-header-fixed ks-theme-primary"> <!-- remove ks-page-header-fixed to unfix header -->
        <div class="ks-page">
            <div class="ks-page-content" style="padding-top: 100px">
                <div class="ks-logo">
                    <img src="/images/fs-logo.png" style="width: 200px" />
                </div>
                <div class="card panel panel-default ks-light ks-panel ks-login">
                    <div class="card-block">
                        @if(!$isSuccess)
                        <form method="POST" action="/user-verification/changepass">
                            @csrf
                            <input name="i" type="hidden" value="{{ $i }}"/>
                            <input name="code" type="hidden" value="{{ $code }}"/>
                            <input name="h" type="hidden" value="{{ $h }}"/>
                            <h4 class="ks-header">Create New Password</h4>
                            <div class="form-group">
                                <div class="input-icon icon-left icon-lg icon-color-primary">
                                    <input type="password" id="pass1" name="password" class="form-control" placeholder="Password" autocomplete="off" autofocus required minlength="6"/>
                                    <span class="icon-addon">
                                        <span class="la la-key"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-icon icon-left icon-lg icon-color-primary">
                                    <input type="password" id="pass2" name="confirm-password" class="form-control" placeholder="Confirm Password" required minlength="6"/>
                                    <span class="icon-addon">
                                        <span class="la la-key"></span>
                                    </span>
                                </div>
                                <div id="form-error-container">
                                    <span class="form-error-message">*** Passwords doesn't match ***</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <button id="changePassBtn" class="btn btn-success btn-block" type="submit">Submit</button>
                            </div>
                        </form>
                        @endif
                        @if($isSuccess)
                            <div id="form-success-container">
                                <span class="form-success-message"><b>SUCCESSFULLY SAVED YOUR PASSWORD. </b><br/>Redirecting you to the login page in 3s</span>
                            </div>
                            <script type="text/javascript">
                                localStorage.removeItem('vuex');
                                localStorage.removeItem('auth_token');
                                setTimeout(function() {
                                    window.location.href = '/login';
                                }, 3000);
                            </script>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <style type="text/css" scoped>
            .ks-logo {
                color: #25628f;
                font-size: 30px;
                font-weight: 700;
                margin-bottom: 40px;
                text-align: center;
            }
            .ks-page {
                width: 364px;
                margin: 0 auto;
                border-radius: 2px;
                background-color: #fff;
            }
            .card-block {
                padding: 30px;
            }
            .ks-header {
                text-align: center;
            }
            .form-error-message {
                color: red;
            }
            #form-error-container {
                display: none;
                margin-top: 10px;
                text-align: center;
            }
            .form-success-message {
                color: green;
            }
            #form-success-container {
                margin-top: 10px;
                text-align: center;
            }
        </style>
        <script type="text/javascript">
            window.onload = function() {
                document.getElementById("changePassBtn").addEventListener("click", function(event){
                    if(document.getElementById("pass1").value.length > 6 && document.getElementById("pass2").value.length > 6 && document.getElementById("pass1").value != document.getElementById("pass2").value){
                        document.getElementById("form-error-container").style.display = "block";
                        event.preventDefault();
                    }
                });
            };
        </script>
    </body>
</html>
