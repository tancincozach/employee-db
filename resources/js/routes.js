import NProgress from 'nprogress';
import Router from 'vue-router';
import OAuth from '@common/oauth/OAuth';
import { ability } from './common/oauth/ability.js';
import defineAbilityFor from '@common/oauth/ability';
import store from './store/index.js';

let routes = [
{
    path: '/',
    component: require('@layouts/master-page'),
    meta: {
        requiresAuth: true,
    },
    children: [
    {
        name: 'dashboard',
        path: '/dashboard',
        // component: require("@views/pages/dashboard/dashboard")
        component: require('@views/pages/wizard/wizard'),
        meta: {
            resource: 'default_dashboard_page',
        },
        beforeEnter: (to, from, next) => {
            let data = store.state.auth.data.data.roles.data;
            // console.log(data[0].role.data);
            if (
                data.filter(e => e.role.data.display_name === 'Employee').length > 0
            ) {
                return next({ path: '/employee-dashboard' });
            } else {
                return next();
            }
        },
    },
    {
        name: 'employee-dashboard',
        path: '/employee-dashboard',
        component: require('@views/pages/dashboard/employee-dashboard'),
    },
    {
        name: 'statistics',
        path: '/statistics',
        component: require('@views/pages/dashboard/statistics'),
    },
    {
        name: 'client-page',
        path: '/client-page',
        component: require('@views/pages/client-wizard/client-wizard'),
    },
    {
        name: 'default',
        path: '/',
        component: require('@views/pages/wizard/wizard'),
        beforeEnter: (to, from, next) => {
            let data = store.state.auth.data.data.roles.data;
            // console.log(data[0].role.data);
            if (
                data.filter(e => e.role.data.display_name === 'Employee').length > 0
            ) {
                return next({ path: '/employee-dashboard' });
            } else {
                return next();
            }
        },
        //If above component path will be changed, please update the sidebar.vue employees > employees router-link
    },
    {
        name: 'wizard',
        path: '/wizard',
        component: require('@views/pages/wizard/wizard'),
        beforeEnter: (to, from, next) => {
            let data = store.state.auth.data.data.roles.data;
            if (
                data.filter(e => e.role.data.display_name === 'Employee').length > 0
            ) {
                return next({ path: '/employee-dashboard' });
            } else {
                return next();
            }
        },
    },
    {
        name: 'manage-clients',
        path: '/manage-clients',
        component: require('@views/pages/wizard/action-items'),
    },
    {
        name: 'manage-employees',
        path: '/manage-employees',
        component: require('@views/pages/wizard/action-items'),
    },
    {
        name: 'manage-reports',
        path: '/manage-reports',
        component: require('@views/pages/wizard/action-items'),
    },
    {
        name: 'manage_operations',
        path: '/manage-operations',
        component: require('@views/pages/operations/index'),
    },
    {
        name: 'time-management',
        path: '/time-management',
        component: require('@views/pages/wizard/action-items'),
    },
    {
        name: 'review-client-prospects',
        path: '/review-client-prospects',
        component: require('@views/pages/wizard/recent-client-prospect'),
        meta: {
            resource: 'review_recent_client_page',
        },
    },
    {
        name: 'form-submitted',
        path: 'form-submitted',
        component: require('@views/pages/wizard/form-submitted'),
    },

    {
        name: 'available-resources',
        path: '/available-resources',
        component: require('@views/pages/available-resources/find-available-resources'),
        meta: {
            resource: 'categories'
        }
    },
    {
        name: 'resource-submitted',
        path: '/resource-submitted',
        component: require('@views/pages/available-resources/resource-submitted'),
    },
    {
        name: 'search',
        path: '/search/:page',
        component: require('@views/pages/search-utility'),
    },
    {
        name: 'search-resources',
        path: '/search-resources/:page',
        component: require('@views/pages/search-utility/search-utility-resources'),
    },
    {
        name: "employees",
        path: "/employees",
        component: require("@views/pages/employee/employee-list"),
        meta: {
            resource: "employees_list"
        }
    },
    {
        name: "employee",
        path: "/employee/:id/profile",
        component: require("@views/pages/employee/employee-profile"),
        meta: {
            resource: "employees"
        }
    },
    {
        name: "my-profile",
        path: "/my/:id/profile",
        component: require("@views/pages/employee/employee-profile"),
        meta: {
            resource: "employee_profile"
        }
    },
    {
        name: "new-employee",
        path: "/employee/new",
        component: require("@views/pages/employee/_forms/new-employee"),
        meta: {
            resource: "employees"
        }
    },
    {
        name: "employee-checklist",
        path: "/employee/:id/checklist",
        component: require("@views/pages/employee/employee-checklist"),
        meta: {
            resource: "employee_checklist"
        }
    },
    {
        //Resource Allocation
        name: "resource_allocation",
        path: "/resource-allocation",
        component: require("@views/pages/resource/resource-allocation"),
        meta: {
            resource: "resources"
        }
    },
    {
        //applicants
        name: "applicants",
        path: "/applicants",
        component: require("@views/pages/applicants/applicant-list"),
        meta: {
            resource: "applicants"
        }
    },
    {
        //Client Management
        name: "clients",
        path: "/clients",
        component: require("@views/pages/client/client-management"),
        meta: {
            resource: "clients_page"
        }
    },
    {
        // Client Report
        name: "client_reports",
        path: "/client-reports",
        component: require("@views/pages/client/report"),
        meta: {
            resource: "client_report_page"
        }
    },
    {
        name: 'team-dashboard',
        path: '/team-dashboard',
        component: require('@views/pages/client/dashboard'),
        meta: {
            resource: "client_report_page"
        }
    },
    {
        name: "email_template",
        path: "/feedback/email-templates",
        component: require("@views/pages/client-feedback/email-templates/email-templates"),
        meta: {
            resource: "email_templates"
        }
    },
    {
        name: "add_email_template",
        path: "/feedback/add-email-template",
        component: require("@views/pages/client-feedback/email-templates/add-email-template"),
        meta: {
            resource: "add_email_template"
        }
    },
    {
        name: "update-email-template",
        path: "/feedback/email-templates/:id",
        component: require("@views/pages/client-feedback/email-templates/add-email-template"),
        meta: {
            resource: "update_email_template"
        }
    },
    {
        name: "client",
        path: "/client/:id/profile",
        component: require("@views/pages/client/client-profile"),
        meta: {
            resource: "clients"
        }
    },
    {
        name: 'client-login-access',
        path: '/client/:id/login-access',
        component: require('@views/pages/client/client-allow-login-access'),
    },
    {
        name: "skills",
        path: "/skills",
        component: require("@views/pages/skills/skill-list"),
        meta: {
            resource: "skill_list"
        }
    },
    {
        name: "categories",
        path: "/categories",
        component: require("@views/pages/categories/categories-list"),
        meta: {
            resource: "categories_list"
        }
    },
    {
        name: "positions",
        path: "/positions",
        component: require("@views/pages/position/position-list"),
        meta: {
            resource: "position_list"
        }
    },
    {
        name: "daily-report",
        path: "/daily-report",
        component: require("@views/pages/daily-report/daily-report-list"),
        meta: {
            resource: "daily_report"
        }
    },
    {
        name: "employee-resources",
        path: "/employee-resources",
        component: require("@views/pages/employee-resources/employee-resources.vue")
    },
    {
        name: "employee-feedback_form",
        path: "/employee/feedback",
        component: require("@views/pages/feedback-entry/feedback-form"),
    },
    {
        name: "referral",
        path: "/referral",
        component: require("@views/pages/referral/referral-form"),
    },
    // Feedback Form
    {
        name: "employee-feedback-list",
        path: "/employee/feedback-list",
        component: require('@views/pages/feedback-entry/response-list'),
        meta: {
            resource: "feedback_list"
        }
    },
    // Certification Leaderboard
    {
        name: "certifications",
        path: "/certifications",
        component: require('@views/pages/certification-leaderboard/leaderboard')
    },
    {
        name: "creative-services-request",
        path: "/creative-services-request",
        component: require("@views/pages/creative-request/creative-services-request-form"),
        meta: {
            //resource: "creative-services-request-form"
        }
    },
    {
        name: "email-report-list",
        path: "/templates",
        component: require("@views/pages/reports/email-report-list"),
        meta: {
            resource: "report_template_nav"
        }
    },
    {
        name: "users",
        path: "/users",
        component: require("@views/pages/users/index.vue"),
        meta: {
            resource: "user_list"
        }
    },
    {
        name: "employee-status",
        path: "/settings/employee-status",
        component: require("@views/pages/employee/employee-status-list"),
        meta: {
            resource: "settings_employee_status"
        }
    },
    {
        name: "holidays",
        path: "/settings/holidays",
        component: require("@views/pages/holiday/holiday-list"),
        meta: {
            resource: 'holidays',
        },
    },
    {
        name: "hacker-rank-results",
        path: "/hacker-rank-results",
        component: require("@views/pages/hacker-ranks/hacker-ranks-list-result"),
        meta: {
            resource: 'hacker_rank_results',
        },
    },
    {
        name: "hacker-rank-tests",
        path: "/hacker-rank-tests",
        component: require("@views/pages/hacker-ranks/hacker-ranks-test-list"),
        meta: {
            resource: 'hacker_rank_tests',
        },
    },
    //Work From Home
    {
        name: "work-from-home-report",
        path: "/work-from-home/report",
        component: require("@views/pages/work-from-home/work-from-home-report"),
        meta: {
            resource: "work_from_home_report"
        }
    },
    {
        name: "weekly-floor-report",
        path: "/weekly-floor-report",
        component: require("@views/pages/weekly-floor-report/weekly-floor-report-wizard"),
        meta: {
            resource: "weekly_floor_report"
        }
    },
    {
        name: "weekly-floor-report-list",
        path: "/weekly-floor-report/list",
        component: require("@views/pages/weekly-floor-report/weekly-floor-report-dashboard"),
        meta: {
            //resource: "weekly_floor_report"
        }
    },
    {
        name: "work-from-home-list",
        path: "/work-from-home/list",
        component: require("@views/pages/work-from-home/work-from-home-list"),
        meta: {
            resource: "work_from_home_employee_requests_list"
        }
    },
    {
        name: "questionnaires",
        path: "/survey-setup/questionnaires",
        component: require("@views/pages/client-feedback/questionnaires/questionnaires-list"),
        meta: {
            resource: "questionnaires"
        }
    },
    {
        name: "question_categories",
        path: "/survey-setup/question-categories",
        component: require("@views/pages/client-feedback/categories/categories-list"),
        meta: {
            resource: "question_categories"
        }
    },
    {
        name: "questions",
        path: "/survey-setup/questions",
        component: require("@views/pages/client-feedback/questions/questions-list"),
        meta: {
            resource: "questions"
        }
    },
    {
        name: "worklogs-parser",
        path: "/worklogs/parser",
        component: require("@views/pages/worklogs/parser"),
        meta: {
            resource: "worklogs_parser"
        }
    },
    {
        name: "daily-reports",
        path: "/daily-reports",
        component: require("@views/pages/daily-report/daily-report-employee-list"),
        meta: {
            resource: "employee_daily_report"
        }
    },
    {
        name: "employee-daily-reports",
        path: "/employee/:id/daily-reports",
        component: require("@views/pages/daily-report/employee-daily-reports"),
        meta: {
            resource: "employee_daily_report"
        }
    },
    {
        name: 'project-surveys',
        path: '/client/:id/project-surveys',
        component: require('@views/pages/client-feedback/survey/survey-form'),
        meta: {
            resource: "project_surveys"
        }
    },
    {
        name: 'project-surveys-manualsend',
        path: '/project-surveys/:id/send-survey',
        component: require('@views/pages/client-feedback/survey/send-survey'),
        meta: {
            resource: "survey_sent"
        }
    },
    {
        name: 'worklogs-report',
        path: '/worklogs/weekly',
        //component: require('@views/pages/worklogs/tsheet-logs'),
        component: require("@views/pages/timesheets/weekly-reports"),
    },
    {
        name: 'worklogs-employee-report',
        path: '/worklogs/monthly',
        //component: require('@views/pages/worklogs/employee-report'),
        component: require("@views/pages/timesheets/monthly-summary"),
        meta: {
            resource: "timesheets",
        },
    },
    {
        name: 'worklogs-daily',
        path: '/worklogs/daily',
        //component: require('@views/pages/worklogs/tsheet-logs-daily'),
        component: require("@views/pages/timesheets/daily-tracker"),
        meta: {
            resource: "timesheets",
        },
    },
    {
        name: "roles",
        path: "/roles",
        component: require("@views/pages/roles/"),
        meta: {
            resource: "role_list"
        }
    },
    {
        name: "settings",
        path: "/settings",
        component: require("@views/pages/settings/"),
        meta: {
            resource: "settings"
        }
    },
    {
        name: "user-management",
        path: "/user-management",
        component: require("@views/pages/users/user-list.vue"),
        meta: {
            resource: "users"
        }
    },
    {
        name: 'performance-review',
        path: '/performance-review/:id?',
        component: require('@views/pages/performance-review/performance-review-wizard.vue'),
        meta: {
            resource: "performance_review_page"
        },
    },
    // {
    //     name: 'smart-team-builder',
    //     path: '/smart-team-builder',
    //     component: require('@views/pages/smart-team-builder'),
    //     meta: {
    //         resource: 'smart_builder_page',
    //     }
    // },
    {
        name: 'smart-team-builder',
        path: '/wizard-smart-team-builder',
        component: require('@views/pages/smart-team-builder/wizard-smart-team-builder.vue'),
        redirect: { name: 'smart-client-team-builder' },
        meta: {
            resource: 'smart_builder_page',
        },
        children: [
        {
            name: "smart-client-team-builder",
            path: "client-team-builder",
            component: require("@views/pages/smart-team-builder/client-team-builder")
        },
        {
            name: "smart-team-builder-resource-type",
            path: "type-of-resource",
            component: require("@views/pages/wizard/type-of-resource")
        },
        {
            name: "smart-team-builder-bucket",
            path: "team-builder-bucket",
            component: require('@views/pages/smart-team-builder/team-builder-bucket.vue'),
        },
        {
            name: "smart-review-team-builder",
            path: "review-and-send",
            component: require('@views/pages/smart-team-builder/review-team-builder.vue'),
        },
        {
            name: "smart-wizard-talent-selected",
            path: "talent-selected",
            component: require('@views/pages/client-wizard/form-submitted.vue'),
        }
        ]
    },
    {
        name: 'client-wizard',
        path: '/client-wizard',
        component: require('@views/pages/client-wizard/review-client-wizard.vue'),
        redirect: { name: 'client-type-of-resource' },
        meta: {
            resource: 'client_talent_pool_page',
        },
        children: [
        {
            name: "client-type-of-resource",
            path: "client-type-of-resource",
            component: require('@views/pages/client-wizard/client-type-of-resource.vue'),
        },
        {
            name: "client-technology-selection",
            path: "client-technology-selection",
            component: require('@views/pages/client-wizard/client-technology-selection.vue'),
        },
        {
            name: "team-builder-client",
            path: "team-builder-client",
            component: require('@views/pages/smart-team-builder/team-builder-client.vue'),
        },
        {
            name: "client-wizard-talent-selected",
            path: "talent-selected",
            component: require('@views/pages/client-wizard/form-submitted.vue'),
        }
        ]
    },

    //Tools
    {
        //Route Wizard Tools Page
        name: "route-wizards-tile",
        path: "/route-wizards-tile",
        component: require("@views/pages/wizard/route-wizard-tiles"),
        meta: {
            resource: "route_wizards_page"
        }
    },
    {
        name: "creative-request-list",
        path: "/creative-request-list",
        component: require("@views/pages/creative-request/creative-request-list"),
    },
    //Timesheets
    {
        name: 'daily-tracker',
        path: '/daily-tracker',
        component: require("@views/pages/timesheets/daily-tracker"),
    },
    {
        name: 'monthly-summary',
        path: '/monthly-summary',
        component: require("@views/pages/timesheets/monthly-summary"),
    },
    {
        name: 'weekly-reports',
        path: '/weekly-reports',
        component: require("@views/pages/timesheets/weekly-reports"),
    },
    {
        name: 'timesheet-user-mapping',
        path: '/timesheet-user-mapping',
        component: require("@views/pages/timesheets/timesheet-user-mapping"),
    },
    {
        name: 'faqs',
        path: '/faqs',
        component: require("@views/pages/faqs/client"),
    }
    ]
},
{
    path: "/",
    component: require("@layouts/guest-page"),
    meta: { requireGuest: true },
    children: [
    {
        path: "/survey/:surveyLink",
        component: require("@layouts/client-survey")
    }
    ]
},
{
    path: "/",
    component: require("@layouts/guest-page"),
    children: [
    {
        path: "/survey/preview/:id",
        component: require("@views/pages/client-feedback/questionnaires/preview-questionnaire")
    }
    ]
},
{
    path: "/",
    component: require("@layouts/guest-page"),
    children: [
    {
        name: "view-responses",
        path: "/survey/responses/:link",
        component: require("@views/pages/client-feedback/survey/preview-response")
    }
    ]
},
{
    name:'index',
    path: '/'
},
{
    path: '/',
    component: require('@layouts/guest-page'),
    meta: { requiresGuest: true },
    children: [
    {
        path: '/login',
        component: require('@views/auth/login'),
    },
    {
        path: '/faq',
        component: require("@views/pages/faqs/client"),
        meta: { requiresGuest: false },
    }
    ],
},
{
    path: '/login/google/success',
    component: require('@views/auth/login-google'),
    meta: {
        requiresAuth: true,
    },
},
{
    path: '/profile/:username/v1',
    component: require('@layouts/profile-card'),
    meta: {
        requireGuest: true,
    },
},
{
    path: '/profile/:username',
    component: require('@layouts/profile-card-video'),
    meta: { requireGuest: true },
},
{
    name: 'applicant',
    path: '/applicant',
    component: require('@layouts/applicant-form'),
    meta: {
        requireGuest: true,
    },
},
{
    name: 'applicant-onboarding',
    path: '/applicant-onboarding',
    component: require('@layouts/applicant-onboarding'),
    meta: {
        requireGuest: true,
    },
},
{
    name: 'agency-partner-survey',
    path: '/agency-partner-survey',
    component: require('@layouts/agency-partner-survey'),
    meta: {
        requireGuest: true,
    },
},
{
    name: 'client-onboarding',
    path: '/client-onboarding',
    component: require('@layouts/client-prospect-onboarding'),
    meta: {
        requireGuest: true,
    },
},
{
    name: 'force-logout',
    path: '/force-logout',
    component: require('@layouts/force-logout'),
    meta: {
        requireGuest: true,
    },
},
{
    path: '/thankyou/:page',
    component: require('@layouts/form-submitted'),
    meta: {
        requireGuest: true,
    },
},
{
    path: '/thankyou',
    component: require('@layouts/form-submitted'),
    meta: {
        requireGuest: true,
    },
},
{
    name: 'surveysubmitted',
    path: '/surveysubmitted',
    component: require('@layouts/survey-submitted'),
    meta: {
        requireGuest: true,
    },
},
{
    path: '*',
    component: require('@layouts/error-page'),
    children: [
    {
        path: '*',
        component: require('@views/errors/page-not-found'),
    },
    ],
},
];

const router = new Router({
    routes,
    linkActiveClass: 'active',
    mode: 'history',
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        } else {
            return { x: 0, y: 0 };
        }
    },
});

// Auth Guards
router.beforeEach((to, from, next) => {
    // This page requires authentication
    if (to.matched.some(m => m.meta.requiresAuth)) {
        return OAuth.isAuthenticated().then(response => {
            if (!response) {
                return next({ path: '/login' });
            }

            if (store.state.auth.data) {
                let roleData = store.state.auth.data.data.roles.data.filter(
                    role => role.is_enabled === 1,
                );
                ability.update(defineAbilityFor(roleData[0]));
            }

            if (to.meta.resource) {
                const canNavigate = ability.can('view', to.meta.resource);

                let employeeId = to.path.split('/')[2];

                if (to.name === 'my-profile') {
                    if (store.state.auth.data.data.employeeId) {
                        if (
                            employeeId != store.state.auth.data.data.employeeId.data.id &&
                            employeeId
                        ) {
                            return next({
                                path:
                                    '/my/' +
                                    store.state.auth.data.data.employeeId.data.id +
                                    '/profile',
                            });
                        }
                    }
                }

                const notAnEmployee =
                    store.state.employees.logged_in_employee.length == 0;
                if (to.name == 'daily-report' && notAnEmployee) {
                    return next({ path: '/' });
                }
                if (!canNavigate) {
                    return next({ path: '/' });
                }
            }

            return next();
        });
    }

    // This page is for unauthenticated user
    if (to.matched.some(m => m.meta.requiresGuest)) {
        return OAuth.isAuthenticated().then(response => {
            if (response) {
                return next({ path: '/' });
            }

            return next();
        });
    }

    return next();
});

// Add page loader
router.beforeResolve((to, from, next) => {
    // if this isn't an initial page load
    if (to.name) {
        // start the route progress bar
        NProgress.start();
    }
    next();
});

router.afterEach((to, from) => {
    // complete the animation of the route progress bar.
    NProgress.done();
});

export default router;
