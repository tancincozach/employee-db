import { User } from '@common/model/User';
import EDBMixin from '@common/mixin/EDBMixin';

export default {
    mixins: [
        EDBMixin
    ],
    data(){
        return {
            UserMixin: {
                email_pattern: '@fullscale.io',
                finalUsername: ''
            }
        };
    },
    methods: {
        async getFinalUsername(fname, mname, lname){
            let fetchUsername = '';
            let pass = false;
            let level = 1;
            let limit = this.userFilter(fname + mname);
            const vm = this;
            while(!pass && level < limit.length){
                fetchUsername = this.generateUsername(fname, mname, lname, level);
                await User.get({
                    applicant: 'any',
                    username: fetchUsername.toLowerCase(),
                    email: fetchUsername.toLowerCase() + this.UserMixin.email_pattern
                }).then((res) => {
                    if(!vm.UserMixin.finalUsername || vm.UserMixin.finalUsername.length <= 0){
                        if(!(res.data && res.data[0] && res.data[0].id)){
                            vm.UserMixin.finalUsername = fetchUsername;
                            pass = true;
                        }
                    } else {
                        pass = true;
                    }
                });
                level++;
            }
        },
        generateUsername(fname, mname, lname, level){
            const part1 = this.userFilter(fname) + this.userFilter(mname);
            const part2 = this.userFilter(lname);
            if(part1.length < level){ return 'conflict-' + (new Date().getTime()); }

            let fhint = '';
            let x = 0;
            for(x = 0; x < level; x++){
                fhint += part1.charAt(x);
            }
            return (fhint + part2);
        },
        userFilter(str){
            let fstr = this.edbReplaceAll(str, ' ', '');
            return fstr.toLowerCase();
        }
    }
}
