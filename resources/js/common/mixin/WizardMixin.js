export default {
    data() {
        return {
            wizardEventName: 'pass-data',
        };
    },
    props: {
        wizardEventBus: {
            type: Object,
        }
    },
    mounted() {
        this.wizardEventBus.$on(this.wizardStepName, proceed => {
            proceed(this.wizardBeforeChange());
        });
    },
    methods: {
        passDataToWizard(data) {
            this.wizardEventBus.$emit(
                `${this.wizardEventName}-${this.wizardStepName}`,
                data
            );
        },
    },
}
