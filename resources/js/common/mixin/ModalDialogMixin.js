export default {
    props: {
        openModal: {
            type: Boolean,
            default: false
        }
    },
    data() {
        return {
            isOpen: null,
            body: document.body
        }
    },
    mounted() {
        this.isOpen = this.openModal;
        if (this.isOpen) {
            this.body.classList.add("modal-open");
        }
    },
    methods: {
        closeModal() {
            this.removeModalOpenClass()
            this.$emit('close');
        },
        removeModalOpenClass() {
            this.body.classList.remove("modal-open");
        }
    },
    beforeDestroy(){
        this.removeModalOpenClass()
    }
}
