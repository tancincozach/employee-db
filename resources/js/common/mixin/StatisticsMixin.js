import EDBMixin from '@common/mixin/EDBMixin';
import _ from 'lodash';

export default {
  data() {
    return {
      injectedCSS: [],
      responsive_sequence: [
        {
          breakpoint: 1700,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 420,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 1600,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 400,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 1550,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 360,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 1400,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 340,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 1240,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 320,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 1100,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 300,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 960,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 280,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 850,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 250,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 768,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 500,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 530,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 485,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 484,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 460,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 467,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 420,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 440,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 390,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 400,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 360,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 350,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 330,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 330,
          options: {
            dataLabels: {
              enabled: false,
            },
            chart: {
              width: 230,
              height: 'auto',
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
            },
          },
        },
        {
          breakpoint: 1980,
          options: {
            /*dataLabels:{
                            enabled: false
                        },*/
            chart: {
              width: 450,
              type: 'donut',
            },
            legend: {
              show: false,
              position: 'bottom',
              horizontalAlign: 'center',
              floating: false,
              width: '100%',
              height: 'auto',
            },
          },
        },
      ],
      chartData: {
        separator: '_@@_',
        colors: [
          '#cbd392',
          '#a2c0e0',
          '#dda59e',
          '#f8dfa4',
          '#afa7c0',
          '#b9a591',
          '#7d6e5f',
          '#8bb0af',
          '#99739e',
          '#6da076',
        ],
        employees: {
          enabled: false,
          fromApplicant: 0,
          rawData: [
            /*{
                            role: 'Role Name',
                            count: 'Total count this skill exists in each resource that have proficiency of 5 and up'
                        }*/
          ],
          series: [],
          chartOptions: {
            plotOptions: {
              pie: {
                donut: {
                  labels: {
                    show: true,
                    name: {
                      show: true,
                      label: 'Percentage',
                    },
                    value: {
                      show: true,
                      formatter: function(val) {
                        const total = $('#chart1-labels').attr('data-total');
                        return total
                          ? parseFloat((val / total).toFixed(2) * 100) + '%'
                          : val;
                      },
                    },
                  },
                },
              },
            },
            labels: [],
            colors: [],
            chart: {
              type: 'donut',
            },
            stroke: {
              show: true,
              curve: 'smooth',
              width: 2,
            },
            dataLabels: {
              enabled: false,
            },
            legend: {
              // This will show the numbers beside the label
              formatter: function(val, opts) {
                return val + '_@@_' + opts.w.globals.series[opts.seriesIndex];
              },
            },
            responsive: [],
          },
        },
        applicants: {
          enabled: false,
          rawData: {
            exam: 0,
            hired: 0,
            pending: 0,
            failed: 0,
            rejected: 0,
            offer_not_accepted: 0,
          },
          series: [],
          chartOptions: {
            plotOptions: {
              pie: {
                donut: {
                  labels: {
                    show: true,
                    name: {
                      show: true,
                      label: 'Percentage',
                    },
                    value: {
                      show: true,
                      formatter: function(val) {
                        const total = $('#chart2-labels').attr('data-total');
                        return total
                          ? parseFloat((val / total).toFixed(2) * 100) + '%'
                          : val;
                      },
                    },
                  },
                },
              },
            },
            colors: [],
            labels: [],
            responsive: [],
            dataLabels: {
              enabled: false,
            },
            legend: {
              // This will show the numbers beside the label
              formatter: function(val, opts) {
                return val + '_@@_' + opts.w.globals.series[opts.seriesIndex];
              },
            },
          },
        },
        clients: {
          enabled: false,
          rawData: {
            prospect: 0,
            active: 0,
            end: 0,
          },
          series: [], //[44, 55, 13, 43, 22, 44, 55, 13, 43, 22],
          chartOptions: {
            plotOptions: {
              pie: {
                donut: {
                  labels: {
                    show: true,
                    name: {
                      show: true,
                      label: 'Percentage',
                    },
                    value: {
                      show: true,
                      formatter: function(val) {
                        const total = $('#chart3-labels').attr('data-total');
                        return total
                          ? parseFloat((val / total).toFixed(2) * 100) + '%'
                          : val;
                      },
                    },
                  },
                },
              },
            },
            colors: [],
            labels: [],
            dataLabels: {
              enabled: false,
            },
            formatter: function(val, opts) {
              return val + '_@@_' + opts.w.globals.series[opts.seriesIndex];
            },
            responsive: [],
          },
        },
        projects: {
          enabled: true,
          rawData: {
            prospect: 0,
            active: 0,
            end: 0,
          },
          series: [44, 55, 13, 43, 22],
          chartOptions: {
            colors: [
              '#cbd392',
              '#a2c0e0',
              '#dda59e',
              '#f8dfa4',
              '#afa7c0',
              '#b9a591',
              '#7d6e5f',
              '#8bb0af',
              '#99739e',
              '#6da076',
            ],
            title: {title: 'Technologies'},
            labels: ['Value 1', 'Value 2', 'Value 3', 'Value 4', 'Value 5'],
            legend: {
              // This will show the numbers beside the label
              formatter: function(val, opts) {
                return val + '_@@_' + opts.w.globals.series[opts.seriesIndex];
              },
            },
            responsive: [],
          },
        },
      },
    };
  },
  watch: {
    'chartData.employees.enabled': function(e) {
      const vm = this;
      setTimeout(function() {
        vm.convertChartLegendsToList(
          'chart1',
          vm.chartData.employees.series,
          vm.chartData.employees.chartOptions.labels,
          vm.chartData.employees.chartOptions.colors,
          'Top Technologies',
        );
      }, 100);
    },
    'chartData.applicants.enabled': function(e) {
      const vm = this;
      setTimeout(function() {
        vm.convertChartLegendsToList(
          'chart2',
          vm.chartData.applicants.series,
          vm.chartData.applicants.chartOptions.labels,
          vm.chartData.applicants.chartOptions.colors,
          'Applicants',
        );
      }, 100);
    },
    'chartData.clients.enabled': function(e) {
      const vm = this;
      setTimeout(function() {
        vm.convertChartLegendsToList(
          'chart3',
          vm.chartData.clients.series,
          vm.chartData.clients.chartOptions.labels,
          vm.chartData.clients.chartOptions.colors,
          'Clients',
        );
      }, 100);
    },
  },
  created() {
    this.chartData.employees.chartOptions.responsive = this.responsive_sequence;
    this.chartData.applicants.chartOptions.responsive = this.responsive_sequence;
    this.chartData.clients.chartOptions.responsive = this.responsive_sequence;

    this.chartData.employees.chartOptions.colors = this.chartData.colors;
    this.chartData.applicants.chartOptions.colors = this.chartData.colors;
    this.chartData.clients.chartOptions.colors = this.chartData.colors;

    this.edbAddCSS(
      'div.apexcharts-tooltip{display:none!important;}',
      'apexcharts-hover-label',
    );
    this.injectedCSS.push('apexcharts-hover-label');
  },
  methods: {
    processEmployeesChart(obj) {
      let role = new Array();
      let counts = new Array();
      let limit = 10; //10th is Others

      /** Insert all in rawData as necessary **/
      _.each(obj, item => {
        /** This is for applicant's hired additional total. All employees with employeeStatuses, meaning, hired via this system **/
        this.chartData.employees.fromApplicant +=
          item.employeeStatuses &&
          item.employeeStatuses.data &&
          item.employeeStatuses.data.length > 0
            ? 1
            : 0;

        if (item.positions && item.positions.data) {
          _.each(item.positions.data, item2 => {
            if (
              this.isItemExists(
                this.chartData.employees.rawData,
                item2.job_title,
                'role',
              )
            ) {
              this.incrementValue(
                this.chartData.employees.rawData,
                'role',
                item2.job_title,
                1,
              );
            } else {
              this.chartData.employees.rawData.push({
                role: item2.job_title,
                count: 1,
              });
            }
          });
        }
      });

      /** Sort rawData by total count descending order, select the top 9, plus others **/
      const finalRaw = _.reverse(
        _.sortBy(this.chartData.employees.rawData, ['count']),
      );
      _.each(finalRaw, obj => {
        if (limit > 0) {
          role.push(obj.role);
          counts.push(parseInt(obj.count));
        }
        limit--;
      });

      /** Assign them to actual variables **/
      this.chartData.employees.series = counts;
      this.chartData.employees.chartOptions.labels = role;

      /** Add delay of enabling the chart, making sure all values in the JSON variable are in place **/
      let vm = this;
      setTimeout(function() {
        vm.chartData.employees.enabled = true;
      }, 100);
    },
    processApplicantsChart(obj) {
      let statuses = [];
      _.each(obj, item => {
        if (item.employeeStatuses && item.employeeStatuses.data) {
          /** It's possible only one record or multiple records **/
          if (item.employeeStatuses.data.length > 1) {
            statuses = _.reverse(
              _.sortBy(item.employeeStatuses.data, [
                function(o) {
                  return o.status.id;
                },
              ]),
            );
          } else {
            statuses = item.employeeStatuses.data;
          }

          if (statuses && statuses.length) {
            switch (statuses[0].status.name.toUpperCase()) {
              case 'HIRED':
                this.chartData.applicants.rawData.hired += 1;
                break;
              case 'PROBATIONARY':
                this.chartData.applicants.rawData.hired += 1;
                break;
              case 'REGULAR':
                this.chartData.applicants.rawData.hired += 1;
                break;
              case 'SUSPENDED':
                this.chartData.applicants.rawData.hired += 1;
                break;
              case 'PENDING OFFER':
                this.chartData.applicants.rawData.pending += 1;
                break;
              case 'FAILED EXAM':
                this.chartData.applicants.rawData.failed += 1;
                break;
              case 'FOR EXAM':
                this.chartData.applicants.rawData.exam += 1;
                break;
              case 'OFFER REJECTED':
                this.chartData.applicants.rawData.rejected += 1;
                break;
              case 'OFFER NOT YET ACCEPTED':
                this.chartData.applicants.rawData.rejected += 1;
                break;
              default:
                break;
            }
          }
        }
      });

      /** Add up those employees who were once an applicant via this system **/
      this.chartData.applicants.rawData.hired += this.chartData.employees.fromApplicant;

      this.chartData.applicants.chartOptions.labels.push('For Exam');
      this.chartData.applicants.series.push(
        this.chartData.applicants.rawData.exam,
      );

      this.chartData.applicants.chartOptions.labels.push('Hired');
      this.chartData.applicants.series.push(
        this.chartData.applicants.rawData.hired,
      );

      this.chartData.applicants.chartOptions.labels.push('Pending Offer');
      this.chartData.applicants.series.push(
        this.chartData.applicants.rawData.pending,
      );

      this.chartData.applicants.chartOptions.labels.push('Failed Exam');
      this.chartData.applicants.series.push(
        this.chartData.applicants.rawData.failed,
      );

      this.chartData.applicants.chartOptions.labels.push(
        'Offer not yet accepted',
      );
      this.chartData.applicants.series.push(
        this.chartData.applicants.rawData.offer_not_accepted,
      );

      this.chartData.applicants.chartOptions.labels.push('Offer Rejected');
      this.chartData.applicants.series.push(
        this.chartData.applicants.rawData.rejected,
      );

      let vm = this;
      setTimeout(function() {
        vm.chartData.applicants.enabled = true;
      }, 100);
    },
    processClientsChart(obj) {
      _.each(obj, item => {
        switch (item.status) {
          case 0:
            this.chartData.clients.rawData.active += 1;
            break;
          case 1:
            this.chartData.clients.rawData.end += 1;
            break;
          case 2:
            this.chartData.clients.rawData.prospect += 1;
            break;
        }
      });

      this.chartData.clients.chartOptions.labels.push('Prospects');
      this.chartData.clients.series.push(
        this.chartData.clients.rawData.prospect,
      );

      this.chartData.clients.chartOptions.labels.push('Active');
      this.chartData.clients.series.push(this.chartData.clients.rawData.active);

      this.chartData.clients.chartOptions.labels.push('End Contract');
      this.chartData.clients.series.push(this.chartData.clients.rawData.end);

      let vm = this;
      setTimeout(function() {
        vm.chartData.clients.enabled = true;
      }, 100);
    },

    isItemExists(items, val, where) {
      return (
        items.filter(item => item[where].toUpperCase() == val.toUpperCase())
          .length > 0
      );
    },
    incrementValue(instance, reference, item, val) {
      //let x = 0;
      _.each(instance, (obj, index) => {
        if (obj[reference] == item) {
          instance[index].count += val;
          return;
        }
      });
    },
    convertChartLegendsToList(selector, series, labels, colors, title) {
      /** selector must be an id **/
      const vm = this;
      const fselector = '#' + selector;
      $(document).ready(function() {
        vm.executeRequiredChartLabelCSS(fselector, selector);
        vm.insertObject(fselector, 'div', selector + '-labels', true);

        let items = [];
        let totals = 0;
        let pointer = 0;
        _.each(labels, label => {
          items.push({
            name: label,
            value: series[pointer],
            color: colors[pointer],
          });
          totals += parseInt(series[pointer]);
          pointer++;
        });

        $(fselector + ' div#' + selector + '-labels').attr({
          'data-total': totals,
        });

        let node = 1;
        _.each(items.reverse(), obj => {
          vm.insertObject(
            fselector + ' div#' + selector + '-labels',
            'div',
            'row' + node,
          );
          vm.insertObject(
            fselector + ' div#' + selector + '-labels' + ' #row' + node,
            'div',
            'itemValue' + node,
          );
          vm.insertObject(
            fselector + ' div#' + selector + '-labels' + ' #row' + node,
            'div',
            'itemText' + node,
          );
          vm.insertObject(
            fselector + ' div#' + selector + '-labels' + ' #row' + node,
            'div',
            'itemIndicator' + node,
          );

          $(fselector + ' div#' + selector + '-labels').addClass(
            'apexchart-items-container',
          );
          $(
            fselector + ' div#' + selector + '-labels' + ' #row' + node,
          ).addClass('apexchart-legend-item');
          $(fselector + ' #row' + node + ' #itemText' + node).addClass(
            'apexchart-legend-desc',
          );
          $(fselector + ' #row' + node + ' #itemValue' + node).addClass(
            'apexchart-legend-value',
          );
          $(fselector + ' #row' + node + ' #itemIndicator' + node).addClass(
            'apexchart-legend-indicator',
          );
          $(fselector + ' #row' + node + ' #itemText' + node).text(obj.name);
          $(fselector + ' #row' + node + ' #itemValue' + node).text(obj.value);

          $(fselector + ' #row' + node + ' #itemIndicator' + node).css({
            'background-color': obj.color,
          });
          node++;
        });

        if (title) {
          vm.insertObject(
            fselector + ' div#' + selector + '-labels',
            'div',
            'total-' + selector,
          );
          vm.insertObject(
            fselector + ' div#' + selector + '-labels',
            'div',
            'about-' + selector,
          );
          $(
            fselector + ' div#' + selector + '-labels div#total-' + selector,
          ).addClass('apexcharts-label-total');
          $(
            fselector + ' div#' + selector + '-labels div#about-' + selector,
          ).addClass('apexcharts-label-about');

          $(
            fselector + ' div#' + selector + '-labels' + ' #total-' + selector,
          ).text('Total: ' + totals);
          $(
            fselector + ' div#' + selector + '-labels' + ' #about-' + selector,
          ).text(title);
        }
      });
    },
    executeRequiredChartLabelCSS(fselector, selector) {
      let mainWidth = $(fselector + ' > div > div[id*="apexcharts"]').width();
      let parentLabelWidth = parseInt(mainWidth * 0.65);

      const apexchart_legend_desc =
        'position: relative!important;' +
        'width: 60%!important;' +
        'display:contents!important;';
      const apexchart_legend_value =
        'position: relative!important;' + 'float: right!important;';
      const apexchart_legend_parent =
        'width: ' +
        parentLabelWidth +
        'px;' +
        'margin-left: 50%;' +
        'left: -' +
        parseInt(parentLabelWidth / 2 / 3 / 2) +
        'px;' +
        'position: relative;' +
        'padding-bottom: 50px;';
      const apexchart_legend_indicator =
        'position: relative!important;' +
        'display: inline!important;' +
        'float: left;' +
        'width: 11px;' +
        'height: 11px;' +
        'margin: 3px 6px 3px 3px;' +
        'border: 1px solid rgba(0, 0, 0, .2);';
      const apexchart_legend_item =
        'color: rgb(55, 61, 63);' +
        'font-size: 12px;' +
        'font-family: Helvetica, Arial, sans-serif;' +
        'padding: 5px 5px 5px 5px;' +
        'border-bottom: 1px solid #eae8e8;';
      const apexchart_handler =
        'position: relative;' +
        'margin-left: 50%!important;' +
        'left: -250px!important;';
      const apexchart_label_title = 'font-weight: bold; padding-bottom: 3px;';
      const apexchart_label_total =
        'color: #4e54a8; font-weight: bold;padding-bottom: 10px!important;';

      this.edbAddCSS(
        fselector +
          '{margin-left: 50%;left: -' +
          parseInt(mainWidth / 2) +
          'px;}',
        fselector + '-main-container',
      );
      this.edbAddCSS(
        '.apexchart-legend-desc{' + apexchart_legend_desc + '}',
        selector + '_desc',
      );
      this.edbAddCSS(
        '.apexchart-legend-value{' + apexchart_legend_value + '}',
        selector + '_value',
      );
      this.edbAddCSS(
        '.apexchart-legend-indicator{' + apexchart_legend_indicator + '}',
        selector + '_indicator',
      );
      this.edbAddCSS(
        '.apexchart-legend-item{' + apexchart_legend_item + '}',
        selector + '_item',
      );

      this.edbAddCSS(
        '.apexcharts-label-about{' + apexchart_label_title + '}',
        selector + '_label_title',
      );
      this.edbAddCSS(
        '.apexcharts-label-total{' + apexchart_label_total + '}',
        selector + '_label_total',
      );
      this.edbAddCSS(
        fselector +
          ' div#' +
          selector +
          '-labels' +
          '{' +
          apexchart_legend_parent +
          '}',
        selector + '_parent',
      );

      this.injectedCSS.push(fselector + '-main-container');
      this.injectedCSS.push(selector + '_desc');
      this.injectedCSS.push(selector + '_value');
      this.injectedCSS.push(selector + '_indicator');
      this.injectedCSS.push(selector + '_item');
      this.injectedCSS.push(selector + '_label_title');
      this.injectedCSS.push(selector + '_label_total');
      this.injectedCSS.push(selector + '_parent');
    },
    insertObject(parentSelector, newElement, newId, insertAfter = false) {
      const item = document.createElement(newElement);
      const ebParentElement = document.querySelector(parentSelector);
      item.id = newId;
      if (!insertAfter) {
        ebParentElement.insertBefore(item, ebParentElement.firstChild);
      } else {
        ebParentElement.appendChild(item);
      }
    },
    insertElement(parentSelector, object, insertAfter = false) {
      const ebParentElement = document.querySelector(parentSelector);
      if (!insertAfter) {
        ebParentElement.insertBefore(object, ebParentElement.firstChild);
      } else {
        ebParentElement.appendChild(object);
      }
    },
    onDestroy() {
      /** This removes all programmatically injected CSS **/
      _.each(this.injectedCSS, id => {
        this.edbRemoveCSS(id);
      });
    },
  },
  mixins: [EDBMixin],
};
