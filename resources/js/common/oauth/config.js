export default {
    url: '/api/auth',
    app_pub_url: '/api/applicant/tokenize',
    app_survey_url: '/api/survey/tokenize',
    auth_token: 'auth_token',
    creds: {
        grant_type: 'password',
        client_id: 2,
        client_secret: 'H5Jq0AfEFG34OYnMKsiRwIfOsdNw0KJnTg3y2Cso'
    }
}
