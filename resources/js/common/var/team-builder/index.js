export default {
    data() {
        return {
            teamBuilderSuggestions: ['available', 'picked'],
            teamBuilderStatus: ['suggested', 'selected', 'declined', 'assigned'],
            teamBuilderLabels: [
                {
                    status: 'available',
                    label: [{
                        'value': 1,
                        'text': 'Suggest to Client',
                        'klass': 'fs-black'
                    }]
                },
                {
                    status: 'picked',
                    label: [{
                        'value': 1,
                        'text': 'Suggested',
                        'klass': 'fs-info'
                    }]
                },
                {
                    status: 'suggested',
                    label: [{
                        'value': 2,
                        'text': 'Save',
                        'klass': 'fs-success'
                    },{
                        'value': 1,
                        'text': 'Select',
                        'klass': 'fs-success'
                    },
                    {
                        'value': 0,
                        'text': 'Not now',
                        'klass': 'fs-danger'
                    }]
                },
                {
                    status: 'selected',
                    label: [{
                        'value': 1,
                        'text': 'Saved',
                        'klass': 'fs-success'
                    },
                    {
                        'value': 0,
                        'text': 'Selected',
                        'klass': 'fs-success'
                    }]
                },
                {
                    status: 'declined',
                    label: [{
                        'value': 1,
                        'text': 'Declined',
                        'klass': 'fs-basic'
                    }]
                },
                {
                    status: 'assigned',
                    label: [{
                        'value': 1,
                        'text': 'Assigned',
                        'klass': 'fs-success'
                    }]
                }
            ]
        }
    },
    methods: {
        checkLabel(isClient, label) {
            let isDisabled = false;
            if(isClient) {
                if(label == 'selected') {
                    isDisabled = true;
                }
            }

            return isDisabled
        },
        getLabel(status, value) {
            let labelViaStatus = this.getButtons(status);
            let label = _.find(labelViaStatus.label, (row) => {
                return row.value == value
            })

            return label.text
        },
        getButtons(stats) {
            let labels = {}
            labels = _.find(this.teamBuilderLabels, (labelList) => {
                return labelList.status === stats
            })
            return labels
        },
        isAvailable(status) {
            return status === this.teamBuilderSuggestions[0]
        },
        isPicked(status) {
            return status === this.teamBuilderSuggestions[1]
        },
        isSuggested(status) {
            return status === this.teamBuilderStatus[0]
        },
        isSelected(status) {
            return status === this.teamBuilderStatus[1]
        },
        isDeclined(status) {
            return status === this.teamBuilderStatus[2]
        },
        isAssigned(status) {
            return status === this.teamBuilderStatus[3]
        }
    }
}
