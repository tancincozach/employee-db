export default [
    {
        label: 'Dashboard',
        key: 'dashboard',
        icon: 'la-dashboard',
        resource: 'default_dashboard_page',
    },
    {
        label: 'Daily Report',
        key: 'daily-report',
        icon: 'la-file',
        resource: 'daily_report',
    },
    {
        label: 'Employees',
        key: 'employees',
        icon: 'la-group',
        resource: 'employees_list',
    },
    {
        label: 'Applicants',
        icon: 'la-inbox',
        resource: 'applicants',
        children: [
            {
                label: 'Applicants',
                key: 'applicants',
                resource: 'applicants',
            },
            {
                label: 'Public Applicant Form',
                key: 'applicant',
                resource: 'public_applicant_form',
            },
            {
                label: 'Applicant Onboarding',
                key: 'applicant-onboarding',
                resource: 'applicant_onboarding',
            },
        ],
    },
    {
        label: 'Clients',
        icon: 'la-sliders',
        resource: 'clients_page',
        children: [
            {
                label: 'Client Management',
                key: 'clients',
                resource: 'clients_page',
            },
            {
                label: 'Client Resources',
                key: 'client_reports',
                resource: 'client_report_page',
            },
            {
                label: 'Survey Questionnaires',
                key: 'questionnaires',
                resource: 'questionnaires',
            },
            {
                label: 'Client Email Template',
                key: 'email_template',
                resource: 'email_templates',
            },
        ],
    },
    {
        label: 'Reporting',
        key: 'daily-reports',
        icon: 'la-calendar',
        resource: 'employee_daily_report',
        children: [
            {
                label: 'Daily Reports',
                key: 'daily-reports',
                resource: 'employee_daily_report',
            },
            {
                label: 'Templates',
                key: 'email-report-list',
                resource: 'report_template_nav',
            },
            {
                label: 'Worklog Parser',
                key: 'worklogs-parser',
                resource: 'worklogs_parser',
            },
            {
                label: 'Work From Home',
                key: 'work-from-home-report',
                resource: 'work_from_home_report'
            },
        ],
    },
    {
        label: 'Settings',
        icon: 'la-gears',
        resource: 'settings',
        children: [
            {
                label: 'Categories',
                key: 'categories',
                resource: 'categories_list',
            },
            {
                label: 'Employee Status',
                key: 'employee-status',
                resource: 'settings_employee_status',
            },
            {
                label: 'Holidays',
                key: 'holidays',
                resource: 'holidays',
            },
            {
                label: 'Positions',
                key: 'positions',
                resource: 'position_list',
            },
            {
                label: 'Roles',
                key: 'roles',
                resource: 'role_list',
            },
            {
                label: 'Route Wizard Tiles',
                key: 'route-wizards-tile',
                resource: 'route_wizards_page',
            },
            {
                label: 'Skills',
                key: 'skills',
                resource: 'skill_list',
            },
            {
                label: 'Users',
                key: 'users',
                resource: 'user_list'
            },
        ],
    },
];
