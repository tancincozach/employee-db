import { BaseResource } from '@common/resource/BaseResource';

class HackerRankTestResultResource extends BaseResource {
    constructor() {
        let options = {
            url: '/hacker-rank-result/:id'
        };

        super(options);
    }
}

export default new HackerRankTestResultResource();
