import { BaseResource } from '@common/resource/BaseResource';

class CreativeServiceFileResource extends BaseResource {
    constructor() {
        let options = {
            url: '/creative-service-file/:id'
        };

        super(options);
    }
}

export default new CreativeServiceFileResource();
