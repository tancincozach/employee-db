import { BaseResource } from '@common/resource/BaseResource';

class RSSFeedsResource extends BaseResource {
    constructor() {
        let options = {
            url: '/rss-feeds'
        };

        super(options);
    }
}

export default new RSSFeedsResource();
