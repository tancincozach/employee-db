import { BaseResource } from '@common/resource/BaseResource';

class FeedbackResponseResource extends BaseResource {
    constructor() {
        let options = {
            url: '/feedback-response/:id'
        };

        super(options);
    }
}

export default new FeedbackResponseResource();
