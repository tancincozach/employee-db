import { BaseResource } from '@common/resource/BaseResource';
class EmployeeReportCalendarResource extends BaseResource {
    constructor() {
        let options = {
            url: '/employee-calendar-reports/:id'
        };

        super(options);
    }
}

export default new EmployeeReportCalendarResource();
