import { BaseResource } from '@common/resource/BaseResource';

class HackerRankTestResource extends BaseResource {
    constructor() {
        let options = {
            url: '/hacker-rank-test/:id'
        };

        super(options);
    }
}

export default new HackerRankTestResource();
