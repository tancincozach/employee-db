import { BaseResource } from '@common/resource/BaseResource';

class SmartSearchResource extends BaseResource {
    constructor() {
        const options = {
            url: '/smart-searches',
        };

        super(options);
    }
}

export default new SmartSearchResource();
