import { BaseResource } from '@common/resource/BaseResource';

class FeedbackCategoryResource extends BaseResource {
    constructor() {
        let options = {
            url: '/feedback-category/:id'
        };

        super(options);
    }
}

export default new FeedbackCategoryResource();
