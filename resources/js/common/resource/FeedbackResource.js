import { BaseResource } from '@common/resource/BaseResource';

class FeedbackResource extends BaseResource {
    constructor() {
        let options = {
            url: '/feedback/:id'
        };

        super(options);
    }
}

export default new FeedbackResource();
