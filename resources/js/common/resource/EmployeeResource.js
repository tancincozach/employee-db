import { BaseResource } from '@common/resource/BaseResource';

class EmployeeResource extends BaseResource {
    constructor(url = '/employees/:id') {
        let options = {
            url,
        };

        super(options);
    }
}

export default EmployeeResource;
