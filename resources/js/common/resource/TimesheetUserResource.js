import { BaseResource } from '@common/resource/BaseResource';

class TimesheetUserResource extends BaseResource {
    constructor(url = '/timesheet-users/:id') {
        let options = {
            url,
        };

        super(options);
    }
}

export default TimesheetUserResource;
