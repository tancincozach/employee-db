import { BaseResource } from "@common/resource/BaseResource";

class PerformEvalQuestionResource extends BaseResource {
    constructor(url = "/perform-eval-questions") {
        let options = {
            url,
        };

        super(options);
    }
}

export default new PerformEvalQuestionResource();
