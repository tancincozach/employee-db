import { BaseResource } from '@common/resource/BaseResource';

class NotificationResource extends BaseResource {
    constructor() {
        let options = {
            url: '/notifications/:id',
        };

        super(options);
    }
}

export default new NotificationResource();
