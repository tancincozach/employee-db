import { BaseResource } from '@common/resource/BaseResource';

class WorkFromHomeRequestResource extends BaseResource {
    constructor(url = '/work-from-home/:id') {
        let options = {
            url,
        };

        super(options);
    }
}

export default WorkFromHomeRequestResource;
