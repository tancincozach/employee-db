import { BaseResource } from "@common/resource/BaseResource";

class AddContactUserResource extends BaseResource {
    constructor() {
        let options = {
            url: "/add-contact-user"
        };

        super(options);
    }
}

export default new AddContactUserResource();
