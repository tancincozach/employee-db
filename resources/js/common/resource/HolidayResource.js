import { BaseResource } from '@common/resource/BaseResource';

class HolidayResource extends BaseResource {
    constructor() {
        let options = {
            url: '/holidays/:id'
        };

        super(options);
    }
}

export default new HolidayResource();
