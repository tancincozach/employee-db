import { BaseResource } from '@common/resource/BaseResource';

class ClientProjectJobcodeResource extends BaseResource {
    constructor() {
        let options = {
            url: '/client-project-jobcodes'
        };
        super(options);
    }
}

export default new ClientProjectJobcodeResource();