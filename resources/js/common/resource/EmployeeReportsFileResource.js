import { BaseResource } from '@common/resource/BaseResource';

class EmployeeReportsFileResource extends BaseResource {
    constructor() {
        let options = {
            url: '/employee-reports-file/:id'
        };

        super(options);
    }
}

export  default new EmployeeReportsFileResource();
