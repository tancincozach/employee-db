import { BaseResource } from '@common/resource/BaseResource';

class TimesheetJobcodeAssignmentResource extends BaseResource {
    constructor() {
        let options = {
            url: '/timesheet-jobcode-assignments/:id'
        };

        super(options);
    }
}

export default new TimesheetJobcodeAssignmentResource();
