import { BaseResource } from "@common/resource/BaseResource";

class PerformEvalResponseResource extends BaseResource {
    constructor(url = "/perform-eval-responses") {
        let options = {
            url,
        };

        super(options);
    }
}

export default new PerformEvalResponseResource();
