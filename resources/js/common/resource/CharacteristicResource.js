import { BaseResource } from '@common/resource/BaseResource';

class CharacteristicResource extends BaseResource {
    constructor() {
        let options = {
            url: '/characteristic/:id'
        };

        super(options);
    }
}

export default new CharacteristicResource();
