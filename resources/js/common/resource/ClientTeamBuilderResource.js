import { BaseResource } from '@common/resource/BaseResource';

class ClientTeamBuilderResource extends BaseResource {
    constructor() {
        let options = {
            url: '/team-builder/:id'
        };

        super(options);
    }
}

export default new ClientTeamBuilderResource();
