import { BaseResource } from '@common/resource/BaseResource';

class PositionTypeResource extends BaseResource {
    constructor() {
        let options = {
            url: '/position-type/:id'
        };

        super(options);
    }
}

export default new PositionTypeResource();
