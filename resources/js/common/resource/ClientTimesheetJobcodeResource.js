import { BaseResource } from '@common/resource/BaseResource';

class ClientTimesheetJobcodeResource extends BaseResource {
    constructor(url = '/client-timesheet-jobcodes/:id') {
        let options = {
            url,
        };

        super(options);
    }
}

export default ClientTimesheetJobcodeResource;
