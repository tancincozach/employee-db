import { BaseResource } from "@common/resource/BaseResource";

class WizardClientSetupResource extends BaseResource {
    constructor() {
        let options = {
            url: "/wizard-client-setup"
        };

        super(options);
    }
}

export default new WizardClientSetupResource();