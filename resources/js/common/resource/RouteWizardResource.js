import { BaseResource } from '@common/resource/BaseResource';

class RouteWizardResource extends BaseResource {
    constructor() {
        let options = {
            url: '/route-wizards/:id'
        };

        super(options);
    }
}

export default new RouteWizardResource();
