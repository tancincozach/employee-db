import { BaseResource } from '@common/resource/BaseResource';

class ClientReportResource extends BaseResource {
    constructor() {
        let options = {
            url: '/client-reports'
        };

        super(options);
    }
}

export default new ClientReportResource();
