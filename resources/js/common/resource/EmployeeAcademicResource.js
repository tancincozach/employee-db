import { BaseResource } from '@common/resource/BaseResource';

class EmployeeAcademicResource extends BaseResource {
    constructor() {
        let options = {
            url: '/employee-academic/:id'
        };

        super(options);
    }
}

export default new EmployeeAcademicResource();
