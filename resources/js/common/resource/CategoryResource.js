import { BaseResource } from '@common/resource/BaseResource';

class CategoryResource extends BaseResource {
    constructor() {
        let options = {
            url: '/categories/:id'
        };

        super(options);
    }
}

export default new CategoryResource();
