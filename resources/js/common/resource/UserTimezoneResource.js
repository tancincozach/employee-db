import { BaseResource } from '@common/resource/BaseResource';

class UserTimezoneResource extends BaseResource {
    constructor(url = '/user-timezones/:id') {
        let options = {
            url,
        };

        super(options);
    }
}

export default UserTimezoneResource;
