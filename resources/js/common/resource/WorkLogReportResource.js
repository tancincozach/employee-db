import { BaseResource } from '@common/resource/BaseResource'

class WorkLogReportResource extends BaseResource {
    constructor(url = '/client-project-reports') {
        let options = {
            url,
        };

        super(options)
    }
}

export default WorkLogReportResource
