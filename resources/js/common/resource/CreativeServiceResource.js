import { BaseResource } from '@common/resource/BaseResource';

class CreativeServiceResource extends BaseResource {
    constructor() {
        let options = {
            url: '/creative-service/:id'
        };

        super(options);
    }
}

export default new CreativeServiceResource();
