import { BaseResource } from '@common/resource/BaseResource'

class QuestionsResource extends BaseResource {
    constructor(url = '/weekly-floor-question') {
        let options = {
            url,
        };

        super(options)
    }
}

export default new QuestionsResource();
