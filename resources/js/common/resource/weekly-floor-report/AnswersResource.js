import { BaseResource } from '@common/resource/BaseResource'

class AnswersResource extends BaseResource {
    constructor(url = '/weekly-floor-answer') {
        let options = {
            url,
        };

        super(options)
    }
}

export default new AnswersResource();
