import { BaseResource } from '@common/resource/BaseResource'

class ReportsResource extends BaseResource {
    constructor(url = '/weekly-floor-report') {
        let options = {
            url,
        };

        super(options)
    }
}


export default ReportsResource;
