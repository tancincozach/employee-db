import { BaseResource } from '@common/resource/BaseResource';

class EmployeeCharacteristicResource extends BaseResource {
    constructor() {
        let options = {
            url: '/employee-characteristic/:id'
        };

        super(options);
    }
}

export default new EmployeeCharacteristicResource();
