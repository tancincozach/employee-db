import { BaseResource } from '@common/resource/BaseResource';

class TimesheetResource extends BaseResource {
    constructor(url = '/timesheets/:id') {
        let options = {
            url,
        };

        super(options);
    }
}

export default TimesheetResource;
