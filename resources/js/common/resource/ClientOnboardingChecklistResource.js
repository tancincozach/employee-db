import { BaseResource } from '@common/resource/BaseResource';

class ClientOnboardingChecklistResource extends BaseResource {
    constructor() {
        let options = {
            url: '/client-onboarding-checklist/:id'
        };

        super(options);
    }
}

export default new ClientOnboardingChecklistResource();
