import { BaseResource } from '@common/resource/BaseResource';

class FaqResource extends BaseResource {
    constructor() {
        let options = {
            url: '/faq/:id'
        };

        super(options);
    }
}

export default new FaqResource();
