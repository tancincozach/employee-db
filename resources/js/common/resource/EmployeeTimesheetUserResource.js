import { BaseResource } from '@common/resource/BaseResource';

class EmployeeTimesheetUserResource extends BaseResource {
    constructor(url = '/employee-timesheet-users/:id') {
        let options = {
            url,
        };

        super(options);
    }
}

export default EmployeeTimesheetUserResource;
