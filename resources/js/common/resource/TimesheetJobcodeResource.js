import { BaseResource } from '@common/resource/BaseResource';

class TimesheetJobcodeResource extends BaseResource {
    constructor(url = '/timesheet-jobcodes/:id') {
        let options = {
            url,
        };

        super(options);
    }
}

export default TimesheetJobcodeResource;
