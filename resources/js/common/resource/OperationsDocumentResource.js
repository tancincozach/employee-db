import { BaseResource } from '@common/resource/BaseResource';

class OperationsDocumentResource extends BaseResource {
    constructor() {
        let options = {
            url: '/operations-documents/:id'
        };

        super(options);
    }
}

export default new OperationsDocumentResource();
