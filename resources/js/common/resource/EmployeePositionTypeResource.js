import { BaseResource } from '@common/resource/BaseResource';

class EmployeePositionTypeResource extends BaseResource {
    constructor() {
        let options = {
            url: '/employee-position-type/:id'
        };

        super(options);
    }
}

export default new EmployeePositionTypeResource();
