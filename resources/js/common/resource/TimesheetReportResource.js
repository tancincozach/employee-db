import { BaseResource } from '@common/resource/BaseResource';

class TimesheetReportResource extends BaseResource {
    constructor(url = '/timesheet-reports/:id') {
        let options = {
            url,
        };

        super(options);
    }
}

export default TimesheetReportResource;
