import { BaseResource } from '@common/resource/BaseResource';

class CreativeServiceApplicantResource extends BaseResource {
    constructor() {
        let options = {
            url: '/creative-service-applicant/:id'
        };

        super(options);
    }
}

export default new CreativeServiceApplicantResource();
