import { BaseModel } from "@common/model/BaseModel";
import PerformEvalQuestionResource from "@common/resource/PerformEvalQuestionResource";

export class PerformEvalQuestion extends BaseModel {
    constructor(data) {
        let relations = {
            data: PerformEvalQuestion
        };

        super(data, relations);
    }

    static get resource() {
        return PerformEvalQuestionResource;
    }
}
