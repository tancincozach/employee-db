import { BaseModel } from '@common/model/BaseModel';
import TimesheetResource from '@common/resource/TimesheetResource';

export class Timesheet extends BaseModel {
    constructor(data) {
        let relations = {
            data: Timesheet,
        };

        super(data, relations);
    }

    static get resource() {
        return new TimesheetResource();
    }

    static monthly(payload) {
        let params = payload || {};
        const resource = new TimesheetResource('/timesheets/monthly');

        return resource.get(params);
    }

    static weekly(payload) {
        let params = payload || {};
        const resource = new TimesheetResource('/timesheets/weekly');

        return resource.get(params);
    }

    static generate(payload) {
        let params = payload || {};
        const resource = new TimesheetResource('/timesheets/generate');

        return resource.post(params);
    }

    static getGenerationResponse(payload) {
        let params = payload || {};
        const resource = new TimesheetResource('/timesheets/generation-response');

        return resource.get(params);
    }
}
