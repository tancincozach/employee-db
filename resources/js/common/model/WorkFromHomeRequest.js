import { BaseModel } from '@common/model/BaseModel';
import WorkFromHomeRequestResource from '@common/resource/WorkFromHomeRequestResource';

export class WorkFromHomeRequest extends BaseModel {
    constructor(data) {
        let relations = {
            'data': WorkFromHomeRequest
        };

        super(data, relations);
    }

    static get resource() {
        return new WorkFromHomeRequestResource;
    }

    static export(payload) {
        //console.log(payload)
        let params = payload || {};
        const resource = new WorkFromHomeRequestResource('/work-from-home/export');

        return resource.get(params);
    }
}
