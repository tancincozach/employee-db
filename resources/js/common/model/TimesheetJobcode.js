import { BaseModel } from '@common/model/BaseModel';
import TimesheetJobcodeResource from '@common/resource/TimesheetJobcodeResource';

export class TimesheetJobcode extends BaseModel {
    constructor(data) {
        let relations = {
            data: TimesheetJobcode,
        };

        super(data, relations);
    }

    static get resource() {
        return new TimesheetJobcodeResource();
    }
}
