import { BaseModel } from '@common/model/BaseModel';
import CategoryResource from '@common/resource/CategoryResource';

export class Category extends BaseModel {
    constructor(data) {
        let relations = {
            'data': Category
        };

        super(data, relations);
    }

    static get resource() {
        return CategoryResource;
    }
}
