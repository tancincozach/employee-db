import { BaseModel } from '@common/model/BaseModel';
import SmartSearchResource from '@common/resource/SmartSearchResource';

export class SmartSearch extends BaseModel {
    constructor(data) {
        const relations = {
            data: SmartSearch,
        };

        super(data, relations);
    }

    static get resource() {
        return SmartSearchResource;
    }
}
