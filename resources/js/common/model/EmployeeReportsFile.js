import { BaseModel } from '@common/model/BaseModel';
import EmployeeReportsFileResource from '@common/resource/EmployeeReportsFileResource';

export class EmployeeReportsFile extends BaseModel {
    constructor(data) {
        let relations = {
            'data': EmployeeReportsFile
        };

        super(data, relations);
    }

    static get resource() {
        return EmployeeReportsFileResource;
    }
}
