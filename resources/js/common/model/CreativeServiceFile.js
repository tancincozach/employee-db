import { BaseModel } from '@common/model/BaseModel';
import CreativeServiceFileResource from '@common/resource/CreativeServiceFileResource';

export class CreativeServiceFile extends BaseModel {
    constructor(data) {
        let relations = {
            'data': CreativeServiceFile
        };

        super(data, relations);
    }

    static get resource() {
        return CreativeServiceFileResource;
    }
}
