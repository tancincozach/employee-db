import { BaseModel } from '@common/model/BaseModel';
import FeedbackResource from '@common/resource/FeedbackResource';

export class Feedback extends BaseModel {
    constructor(data) {
        let relations = {
            'data': Feedback
        };

        super(data, relations);
    }

    static get resource() {
        return FeedbackResource;
    }
}
