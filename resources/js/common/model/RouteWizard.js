import { BaseModel } from '@common/model/BaseModel';
import RouteWizardResource from '@common/resource/RouteWizardResource';

export class RouteWizard extends BaseModel {
    constructor(data) {
        let relations = {
            'data': RouteWizard
        };

        super(data, relations);
    }

    static get resource() {
        return RouteWizardResource;
    }
}
