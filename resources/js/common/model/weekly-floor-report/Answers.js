import { BaseModel } from '@common/model/BaseModel';
import WeeklyFloorAnswerResource from '@common/resource/weekly-floor-report/AnswersResource';

export class Answers extends BaseModel {
    constructor(data) {
        let relations = {
            'data': Answers
        };

        super(data, relations);
    }

    static get resource() {
        return WeeklyFloorAnswerResource;
    }
}
