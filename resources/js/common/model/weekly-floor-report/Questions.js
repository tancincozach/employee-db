import { BaseModel } from '@common/model/BaseModel';
import WeeklyFloorQuestionResource from '@common/resource/weekly-floor-report/QuestionsResource';

export class Questions extends BaseModel {
    constructor(data) {
        let relations = {
            'data': Questions
        };

        super(data, relations);
    }

    static get resource() {
        return WeeklyFloorQuestionResource;
    }
}
