import { BaseModel } from '@common/model/BaseModel';
import WeeklyFloorReportResource from '@common/resource/weekly-floor-report/ReportsResource';

export class Reports extends BaseModel {
    constructor(data) {
        let relations = {
            'data': Reports
        };

        super(data, relations);
    }

    static get resource() {
        return new WeeklyFloorReportResource();
    }

    static saveFullReport(payload) {
        let params = payload || {};
        const resource = new WeeklyFloorReportResource('/weekly-floor-report/save');

        return resource.post(params);
    }
}
