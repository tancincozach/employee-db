import { BaseModel } from '@common/model/BaseModel';
import EmployeeAcademicResource from '@common/resource/EmployeeAcademicResource';

export class EmployeeAcademic extends BaseModel {
    constructor(data) {
        let relations = {
            'data': EmployeeAcademic
        };

        super(data, relations);
    }

    static get resource() {
        return EmployeeAcademicResource;
    }
}
