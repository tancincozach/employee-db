import { BaseModel } from '@common/model/BaseModel';
import EmployeeCharacteristicResource from '@common/resource/EmployeeCharacteristicResource';

export class EmployeeCharacteristic extends BaseModel {
    constructor(data) {
        let relations = {
            'data': EmployeeCharacteristic
        };

        super(data, relations);
    }

    static get resource() {
        return EmployeeCharacteristicResource;
    }
}
