import { BaseModel } from '@common/model/BaseModel';
import ClientTeamBuilderResource from '@common/resource/ClientTeamBuilderResource';

export class ClientTeamBuilder extends BaseModel {
    constructor(data) {
        let relations = {
            'data': ClientTeamBuilder
        };

        super(data, relations);
    }

    static get resource() {
        return ClientTeamBuilderResource;
    }
}
