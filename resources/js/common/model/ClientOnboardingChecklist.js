import { BaseModel } from '@common/model/BaseModel';
import ClientOnboardingChecklistResource from '@common/resource/ClientOnboardingChecklistResource';

export class ClientOnboardingChecklist extends BaseModel {
    constructor(data) {
        let relations = {
            'data': ClientOnboardingChecklist
        };

        super(data, relations);
    }

    static get resource() {
        return ClientOnboardingChecklistResource;
    }
}
