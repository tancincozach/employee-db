import { BaseModel } from '@common/model/BaseModel';
import TimesheetUserResource from '@common/resource/TimesheetUserResource';

export class TimesheetUser extends BaseModel {
    constructor(data) {
        let relations = {
            data: TimesheetUser,
        };

        super(data, relations);
    }

    static get resource() {
        return new TimesheetUserResource();
    }
}
