import { BaseModel } from "@common/model/BaseModel";
import PerformEvalResponseResource from "@common/resource/PerformEvalResponseResource";

export class PerformEvalResponse extends BaseModel {
    constructor(data) {
        let relations = {
            data: PerformEvalResponse
        };

        super(data, relations);
    }

    static get resource() {
        return PerformEvalResponseResource;
    }
}
