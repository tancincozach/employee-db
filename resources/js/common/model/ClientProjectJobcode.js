import { BaseModel } from '@common/model/BaseModel';
import ClientProjectJobcodeResource from '@common/resource/ClientProjectJobcodeResource';

export class ClientProjectJobcode extends BaseModel {
    constructor(data) {
        let relations = {
            'data': ClientProjectJobcode
        };

        super(data, relations);
    }

    static get resource() {
        return ClientProjectJobcodeResource;
    }
}