import { BaseModel } from '@common/model/BaseModel';
import ClientTimesheetJobcodeResource from '@common/resource/ClientTimesheetJobcodeResource';

export class ClientTimesheetJobcode extends BaseModel {
    constructor(data) {
        let relations = {
            data: ClientTimesheetJobcode,
        };

        super(data, relations);
    }

    static get resource() {
        return new ClientTimesheetJobcodeResource();
    }
}
