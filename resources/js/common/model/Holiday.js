import { BaseModel } from '@common/model/BaseModel';
import HolidayResource from '@common/resource/HolidayResource';

export class Holiday extends BaseModel {
    constructor(data) {
        let relations = {
            'data': Holiday
        };

        super(data, relations);
    }

    static get resource() {
        return HolidayResource;
    }
}
