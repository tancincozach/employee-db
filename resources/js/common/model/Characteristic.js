import { BaseModel } from '@common/model/BaseModel';
import CharacteristicResource from '@common/resource/CharacteristicResource';

export class Characteristic extends BaseModel {
    constructor(data) {
        let relations = {
            'data': Characteristic
        };

        super(data, relations);
    }

    static get resource() {
        return CharacteristicResource;
    }
}
