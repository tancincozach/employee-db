import { BaseModel } from '@common/model/BaseModel';
import EmployeeReportCalendarResource from '@common/resource/EmployeeCalendarReportResource';

export class EmployeeCalendarReports extends BaseModel {
    constructor(data) {
        let relations = {
            'data': EmployeeCalendarReports
        };

        super(data, relations);
    }

    static get resource() {
        return EmployeeReportCalendarResource;
    }
}
