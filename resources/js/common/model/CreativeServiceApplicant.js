import { BaseModel } from '@common/model/BaseModel';
import CreativeServiceApplicantResource from '@common/resource/CreativeServiceApplicantResource';

export class CreativeServiceApplicant extends BaseModel {
    constructor(data) {
        let relations = {
            'data': CreativeServiceApplicant
        };

        super(data, relations);
    }

    static get resource() {
        return CreativeServiceApplicantResource;
    }
}
