import { BaseModel } from '@common/model/BaseModel';
import OperationsDocumentResource from '@common/resource/OperationsDocumentResource';

export class OperationsDocument extends BaseModel {
    constructor(data) {
        let relations = {
            'data': OperationsDocument
        };

        super(data, relations);
    }

    static get resource() {
        return OperationsDocumentResource;
    }
}
