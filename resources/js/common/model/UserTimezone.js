import { BaseModel } from '@common/model/BaseModel';
import UserTimezoneResource from '@common/resource/UserTimezoneResource';

export class UserTimezone extends BaseModel {
    constructor(data) {
        let relations = {
            data: UserTimezone,
        };

        super(data, relations);
    }

    static get resource() {
        return new UserTimezoneResource();
    }
}
