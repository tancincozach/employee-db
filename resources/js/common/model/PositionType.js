import { BaseModel } from '@common/model/BaseModel';
import PositionTypeResource from '@common/resource/PositionTypeResource';

export class PositionType extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PositionType
        };

        super(data, relations);
    }

    static get resource() {
        return PositionTypeResource;
    }
}
