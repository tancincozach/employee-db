import { BaseModel } from '@common/model/BaseModel';
import NotificationResource from '@common/resource/NotificationResource';

export class Notification extends BaseModel {
    constructor(data) {
        let relations = {
            'data': Notification
        };

        super(data, relations);
    }

    static get resource() {
        return NotificationResource;
    }
}
