import { BaseModel } from '@common/model/BaseModel';
import EmployeePositionTypeResource from '@common/resource/EmployeePositionTypeResource';

export class EmployeePositionType extends BaseModel {
    constructor(data) {
        let relations = {
            'data': EmployeePositionType
        };

        super(data, relations);
    }

    static get resource() {
        return EmployeePositionTypeResource;
    }
}
