import { BaseModel } from '@common/model/BaseModel';
import EmployeeTimesheetUserResource from '@common/resource/EmployeeTimesheetUserResource';

export class EmployeeTimesheetUser extends BaseModel {
    constructor(data) {
        let relations = {
            data: EmployeeTimesheetUser,
        };

        super(data, relations);
    }

    static get resource() {
        return new EmployeeTimesheetUserResource();
    }
}
