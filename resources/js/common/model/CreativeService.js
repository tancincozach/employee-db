import { BaseModel } from '@common/model/BaseModel';
import CreativeServiceResource from '@common/resource/CreativeServiceResource';

export class CreativeService extends BaseModel {
    constructor(data) {
        let relations = {
            'data': CreativeService
        };

        super(data, relations);
    }

    static get resource() {
        return CreativeServiceResource;
    }
}
