import { BaseModel } from '@common/model/BaseModel';
import HackerRankTestResource from '@common/resource/HackerRankTestResource';

export class HackerRankTest extends BaseModel {
    constructor(data) {
        let relations = {
            'data': HackerRankTest
        };

        super(data, relations);
    }

    static get resource() {
        return HackerRankTestResource;
    }
}
