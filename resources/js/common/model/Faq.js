import { BaseModel } from '@common/model/BaseModel';
import FaqResource from '@common/resource/FaqResource';

export class Faq extends BaseModel {
    constructor(data) {
        let relations = {
            'data': Faq
        };

        super(data, relations);
    }

    static get resource() {
        return FaqResource;
    }
}
