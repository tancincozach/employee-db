import { BaseModel } from "@common/model/BaseModel";
import WizardClientSetupResource from "@common/resource/WizardClientSetupResource";

export class WizardClientSetup extends BaseModel {
    constructor(data) {
        let relations = {
            data: WizardClientSetup
        };

        super(data, relations);
    }

    static get resource() {
        return WizardClientSetupResource;
    }
}
