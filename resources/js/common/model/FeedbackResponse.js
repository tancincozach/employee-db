import { BaseModel } from '@common/model/BaseModel';
import FeedbackResponseResource from '@common/resource/FeedbackResponseResource';

export class FeedbackResponse extends BaseModel {
    constructor(data) {
        let relations = {
            'data': FeedbackResponse
        };

        super(data, relations);
    }

    static get resource() {
        return FeedbackResponseResource;
    }
}
