import { BaseModel } from '@common/model/BaseModel';
import ClientReportResource from '@common/resource/ClientReportResource';

export class ClientReport extends BaseModel {
    constructor(data) {
        let relations = {
            'data': ClientReport,
        };

        super(data, relations);
    }

    static get resource() {
        return ClientReportResource;
    }
}
