import { BaseModel } from '@common/model/BaseModel';
import WorkLogReportResource from '@common/resource/WorkLogReportResource';

export class WorkLogReport extends BaseModel {
    constructor(data) {
        let relations = {
            'data': WorkLogReport
        };

        super(data, relations);
    }

    static get resource() {
        return new WorkLogReportResource();
    }

    static override(payload) {
        let params = payload || {};
        const resource = new WorkLogReportResource('/tsheet-report-override');

        return resource.get(params);
    }
}
