import { BaseModel } from '@common/model/BaseModel';
import TimesheetReportResource from '@common/resource/TimesheetReportResource';

export class TimesheetReport extends BaseModel {
    constructor(data) {
        let relations = {
            data: TimesheetReport,
        };

        super(data, relations);
    }

    static get resource() {
        return new TimesheetReportResource();
    }

    static monthly(payload) {
        let params = payload || {};
        const resource = new TimesheetReportResource('/timesheet-reports/monthly');

        return resource.get(params);
    }

    static weekly(payload) {
        let params = payload || {};
        const resource = new TimesheetReportResource('/timesheet-reports/weekly');

        return resource.get(params);
    }

    static generate(payload) {
        let params = payload || {};
        const resource = new TimesheetReportResource('/timesheet-reports/generate');

        return resource.post(params);
    }

    static getGenerationResponse(payload) {
        let params = payload || {};
        const resource = new TimesheetReportResource('/timesheet-reports/generation-response');

        return resource.get(params);
    }
}
