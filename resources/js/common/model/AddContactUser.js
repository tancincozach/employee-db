import { BaseModel } from '@common/model/BaseModel';
import AddContactUserResource from '@common/resource/AddContactUserResource';

export class AddContactUser extends BaseModel {
    constructor(data) {
        let relations = {
            'data': AddContactUser
        };

        super(data, relations);
    }

    static get resource() {
        return AddContactUserResource;
    }
}
