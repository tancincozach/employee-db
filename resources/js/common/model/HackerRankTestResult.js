import { BaseModel } from '@common/model/BaseModel';
import HackerRankTestResultResource from '@common/resource/HackerRankTestResultResource';

export class HackerRankTestResult extends BaseModel {
    constructor(data) {
        let relations = {
            'data': HackerRankTestResult
        };

        super(data, relations);
    }

    static get resource() {
        return HackerRankTestResultResource;
    }
}
