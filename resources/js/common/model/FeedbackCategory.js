import { BaseModel } from '@common/model/BaseModel';
import FeedbackCategoryResource from '@common/resource/FeedbackCategoryResource';

export class FeedbackCategory extends BaseModel {
    constructor(data) {
        let relations = {
            'data': FeedbackCategory
        };

        super(data, relations);
    }

    static get resource() {
        return FeedbackCategoryResource;
    }
}
