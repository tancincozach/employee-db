import { BaseModel } from '@common/model/BaseModel';
import TimesheetJobcodeAssignmentResource from '@common/resource/TimesheetJobcodeAssignmentResource';

export class TimesheetJobcodeAssignment extends BaseModel {
    constructor(data) {
        let relations = {
            data: TimesheetJobcodeAssignment,
        };

        super(data, relations);
    }

    static get resource() {
        return TimesheetJobcodeAssignmentResource;
    }
}
