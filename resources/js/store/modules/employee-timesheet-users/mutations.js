const SET_DATA = (state, data) => {
    state.data = data;
}

const SET_META = (state, meta) => {
    state.meta = meta;
}

const CLEAR_STATES = state => {
    state.data = [];
    state.meta = {};
}

export {
    SET_DATA,
    SET_META,
    CLEAR_STATES,
}
