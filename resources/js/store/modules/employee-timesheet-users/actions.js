import { EmployeeTimesheetUser } from '@common/model/EmployeeTimesheetUser';
import _ from 'lodash';

const index = (context, payload) => {
    return EmployeeTimesheetUser.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('SET_DATA', res.data);
        context.commit('SET_META', res.meta);
    });
};

const clearStates = context => context.commit('CLEAR_STATES');

export {
    index,
    clearStates,
}
