import _ from 'lodash';

const POSITION_TYPES_UPDATED = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            text: row.name,
            description: row.description
        }, payload.extra);

        state.formatted.push(obj);
    });
};

const POSITION_TYPE_UPDATED = (state, payload) => {
    state.position_type = payload.data
}

const CLEAR_POSITION_TYPES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
};

const DELETE_POSITION_TYPE = (state) => {};

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

export {
    SAVE_META,
    DELETE_POSITION_TYPE,
    CLEAR_POSITION_TYPES,
    POSITION_TYPES_UPDATED,
    POSITION_TYPE_UPDATED
}
