const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const position_type = state => state.position_type;

export {
    data,
    formatted,
    meta,
    position_type
}
