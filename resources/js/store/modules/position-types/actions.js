import { PositionType } from '@common/model/PositionType';

const getPositionTypes = (context, payload) => {
    return PositionType.get(payload.query).then((res) => {
    	context.commit('CLEAR_POSITION_TYPES');
        context.commit('POSITION_TYPES_UPDATED', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    })
};

const getPositionType = (context, id) => {
    return PositionType.get({id:id}).then((res) => {
        context.commit('POSITION_TYPE_UPDATED', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find position!');
    })
}

const savePositionType = (context, payload) => {
	const id    = payload.id;
    const data  = {
        name:payload.name
    };
    const positionType = (id != "" && id > 0) ? new PositionType({id:id}) : new PositionType();
    return positionType.save(data);
}

const clearPositionTypes = (context) => {
    context.commit('CLEAR_POSITION_TYPES');
};

const deletePositionType = (context, id) => {
    const positionType = new PositionType({id: id});
    return positionType.delete().then((res) => {
        context.commit('DELETE_POSITION_TYPE');
    });
};

const searchPositionType = (context, name) => {
    return PositionType.get({name: name, withTrashed: true}).then((res) => {
        context.commit('POSITION_TYPE_UPDATED', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find position!');
    })
}

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
	saveMeta,
    getPositionTypes,
    getPositionType,
    clearPositionTypes,
    deletePositionType,
    savePositionType,
    searchPositionType
}
