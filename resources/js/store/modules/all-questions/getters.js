const data = state => state.data;
const formatted = state => state.formatted;
const resourceTypes = state => state.resourceTypes;

export {
    data,
    formatted
};
