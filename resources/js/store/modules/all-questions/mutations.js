const UPDATE_ALL_QUESTIONS = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];
    let tabData = [
        "Getting started",
        "Timeframe",
        "Your Project Needs",
        "Technology",
        "Finishing up...",
    ];

    _.each(state.data, (row, index) => {

        let obj = _.defaults({
            title: tabData[index - 1],
            content: `tab${index}`,
        }, payload.extra);

        state.formatted.push(obj);
    });
};

export { UPDATE_ALL_QUESTIONS };
