const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const tests = state => state.tests

export {
    data,
    formatted,
    meta,
    tests
}
