import { HackerRankTest } from '@common/model/HackerRankTest';
import { BaseResource } from '@common/resource/BaseResource';
import _ from 'lodash';


const getTests = (context, payload) => {
    return HackerRankTest.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('GET_HRANK_TESTS', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    });
};

const getTest = (context, id) => {
    return HackerRankTest.get({id:id}).then((res) => {
        context.commit('GET_HRANK_TEST', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find Hacker Rank Test!');
    })
}

const saveHackerRankTest = (context, payload) => {
    const id    = payload.id;
    const data  = {name:payload.name, date:payload.date, type:payload.type};
    const hacktest = (id != "" && id > 0) ? new HackerRankTest({id:id}) : new HackerRankTest();
    return hacktest.save();

};

const importHackerRankTest = (context, payload) => {
    const hackerRankTest = new BaseResource({ url: `/hacker-rank-test/import` })

    return hackerRankTest.get();
};

const deleteHackerRankTest = (context, id) => {
    const hackTest = new HackerRankTest({id: id});
    return hackTest.delete().then((res) => {
        context.commit('DELETE_HRANK_TEST');
    });
};

const searchHackerRankTestByEmail = (context, email) => {
    return HackerRankTest.get({name: email, withTrashed: true}).then((res) => {
        context.commit('SEARCH_HRANK_TEST', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find Hacker Rank Test!');
    })
}

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
    getTests,
    getTest,
    saveHackerRankTest,
    importHackerRankTest,
    deleteHackerRankTest,
    searchHackerRankTestByEmail,
    saveMeta,
}
