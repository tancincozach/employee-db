import _ from 'lodash';

const GET_HRANK_TESTS = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let skills = row.skill;
        let obj = _.defaults({
            id: row.id,
            text: row.name,
            test_url: row.test_url
        }, payload.extra);
        
        _.each(skills, skill => {
            if (skill.id !== undefined) {
                obj.skill_id = skill.id;
                obj.skill_name = skill.name;
                obj.skill_color = skill.color;
            }
        })
        

        state.formatted.push(obj);
    });
}

const GET_HRANK_TEST = (state, payload) => {
    state.tests = payload.data
}

const DELETE_HRANK_TEST = (state) => {}

const SEARCH_HRANK_TEST = (state, payload) => {
    state.tests = payload.data
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const CLEAR_STATES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
    state.tests = {};
}



export {
    GET_HRANK_TESTS,
    GET_HRANK_TEST,
    DELETE_HRANK_TEST,
    SEARCH_HRANK_TEST,
    CLEAR_STATES,
    SAVE_META
}
