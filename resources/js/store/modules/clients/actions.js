import { Client } from '@common/model/Client';
import { ClientReport } from '@common/model/ClientReport';
import RSSFeedsResource from '@common/resource/RSSFeedsResource';
import { BaseResource } from '@common/resource/BaseResource';
import _ from "lodash";

const getClients = (context, payload) => {
    return Client.get(payload.query).then(res => {
        context.commit('CLEAR_STATES');
        context.commit('GET_CLIENTS', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const getCompanies = (context, payload) => {
    let resource = new BaseResource({url: `/client-companies`})
    return resource.get(payload.query).then((res) => {
        res = res.data;
        context.commit('PULL_CLIENTS', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullClients = (context, payload) => {
    return Client.get(payload.query).then(res => {
        context.commit('PULL_CLIENTS', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};

const getClient = (context, id) => {
    return Client.get({ id }).then((res) => {
        context.commit('EDIT_CLIENT', { data: res.data });
    })
    .catch(e => {
        throw new Error("Can't find client!");
    });
};

const setClient = (context, payload) => {
    context.commit('SET_CLIENT', payload)
};

const getClientReport = (context, payload) => {
    return ClientReport.get().then((res) => {
        context.commit('GET_CLIENT_REPORT', { data: res.data });
    })
        .catch(e => {
            throw new Error("Can't find data client report!");
        });
};

const getSpecificClient = (context, payload) => {
    return Client.get(payload.query).then(res => {
        context.commit('GET_CLIENT', { data: res.data });
    });
};

const saveClient = (context, payload) => {
    const id = payload.id;
    const client = (id != "" && id > 0) ? new Client({ id: id }) : new Client();
    return client.save(payload).then((res) => {
        context.commit('CLIENT_UPDATED', { data: res.data });
    });
};

const deleteClient = (context, id) => {
    const client = new Client({ id });
    return client.delete().then(res => {
        context.commit('DELETE_CLIENT');
    });
};

const getRssFeeds = (context) => {
    return RSSFeedsResource.get().then(response => {
        context.commit('SET_RSS_FEEDS', response.data);
    });
};

export { saveClient, getClients, getCompanies, setClient, getSpecificClient, deleteClient, getClient, getClientReport, pullClients, getRssFeeds };
