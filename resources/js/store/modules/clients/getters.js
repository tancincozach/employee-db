const data = state => state.data;
const formatted = state => state.formatted;
const formatted_with_name = state => state.formatted_with_name;
const formatted_with_projects = state => state.formatted_with_projects;
const meta = state => state.meta;
const client = state => state.client;
const contact = state => state.contact;
const clientReport = state => state.clientReport;
const search = state => state.search;
const rss_feeds = state => state.rss_feeds;
const client_names = state => state.client_names;

export {
    data,
    formatted,
    formatted_with_name,
    formatted_with_projects,
    meta,
    client,
    contact,
    clientReport,
    search,
    rss_feeds,
    client_names,
};
