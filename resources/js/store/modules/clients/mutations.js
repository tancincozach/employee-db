import _ from 'lodash';

const GET_CLIENTS = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];
    state.formatted_with_name = [];
    state.formatted_with_projects = [];

    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            company: row.company,
            is_high_growth_client: row.is_high_growth_client,
            contract_signed: row.contract_signed,
            initial_deposit: row.initial_deposit,
            start_guide: row.start_guide,
            first_week_check_up: row.first_week_check_up,
            team_emails_sent: row.team_emails_sent,
            first_month_check_up: row.first_month_check_up,
            is_partner: row.is_partner,
            company_do: row.company_do,
            employee_spoken: row.employee_spoken,
            referred_by: row.referred_by,
            status: row.status,
            location: row.location,
            timezone: row.timezone,
            notes: row.notes,
            projects: row.projects
        }, payload.extra);
        let obj2 = _.defaults({
            client_id: row.id,
            text: row.company
        }, payload.extra);
        let obj3 = _.defaults({
            id: row.id,
            text: row.company,
            projects: row.projects
        }, payload.extra);
        state.formatted.push(obj);
        state.formatted_with_name.push(obj2);
        state.formatted_with_projects.push(obj3);
    });
};

const PULL_CLIENTS = (state, payload) => {
    state.search = null;
    state.search = payload.data;
};

const GET_CLIENT = (state, payload) => {
    state.client = payload.data;
};

const GET_CLIENT_REPORT = (state, payload) => {
    state.clientReport = payload.data;
};

const CLIENT_UPDATED = (state, payload) => {
    state.client = payload.data;
};

const EDIT_CLIENT = (state, payload) => {
    state.client = payload.data;
    try {
        state.contact = payload.data.contacts.data[0];
    } catch (e) {
        state.contact = {};
    }
};

const SET_CLIENT = (state, payload) => {
    state.client = payload;
    try {
        state.contact = payload.contacts.data[0];
    } catch (e) {
        state.contact = {};
    }
};


const DELETE_CLIENT = state => { };

const CLEAR_STATES = state => {
    state.data = [];
    state.formatted = [];
    state.client = {};
    state.contact = {};
};

const SAVE_META = (state, meta) => {
    state.meta = meta;
};

const SET_RSS_FEEDS = (state, rss_feeds) => {
    state.rss_feeds = rss_feeds;
};

export { GET_CLIENTS, GET_CLIENT, SET_CLIENT, CLIENT_UPDATED, DELETE_CLIENT, EDIT_CLIENT, CLEAR_STATES, SAVE_META, GET_CLIENT_REPORT, PULL_CLIENTS, SET_RSS_FEEDS };
