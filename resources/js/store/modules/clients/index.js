import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';

const state = {
    data: [],
    formatted: [],
    formatted_with_name: [],
    formatted_with_projects: [],
    meta: {},
    client: {},
    contact: {},
    clientReport: [],
    search: [],
    rss_feeds: [],
    client_names: [],
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
