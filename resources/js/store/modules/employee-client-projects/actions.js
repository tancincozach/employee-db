import { EmployeeClientProject } from '@common/model/EmployeeClientProject';
import { BaseResource } from '@common/resource/BaseResource';

const saveResource = (context, data) => {
    const id = data.id;
    const employeeClientProject = (id != null && id != '') ? new EmployeeClientProject({id:id}) : new EmployeeClientProject();
    return employeeClientProject.save(data).then( (res) => {
        context.commit('RESOURCE_UPDATED', {data: res.data});
    });
}

const saveAssignedResource = (context, payload) => {
    const saveResource = new BaseResource({ url: `/save-assigned-resource` });

    return saveResource.post(payload);
};

const getResource = (context, id) => {
    return EmployeeClientProject.get({id:id}).then((res) => {
        context.commit('RESOURCE_UPDATED', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find resource!');
    })
}

const searchResource = (context, id) => {
    return EmployeeClientProject.get({employee_id: id, query: {}}).then((res) => {
        context.commit('RESOURCE_UPDATED', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find resource! -- ');
    })
}

const getEmployeeClientProjects = (context, payload) => {

    return EmployeeClientProject.get(payload.query);
};

const getProjectsOfResource = (context, payload) => {
    return EmployeeClientProject.get(payload).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('GET_RESOURCE_PROJECTS', {data: res.data});
    });
}

const getResourcesByProject = (context, payload) => {
    return EmployeeClientProject.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('GET_RESOURCES', {data: res.data});
    });
}

const getResources = (context) => {
    return EmployeeClientProject.get().then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('GET_RESOURCES', {data: res.data});
    });
};

const deleteResource = (context, id) => {
    const resource = new EmployeeClientProject({id: id});
    return resource.delete().then(() => {
        context.commit('DELETE_RESOURCE');
    });
};

export {
    getResource,
    getEmployeeClientProjects,
    saveResource,
    getResources,
    searchResource,
    deleteResource,
    getResourcesByProject,
    getProjectsOfResource,
    saveAssignedResource
}

