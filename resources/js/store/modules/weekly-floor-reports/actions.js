import { Reports } from '@common/model/weekly-floor-report/Reports'

const getReports = (context, payload) => {
    return Reports.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES')
        context.commit('GET_WEEKLY_REPORT', {reports: res.data, extra: payload.extra})
        context.commit('SAVE_META', res.reports)
    }).catch((e) => {
        throw new Error('Something went wrong: ' + e);
    })
}

const saveWeeklyFloorReport = (context,payload) => {
    const id = payload.id;
    //const model = (id != "") ? new Reports({id:id}) : new Reports();
    return Reports.saveFullReport(payload);
}

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta)
}

export {
    getReports,
    saveWeeklyFloorReport,
    saveMeta,
}
