const CLEAR_STATES = (state) => {
    state.reports = []
    state.formatted = []
    state.meta = {}
}

const GET_WEEKLY_REPORT = (state, payload) => {
    state.reports = payload.reports
    //console.log(payload);
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

export {
    CLEAR_STATES,
    GET_WEEKLY_REPORT,
    SAVE_META,
}
