const reports = state => state.reports
const formatted = state => state.formatted
const meta =  state => state.meta

export {
    reports,
    formatted,
    meta,
}
