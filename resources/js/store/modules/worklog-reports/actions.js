import { ClientProject } from '@common/model/ClientProject';
import { WorkLogReport } from '@common/model/WorkLogReport';
import { BaseResource } from '@common/resource/BaseResource';
import _ from 'lodash';
import moment from 'moment';

const getClientProjects = (context, payload) => {
  return ClientProject.get(payload.query)
    .then(res => {
      context.commit('PROJECTS_UPDATED', { data: res.data });
    })
    .catch(e => {
      throw new Error('Something went wrong: ' + e);
    });
};

const getWorkLogReports = (context, payload) => {
  return WorkLogReport.get(payload.query).then(res => {
    context.commit('CLEAR_STATES');
    context.commit('WORKLOG_REPORTS_UPDATED', { data: res.data, extra: payload.extra });
    context.commit('SAVE_META', res.meta);
  })
    .catch(e => {
      throw new Error('Something went wrong: ' + e);
    });
};

const getRawWorkLogReports = (context, payload) => {
    return WorkLogReport.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES')
        context.commit('WORKLOG_RAW_REPORTS_UPDATED', { data: res.data, extra: payload.extra, totalHrsMeta: res.meta })
        context.commit('SAVE_META', res.meta)
    }).catch((e) => {
        throw new Error('Something went wrong: ' + e);
    })
}

const getYearsWithMonths = (context, payload) => {
  return WorkLogReport.get(payload.query).then((res) => {
    context.commit('CLEAR_OVERALL_FORMATTED_STATE');
    context.commit('WORKLOG_REPORT_YEARS_UPDATED', { years: res.data, extra: payload.extra });
  }).catch((e) => {
    throw new Error('Something went wrong: ' + e);
  });
};

const saveMeta = (context, meta) => {
  context.commit('SAVE_META', meta);
};

const getReport = (context, payload) => {
  const report = new BaseResource({ url: '/tsheet-report' });

  return report
    .get(payload.query)
    .then(res => {
      context.commit('CLEAR_STATES');
      if (payload.query.weekly) {
        context.commit('CLEAR_OVERALL_FORMATTED_STATE');
        context.commit('REPORT_WEEKLY_FORMAT', { data: res.data });
      } else {
        context.commit('REPORT_FILTER_FORMAT', { data: res.data });
      }
      context.commit('LOADER');
    })
    .catch(e => {
      throw new Error('Something went wrong: ' + e);
    });
};

const getClientReport = (context, payload) => {
  const report = new BaseResource({ url: '/client-tsheet-report' });

  return report
    .get(payload.query)
    .then(res => {
      if (!_.isEmpty(res.data.data)) {
        context.commit('CLEAR_STATES');
        context.commit('CLEAR_OVERALL_FORMATTED_STATE');
        context.commit('REPORT_WEEKLY_FORMAT', { data: res.data });
        context.commit('REPORT_CONSOLIDATOR', { jobcode: res.data.data[0].jobcode, query: payload.query });
      } else {
        return 'Empty';
      }
    })
    .catch(e => {
      throw new Error('Something went wrong: ' + e);
    });
};

const setLoaderValue = (context, payload) => {
  context.commit('SET_LOADER');
};

const clearStates = (context) => {
  context.commit('CLEAR_STATES');
  context.commit('CLEAR_OVERALL_FORMATTED_STATE');
};

const postWorklogReport = (context, data) => {
    const request = {
        client_id:         data.activeParameters.client,
        jobcode_id:        data.activeParameters.id,
        start_date:        data.weekRange.start,
        end_date:          data.weekRange.end,
        is_posted:         1,
        posted_date:       new Date(),
        posted_by_user_id: data.user_id,
        posted_version:    1,
    };

    const workLog = new BaseResource({ url: '/post-report' });
    return workLog.post(request);
};

const getEmployeeWorkingReport = (context, payload) => {
  const workingReport = new BaseResource({ url: '/employee-working-report' });

  return workingReport
    .get(payload)
    .then(res => {
      context.commit('CLEAR_STATES');
      context.commit('WORKING_REPORT_LIST', { data: res.data });
      context.commit('SAVE_META', res.data.meta);
    })
    .catch(e => {
      throw new Error('Something went wrong: ' + e);
    });

};

const getReportNow = (context, payload) => {
  return WorkLogReport.override(payload);
};

const getEmployeeNames = (context, payload) => {
  const workingReport = new BaseResource({ url: '/employee-working-report' });

  return workingReport
    .get(payload)
    .then(res => {
        context.commit('SET_EMPLOYEE_NAMES', res.data);
    })
    .catch(e => {
      throw new Error('Something went wrong: ' + e);
    });

};

const getOverrideResponse = (context, payload) => {
  const overrideResponse = new BaseResource({ url: '/override-response' });

  overrideResponse
    .get(payload)
    .then(res => {
        context.commit('SET_OVERRIDE_RESPONSE', res.data);
    })
    .catch(e => {
      throw new Error('Something went wrong: ' + e);
    });
};

export {
    getClientProjects,
    getWorkLogReports,
    getYearsWithMonths,
    saveMeta,
    getReport,
    clearStates,
    setLoaderValue,
    getClientReport,
    postWorklogReport,
    getReportNow,
    getRawWorkLogReports,
    getEmployeeWorkingReport,
    getEmployeeNames,
    getOverrideResponse,
}
