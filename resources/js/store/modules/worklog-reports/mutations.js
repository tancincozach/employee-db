import moment from 'moment';
import _ from 'lodash';

const PROJECTS_UPDATED = (state, payload) => {
  state.projects = [];
  payload.data.map(e => {
    state.projects.push({ id: e.id, text: e.project_name });
  });
};

const CLEAR_STATES = state => {
  state.data = [];
  state.formatted = {
    chart: {
      categories: [],
      series: [],
      rangeTitle: '',
    },
    table: {
      headers: new Set(),
      content: [],
    },
    month: '',
  };
  state.meta = {};
  state.formattedReportFilter = {
    content: [],
    chart: {
      categories: [],
      series: [],
    },
  };
  state.employeeReports = {};
  state.weeklyHeaders = [];
  state.clientReports = {};
};

const CLEAR_OVERALL_FORMATTED_STATE = state => {
  state.formattedOverall = {};
};

const WORKLOG_REPORTS_UPDATED = (state, payload) => {
  state.data = payload.data;
  if (state.data.length > 0) {
    let names = new Set();
    let headers = state.data[0].headers;

    let firstIndexArray = state.data[0];
    let firstHeader = firstIndexArray.headers[0];
    let lastHeader =
      firstIndexArray.headers[firstIndexArray.headers.length - 1];
    let minDate = firstHeader.substr(0, firstHeader.indexOf('-'));
    let maxDate = lastHeader.substr(lastHeader.indexOf('-'), lastHeader.length);

    let startDate = moment(
      firstHeader.substr(0, firstHeader.indexOf('-')) +
      ' ' +
      firstIndexArray.year,
      'MMM D YYYY',
    );

    // Table
    for (let index in state.data) {
      let currentItem = state.data[index];

      let workHours = Object.values(currentItem.workLogs).filter(e => {
        return !!e;
      });
      let totalHours = workHours.reduce((a, b) => a + parseFloat(b), 0);
      currentItem.workLogs['total'] = Math.round(totalHours * 100) / 100;
      state.formatted.table.content.push({
        name: currentItem.employee,
        workLogs: currentItem.workLogs,
      });
      names.add(currentItem.employee);
    }

    // Chart
    let series = [];
    for (let indexHeader in headers) {
      let seriesItem = { name: '', data: [] };
      seriesItem.name = headers[indexHeader];
      for (let indexWorklog in state.data) {
        seriesItem.data.push(
          state.data[indexWorklog].workLogs[headers[indexHeader]],
        );
      }
      series.push(seriesItem);
    }

    state.formatted.table.headers = [...headers, 'Total'];
    state.formatted.chart.series = series;
    state.formatted.chart.rangeTitle = minDate + maxDate;
    state.formatted.chart.categories = Array(...names);
    state.formatted.month = startDate.month() + 1; // Moment.js months starts with 0

    state.formattedOverall = {
      ...state.formattedOverall,
      [startDate.year()]: {
        ...state.formattedOverall[startDate.year()],
        [startDate.month() + 1]: state.formatted,
      },
    };
  }
};

const WORKLOG_RAW_REPORTS_UPDATED = (state, payload) => {
  state.data = payload.data;
  state.totalSummaryPerClient = payload.totalHrsMeta;
};

const WORKLOG_REPORT_YEARS_UPDATED = (state, payload) => {
  state.years = {};
  for (let item in payload.years) {
    let year = Object.keys(payload.years[item])[0];

    if (Object.keys(state.years).includes(year)) {
      state.years = {
        ...state.years,
        [year]: {
          class: '',
          months: [...state.years[year].months, payload.years[item][year]],
        },
      };
    } else {
      state.years = {
        ...state.years,
        [year]: {
          class: '',
          months: [payload.years[item][year]],
        },
      };
    }
  }

  let years = Object.keys(state.years)
    .sort()
    .reverse();
  if (years.length > 0) {
    // Set active year as default view
    let latestYear = years[0];
    state.years[latestYear].class = 'active';
  }
};

const SAVE_META = (state, meta) => {
  state.meta = meta;
};

function formatWeek(week) {
  let weekSplit = week.split(' ');
  let startDate = moment(weekSplit[0]);
  let endDate = moment(weekSplit[1]);
  return (
    startDate.format('MMM') + '. ' + startDate.date() + ' - ' + endDate.date()
  );
}

const REPORT_FILTER_FORMAT = (state, payload) => {
  let data = payload.data.data;
  let names = [];
  let seriesItem = [];
  let contentItems = [];

  for (let i = 0; i < data.length; i++) {
    contentItems.push({
      name: data[i].employee,
      workLogs: [data[i].working_hrs],
    });
    names.push(data[i].employee);
    seriesItem.push(data[i].working_hrs);
  }

  state.formattedReportFilter.chart.categories = names;
  // state.formattedReportFilter.chart.series.data = seriesItem;
  state.formattedReportFilter.content = contentItems;

  state.formattedReportFilter.chart.series.push({ data: seriesItem, name: '' });
};

const REPORT_WEEKLY_FORMAT = (state, payload) => {
  let data = payload.data.data;
  let weeklyHeaders = data[0].weekly_headers;
  let tableOverall = [];

  for (let i = 0; i < weeklyHeaders.length; i++) {
    let contents = [];
    let headers = [];

    for (let j = 0; j < weeklyHeaders[i].dates.length; j++) {
      let log_date = weeklyHeaders[i].dates[j].log_date;
      headers.push({
        day: weeklyHeaders[i].dates[j].day,
        date: moment(log_date).format('MMM DD'),
      });
    }

    for (let k = 0; k < data.length; k++) {
      if (
        data[k].weekly_content[i].Total != 0 ||
        data[k].weekly_content[i].ptoTotal != 0 ||
        data[k].weekly_content[i].holidayTotal != 0
      ) {
        contents.push({
          id: data[k].employee_id,
          name: data[k].employee,
          workLogs: data[k].weekly_content[i].workLogs,
          total: data[k].weekly_content[i].Total,
        });
      }
    }

    tableOverall.push({
      contents: contents,
      headers: headers,
      isPosted: weeklyHeaders[i].isPosted,
      name: weeklyHeaders[i].name,
    });
  }
  state.weeklyHeaders = weeklyHeaders.map(header => header.name);
  state.formattedOverall = tableOverall.reverse();
};

const REPORT_CONSOLIDATOR = (state, payload) => {
  state.clientReports = {
    ...state.clientReports,
    [payload.jobcode]: {
      [payload.query.year]: {
        [payload.query.month]: state.formattedOverall.reverse(),
      },
    },
  };
};

const LOADER = state => {
  state.loaderValue = false;
};

const SET_LOADER = state => {
  state.loaderValue = true;
};

const WORKING_REPORT_LIST = (state, payload) => {
  state.employeeReports = payload.data;
};

const SET_EMPLOYEE_NAMES = (state, {data}) => {
    state.employeeNames = [];

    const formatted = _.map(data, item => ({
        key: item.employee_id,
        value: item.employee,
    }));

    state.employeeNames = formatted;
};

const SET_OVERRIDE_RESPONSE = (state, payload) => {
    state.overrideResponse = payload;
};

export {
  PROJECTS_UPDATED,
  CLEAR_STATES,
  CLEAR_OVERALL_FORMATTED_STATE,
  WORKLOG_REPORTS_UPDATED,
  WORKLOG_RAW_REPORTS_UPDATED,
  WORKLOG_REPORT_YEARS_UPDATED,
  SAVE_META,
  REPORT_FILTER_FORMAT,
  LOADER,
  SET_LOADER,
  REPORT_WEEKLY_FORMAT,
  REPORT_CONSOLIDATOR,
  WORKING_REPORT_LIST,
  SET_EMPLOYEE_NAMES,
  SET_OVERRIDE_RESPONSE,
};
