const projects = state => state.projects;
const data = state => state.data;
const formatted = state => state.formatted;
const formattedOverall = state => state.formattedOverall;
const years = state => state.years;
const meta = state => state.meta;
const formattedReportFilter = state => state.formattedReportFilter;
const loaderValue = state => state.loaderValue;
const weeklyHeaders = state => state.weeklyHeaders;
const clientReports = state => state.clientReports;
const employeeReports = state => state.employeeReports;
const employeeNames = state => state.employeeNames;
const totalSummaryPerClient = state => state.totalSummaryPerClient;
const overrideResponse = state => state.overrideResponse;

export {
  projects,
  data,
  years,
  formatted,
  formattedOverall,
  meta,
  formattedReportFilter,
  loaderValue,
  weeklyHeaders,
  clientReports,
  employeeReports,
  employeeNames,
  totalSummaryPerClient,
  overrideResponse,
};
