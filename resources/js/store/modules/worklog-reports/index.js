import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';

const state = {
  projects: [],
  data: [],
  formatted: {},
  formattedOverall: {},
  years: {},
  meta: {},
  formattedReportFilter: {},
  loadingValue: true,
  weeklyHeaders: [],
  clientReports: {},
  employeeReports: {},
  employeeNames: [],
  totalSummaryPerClient: [],
  overrideResponse: {},
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
