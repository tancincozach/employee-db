import _ from 'lodash';

const CHARACTERISTICS_UPDATED = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            name: row.name,
            description: row.description
        }, payload.extra);

        state.formatted.push(obj);
    });
}

const EDIT_CHARACTERISTIC = (state, payload) => {
    state.characteristic = payload.data;
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const DELETE_CHARACTERISTIC = (state) => {};

export {
    CHARACTERISTICS_UPDATED,
    EDIT_CHARACTERISTIC,
    SAVE_META,
    DELETE_CHARACTERISTIC
}
