const data = state => state.data;
const formatted = state => state.formatted;
const characteristic = state => state.characteristic;
const meta = state => state.meta;

export {
    data,
    formatted,
    characteristic,
    meta
}

