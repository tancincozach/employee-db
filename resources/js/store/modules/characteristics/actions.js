import { Characteristic } from '@common/model/Characteristic';

const getCharacteristics = (context, payload) => {
    return Characteristic.get(payload.query).then((res) => {
        context.commit('CHARACTERISTICS_UPDATED', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    })
};

const saveCharacteristic = (context, payload) => {
    const data = {
        name: payload.name,
        description: payload.description
    }

    const Characteristic = (payload.id != '' && payload.id > 0)
        ? new Characteristic({id: payload.id}) : new Characteristic();

    return Characteristic.save(data);
}

const getCharacteristic = (context, id) => {
    return Characteristic.get({id:id}).then((res) => {
        context.commit('EDIT_CHARACTERISTIC', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find Characteristic!');
    })
}

const deleteCharacteristic = (context, id) => {
    const Characteristic = new Characteristic({id: id});
    return Characteristic.delete().then((res) => {
        context.commit('DELETE_CHARACTERISTIC');
    });
};

export {
    getCharacteristics,
    saveCharacteristic,
    getCharacteristic,
    deleteCharacteristic
}
