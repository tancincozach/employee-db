import { TimesheetJobcodeAssignment } from '@common/model/TimesheetJobcodeAssignment';

const index = (context, payload) => {
    return TimesheetJobcodeAssignment.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('SET_DATA', res.data);
        context.commit('SET_META', res.meta);
    });
};

export {
    index,
}
