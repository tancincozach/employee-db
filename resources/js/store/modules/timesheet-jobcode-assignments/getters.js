const data = state => state.data;
const meta = state => state.meta;

export {
    data,
    meta,
}
