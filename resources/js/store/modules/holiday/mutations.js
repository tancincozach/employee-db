import _ from 'lodash';

const GET_HOLIDAYS = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            text: row.name,
        }, payload.extra);

        state.formatted.push(obj);
    });
}

const GET_HOLIDAY = (state, payload) => {
    state.holiday = payload.data
}

const DELETE_HOLIDAY = (state) => {}

const SEARCH_HOLIDAY = (state, payload) => {
    state.holiday = payload.data
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const CLEAR_STATES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
    state.holiday = {};
}



export {
    GET_HOLIDAYS,
    GET_HOLIDAY,
    DELETE_HOLIDAY,
    SEARCH_HOLIDAY,
    CLEAR_STATES,
    SAVE_META
}
