const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const holiday = state => state.holiday

export {
    data,
    formatted,
    meta,
    holiday
}
