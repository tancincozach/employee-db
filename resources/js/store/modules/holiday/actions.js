import { Holiday } from '@common/model/Holiday';

const getHolidays = (context, payload) => {
    return Holiday.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('GET_HOLIDAYS', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    });
};

const getHoliday = (context, id) => {
    return Holiday.get({id:id}).then((res) => {
        context.commit('GET_HOLIDAY', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find holiday!');
    })
}

const saveHoliday = (context, payload) => {
    const id    = payload.id;
    const data  = {name:payload.name, date:payload.date, type:payload.type};
    const holiday = (id != "" && id > 0) ? new Holiday({id:id}) : new Holiday();
    return holiday.save(data);

};

const deleteHoliday = (context, id) => {
    const holiday = new Holiday({id: id});
    return holiday.delete().then((res) => {
        context.commit('DELETE_HOLIDAY');
    });
};

const searchHolidayByName = (context, name) => {
    return Holiday.get({name: name, withTrashed: true}).then((res) => {
        context.commit('SEARCH_HOLIDAY', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find holiday!');
    })
}

const searchByDate = (context, date) => {
    return Holiday.get({searchByDate: date});
}

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
    getHolidays,
    getHoliday,
    saveHoliday,
    deleteHoliday,
    searchHolidayByName,
    saveMeta,
    searchByDate,
}
