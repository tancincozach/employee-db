import _ from 'lodash';

const SAVE_DATA = (state, payload) => {
    state.data = [];    
    let data = [];
    const timelined = _.values(_.groupBy(payload, 'separator'));

    _.each(timelined, row => {
        data.push({timeline: _.head(row).separator});
        
        _.each(row, item => {
            data.push(_.defaults(item));
        });
    });

    state.data = data;
};

const SAVE_UNREAD = (state, payload) => {
    state.unread = [];
    state.unread = _.filter(payload, ['read_at', null]);
}

const SET_HAS_NOTIFICATION = (state, payload) => {
    state.has_notification = payload;
};

export {
    SAVE_DATA,
    SAVE_UNREAD,
    SET_HAS_NOTIFICATION,
}
