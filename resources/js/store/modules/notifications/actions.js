import _ from 'lodash';
import { Notification } from '@common/model/Notification';

const getNotifications = (context, payload) => {
    return Notification.get(payload).then((res) => {
        context.commit('SAVE_DATA', res.data);
        context.commit('SAVE_UNREAD', res.data);

        const has_notification = res.data.length ? _.head(res.data).has_notification : 0;
        context.commit('SET_HAS_NOTIFICATION', has_notification);
    });
};

const readNotifications = (context, payload) => {
    return (new Notification()).save(payload).then(() => {
        context.commit('SET_HAS_NOTIFICATION', 0);
    });
} 

export {
    getNotifications,
    readNotifications,
}
