const data = state => state.data;
const unread = state => state.unread;
const hasNotification = state => state.has_notification;

export {
    data,
    unread,
    hasNotification,
}
