import _ from 'lodash';

const CREATIVE_SERVICE_APPLICANTS_UPDATED = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: payload.id,
            creative_service_id: payload.creative_service_id,
            full_name: payload.full_name,
            email: payload.email,
            primary: payload.primary,
        }, payload.extra);

        state.formatted.push(obj);
    });
}

const EDIT_CREATIVE_SERVICE_APPLICANT = (state, payload) => {
    state.creativeServiceApplicant = payload.data;
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const DELETE_CREATIVE_SERVICE_APPLICANT = (state) => {};

export {
    CREATIVE_SERVICE_APPLICANTS_UPDATED,
    EDIT_CREATIVE_SERVICE_APPLICANT,
    SAVE_META,
    DELETE_CREATIVE_SERVICE_APPLICANT
}
