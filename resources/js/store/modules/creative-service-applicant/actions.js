import { CreativeServiceApplicant } from '@common/model/CreativeServiceApplicant';

const getCreativeServiceApplicants = (context, payload) => {
    return CreativeServiceApplicant.get(payload.query).then((res) => {
        context.commit('CREATIVE_SERVICE_APPLICANTS_UPDATED', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    })
};

const saveCreativeServiceApplicant = (context, payload) => {

    const creativeServiceApplicant = (payload.id != '')
        ? new CreativeServiceApplicant({id: payload.id}) : new CreativeServiceApplicant();

    return creativeServiceApplicant.save(payload);
}

const getCreativeServiceApplicant = (context, id) => {
    return CreativeServiceApplicant.get({id:id}).then((res) => {
        context.commit('EDIT_CREATIVE_SERVICE_APPLICANT', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find category!');
    })
}

const getCreativeServiceApplicantByServiceCreativeId = (context, id) => {
    return CreativeServiceApplicant.get({creative_service_id:id}).then((res) => {
        context.commit('EDIT_CREATIVE_SERVICE_APPLICANT', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find category!');
    })
}

const deleteCreativeServiceApplicant = (context, id) => {
    const creativeServiceApplicant = new CreativeServiceApplicant({id: id});
    return creativeServiceApplicant.delete().then((res) => {
        context.commit('DELETE_CREATIVE_SERVICE_APPLICANT');
    });
};

export {
    getCreativeServiceApplicants,
    saveCreativeServiceApplicant,
    getCreativeServiceApplicant,
    getCreativeServiceApplicantByServiceCreativeId,
    deleteCreativeServiceApplicant
}
