const data = state => state.data;
const formatted = state => state.formatted;
const creativeServiceApplicant = state => state.creativeServiceApplicant;
const meta = state => state.meta;

export {
    data,
    formatted,
    creativeServiceApplicant,
    meta
}

