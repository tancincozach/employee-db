import _ from 'lodash';

const CATEGORIES_UPDATED = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            parent_id: row.parent_id,
            role: row.name,
            type: row.resource_type,
            description: row.description
        }, payload.extra);

        state.formatted.push(obj);
    });
}

const EDIT_CATEGORY = (state, payload) => {
    state.category = payload.data;
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const DELETE_CATEGORY = (state) => {};

export {
    CATEGORIES_UPDATED,
    EDIT_CATEGORY,
    SAVE_META,
    DELETE_CATEGORY
}
