const data = state => state.data;
const formatted = state => state.formatted;
const category = state => state.category;
const meta = state => state.meta;

export {
    data,
    formatted,
    category,
    meta
}

