import { Category } from '@common/model/Category';

const getCategories = (context, payload) => {
    return Category.get(payload.query).then((res) => {
        context.commit('CATEGORIES_UPDATED', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    })
};

const saveCategory = (context, payload) => {
    const data = {
        name: payload.name,
        resource_type: payload.resourceTypes,
        description: payload.description
    }

    const category = (payload.id != '' && payload.id > 0)
        ? new Category({id: payload.id}) : new Category();

    return category.save(data);
}

const getCategory = (context, id) => {
    return Category.get({id:id}).then((res) => {
        context.commit('EDIT_CATEGORY', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find category!');
    })
}

const deleteCategory = (context, id) => {
    const category = new Category({id: id});
    return category.delete().then((res) => {
        context.commit('DELETE_CATEGORY');
    });
};

export {
    getCategories,
    saveCategory,
    getCategory,
    deleteCategory
}
