import { UserTimezone } from '@common/model/UserTimezone';

const index = (context, payload) => {
    return UserTimezone.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('SET_DATA', res.data);
        context.commit('SET_META', res.meta);
    });
};

const save = (context, payload) => {
    const id = payload.id;
    const data = {
        user_id: payload.user_id,
        zone_id: payload.zone_id,
    };
    const userTimezone = (id != "" && id > 0) ? new UserTimezone({id:id}) : new UserTimezone();

    return userTimezone.save(data);
};

const clearStates = context => context.commit('CLEAR_STATES');

export {
    index,
    save,
    clearStates,
}
