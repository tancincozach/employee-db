import _ from 'lodash';

const ADD_CONTACT_USER = (state, payload) => {
    state.data = payload;
    state.formatted = [];
}

export {
    ADD_CONTACT_USER
}
