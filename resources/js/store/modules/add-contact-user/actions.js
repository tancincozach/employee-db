import { AddContactUser } from '@common/model/AddContactUser';

const addContactUser = (context, payload) => {
    const addContactUser = new AddContactUser();
    return addContactUser.save(payload);
};


export {
    addContactUser
}
