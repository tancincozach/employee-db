import { FeedbackCategory } from '@common/model/FeedbackCategory';

const getFeedbackCategories = (context, payload) => {
    return FeedbackCategory.get(payload.query).then((res) => {
        context.commit('FEEDBACK_CATEGORIES_UPDATED', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    })
};

const saveFeedbackCategory = (context, payload) => {
    const data = {
        id: payload.id,
        name: payload.name
    }

    const category = (payload.id != '' && payload.id > 0)
        ? new FeedbackCategory({id: payload.id}) : new FeedbackCategory();

    return category.save(data);
}

const getFeedbackCategory = (context, id) => {
    return FeedbackCategory.get({id:id}).then((res) => {
        context.commit('EDIT_FEEDBACK_CATEGORY', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find category!');
    })
}

const deleteFeedbackCategory = (context, id) => {
    const category = new FeedbackCategory({id: id});
    return category.delete().then((res) => {
        context.commit('DELETE_FEEDBACK_CATEGORY');
    });
};

export {
    getFeedbackCategories,
    saveFeedbackCategory,
    getFeedbackCategory,
    deleteFeedbackCategory
}
