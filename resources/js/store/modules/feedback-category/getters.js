const data = state => state.data;
const formatted = state => state.formatted;
const feedbackCategory = state => state.feedbackCategory;
const meta = state => state.meta;

export {
    data,
    formatted,
    feedbackCategory,
    meta
}

