import _ from 'lodash';

const FEEDBACK_CATEGORIES_UPDATED = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            name: row.name,
        }, payload.extra);

        state.formatted.push(obj);
    });
}

const EDIT_FEEDBACK_CATEGORY = (state, payload) => {
    state.category = payload.data;
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const DELETE_FEEDBACK_CATEGORY = (state) => {};

export {
    FEEDBACK_CATEGORIES_UPDATED,
    EDIT_FEEDBACK_CATEGORY,
    SAVE_META,
    DELETE_FEEDBACK_CATEGORY
}
