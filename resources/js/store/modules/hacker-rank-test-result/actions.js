import { HackerRankTestResult } from '@common/model/HackerRankTestResult';
import { BaseResource } from '@common/resource/BaseResource';
import _ from 'lodash'

const getTestResults = (context, payload) => {
    return HackerRankTestResult.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('GET_TEST_RESULTS', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    });
};

const getTestResult = (context, id) => {
    return HackerRankTestResult.get({id:id}).then((res) => {
        context.commit('GET_TEST_RESULT', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find Test Result!');
    })
}

const saveHackerRankTestResult = (context, payload) => {
    const id    = payload.id;
    const data  = {name:payload.name, date:payload.date, type:payload.type};
    const hackresult = (id != "" && id > 0) ? new HackerRankTestResult({id:id}) : new HackerRankTestResult();
    return hackresult.save(data);

};

const deleteHackerRankTestResult = (context, id) => {
    const hackresult = new HackerRankTestResult({id: id});
    return hackresult.delete().then((res) => {
        context.commit('DELETE_TEST_RESULT');
    });
};

const searchHackerRankTestResultByEmail = (context, email) => {
    return HackerRankTestResult.get({name: name, withTrashed: true}).then((res) => {
        context.commit('SEARCH_TEST_RESULT', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find Hacker Rank Test Result!');
    })
}

const importHackerRankTestResults = (context, payload) => {
    const hackerRankTestResults = new BaseResource({ url: `/hacker-rank-result/import` })

    return hackerRankTestResults.get(payload.query);
};

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
    getTestResults,
    getTestResult,
    saveHackerRankTestResult,
    deleteHackerRankTestResult,
    searchHackerRankTestResultByEmail,
    importHackerRankTestResults,
    saveMeta,
}
