const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const results = state => state.results

export {
    data,
    formatted,
    meta,
    results
}
