import _ from 'lodash';

const GET_TEST_RESULTS = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            text: row.name,
        }, payload.extra);

        state.formatted.push(obj);
    });
}

const GET_TEST_RESULT = (state, payload) => {
    state.results = payload.data
}

const DELETE_TEST_RESULT = (state) => {}

const SEARCH_TEST_RESULT = (state, payload) => {
    state.results = payload.data
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const CLEAR_STATES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
    state.results = {};
}



export {
    GET_TEST_RESULTS,
    GET_TEST_RESULT,
    DELETE_TEST_RESULT,
    SEARCH_TEST_RESULT,
    CLEAR_STATES,
    SAVE_META
}
