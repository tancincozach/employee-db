import { WizardClientSetup } from '@common/model/WizardClientSetup';

const addStep1Data = (context, client_id) => {
    context.commit('ADD_STEP1_DATA', client_id);
    context.commit('SET_DATA', { step: 'step_1', data: client_id });
};

const addStep2Data = (context, payload) => {
    context.commit('ADD_STEP2_DATA', payload);
    context.commit('SET_DATA', { step: 'step_2', data: payload });
};

const addStep3Data = (context, payload) => {
    context.commit('ADD_STEP3_DATA', payload);
    context.commit('SET_DATA', { step: 'step_3', data: payload });
};

const addStepLastData = (context, payload) => {
    context.commit('ADD_STEPLAST_DATA', payload);
    // context.commit('SET_DATA', { step: 'step_last', data: payload });
};

const setCurrentStep = (context, step_number) => {
    context.commit('SET_CURRENT_STEP', step_number);
};

const saveWizardClientSetup = (context, payload) => {
    const id = payload.id;
    const wizardClientSetup = (id != null && id != "" && id > 0) ? new WizardClientSetup({ id: id }) : new WizardClientSetup();
    return wizardClientSetup.save(payload);
}

const setStep2ValidationResult = (context, result) => {
    context.commit('SET_STEP2_VALIDATION_RESULT', result);
};

const setStep3ValidationResult = (context, result) => {
    context.commit('SET_STEP3_VALIDATION_RESULT', result);
};

const initializeWizardData = (context) => {
    context.commit('CLEAR_WIZARD_DATA');
};

/**
 * Triggers
 */
const triggerStep2AddData = (context, payload) => { };
const triggerStep3AddData = (context, payload) => { };
const triggerStepLastAddData = (context, payload) => { };

const triggerStep2Validation = (context, payload) => { };
const triggerStep3Validation = (context, payload) => { };


export {
    addStep1Data,
    addStep2Data,
    addStep3Data,
    addStepLastData,
    setCurrentStep,
    triggerStep2AddData,
    triggerStep3AddData,
    triggerStepLastAddData,
    saveWizardClientSetup,
    triggerStep2Validation,
    triggerStep3Validation,
    setStep2ValidationResult,
    setStep3ValidationResult,
    initializeWizardData,
}
