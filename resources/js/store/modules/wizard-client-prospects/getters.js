const current_step = state => state.current_step;

const step_1 = state => state.step_1;

const step_2 = state => state.step_2;

const step_3 = state => state.step_3;

const step2HasError = state => state.step2HasError;

const step3HasError = state => state.step3HasError;

const step_last = state => state.step_last;

const data = state => state.data;

export {
    data,
    step_1,
    step_2,
    step_3,
    step_last,
    current_step,
    step2HasError,
    step3HasError,
}
