import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';

const state = {
    data: [],
    current_step: 0,
    step_1: 0,
    step_2: {
        client: {},
        contacts: [],
    },
    step_3: {},
    step2HasError: false,
    step3HasError: false,
    step_last: 1,
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
