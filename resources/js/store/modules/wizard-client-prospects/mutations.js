const ADD_STEP1_DATA = (state, client_id) => {
    state.step_1 = client_id;
};

const ADD_STEP2_DATA = (state, { data, contacts }) => {
    state.step_2.client = data;
    state.step_2.contacts = contacts;
};

const ADD_STEP3_DATA = (state, data) => {
    state.step_3 = data;
};

const ADD_STEPLAST_DATA = (state, data) => {
    state.step_last = data;
};

const SET_CURRENT_STEP = (state, step_number) => {
    state.current_step = step_number;
};

const SET_DATA = (state, payload) => {
    const step = payload.step;
    const data = payload.data;
    state.data[step] = data;
};

const SET_STEP2_VALIDATION_RESULT = (state, result) => {
    state.step2HasError = result;
};

const SET_STEP3_VALIDATION_RESULT = (state, result) => {
    state.step3HasError = result;
};

const CLEAR_WIZARD_DATA = (state) => {
    state.data = [];
    state.step_1 = 0,
    state.step_2 = {
        client: {},
        contacts: [],
    };
    state.step_3 = {};
    state.step2HasError = false;
    state.step3HasError = false;
    state.step_last = 1;
};

export {
    ADD_STEP1_DATA,
    ADD_STEP2_DATA,
    ADD_STEP3_DATA,
    ADD_STEPLAST_DATA,
    SET_CURRENT_STEP,
    SET_DATA,
    SET_STEP2_VALIDATION_RESULT,
    SET_STEP3_VALIDATION_RESULT,
    CLEAR_WIZARD_DATA,
}
