import { SmartSearch } from '@common/model/SmartSearch';

const getSmartSearches = (context, payload) => SmartSearch.get(payload.query).then((res) => {
    context.commit('CLEAR_STATES');
    context.commit('SMARTSEARCHES_UPDATED', {
        data: res.data,
        extra: payload.extra,
    });
    context.commit('SAVE_META', res.meta);
});

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export { getSmartSearches, saveMeta };
