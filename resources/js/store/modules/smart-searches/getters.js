const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;

export {
    data,
    formatted,
    meta,
};
