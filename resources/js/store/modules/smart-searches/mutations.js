import _ from 'lodash';

const SMARTSEARCHES_UPDATED = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        const obj = _.defaults(
            {
                id: row.id,
                text: row.name,
                type: row.class,
                photo: row.photo,
            },
            payload.extra,
        );

        state.formatted.push(obj);
    });
};


const SAVE_META = (state, meta) => {
    state.meta = meta;
};

const CLEAR_STATES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
};

export {
    SMARTSEARCHES_UPDATED,
    SAVE_META,
    CLEAR_STATES,
};
