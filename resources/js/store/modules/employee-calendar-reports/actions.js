import { EmployeeCalendarReports } from '@common/model/EmployeeCalendarReports';
const getEmployeeCalendarReports = (context, payload) => {
    return EmployeeCalendarReports.get(payload.query).then((res) => {
        payload.data =  _.map(res.data,function(EmpReportData){                              
                            let EmpObject = {};
                                EmpObject.id=EmpReportData.employee.id,
                                EmpObject.user_id=EmpReportData.employee.user_id,
                                EmpObject.first_name=EmpReportData.employee.first_name,
                                EmpObject.last_name=EmpReportData.employee.last_name,
                                EmpReportData.employee = EmpObject;
                                return EmpReportData;
        });         
        context.commit('EMPLOYEE_CALENDAR_REPORTS', {data:payload.data,extra:payload.extra});
    });
};

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
    getEmployeeCalendarReports,
    saveMeta
}
