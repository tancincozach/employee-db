const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const employeeCalendarReport = state => state.employeeCalendarReport;
export {
    data,
    formatted,
    meta,
    employeeCalendarReport
}
