import _ from 'lodash';


const EMPLOYEE_CALENDAR_REPORTS = (state, payload) => {    
    state.data =  payload.data;    
    state.formatted = [];
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const CLEAR_STATES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = [];

}

export {
    EMPLOYEE_CALENDAR_REPORTS,
    SAVE_META,
    CLEAR_STATES
}
