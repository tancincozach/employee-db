const ORGANIZATION_NAME_DEFAULT_SET = (state, defaultVal) => {
    state.defaultVal = [defaultVal];
}

const TRIGGER_SIDEBAR = (state, defaultVal) => {
    state.sidebarToggle = new Date().getTime();
}

const TRIGGER_EMPLOYEE_PROFILE = (state, defaultVal) => {
    state.searchEmployeeGlobal = new Date().getTime();
}

export {
    ORGANIZATION_NAME_DEFAULT_SET,
    TRIGGER_SIDEBAR,
    TRIGGER_EMPLOYEE_PROFILE
}