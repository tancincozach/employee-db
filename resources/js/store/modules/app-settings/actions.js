const setDefaultVal = (context, defaultVal) => {
    context.commit('ORGANIZATION_NAME_DEFAULT_SET', defaultVal);
};

const triggerSidebar = (context, defaultVal) => {
    context.commit('TRIGGER_SIDEBAR', defaultVal);
};

const triggerEmployeeProfile = (context, defaultVal) => {
    context.commit('TRIGGER_EMPLOYEE_PROFILE', defaultVal);
};

export {
    setDefaultVal,
    triggerSidebar,
    triggerEmployeeProfile
}
