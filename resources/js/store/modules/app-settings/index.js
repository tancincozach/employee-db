import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';

const state = {
    fullNameText: 'Gigabook Inc./Full Scale',
    shortNameText: 'Full Scale',
    fullScaleLogo: '/images/fs-logo-white.png',

    sidebarToggle: '', /** This is to capture the sidebar dynamic value, not necessarily its state **/
    searchEmployeeGlobal: ''
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
