const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const employee_academic = state => state.employee_academic;

export {
    data,
    formatted,
    meta,
    employee_academic
}
