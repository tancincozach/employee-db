import _ from 'lodash';

const EMPLOYEE_ACADEMICS_UPDATED = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            employee_id: row.employee_id,
            degree_holder: row.degree_holder,
            academic_accomplishment: row.academic_accomplishment,
            certification: row.certification
        }, payload.extra);

        state.formatted.push(obj);
    });
};

const EMPLOYEE_ACADEMIC_UPDATED = (state, payload) => {
    state.employee_academic = payload.data
}

const CLEAR_EMPLOYEE_ACADEMICS = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
};

const DELETE_EMPLOYEE_ACADEMIC = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
};

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

export {
    SAVE_META,
    DELETE_EMPLOYEE_ACADEMIC,
    CLEAR_EMPLOYEE_ACADEMICS,
    EMPLOYEE_ACADEMICS_UPDATED,
    EMPLOYEE_ACADEMIC_UPDATED
}
