import { EmployeeAcademic } from '@common/model/EmployeeAcademic';

const getEmployeeAcademics = (context, payload) => {
    return EmployeeAcademic.get(payload.query).then((res) => {
    	context.commit('CLEAR_EMPLOYEE_ACADEMICS');
        context.commit('EMPLOYEE_ACADEMICS_UPDATED', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    })
};

const getEmployeeAcademic = (context, id) => {
    return EmployeeAcademic.get({id:id}).then((res) => {
        context.commit('EMPLOYEE_ACADEMIC_UPDATED', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find position!');
    })
}

const saveEmployeeAcademic = (context, payload) => {
	const id    = payload.id;
    const data  = {
        employee_id:payload.employee_id,
        degree_holder: payload.degree_holder,
        academic_accomplishment: payload.academic_accomplishment,
        certification: payload.certification
    };
    const EmpAcademic = (id != "" && id > 0) ? new EmployeeAcademic({id:id}) : new EmployeeAcademic();
    return EmpAcademic.save(data);
}

const clearEmployeeAcademics = (context) => {
    context.commit('CLEAR_EMPLOYEE_ACADEMICS');
};

const deleteEmployeeAcademic = (context, id) => {
    const EmpAcademic = new EmployeeAcademic({id: id});
    return EmpAcademic.delete().then((res) => {
        context.commit('DELETE_EMPLOYEE_ACADEMIC');
    });
};

const searchEmployeeAcademic = (context, employee_id) => {
    return EmployeeAcademic.get({employee_id: employee_id, withTrashed: true}).then((res) => {
        context.commit('EMPLOYEE_ACADEMIC_UPDATED', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find position!');
    })
}

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
	saveMeta,
    getEmployeeAcademics,
    getEmployeeAcademic,
    clearEmployeeAcademics,
    deleteEmployeeAcademic,
    saveEmployeeAcademic,
    searchEmployeeAcademic
}
