import { ClientTeamBuilder } from '@common/model/ClientTeamBuilder';
import { BaseResource } from '@common/resource/BaseResource';
import _ from 'lodash';

const getClientTeamBuilder = (context, payload) => {
    return ClientTeamBuilder.get(payload.query).then((res) => {
        context.commit('CLIENTTEAMBUILDER_UPDATED', { data: res.data, extra: payload.extra });
        context.commit('META_SET', res.meta);
    });
};

const setResourceType = (context, type) => {
    context.commit('RESOURCE_TYPES_SET', type);
};

const setCurrentResourcePage = (context, resource) => {
    context.commit('CURRENT_RESOURCE_SET', resource);
};

const saveSuggestedResource = (context, suggestedResource) => {
    context.commit('SAVE_SUGGESTED_RESOURCE', suggestedResource);
};

const removeSuggestedResource = (context, id) => {
    context.commit('REMOVE_SUGGESTED_RESOURCE', id);
};

const initializeSuggestedResource = (context) => {
    context.commit('INITIALIZE_SUGGESTED_RESOURCE');
};

const initializeResourceTypes = (context) => {
    context.commit('INITIALIZE_RESOURCE_TYPES');
};

const initializeCountSelected = (context) => {
    context.commit('INITIALIZE_COUNT_SELECTED');
};

const initializeCurrentRoute = (context, currentRoute) => {
    context.commit('INITIALIZE_CURRENT_ROUTE', currentRoute);
};

const initializeUpdatedBucket = (context, updatedBucket) => {
    context.commit('INITIALIZE_UPDATED_BUCKET', updatedBucket);
};

const incrementCountSelected = (context, count) => {
    context.commit('INCREMENT_COUNT_SELECTED', count);
};

const setResourceTitle = (context, title) => {
    context.commit('SAVE_RESOURCE_TITLE', title);
};

const setCurrentSuggestedResource = (context, resource) => {
    context.commit('INITIALIZE_SUGGESTED_RESOURCE', resource);
    context.commit('CURRENT_SUGGESTED_RESOURCE_SET', resource);
};

const initializeSelectedResource = (context) => {
    context.commit('INITIALIZE_SELECTED_RESOURCE');
};

const setSelectedResource = (context, selectedResource) => {
    context.commit('SET_SELECTED_RESOURCE', selectedResource);
};

const removeSelectedResource = (context, unselectedResource) => {
    context.commit('REMOVE_SELECTED_RESOURCE', unselectedResource);
};

const saveSelectedResource = (context, payload) => {
    if (context.state.selectedResource.length > 0) {
        const clientTeamBuilder = new ClientTeamBuilder();
        const data = _.assign(context.state.selectedResource, { payload });
        return clientTeamBuilder.save(data).then((res) => {
        });
    }
};

const saveSelectedTalents = (context, payload) => {
    const clientTeamBuilder = new BaseResource({ url: `/team-builder/save-talent` })

    return clientTeamBuilder.post(payload);
};

const saveClientTeamBuilder = (context, payload) => {
    const clientTeamBuilder = new ClientTeamBuilder();
    //put payload on [] as it needs to be array
    return clientTeamBuilder.save([payload]).then((res) => {
    });
}

const saveUpdatedBucketStatus = (context, updatedStatus) => {
    context.commit('UPDATED_BUCKET_STATUS', updatedStatus);
}

const saveTeamBuilder = (context, payload) => {
    const clientTeamBuilder = new BaseResource({ url: `/team-builder/save-team-builder` })

    return clientTeamBuilder.post(payload);
};

const sendClientNotification = (context, select) => {
    context.commit('SEND_CLIENT_NOTIFICATION', select);
};

const emptyResourceTypes = (context) => {
    context.commit('EMPTY_RESOURCE_TYPES');
}

const getAvailableEmployees = (context, payload) => {
    let resource = new BaseResource({ url: `/employees/available` })
    return resource.get(payload).then((res) => {
        if ('resourceTypes' in payload) {
            context.commit('FORMAT_AVAILABLE_CAROUSELS', { data: res.data, extra: res.extra });
        } else {
            return res.data.data;
        }
        
    });
}

const getEmployeeAvailables = (context, payload) => {
    let resource = new BaseResource({ url: `/employees/get-availables` })
    return resource.get(payload).then((res) => {
       
        if ('resourceTypes' in payload) {
            if (res.hasOwnProperty('extra')) {
                context.commit('FORMAT_AVAILABLE_CAROUSELS', { data: res.data.data, extra: res.extra });
            } else {
                return res.data.data;
            }
        } else {
            return res.data.data;
        }
        
    });
}

const getAvailableResource = (context, payload) => {
    let resource = new BaseResource({ url: `/employees/available-resource` })
    return resource.get(payload).then((res) => {
        if ('resourceTypes' in payload) {
            context.commit('FORMAT_AVAILABLE_CAROUSELS', { data: res.data, extra: res.extra });
        } else {
            return res.data.data;
        }

    });
}

const saveSelectedTechnologyByClient = (context, technology) => {
    context.commit('SAVE_SELECTED_TECHNOLOGY_BY_CLIENT', technology);
}

const emptySelectedTechnologyByClient = (context) => {
    context.commit('EMPTY_SELECTED_TECHNOLOGY_BY_CLIENT');
}

const getOnboardingResults = (context, payload) => {
    const results = new BaseResource({ url: `/onboarding-results` });

    return results.get(payload).then((res) => {
        context.commit('GET_ONBOARDING_RESULTS', res.data);

    });
}

const setSubResources = (context, subResource) => {
    context.commit('SET_SUB_RESOURCE', subResource);
}

export {
    getClientTeamBuilder,
    setResourceType,
    setCurrentResourcePage,
    saveSuggestedResource,
    initializeSuggestedResource,
    initializeResourceTypes,
    initializeCurrentRoute,
    setResourceTitle,
    setCurrentSuggestedResource,
    initializeSelectedResource,
    initializeUpdatedBucket,
    setSelectedResource,
    removeSelectedResource,
    saveSelectedResource,
    removeSuggestedResource,
    initializeCountSelected,
    incrementCountSelected,
    saveSelectedTalents,
    saveClientTeamBuilder,
    saveUpdatedBucketStatus,
    saveTeamBuilder,
    sendClientNotification,
    emptyResourceTypes,
    getAvailableEmployees,
    getEmployeeAvailables,
    getAvailableResource,
    saveSelectedTechnologyByClient,
    emptySelectedTechnologyByClient,
    getOnboardingResults,
    setSubResources
}
