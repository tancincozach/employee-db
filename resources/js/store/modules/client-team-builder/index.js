import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';

const state = {
    data                   : [],
    formatted              : [],
    meta                   : {},
    countSelected          : '',
    resourceTypes          : [],
    currentResource        : '',
    currentRoute           : '',
    suggestedResource      : [],
    selectedResource       : [],
    tempResource           : '',
    resourceTitle          : '',
    updatedBucket          : [],
    clientNotification     : [],
    formattedAvailable     : [],
    clientChosenTechnology : {},
    clientOnboardingResults: {},
    subResourceTypes       : [],
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
