const data                    = state => state.data;
const formatted               = state => state.formatted;
const meta                    = state => state.meta;
const resourceTypes           = state => state.resourceTypes;
const currentResource         = state => state.currentResource;
const suggestedResource       = state => state.suggestedResource;
const currentRoute            = state => state.currentRoute;
const tempResource            = state => state.tempResource;
const resourceTitle           = state => state.resourceTitle;
const selectedResource        = state => state.selectedResource;
const countSelected           = state => state.countSelected;
const updatedBucket           = state => state.updatedBucket;
const clientNotification      = state => state.clientNotification;
const formattedAvailable      = state => state.formattedAvailable;
const clientChosenTechnology  = state => state.clientChosenTechnology;
const clientOnboardingResults = state => state.clientOnboardingResults;
const subResourceTypes        = state => state.subResourceTypes;

export {
    data,
    formatted,
    meta,
    resourceTypes,
    currentResource,
    suggestedResource,
    currentRoute,
    tempResource,
    resourceTitle,
    selectedResource,
    countSelected,
    updatedBucket,
    clientNotification,
    formattedAvailable,
    clientChosenTechnology,
    clientOnboardingResults,
    subResourceTypes
}
