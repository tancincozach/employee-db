import _ from 'lodash';

const CLIENTTEAMBUILDER_UPDATED = (state, payload) => {
    state.data = payload.data;
}

const META_SET = (state, meta) => {
    state.meta = meta;
};

const RESOURCE_TYPES_SET = (state, type) => {
    if (type[1]) { // If checked
        state.resourceTypes.push(type[0]);
    }
    else { // If unchecked
        let index = state.resourceTypes.findIndex(v => v.route_resource == type[0].route_resource);
        if (index > -1){
            state.resourceTypes.splice(index, 1);
        }
    }
};

const CURRENT_RESOURCE_SET = (state, resource) => {
    state.currentResource = resource;
};

const SAVE_SUGGESTED_RESOURCE = (state, suggestedResource) => {
    state.suggestedResource.push(suggestedResource);

    if (state.updatedBucket.some(el => el.employee_id == suggestedResource.employee_id && el.status == 'available')) {
        let index = state.updatedBucket.findIndex(e => e.employee_id == suggestedResource.employee_id)
        state.updatedBucket.splice(index, 1);
    }
};

const REMOVE_SUGGESTED_RESOURCE = (state, id) => {
    if (id > 0) {
        let index = state.suggestedResource.findIndex(v => v.employee_id == id);
        if (index > -1)
            state.suggestedResource.splice(index, 1);
    }
};

const INITIALIZE_RESOURCE_TYPES = (state) => {
    state.resourceTypes = [
        {
            route_name: 'Choose the Developers you would like to suggest',
            route_uri: state.currentRoute + '/team-builder-bucket',
            route_resource: 'Developers'
        },
        {
            route_name: 'Choose the Project Managers you would like to suggest',
            route_uri: state.currentRoute + '/team-builder-bucket',
            route_resource: 'Project Managers'
        }
    ];
};

const INITIALIZE_SUGGESTED_RESOURCE = (state) => {
    state.suggestedResource = [];
};

const INITIALIZE_COUNT_SELECTED = (state) => {
    state.countSelected = 0;
};

const INITIALIZE_CURRENT_ROUTE = (state, currentRoute) => {
    state.currentRoute = currentRoute;
};

const SAVE_RESOURCE_TITLE = (state, title) => {
    state.resourceTitle = title;
};

const CURRENT_SUGGESTED_RESOURCE_SET = (state, resource) => {
    state.suggestedResource = resource;
};

const INITIALIZE_SELECTED_RESOURCE = (state) => {
    state.selectedResource = [];
};

const INITIALIZE_UPDATED_BUCKET = (state) => {
    state.updatedBucket = [];
};

const SET_SELECTED_RESOURCE = (state, selectedResource) => {
    let index = ('client_teambuilder_bucket_id' in selectedResource) 
        ? state.selectedResource.findIndex(v => v.client_teambuilder_bucket_id == selectedResource.client_teambuilder_bucket_id)
        : state.selectedResource.findIndex(v => v.employee_id == selectedResource.employee_id);
        
    if (index > -1) {
        state.selectedResource.splice(index, 1);
    } else {
        state.selectedResource.push(selectedResource);
    }
};

const REMOVE_SELECTED_RESOURCE = (state, unselectedResource) => {
    let index = ('client_teambuilder_bucket_id' in unselectedResource) 
        ? state.selectedResource.findIndex(v => v.client_teambuilder_bucket_id == unselectedResource.client_teambuilder_bucket_id)
        : state.selectedResource.findIndex(v => v.employee_id == unselectedResource.employee_id);

    if (index > -1) {
        state.selectedResource.splice(index, 1);
        if ('client_teambuilder_bucket_id' in unselectedResource) {
            state.selectedResource.push(unselectedResource);
        }
    } else {
        state.selectedResource.push(unselectedResource);
    }
};

const INCREMENT_COUNT_SELECTED = (state, count) => {
    state.countSelected = count;
};

const UPDATE_CLIENTTEAMBUILDER = (state, payload) => {

};

const UPDATED_BUCKET_STATUS = (state, updatedStatus) => {
    state.updatedBucket.push(updatedStatus);
};

const SEND_CLIENT_NOTIFICATION = (state, select) => {
    state.clientNotification = select;
};

const EMPTY_RESOURCE_TYPES = (state) => {
    state.resourceTypes = [];
    state.subResourceTypes = [];
    state.clientChosenTechnology = {};
};

/*Temporary Store the click technology on client-technology-select.vue*/
const SAVE_SELECTED_TECHNOLOGY_BY_CLIENT = (state, technology) => {
    if (technology[1] != '') {
        let group = (technology[1] == 'Mobile') ? 'Developers' : technology[1];
        if (_.isUndefined(state.clientChosenTechnology[group])) {
            state.clientChosenTechnology[group] = [];
        }
        /*When selecting any technology it will compare state.clientChosen[] if there are matches on technology */
        let index = state.clientChosenTechnology[group].findIndex(v => v == technology[0]);
        /*when there is a match on technology this condition will remove it */
        if (index > -1) {
            state.clientChosenTechnology[group].splice(index, 1);
        } else {
            state.clientChosenTechnology[group].push(technology[0]);
        }
    }
};
/*At the start on the page it will always run a create thus this will be place on the created to clear its caching on technology */
const EMPTY_SELECTED_TECHNOLOGY_BY_CLIENT = (state) => {
    state.clientChosenTechnology = [];
};

const GET_ONBOARDING_RESULTS = (state, payload) => {
    state.clientOnboardingResults = payload;
}

const FORMAT_AVAILABLE_CAROUSELS = (state, payload) => {
    state.formattedAvailable = [];

    // format data for use with the component
    _.each(payload.data.data, (row) => {
        let empSkills = [];
        _.each(row.topSkills.data, (skillRow) => {
            let skillObject = {
                skill: {
                    id: skillRow.id,
                    name: skillRow.skill_name
                },
                proficiency: skillRow.proficiency
            };
            empSkills.push(skillObject);
        })

        let obj = _.defaults({
            employee_id: row.id,
            job_position_id: row.positions.data[0].position_id,
            positions: row.positions.data[0].job_title,
            profile_url: row.profile_url,
            first_name: row.first_name,
            last_name: row.last_name,
            skills: empSkills,
            status: 'suggested',
            photo: row.photo,
        }, payload.extra);
        state.formattedAvailable.push(obj);
    });
};

const SET_SUB_RESOURCE = (state, type) => {
    if (type[1]) { // If checked
        state.subResourceTypes.push(type[0]);
    }
    else { // If unchecked
        let index = state.subResourceTypes.findIndex(subResource => subResource.subAsset == type[0].subAsset);
        if (index > -1){
            state.subResourceTypes.splice(index, 1);
        }
    }
}

export {
    META_SET,
    CLIENTTEAMBUILDER_UPDATED,
    RESOURCE_TYPES_SET,
    CURRENT_RESOURCE_SET,
    SAVE_SUGGESTED_RESOURCE,
    INITIALIZE_RESOURCE_TYPES,
    INITIALIZE_SUGGESTED_RESOURCE,
    SAVE_RESOURCE_TITLE,
    CURRENT_SUGGESTED_RESOURCE_SET,
    INITIALIZE_SELECTED_RESOURCE,
    INITIALIZE_CURRENT_ROUTE,
    INITIALIZE_UPDATED_BUCKET,
    SET_SELECTED_RESOURCE,
    REMOVE_SELECTED_RESOURCE,
    INITIALIZE_COUNT_SELECTED,
    REMOVE_SUGGESTED_RESOURCE,
    INCREMENT_COUNT_SELECTED,
    UPDATE_CLIENTTEAMBUILDER,
    UPDATED_BUCKET_STATUS,
    SEND_CLIENT_NOTIFICATION,
    EMPTY_RESOURCE_TYPES,
    FORMAT_AVAILABLE_CAROUSELS,
    SAVE_SELECTED_TECHNOLOGY_BY_CLIENT,
    EMPTY_SELECTED_TECHNOLOGY_BY_CLIENT,
    GET_ONBOARDING_RESULTS,
    SET_SUB_RESOURCE
}