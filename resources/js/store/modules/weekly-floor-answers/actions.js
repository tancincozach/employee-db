import { Answers } from '@common/model/weekly-floor-report/Answers'

const getWeeklyFloorAnswers = (context, payload) => {
    return Answers.get(payload.query).then((res) => {
        //context.commit('CLEAR_STATES')
        //context.commit('PARSE_HISTORY_UPDATED', {parseHistory: res.data, extra: payload.extra})
        //context.commit('SAVE_META', res.meta)
    }).catch((e) => {
        throw new Error('Something went wrong: ' + e);
    })
}

const saveWeeklyFloorAnswers = (context, payload) => {
    const id    = payload.id;
    const data  = {payload};

    const answers = (id != "" && id > 0) ? new Answers({id:id}) : new Answers();
    return answers.save(data);

};

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta)
}

export {
    getWeeklyFloorAnswers,
    saveWeeklyFloorAnswers,
    saveMeta,
}
