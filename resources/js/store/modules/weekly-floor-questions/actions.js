import { Questions } from '@common/model/weekly-floor-report/Questions'

const getQuestions = (context, payload) => {
    return Questions.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES')
        context.commit('PARSE_QUESTION_UPDATED', {data: res.data, extra: payload.extra})
        context.commit('SAVE_META', res.data)
    }).catch((e) => {
        throw new Error('Something went wrong: ' + e);
    })
}

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta)
}

export {
    getQuestions,
    saveMeta,
}
