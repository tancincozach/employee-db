const CLEAR_STATES = (state) => {
    state.data = []
    state.formatted = []
    state.dropdownIndeces = {}
    state.meta = {}
}

const PARSE_QUESTION_UPDATED = (state, payload) => {
    state.data = payload.data
    state.formatted = [];

    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            parent_id: row.parent_id,
            answer_element_type: row.answer_element_type,
            weekly_floor_question: row.weekly_floor_question,
            weekly_floor_question_options: row.weekly_floor_question_options,
            sequence_no: row.sequence_no
        }, payload.extra);

        state.formatted.push(obj);
    });
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

export {
    CLEAR_STATES,
    PARSE_QUESTION_UPDATED,
    SAVE_META,
}
