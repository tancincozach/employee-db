const getQuestionslist = state => state.data
const formatted = state => state.formatted
const dropdownIndeces = state => state.dropdownIndeces
const savemeta =  state => state.meta

export {
    getQuestionslist,
    formatted,
    dropdownIndeces,
    savemeta,
}
