import { Faq } from '@common/model/Faq';

const getFaqs = (context, payload) => {
    return Faq.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('GET_FAQS', {data: res.data});
        context.commit('SAVE_META', res.meta);
    });
};

const getFaq = (context, id) => {
    return Faq.get({id:id}).then((res) => {
        context.commit('GET_FAQ', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find faq!');
    });
}

const saveFaq = (context, payload) => {
    const id = payload.id;
    const model = (id != "" && id > 0) ? new Faq({id:id}) : new Faq();

    return model.save(payload);

};

const deleteFaq = (context, id) => {
    const faq = new Faq({id: id});
    return faq.delete().then((res) => {
        context.commit('DELETE_FAQ');
    });
};

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
    getFaqs,
    getFaq,
    saveFaq,
    deleteFaq,
    saveMeta,
}
