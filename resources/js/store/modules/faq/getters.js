const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const faq = state => state.faq

export {
    data,
    formatted,
    meta,
    faq
}
