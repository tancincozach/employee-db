import _ from 'lodash';

const GET_FAQS = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            question: row.question,
            answer: row.answer
        });

        state.formatted.push(obj);
    });
}

const GET_FAQ = (state, payload) => {
    state.faq = payload.data
}

const DELETE_FAQ = (state) => {}

const SEARCH_FAQ = (state, payload) => {
    state.faq = payload.data
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const CLEAR_STATES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
    state.faq = {};
}



export {
    GET_FAQS,
    GET_FAQ,
    DELETE_FAQ,
    SEARCH_FAQ,
    CLEAR_STATES,
    SAVE_META
}
