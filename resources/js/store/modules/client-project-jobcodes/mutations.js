import _ from 'lodash';

const CLIENT_PROJECT_JOBCODES_UPDATED = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            text: row.job_title
        }, payload.extra);

        state.formatted.push(obj);
    });
};

const CLIENT_PROJECT_JOBCODE_UPDATED = (state, payload) => {
    state.clientProjectJobcode = payload.data
}

const CLEAR_CLIENT_PROJECT_JOBCODES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
};

const DELETE_CLIENT_PROJECT_JOBCODE = (state) => {};

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

export {
    SAVE_META,
    DELETE_CLIENT_PROJECT_JOBCODE,
    CLEAR_CLIENT_PROJECT_JOBCODES,
    CLIENT_PROJECT_JOBCODES_UPDATED,
    CLIENT_PROJECT_JOBCODE_UPDATED
}
