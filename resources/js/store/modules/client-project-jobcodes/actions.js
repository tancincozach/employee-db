import { ClientProjectJobcode } from '@common/model/ClientProjectJobcode';

const getClientProjectJobcodes = (context, payload) => {  
    return ClientProjectJobcode.get(payload.query).then((res) => {
    	context.commit('CLEAR_CLIENT_PROJECT_JOBCODES');
        context.commit('CLIENT_PROJECT_JOBCODES_UPDATED', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    })
};

const getClientProjectJobcode = (context, id) => {
    return ClientProjectJobcode.get({id:id}).then((res) => {
        context.commit('CLIENT_PROJECT_JOBCODE_UPDATED', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find client project jobcode!');
    })
};

const saveClientProjectJobcode = (context, payload) => {
    
    const id = payload.id;
    const data = { 
        id: payload.id, 
        jobcode:payload.jobcode, 
        client_project_id: payload.clientProjectId
    };
    const clientProjectJobcode = (id != "" && id > 0) ? new ClientProjectJobcode({id:id}) : new ClientProjectJobcode();
    return clientProjectJobcode.save(data).then((res) => {
        context.commit('CLIENT_PROJECT_JOBCODE_UPDATED', { data: res.data });
    });
};

const clearClientProjectJobcodes = (context) => {
    context.commit('CLEAR_CLIENT_PROJECT_JOBCODES');
};

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
	saveMeta,
    getClientProjectJobcodes,
    getClientProjectJobcode,
    clearClientProjectJobcodes,
    saveClientProjectJobcode
}
