const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const clientProjectJobcode = state => state.clientProjectJobcode

export {
    data,
    formatted,
    meta,
    clientProjectJobcode
}
