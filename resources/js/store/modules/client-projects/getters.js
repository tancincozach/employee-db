const data = state => state.data;
const formatted = state => state.formatted;
const formatted_with_name = state => state.formatted_with_name;
const formatted_with_client = state => state.formatted_with_client;
const project = state => state.project;
const meta = state => state.meta;

export {
    data,
    formatted,
    formatted_with_name,
    formatted_with_client,
    project,
    meta
}