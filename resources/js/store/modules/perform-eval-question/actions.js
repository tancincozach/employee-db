import { PerformEvalQuestion } from "@common/model/PerformEvalQuestion";

const getPerformEvalQuestions = (context, payload) => {
    return PerformEvalQuestion.get(payload.query).then(res => {
        context.commit("CLEAR_PERFORM_EVAL_QUESTIONS");
        context.commit("UPDATE_PERFORM_EVAL_QUESTIONS", res.data);
    }).catch(e => {
        throw new Error("Can't find resource!");
    });
};


export {
    getPerformEvalQuestions,
};
