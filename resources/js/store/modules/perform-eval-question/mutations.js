const UPDATE_PERFORM_EVAL_QUESTIONS = (state, data) => {
    state.data.push(...data);
};

const CLEAR_PERFORM_EVAL_QUESTIONS = (state, payload) => {
    state.data = [];
};

export {
    UPDATE_PERFORM_EVAL_QUESTIONS,
    CLEAR_PERFORM_EVAL_QUESTIONS,
};
