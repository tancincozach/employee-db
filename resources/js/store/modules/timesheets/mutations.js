import _ from 'lodash';

const SET_DATA = (state, data) => {
    let daily = _.groupBy(data, 'log_date');

    state.data = data;
    state.daily = daily;
}

const SET_META = (state, meta) => {
    state.meta = meta;
}

const CLEAR_STATES = state => {
    state.data = [];
    state.daily = [];
    state.monthly = [];
    state.meta = {};
}

const SET_MONTHLY = (state, data) => {
    state.monthly = data;
}

export {
    SET_DATA,
    SET_META,
    CLEAR_STATES,
    SET_MONTHLY,
}
