import { Timesheet } from '@common/model/Timesheet';
import _ from 'lodash';

const index = (context, payload) => {
    return Timesheet.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('SET_DATA', res.data);
        context.commit('SET_META', res.meta);
    });
};

const monthly = (context, payload) => {
    return Timesheet.monthly(payload.query).then(({data}) => {
        context.commit('CLEAR_STATES');
        context.commit('SET_MONTHLY', data.data);
        context.commit('SET_META', data.meta);
    });
};

const weekly = (context, payload) => {
    return Timesheet.weekly(payload.query);
};

const generate = (context, payload) => {
    return Timesheet.generate(payload.query);
};

const getGenerationResponse = (context, payload) => {
    return Timesheet.getGenerationResponse(payload.query);
};

const clearStates = context => context.commit('CLEAR_STATES');

export {
    index,
    monthly,
    weekly,
    generate,
    getGenerationResponse,
    clearStates,
}
