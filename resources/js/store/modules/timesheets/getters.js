const data = state => state.data;
const daily = state => state.daily;
const jobcodes = state => state.jobcodes;
const monthly = state => state.monthly;
const meta = state => state.meta;

export {
    data,
    daily,
    meta,
    jobcodes,
    monthly,
}
