import _ from 'lodash';

const ROUTE_WIZARDS = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, row => {
        let obj = _.defaults(
            {
                group_id: row.group_id,
                icon: row.icon,
                route_name: row.route_name,
                sequence_order: row.sequence_order,
                route_uri: row.route_uri,
                caption: row.caption,
                goto_group_id: row.goto_group_id,
            }
        );

        state.formatted.push(obj);
    });
};

const SET_ROUTE_WIZARD = (state, payload) => {
    state.route = payload
};

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const DELETE_ROUTE_WIZARD = (state) => {}

const CLEAR_STATES = state => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
    state.route = {};
};

export {
    ROUTE_WIZARDS,
    CLEAR_STATES,
    SET_ROUTE_WIZARD,
    SAVE_META,
    DELETE_ROUTE_WIZARD
}
