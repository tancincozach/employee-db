const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;

const getRouteWizard = (state) => {
    return state.route;
};

export {
    data,
    formatted,
    meta,
    getRouteWizard,
}
