import { RouteWizard } from "@common/model/RouteWizard";

const getRouteWizards = (context, payload) => {
    return RouteWizard.get(payload.query).then((res) => {
        context.commit('ROUTE_WIZARDS', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    })
};

const saveRouteWizard = (context, payload) => {
    context.commit('SET_ROUTE_WIZARD', payload)
};

const saveRouteWizardDB = (context, data) => {
    const id = data.id;
    const routeWizard = (id && id != "" && id > 0) ? new RouteWizard({id:id}) : new RouteWizard();
    return routeWizard.save(data);
};

const deleteRouteWizardDB = (context, id) => {
    const routeWizard = new RouteWizard({id: id});
    return routeWizard.delete().then((res) => {
        context.commit('DELETE_ROUTE_WIZARD');
    });
};

export {
    getRouteWizards,
    saveRouteWizard,
    saveRouteWizardDB,
    deleteRouteWizardDB
}
