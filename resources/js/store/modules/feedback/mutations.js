import _ from 'lodash';

const GET_FEEDBACKS = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            name: row.name,
        }, payload.extra);

        state.formatted.push(obj);
    });
}

const GET_FEEDBACK = (state, payload) => {
    state.feedback = payload.data
}

const DELETE_FEEDBACK = (state) => {}

const SEARCH_FEEDBACK = (state, payload) => {
    state.feedback = payload.data
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const CLEAR_STATES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
    state.feedback = {};
}



export {
    GET_FEEDBACKS,
    GET_FEEDBACK,
    DELETE_FEEDBACK,
    SEARCH_FEEDBACK,
    CLEAR_STATES,
    SAVE_META
}
