const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const feedback = state => state.feedback

export {
    data,
    formatted,
    meta,
    feedback
}
