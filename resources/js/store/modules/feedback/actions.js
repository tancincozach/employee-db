import { Feedback } from '@common/model/Feedback';

const getFeedbacks = (context, payload) => {
    return Feedback.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('GET_FEEDBACKS', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    });
};

const getFeedback = (context, id) => {
    return Feedback.get({id:id}).then((res) => {
        context.commit('GET_FEEDBACK', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find feedback!');
    });
}

const saveFeedback = (context, payload) => {
    const id = payload.id;
    const model = (id != "" && id > 0) ? new Feedback({id:id}) : new Feedback();

    return model.save(payload);

};

const deleteFeedback = (context, id) => {
    const feedback = new Feedback({id: id});
    return feedback.delete().then((res) => {
        context.commit('DELETE_FEEDBACK');
    });
};

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
    getFeedbacks,
    getFeedback,
    saveFeedback,
    deleteFeedback,
    saveMeta,
}
