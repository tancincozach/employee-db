import { PerformEvalResponse } from "@common/model/PerformEvalResponse";

const submitResponses = (context, payload) => {
    const response = new PerformEvalResponse();

    return response.save(payload).then(res => {
        // no update on the data yet
    }).catch(e => {
        throw new Error("Internal Error occured!");
    });
};


export {
    submitResponses,
};
