const UPDATE_PERFORM_EVAL_RESPONSES = (state, data) => {
    state.data.push(...data);
};

const CLEAR_PERFORM_EVAL_RESPONSES = (state, payload) => {
    state.data = [];
};

export {
    UPDATE_PERFORM_EVAL_RESPONSES,
    CLEAR_PERFORM_EVAL_RESPONSES,
};
