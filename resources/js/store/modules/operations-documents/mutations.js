import _ from 'lodash';

const GET_DOCUMENTS = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            name: row.name,
            url: row.url
        });

        state.formatted.push(obj);
    });
}

const GET_DOCUMENT = (state, payload) => {
    state.document = payload;
    state.formatted = [];
}

const DELETE_DOCUMENT = (state) => { }

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const CLEAR_STATES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
    state.document = {};
}

const EDIT_DOCUMENT = (state, payload) => {
    state.document = payload.data
}

export {
    GET_DOCUMENTS,
    GET_DOCUMENT,
    DELETE_DOCUMENT,
    CLEAR_STATES,
    SAVE_META,
    EDIT_DOCUMENT
}
