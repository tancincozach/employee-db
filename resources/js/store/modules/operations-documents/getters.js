const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const document = state => state.document

export {
    data,
    formatted,
    meta,
    document
}
