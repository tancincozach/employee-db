import { OperationsDocument } from '@common/model/OperationsDocument';

const getDocuments = (context, payload) => {
    return OperationsDocument.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('GET_DOCUMENTS', { data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    });
};

const getDocument = (context, id) => {
    return OperationsDocument.get({ id: id }).then((res) => {
        context.commit('EDIT_DOCUMENT', { data: res.data });
    }).catch((e) => {
        throw new Error('Can\'t find document!');
    })
}

const saveDocument = (context, payload) => {
    const id = payload.id;
    const data = {
        name: payload.name,
        url: payload.url,
        updated_by_user: payload.updated_by_user
    };

    const document = (id != "" && id > 0) ? new OperationsDocument({ id: id }) : new OperationsDocument();

    return document.save(data);
};

const deleteDocument = (context, id) => {
    const doc = new OperationsDocument({ id: id });
    return doc.delete().then((res) => {
        context.commit('DELETE_DOCUMENT');
    });
};

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

const searchDocumentByName = (context, name) => {
    return OperationsDocument.get({ name: name, withTrashed: true }).then((res) => {
        context.commit('EDIT_DOCUMENT', { data: res.data });
    }).catch((e) => {
        throw new Error('Can\'t find document!');
    })
}

export {
    getDocuments,
    getDocument,
    saveDocument,
    deleteDocument,
    saveMeta,
    searchDocumentByName
}
