import { ClientOnboardingChecklist } from '@common/model/ClientOnboardingChecklist';

const getChecklist = (context, payload) => {
    return ClientOnboardingChecklist.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('GET_CLIENT_CHECKLISTS', {data: res.data});
    });
}

const saveChecklist = (context, data) => {
    const id = data.id;
    const clientOnboardingChecklist = (id && id != "" && id > 0) ? new ClientOnboardingChecklist({id:id}) : new ClientOnboardingChecklist();
    return clientOnboardingChecklist.save(data);
}

export {
    getChecklist,
    saveChecklist,
}
