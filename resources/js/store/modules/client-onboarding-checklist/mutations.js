import _ from 'lodash';

const GET_CLIENT_CHECKLISTS = (state, payload) => {
    state.data = payload.data[0];
}

const CLEAR_STATES = (state) => {
    state.data = [];
    state.formatted = [];
}

export {
    GET_CLIENT_CHECKLISTS,
    CLEAR_STATES,
}
