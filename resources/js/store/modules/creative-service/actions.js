import { CreativeService } from '@common/model/CreativeService';

const getCreativeServices = (context, payload) => {
    return CreativeService.get(payload.query).then((res) => {
        context.commit('CLEAR_STATES');
        context.commit('GET_CREATIVE_SERVICES', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    });
};

const getCreativeService = (context, id) => {
    return CreativeService.get({id:id}).then((res) => {
        context.commit('GET_CREATIVE_SERVICE', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find feedback!');
    });
}

const saveCreativeService = (context, payload) => {
    const id = payload.id;
    const model = (id != "") ? new CreativeService({id:id}) : new CreativeService();

    return model.save(payload);

};

const deleteCreativeService = (context, id) => {
    const feedback = new CreativeService({id: id});
    return CreativeService.delete().then((res) => {
        context.commit('DELETE_FEEDBACK');
    });
};

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
    getCreativeServices,
    getCreativeService,
    saveCreativeService,
    deleteCreativeService,
    saveMeta,
}
