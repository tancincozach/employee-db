const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const creativeService = state => state.creativeService

export {
    data,
    formatted,
    meta,
    creativeService
}
