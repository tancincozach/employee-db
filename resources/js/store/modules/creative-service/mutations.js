import _ from 'lodash';

const GET_CREATIVE_SERVICES = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            company: row.company,
            project: row.project,
            purpose: row.purpose,
            needed_at: row.needed_at,
        }, payload.extra);

        state.formatted.push(obj);
    });
}

const GET_CREATIVE_SERVICE = (state, payload) => {
    state.creativeService = payload.data
}

const DELETE_CREATIVE_SERVICE = (state) => {}

const SEARCH_CREATIVE_SERVICE = (state, payload) => {
    state.creativeService = payload.data
}

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

const CLEAR_STATES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
    state.creative_service = {};
}



export {
    GET_CREATIVE_SERVICES,
    GET_CREATIVE_SERVICE,
    DELETE_CREATIVE_SERVICE,
    SEARCH_CREATIVE_SERVICE,
    CLEAR_STATES,
    SAVE_META
}
