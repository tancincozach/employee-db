const UPDATE_ALL_QUESTION_RESPONSES = (state, payload) => {
    state.data = payload.data;
};

const SET_RESOURCE_TYPES = (state, data) => {
    state.resourceTypes = data;
};

export { UPDATE_ALL_QUESTION_RESPONSES, SET_RESOURCE_TYPES };
