import { EmployeePositionType } from '@common/model/EmployeePositionType';

const getEmployeePositionTypes = (context, payload) => {
    return EmployeePositionType.get(payload.query).then((res) => {
    	context.commit('CLEAR_EMPLOYEE_POSITION_TYPES');
        context.commit('EMPLOYEE_POSITION_TYPES_UPDATED', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    })
};

const getEmployeePositionType = (context, id) => {
    return EmployeePositionType.get({id:id}).then((res) => {
        context.commit('EMPLOYEE_POSITION_TYPE_UPDATED', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find position!');
    })
}

const saveEmployeePositionType = (context, payload) => {
	const id    = payload.id;
    const data  = {
        employee_id: payload.employee_id,
        position_type_id: payload.position_type_id,
        experience: payload.experience
    };
    const employeePositionType = (id != "" && id > 0) ? new EmployeePositionType({id:id}) : new EmployeePositionType();
    return employeePositionType.save(data);
}

const clearEmployeePositionTypes = (context) => {
    context.commit('CLEAR_EMPLOYEE_POSITION_TYPES');
};

const deleteEmployeePositionType = (context, id) => {
    const employeePositionType = new EmployeePositionType({id: id});
    return employeePositionType.delete().then((res) => {
        context.commit('DELETE_EMPLOYEE_POSITION_TYPE');
    });
};

const searchEmployeePositionType = (context, employee_id) => {
    return EmployeePositionType.get({employee_id: employee_id, withTrashed: true}).then((res) => {
        context.commit('EMPLOYEE_POSITION_TYPE_UPDATED', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find position!');
    })
}

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
	saveMeta,
    getEmployeePositionTypes,
    getEmployeePositionType,
    clearEmployeePositionTypes,
    deleteEmployeePositionType,
    saveEmployeePositionType,
    searchEmployeePositionType
}
