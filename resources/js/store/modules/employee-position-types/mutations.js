import _ from 'lodash';

const EMPLOYEE_POSITION_TYPES_UPDATED = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            employee_id: row.employee_id,
            position_type_id: row.position_type_id,
            experience: row.experience
        }, payload.extra);

        state.formatted.push(obj);
    });
};

const EMPLOYEE_POSITION_TYPE_UPDATED = (state, payload) => {
    state.employee_position_type = payload.data
}

const CLEAR_EMPLOYEE_POSITION_TYPES = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
};

const DELETE_EMPLOYEE_POSITION_TYPE = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
};

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

export {
    SAVE_META,
    DELETE_EMPLOYEE_POSITION_TYPE,
    CLEAR_EMPLOYEE_POSITION_TYPES,
    EMPLOYEE_POSITION_TYPES_UPDATED,
    EMPLOYEE_POSITION_TYPE_UPDATED
}
