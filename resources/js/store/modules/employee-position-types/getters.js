const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const employee_position_type = state => state.employee_position_type;

export {
    data,
    formatted,
    meta,
    employee_position_type
}
