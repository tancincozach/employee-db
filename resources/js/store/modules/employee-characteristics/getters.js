const data = state => state.data;
const formatted = state => state.formatted;
const meta = state => state.meta;
const employee_characteristic = state => state.employee_characteristic;

export {
    data,
    formatted,
    meta,
    employee_characteristic
}
