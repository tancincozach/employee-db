import _ from 'lodash';

const EMPLOYEE_CHARACTERISTICS_UPDATED = (state, payload) => {
    state.data = payload.data;
    state.formatted = [];

    // format data for use with the component
    _.each(state.data, (row) => {
        let obj = _.defaults({
            id: row.id,
            employee_id: row.employee_id,
            characteristic_id: row.characteristic_id,
            rate: row.rate
        }, payload.extra);

        state.formatted.push(obj);
    });
};

const EMPLOYEE_CHARACTERISTIC_UPDATED = (state, payload) => {
    state.EMPLOYEE_CHARACTERISTIC = payload.data
}

const CLEAR_EMPLOYEE_CHARACTERISTICS = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
};

const DELETE_EMPLOYEE_CHARACTERISTIC = (state) => {
    state.data = [];
    state.formatted = [];
    state.meta = {};
};

const SAVE_META = (state, meta) => {
    state.meta = meta;
}

export {
    SAVE_META,
    DELETE_EMPLOYEE_CHARACTERISTIC,
    CLEAR_EMPLOYEE_CHARACTERISTICS,
    EMPLOYEE_CHARACTERISTICS_UPDATED,
    EMPLOYEE_CHARACTERISTIC_UPDATED
}
