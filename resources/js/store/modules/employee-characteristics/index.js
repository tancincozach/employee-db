import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';

const state = {
    data: [],
    formatted: [],
    meta: {},
    employee_characteristic: {}
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
