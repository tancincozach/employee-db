import { EmployeeCharacteristic } from '@common/model/EmployeeCharacteristic';

const getEmployeeCharacteristics = (context, payload) => {
    return EmployeeCharacteristic.get(payload.query).then((res) => {
    	context.commit('CLEAR_EMPLOYEE_CHARACTERISTICS');
        context.commit('EMPLOYEE_CHARACTERISTICS_UPDATED', {data: res.data, extra: payload.extra});
        context.commit('SAVE_META', res.meta);
    })
};

const getEmployeeCharacteristic = (context, id) => {
    return EmployeeCharacteristic.get({id:id}).then((res) => {
        context.commit('EMPLOYEE_CHARACTERISTIC_UPDATED', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find position!');
    })
}

const saveEmployeeCharacteristic = (context, payload) => {
	const id    = payload.id;
    const data  = {
        employee_id:payload.employee_id,
        characteristic_id: payload.characteristic_id,
        rate: payload.rate
    };
    const EmployeeChar = (id != "" && id > 0) ? new EmployeeCharacteristic({id:id}) : new EmployeeCharacteristic();
    return EmployeeChar.save(data);
}

const clearEmployeeCharacteristics = (context) => {
    context.commit('CLEAR_EMPLOYEE_CHARACTERISTICS');
};

const deleteEmployeeCharacteristic = (context, id) => {
    const EmployeeChar = new EmployeeCharacteristic({id: id});
    return EmployeeChar.delete().then((res) => {
        context.commit('DELETE_EMPLOYEE_CHARACTERISTIC');
    });
};

const searchEmployeeCharacteristic = (context, employee_id) => {
    return EmployeeCharacteristic.get({employee_id: employee_id, withTrashed: true}).then((res) => {
        context.commit('EMPLOYEE_CHARACTERISTIC_UPDATED', {data: res.data});
    }).catch((e) => {
        throw new Error('Can\'t find position!');
    })
}

const saveMeta = (context, meta) => {
    context.commit('SAVE_META', meta);
};

export {
	saveMeta,
    getEmployeeCharacteristics,
    getEmployeeCharacteristic,
    clearEmployeeCharacteristics,
    deleteEmployeeCharacteristic,
    saveEmployeeCharacteristic,
    searchEmployeeCharacteristic
}
