<?php

/**
 * Place here all API web service responses including custom message for http header codes
 */
return [
    'http' => [
        200 => 'Success',
        400 => 'Bad Request',
        401 => 'Unauthorized Access',
        // ... add your custom message for http code here
    ],
    'custom' => [
        9000 => 'Unknown error',
        9001 => 'Token Not Found',
        // ... add your custom error code and message here
    ],
];
